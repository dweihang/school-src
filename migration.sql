USE msc;
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'student_com_act_tbl' AND COLUMN_NAME = 'teacher_user_id') 
ALTER TABLE student_com_act_tbl ADD teacher_user_id varchar(12) COLLATE Latin1_General_CI_AI default NULL;
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'student_com_merit_class_metadata_tbl' AND COLUMN_NAME = 'level') 
ALTER TABLE student_com_merit_class_metadata_tbl ADD "level" int default NULL;
IF NOT EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'student_com_merit_class_metadata_tbl' AND COLUMN_NAME = 'upgrade_to')
ALTER TABLE student_com_merit_class_metadata_tbl ADD upgrade_to int default NULL;
IF NOT EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'student_com_merit_tbl' AND COLUMN_NAME = 'teacher_user_id')
ALTER TABLE student_com_merit_tbl ADD teacher_user_id varchar(12) COLLATE Latin1_General_CI_AI DEFAULT NULL;
IF NOT EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'student_com_position_tbl' AND COLUMN_NAME = 'teacher_user_id')
ALTER TABLE student_com_position_tbl ADD teacher_user_id varchar(12) COLLATE Latin1_General_CI_AI DEFAULT NULL;
DROP TABLE IF EXISTS transcript_edit_permission;
CREATE TABLE transcript_edit_permission (table_name varchar(50) NOT NULL, can_edit tinyint, PRIMARY KEY (table_name), UNIQUE (table_name));
INSERT INTO transcript_edit_permission (table_name, can_edit) VALUES ("student_com_merit_tbl", 1), ("student_com_position_tbl", 1), ("student_com_act_tbl", 1);

-- TODO: copy procedures
