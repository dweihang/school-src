[
  {
    academic_year: '2018',
    subject_title_eng: 'Conduct',
    subject_title_chi: '操行',
    subject_group_no: 'P2-2A',
    programme_id: 'PG1000000007',
    user_id: 'US1000010316',
    arts_assignment_id: 'AA1000023392',
    assign_eng_name: 'Diligence',
    assign_chi_name: '勤        學',
    parent: 'AA1000023391',
    mark: 85,
    grade: 'Very Good',
    mark2: null,
    grade2: '---',
    absence: 0,
    early_leave: 0,
    late_for_class: 0,
    absence2: null,
    early_leave2: null,
    late_for_class2: null
  },
  {
    academic_year: '2018',
    subject_title_eng: 'Conduct',
    subject_title_chi: '操行',
    subject_group_no: 'P2-2A',
    programme_id: 'PG1000000007',
    user_id: 'US1000010316',
    arts_assignment_id: 'AA1000023392',
    assign_eng_name: 'Diligence',
    assign_chi_name: '勤        學',
    parent: 'AA1000023391',
    mark: 85,
    grade: 'Very Good',
    mark2: null,
    grade2: '---',
    absence: 0,
    early_leave: 0,
    late_for_class: 0,
    absence2: null,
    early_leave2: null,
    late_for_class2: null
  },
  {
    academic_year: '2018',
    subject_title_eng: 'Conduct',
    subject_title_chi: '操行',
    subject_group_no: 'P2-2A',
    programme_id: 'PG1000000007',
    user_id: 'US1000010316',
    arts_assignment_id: 'AA1000023393',
    assign_eng_name: 'Discipline',
    assign_chi_name: '紀        律',
    parent: 'AA1000023391',
    mark: 75,
    grade: 'Good',
    mark2: null,
    grade2: '---',
    absence: 0,
    early_leave: 0,
    late_for_class: 0,
    absence2: null,
    early_leave2: null,
    late_for_class2: null
  },
  {
    academic_year: '2018',
    subject_title_eng: 'Conduct',
    subject_title_chi: '操行',
    subject_group_no: 'P2-2A',
    programme_id: 'PG1000000007',
    user_id: 'US1000010316',
    arts_assignment_id: 'AA1000023393',
    assign_eng_name: 'Discipline',
    assign_chi_name: '紀        律',
    parent: 'AA1000023391',
    mark: 75,
    grade: 'Good',
    mark2: null,
    grade2: '---',
    absence: 0,
    early_leave: 0,
    late_for_class: 0,
    absence2: null,
    early_leave2: null,
    late_for_class2: null
  },
  {
    academic_year: '2018',
    subject_title_eng: 'Conduct',
    subject_title_chi: '操行',
    subject_group_no: 'P2-2A',
    programme_id: 'PG1000000007',
    user_id: 'US1000010316',
    arts_assignment_id: 'AA1000023394',
    assign_eng_name: 'Politeness',
    assign_chi_name: '禮        貌',
    parent: 'AA1000023391',
    mark: 75,
    grade: 'Good',
    mark2: null,
    grade2: '---',
    absence: 0,
    early_leave: 0,
    late_for_class: 0,
    absence2: null,
    early_leave2: null,
    late_for_class2: null
  },
  {
    academic_year: '2018',
    subject_title_eng: 'Conduct',
    subject_title_chi: '操行',
    subject_group_no: 'P2-2A',
    programme_id: 'PG1000000007',
    user_id: 'US1000010316',
    arts_assignment_id: 'AA1000023394',
    assign_eng_name: 'Politeness',
    assign_chi_name: '禮        貌',
    parent: 'AA1000023391',
    mark: 75,
    grade: 'Good',
    mark2: null,
    grade2: '---',
    absence: 0,
    early_leave: 0,
    late_for_class: 0,
    absence2: null,
    early_leave2: null,
    late_for_class2: null
  },
  {
    academic_year: '2018',
    subject_title_eng: 'Conduct',
    subject_title_chi: '操行',
    subject_group_no: 'P2-2A',
    programme_id: 'PG1000000007',
    user_id: 'US1000010316',
    arts_assignment_id: 'AA1000023395',
    assign_eng_name: 'Sociability',
    assign_chi_name: '群        性',
    parent: 'AA1000023391',
    mark: 85,
    grade: 'Very Good',
    mark2: null,
    grade2: '---',
    absence: 0,
    early_leave: 0,
    late_for_class: 0,
    absence2: null,
    early_leave2: null,
    late_for_class2: null
  },
  {
    academic_year: '2018',
    subject_title_eng: 'Conduct',
    subject_title_chi: '操行',
    subject_group_no: 'P2-2A',
    programme_id: 'PG1000000007',
    user_id: 'US1000010316',
    arts_assignment_id: 'AA1000023395',
    assign_eng_name: 'Sociability',
    assign_chi_name: '群        性',
    parent: 'AA1000023391',
    mark: 85,
    grade: 'Very Good',
    mark2: null,
    grade2: '---',
    absence: 0,
    early_leave: 0,
    late_for_class: 0,
    absence2: null,
    early_leave2: null,
    late_for_class2: null
  },
  {
    academic_year: '2018',
    subject_title_eng: 'Conduct',
    subject_title_chi: '操行',
    subject_group_no: 'P2-2A',
    programme_id: 'PG1000000007',
    user_id: 'US1000010316',
    arts_assignment_id: 'AA1000023396',
    assign_eng_name: 'Tidiness',
    assign_chi_name: '整        潔',
    parent: 'AA1000023391',
    mark: 85,
    grade: 'Very Good',
    mark2: null,
    grade2: '---',
    absence: 0,
    early_leave: 0,
    late_for_class: 0,
    absence2: null,
    early_leave2: null,
    late_for_class2: null
  },
  {
    academic_year: '2018',
    subject_title_eng: 'Conduct',
    subject_title_chi: '操行',
    subject_group_no: 'P2-2A',
    programme_id: 'PG1000000007',
    user_id: 'US1000010316',
    arts_assignment_id: 'AA1000023396',
    assign_eng_name: 'Tidiness',
    assign_chi_name: '整        潔',
    parent: 'AA1000023391',
    mark: 85,
    grade: 'Very Good',
    mark2: null,
    grade2: '---',
    absence: 0,
    early_leave: 0,
    late_for_class: 0,
    absence2: null,
    early_leave2: null,
    late_for_class2: null
  },
  {
    academic_year: '2018',
    subject_title_eng: 'Conduct',
    subject_title_chi: '操行',
    subject_group_no: 'P2-2A',
    programme_id: 'PG1000000007',
    user_id: 'US1000010316',
    arts_assignment_id: 'AA1000023391',
    assign_eng_name: 'Conduct',
    assign_chi_name: '操        行',
    parent: 'Null',
    mark: 81,
    grade: 'Very Good',
    mark2: null,
    grade2: '---',
    absence: 0,
    early_leave: 0,
    late_for_class: 0,
    absence2: null,
    early_leave2: null,
    late_for_class2: null
  },
  {
    academic_year: '2018',
    subject_title_eng: 'Conduct',
    subject_title_chi: '操行',
    subject_group_no: 'P2-2A',
    programme_id: 'PG1000000007',
    user_id: 'US1000010316',
    arts_assignment_id: 'AA1000023391',
    assign_eng_name: 'Conduct',
    assign_chi_name: '操        行',
    parent: 'Null',
    mark: 81,
    grade: 'Very Good',
    mark2: null,
    grade2: '---',
    absence: 0,
    early_leave: 0,
    late_for_class: 0,
    absence2: null,
    early_leave2: null,
    late_for_class2: null
  }
]
