import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.Test;
import smile.cpce.common.exception.CpceException;

/**
 * @author raymonddu 21/4/2019
 */
public class ArtsMarkServiceImplTest {


  @Test
  public void testWorkBook() {
    try {
      HSSFWorkbook workbook;
      String dataFile  = getClass().getClassLoader().getResource("download_markTemplate2.xls").getFile();
      workbook = new HSSFWorkbook(new FileInputStream(dataFile));
      HSSFSheet sheet = workbook.getSheetAt(0);
      HSSFRow row = sheet.getRow(1);
      HSSFCell cell = row.getCell((short) 1);
      String groupId = null;
      try {
        groupId = this.readStringCellValue(cell);
      } catch (CpceException e) {
        e.printStackTrace();
      }
      List artsAssignmentIds = new ArrayList();
      row = sheet.getRow(3);
      short currentCellNo = 2;
      currentCellNo = (short) (currentCellNo + 1);

      String artsAssId;
      for (cell = row.getCell(currentCellNo); cell != null; cell = row.getCell(currentCellNo)) {
        artsAssId = this.readStringCellValue(cell);

        artsAssignmentIds.add(artsAssId);
        ++currentCellNo;
      }
    } catch (Exception var40) {
      System.err.println(var40);
    }
  }

  private String readStringCellValue(HSSFCell cell) throws CpceException {
    if (cell != null && cell.getCellType() == 1) {
      return cell.getStringCellValue().trim();
    } else if (cell != null && cell.getCellType() == 3) {
      return "";
    } else {
      throw new CpceException(2002, "cpce.exceptionCode.dataFormatError");
    }
  }

}
