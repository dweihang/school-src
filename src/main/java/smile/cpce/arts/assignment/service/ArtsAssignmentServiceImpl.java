//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.arts.assignment.service;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import smile.cpce.arts.assignment.common.ArtsAssignmentUtil;
import smile.cpce.common.CPCE;
import smile.cpce.common.Constants;
import smile.cpce.common.DataAccessExceptionResolver;
import smile.cpce.common.Sequencer;
import smile.cpce.db.dao.Arts_assignment_tblDAO;
import smile.cpce.db.dao.Arts_group_tblDAO;
import smile.cpce.db.dto.Arts_assignment_viewDTO;
import smile.cpce.db.exception.Arts_assignment_tblException;
import smile.cpce.db.exception.Arts_assignment_viewDTOException;
import smile.cpce.db.to.Arts_assignment_viewTO;
import smile.cpce.db.vo.Arts_assignment_tbl;
import smile.cpce.db.vo.Arts_assignment_tblPK;
import smile.cpce.db.vo.Arts_group_tbl;
import smile.cpce.db.vo.Arts_group_tblPK;

public class ArtsAssignmentServiceImpl implements ArtsAssignmentService {
  private PlatformTransactionManager transactionManager;
  private DefaultTransactionDefinition defaultTransactionDefinition;
  private JdbcTemplate jdbcTemplate;
  private final Logger logger = Logger.getLogger("cpce");

  public ArtsAssignmentServiceImpl(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public ArtsAssignmentServiceImpl() {
  }

  public void setDefaultTransactionDefinition(DefaultTransactionDefinition defaultTransactionDefinition) {
    this.defaultTransactionDefinition = defaultTransactionDefinition;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  public Set getHasGradeConvByAssignment(String assignmentId) {
    String sql = "select A.arts_assignment_id from arts_assignment_tbl A join arts_assignment_mapping_tbl C  on C.arts_assignment_id = A.arts_assignment_id  where C.assignment_id = ?  and exists (select 1 from arts_grade_conv_tbl B where  A.arts_assignment_id = B.arts_assignment_id) ";
    Object[] param = new Object[]{assignmentId};
    List list = this.jdbcTemplate.queryForList(sql, param);
    Set hasGradeConv = new HashSet();
    Iterator it = list.iterator();
    Map tmpMap = null;

    while(it.hasNext()) {
      tmpMap = (Map)it.next();
      hasGradeConv.add(tmpMap.get("arts_assignment_id"));
    }

    return hasGradeConv;
  }

  public Set getHasGradeConvByArtsGroup(String groupId) {
    String sql = "select A.arts_assignment_id from arts_assignment_tbl A where A.arts_group_id = ?  and exists (select 1 from arts_grade_conv_tbl B where  A.arts_assignment_id = B.arts_assignment_id) ";
    Object[] param = new Object[]{groupId};
    List list = this.jdbcTemplate.queryForList(sql, param);
    Set hasGradeConv = new HashSet();
    Iterator it = list.iterator();
    Map tmpMap = null;

    while(it.hasNext()) {
      tmpMap = (Map)it.next();
      hasGradeConv.add(tmpMap.get("arts_assignment_id"));
    }

    return hasGradeConv;
  }

  public boolean checkDefaultGradeTable(String groupId) {
    String sql1 = "select marked_by_mark, count(1) cnt from arts_assignment_tbl where arts_group_id = ? group by marked_by_mark ";
    String sql2 = "select count(1) from arts_grade_conv_tbl where arts_group_id = ? and arts_assignment_id is null ";
    Object[] param = new Object[]{groupId};
    List list = this.jdbcTemplate.queryForList(sql1, param);
    if (list != null && list.size() > 0) {
      Iterator it = list.iterator();
      int marked_by_mark = 0;
      int marked_by_grade = 0;

      while(it.hasNext()) {
        Map map = (Map)it.next();
        if (map.get("marked_by_mark").equals("1")) {
          marked_by_mark = (Integer)map.get("cnt");
        } else if (map.get("marked_by_mark").equals("0")) {
          marked_by_grade = (Integer)map.get("cnt");
        }
      }

      if (marked_by_mark > 0 && marked_by_grade > 0) {
        int rst = this.jdbcTemplate.queryForInt(sql2, param);
        return rst > 0;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  public int updateArtsAssignment(Map parameters, String userId) {
    HashMap artsAssignments = new HashMap();
    String key = null;
    String level = null;
    Arts_assignment_tbl tbl = null;
    Object obj = null;
    String idStr = null;
    String markingMethod = null;
    String rubricsId = null;
    String iloId = null;
    String groupId = null;
    Timestamp now = new Timestamp(System.currentTimeMillis());
    List markingSchemeSavedFileList = new ArrayList();
    List deleteMarkingSchemeFileList = new ArrayList();
    String deleteMarkingScheme = null;
    String markingMethodChanged = "";
    String subjectGroupId = null;
    String assignmentId = null;
    boolean isFromAssignment = false;
    boolean isOnlyAssignmentArts = false;
    obj = parameters.get("subjectGroupId");
    if (obj != null) {
      subjectGroupId = ((String[])((String[])obj))[0];
    }

    obj = parameters.get("assignmentId");
    if (obj != null) {
      assignmentId = ((String[])((String[])obj))[0];
    }

    obj = parameters.get("groupId");
    if (obj != null) {
      groupId = ((String[])((String[])obj))[0];
      if (groupId.length() == 0) {
        groupId = null;
      }
    }

    obj = parameters.get("deleteMarkingScheme");
    if (obj != null) {
      deleteMarkingScheme = ((String[])((String[])obj))[0];
    }

    if (assignmentId != null && assignmentId.length() > 0) {
      isFromAssignment = true;
    }

    String allIds = "";
    Map oldMarkingMethodMap = new HashMap();
    Map oldRubricsMap = new HashMap();
    Map oldAssignmentOrderMap = new HashMap();
    String getOldInfo = null;
    List list = null;
    Object[] param = null;
    if (!isFromAssignment) {
      getOldInfo = "Select arts_assignment_id, marked_by_mark, arts_rubrics_id, assignment_order  from arts_assignment_tbl  where arts_group_id = ? ";
      param = new Object[]{groupId};
    } else {
      getOldInfo = "Select A.arts_assignment_id, A.marked_by_mark, A.arts_rubrics_id, A.assignment_order  from arts_assignment_tbl A  join arts_assignment_mapping_tbl B on A.arts_assignment_id = B.arts_assignment_id   where B.assignment_id = ?  ";
      param = new Object[]{assignmentId};
    }

    list = this.jdbcTemplate.queryForList(getOldInfo, param);
    Iterator it = list.iterator();

    Map map;
    while(it.hasNext()) {
      map = (Map)it.next();
      String artsAssId = (String)map.get("arts_assignment_id");
      oldMarkingMethodMap.put(artsAssId, map.get("marked_by_mark"));
      oldRubricsMap.put(artsAssId, map.get("arts_rubrics_id"));
      oldAssignmentOrderMap.put(artsAssId, map.get("assignment_order"));
    }

    Set keys = parameters.keySet();
    it = keys.iterator();

    while(true) {
      String filePath;
      String oldMarkingSchemeFile;
      do {
        if (!it.hasNext()) {
          oldMarkingSchemeFile = "";
          filePath = "";
          String deleteArtsAssignments = "";
          String deleteMarkGradeConv = "";
          String deleteArtsAssMapping = "";
          String updateMarkingScheme;
          String getMarkingSchemeFiles;
          if (!isFromAssignment) {
            if (allIds.length() > 0) {
              allIds = allIds.substring(2) + "'";
              oldMarkingSchemeFile = " (exists (select 1 from arts_assignment_tbl B  where arts_mark_entries_tbl.arts_assignment_id = B.arts_assignment_id and  B.arts_group_id=? and B.arts_assignment_id not in (" + allIds + ")";
              filePath = " (exists (select 1 from arts_assignment_tbl B  where arts_raw_mark_entries_tbl.arts_assignment_id = B.arts_assignment_id and  B.arts_group_id=? and B.arts_assignment_id not in (" + allIds + ") ";
              oldMarkingSchemeFile = oldMarkingSchemeFile + ") and not exists ( select 1 from arts_assignment_mapping_tbl C  where C.arts_assignment_id = arts_mark_entries_tbl.arts_assignment_id ))";
              filePath = filePath + ") and not exists ( select 1 from arts_assignment_mapping_tbl C  where C.arts_assignment_id = arts_raw_mark_entries_tbl.arts_assignment_id )) ";
              if (markingMethodChanged.length() > 0) {
                oldMarkingSchemeFile = oldMarkingSchemeFile + " or arts_mark_entries_tbl.arts_assignment_id in (" + markingMethodChanged.substring(2) + "' ) ";
                filePath = filePath + " or arts_raw_mark_entries_tbl.arts_assignment_id in (" + markingMethodChanged.substring(2) + "' ) ";
              }

              deleteArtsAssignments = " arts_group_id=? and arts_assignment_id not in (" + allIds + ")" + " and not exists (select 1 from arts_assignment_mapping_tbl B " + " where B.arts_assignment_id = arts_assignment_tbl.arts_assignment_id) ";
            } else {
              oldMarkingSchemeFile = " exists (select 1 from arts_assignment_tbl B  where arts_mark_entries_tbl.arts_assignment_id = B.arts_assignment_id and  B.arts_group_id=? ) and not exists ( select 1 from arts_assignment_mapping_tbl C  where arts_mark_entries_tbl.arts_assignment_id  = C.arts_assignment_id )  ";
              filePath = " exists (select 1 from arts_assignment_tbl B  where arts_raw_mark_entries_tbl.arts_assignment_id = B.arts_assignment_id and  B.arts_group_id=? ) and not exists ( select 1 from arts_assignment_mapping_tbl C  where arts_raw_mark_entries_tbl.arts_assignment_id  = C.arts_assignment_id )  ";
              deleteArtsAssignments = " arts_group_id=? and not exists (select 1 from arts_assignment_mapping_tbl B  where arts_assignment_tbl.arts_assignment_id = B.arts_assignment_id )";
            }

            deleteMarkGradeConv = "delete arts_grade_conv_tbl from  arts_grade_conv_tbl A join arts_assignment_tbl B on A.arts_assignment_id = B.arts_assignment_id  where B.marked_by_mark = '0' or (   B.arts_group_id = ?  ";
            if (allIds.length() > 0) {
              deleteMarkGradeConv = deleteMarkGradeConv + " and  A.arts_assignment_id not in (" + allIds + ") " + " and not exists (select 1 from arts_assignment_mapping_tbl C " + " where C.arts_assignment_id = A.arts_assignment_id )";
            }

            deleteMarkGradeConv = deleteMarkGradeConv + ")";
          } else {
            updateMarkingScheme = "";
            getMarkingSchemeFiles = "";
            if (allIds.length() > 0) {
              allIds = allIds.substring(2) + "'";
              getMarkingSchemeFiles = "select arts_assignment_id from arts_assignment_mapping_tbl where assignment_id = ? and arts_assignment_id not in (" + allIds + ")";
            } else {
              getMarkingSchemeFiles = "select arts_assignment_id from arts_assignment_mapping_tbl where assignment_id = ?";
            }

            List deleleArtsAssList = this.jdbcTemplate.queryForList(getMarkingSchemeFiles, new Object[]{assignmentId});
            if (deleleArtsAssList != null && deleleArtsAssList.size() > 0) {
              for(it = deleleArtsAssList.iterator(); it.hasNext(); updateMarkingScheme = updateMarkingScheme + ",'" + (String)map.get("arts_assignment_id") + "'") {
                map = (Map)it.next();
              }

              updateMarkingScheme = updateMarkingScheme.substring(1);
            } else {
              updateMarkingScheme = "''";
            }

            oldMarkingSchemeFile = " arts_assignment_id in (" + updateMarkingScheme + ")";
            filePath = " arts_assignment_id in (" + updateMarkingScheme + ")";
            if (markingMethodChanged.length() > 0) {
              markingMethodChanged = markingMethodChanged.substring(2);
              oldMarkingSchemeFile = oldMarkingSchemeFile + " or arts_assignment_id in (" + markingMethodChanged + "' ) ";
              filePath = filePath + " or arts_assignment_id in (" + markingMethodChanged + "' ) ";
            }

            deleteArtsAssMapping = " arts_assignment_id in (" + updateMarkingScheme + ")";
            deleteArtsAssignments = " arts_assignment_id in (" + updateMarkingScheme + ")";
            deleteMarkGradeConv = "delete arts_grade_conv_tbl from  arts_grade_conv_tbl A join arts_assignment_tbl B on A.arts_assignment_id = B.arts_assignment_id  where B.marked_by_mark = '0' ";
            if (updateMarkingScheme.length() > 0) {
              deleteMarkGradeConv = deleteMarkGradeConv + " or B.arts_assignment_id in (" + updateMarkingScheme + ")";
            }
          }

          updateMarkingScheme = null;
          if (deleteMarkingScheme != null && deleteMarkingScheme.length() > 0) {
            deleteMarkingScheme = deleteMarkingScheme.substring(1);
            deleteMarkingScheme = deleteMarkingScheme.replaceAll(",", "','");
            deleteMarkingScheme = "'" + deleteMarkingScheme + "'";
            updateMarkingScheme = "update arts_assignment_tbl set marking_scheme_filepath=null where  arts_assignment_id in (" + deleteMarkingScheme + ")";
            getMarkingSchemeFiles = "select marking_scheme_filepath from arts_assignment_tbl  where arts_assignment_id in (" + deleteMarkingScheme + ")";
            list = this.jdbcTemplate.queryForList(getMarkingSchemeFiles);
            if (list.size() > 0) {
              it = list.iterator();

              while(it.hasNext()) {
                map = (Map)it.next();
                deleteMarkingSchemeFileList.add(map.get("marking_scheme_filepath"));
              }
            }
          }

          TreeSet allArtsAssTreeSet = new TreeSet(new ArtsAssignmentServiceImpl.MyComparator());
          allArtsAssTreeSet.addAll(artsAssignments.keySet());
          Arts_assignment_tblDAO dao = CPCE.DAOFACTORY.createArts_assignment_tblDAO();
          String parent = null;
          Arts_assignment_tbl parentTbl = null;
          TransactionStatus status = this.transactionManager.getTransaction(this.defaultTransactionDefinition);
          Arts_group_tblDAO artsGroupDao = CPCE.DAOFACTORY.createArts_group_tblDAO();
          Object[] params;
          if (isFromAssignment) {
            params = new Object[0];
          } else {
            params = new Object[]{groupId};
          }

          try {
            this.logger.info("delete mark entries");
            CPCE.DAOFACTORY.createArts_mark_entries_tblDAO().delete(oldMarkingSchemeFile, params, this.jdbcTemplate);
            this.logger.info("delete raw mark entries");
            CPCE.DAOFACTORY.createArts_raw_mark_entries_tblDAO().delete(filePath, params, this.jdbcTemplate);
            this.logger.info("delete mark grade conv table");
            this.jdbcTemplate.update(deleteMarkGradeConv, params);
            this.logger.info("delete arts_assignment_mapping");
            if (deleteArtsAssMapping.length() > 0) {
              CPCE.DAOFACTORY.createArts_assignment_mapping_tblDAO().delete(deleteArtsAssMapping, params, this.jdbcTemplate);
            }

            this.logger.info("delete arts assignment");
            dao.delete(deleteArtsAssignments, params, this.jdbcTemplate);
            if (updateMarkingScheme != null) {
              this.jdbcTemplate.update(updateMarkingScheme);
            }

            if (!isFromAssignment) {
              String updateArtsAssGroupIdSql = null;
              String updateArtsAssignmentRootSql = null;
              if (allIds.length() > 0) {
                updateArtsAssignmentRootSql = "update arts_assignment_tbl set parent = null  where arts_assignment_id not in (" + allIds + ") and arts_group_id = ? " + " and exists (select 1 from arts_assignment_mapping_tbl B " + " where B.arts_assignment_id = arts_assignment_tbl.arts_assignment_id) " + " and not exists ( select 1 from arts_assignment_mapping_tbl C " + "  where C.arts_assignment_id = arts_assignment_tbl.parent)";
                updateArtsAssGroupIdSql = "update arts_assignment_tbl set arts_group_id =  null where arts_assignment_id not in (" + allIds + ") and arts_group_id = ? " + " and exists (select 1 from arts_assignment_mapping_tbl B " + " where B.arts_assignment_id = arts_assignment_tbl.arts_assignment_id) ";
              } else {
                updateArtsAssignmentRootSql = "update arts_assignment_tbl set parent = null  where arts_group_id = ?  and exists (select 1 from arts_assignment_mapping_tbl B  where B.arts_assignment_id = arts_assignment_tbl.arts_assignment_id)  and not exists ( select 1 from arts_assignment_mapping_tbl C  where C.arts_assignment_id = arts_assignment_tbl.parent)";
                updateArtsAssGroupIdSql = "update arts_assignment_tbl set arts_group_id =  null where arts_group_id = ?  and exists (select 1 from arts_assignment_mapping_tbl B  where B.arts_assignment_id = arts_assignment_tbl.arts_assignment_id) ";
              }

              this.jdbcTemplate.update(updateArtsAssignmentRootSql, new Object[]{groupId});
              this.jdbcTemplate.update(updateArtsAssGroupIdSql, new Object[]{groupId});
            }

            if (artsGroupDao.findByPrimaryKey(new Arts_group_tblPK(groupId, subjectGroupId), this.jdbcTemplate) == null) {
              artsGroupDao.insert(new Arts_group_tbl(groupId, subjectGroupId), this.jdbcTemplate);
            }

            List newArtsAssIdList = new ArrayList();
            List assignmentIdMappingList = new ArrayList();
            final List insertArtsAssMappingList = new ArrayList();
            it = allArtsAssTreeSet.iterator();

            Arts_assignment_tbl rootTbl;
            while(it.hasNext()) {
              level = (String)it.next();
              tbl = (Arts_assignment_tbl)artsAssignments.get(level);
              idStr = tbl.getArts_assignment_id();
              int idx = level.lastIndexOf("_");
              if (idx != -1) {
                parent = level.substring(0, idx);
                parentTbl = (Arts_assignment_tbl)artsAssignments.get(parent);
                if (parentTbl == null) {
                  this.transactionManager.rollback(status);
                  this.logger.log(Level.SEVERE, "cannot find parent assignment");
                  return -3;
                }

                tbl.setParent(parentTbl.getArts_assignment_id());
              } else if (isFromAssignment) {
                if (idStr == null) {
                  isOnlyAssignmentArts = true;
                  tbl.setParent((String)null);
                } else {
                  rootTbl = dao.findByPrimaryKey(new Arts_assignment_tblPK(idStr), this.jdbcTemplate);
                  if (rootTbl.getArts_group_id() == null) {
                    isOnlyAssignmentArts = true;
                  }

                  tbl.setParent(rootTbl.getParent());
                }
              } else {
                tbl.setParent((String)null);
              }

              if (isOnlyAssignmentArts) {
                tbl.setArts_group_id((String)null);
              }

              if (idStr != null) {
                dao.update(new Arts_assignment_tblPK(idStr), tbl, this.jdbcTemplate);
              } else {
                obj = Sequencer.getInstance().getNextSeqNum(this.jdbcTemplate, "ARTS_ASSIGNMENT_ID");
                tbl.setArts_assignment_id((String)obj);
                dao.insert(tbl, this.jdbcTemplate);
                newArtsAssIdList.add(obj);
              }
            }

            String delFileStr;
            if (newArtsAssIdList.size() > 0) {
              String inertArtsAssMappingSql;
              String parentArtsAssId;
              Iterator it2;
              if (isFromAssignment) {
                rootTbl = (Arts_assignment_tbl)artsAssignments.get(allArtsAssTreeSet.first());
                inertArtsAssMappingSql = rootTbl.getArts_assignment_id();
                filePath = "select assignment_id from arts_assignment_mapping_tbl where arts_assignment_id = ? ";
                List mappingList = this.jdbcTemplate.queryForList(filePath, new Object[]{inertArtsAssMappingSql});
                Iterator it1;
                if (mappingList != null && mappingList.size() != 0) {
                  it1 = mappingList.iterator();

                  while(it1.hasNext()) {
                    map = (Map)it1.next();
                    assignmentIdMappingList.add(map.get("assignment_id"));
                  }
                } else {
                  assignmentIdMappingList.add(assignmentId);
                }

                it1 = newArtsAssIdList.iterator();

                while(it1.hasNext()) {
                  parentArtsAssId = (String)it1.next();
                  it2 = assignmentIdMappingList.iterator();

                  while(it2.hasNext()) {
                    String assignmentIdMapping = (String)it2.next();
                    insertArtsAssMappingList.add(new String[]{parentArtsAssId, assignmentIdMapping});
                  }
                }
              } else if (allIds.length() > 0) {
                delFileStr = "select arts_assignment_id, assignment_id from arts_assignment_mapping_tbl where arts_assignment_id in ( " + allIds + ") order by arts_assignment_id ";
                List artsAssMappingList = this.jdbcTemplate.queryForList(delFileStr);
                Map artsAssMappingMap = new HashMap();
                Iterator it1;
                String artsAssId;
                if (artsAssMappingList != null && artsAssMappingList.size() > 0) {
                  it1 = artsAssMappingList.iterator();
                  artsAssId = "";

                  ArrayList idList;
                  for(idList = new ArrayList(); it1.hasNext(); idList.add(map.get("assignment_id"))) {
                    map = (Map)it1.next();
                    artsAssId = (String)map.get("arts_assignment_id");
                    if (!artsAssId.equals(artsAssId)) {
                      if (artsAssId.length() > 0) {
                        artsAssMappingMap.put(artsAssId, idList);
                      }

                      idList = new ArrayList();
                      artsAssId = artsAssId;
                    }
                  }

                  artsAssMappingMap.put(artsAssId, idList);
                }

                it1 = allArtsAssTreeSet.iterator();

                label313:
                while(true) {
                  do {
                    do {
                      do {
                        if (!it1.hasNext()) {
                          break label313;
                        }

                        level = (String)it1.next();
                        tbl = (Arts_assignment_tbl)artsAssignments.get(level);
                        artsAssId = tbl.getArts_assignment_id();
                      } while(!newArtsAssIdList.contains(artsAssId));

                      parentArtsAssId = tbl.getParent();
                      artsAssMappingList = (List)artsAssMappingMap.get(parentArtsAssId);
                    } while(artsAssMappingList == null);
                  } while(artsAssMappingList.size() <= 0);

                  it2 = artsAssMappingList.iterator();

                  while(it2.hasNext()) {
                    insertArtsAssMappingList.add(new String[]{artsAssId, (String)it2.next()});
                  }

                  artsAssMappingMap.put(artsAssId, artsAssMappingList);
                }
              }

              if (insertArtsAssMappingList.size() > 0) {
                BatchPreparedStatementSetter setterInertArtsAssMapping = new BatchPreparedStatementSetter() {
                  public void setValues(PreparedStatement ps, int i) throws SQLException {
                    String[] tmp = (String[])((String[])insertArtsAssMappingList.get(i));
                    ps.setString(1, tmp[0]);
                    ps.setString(2, tmp[1]);
                  }

                  public int getBatchSize() {
                    return insertArtsAssMappingList.size();
                  }
                };
                inertArtsAssMappingSql = "insert into arts_assignment_mapping_tbl (arts_assignment_id, assignment_id) values(?, ?)";
                this.jdbcTemplate.batchUpdate("insert into arts_assignment_mapping_tbl (arts_assignment_id, assignment_id) values(?, ?)", setterInertArtsAssMapping);
              }
            }

            if (markingSchemeSavedFileList.size() > 0) {
              it = markingSchemeSavedFileList.iterator();

              while(it.hasNext()) {
                delFileStr = (String)it.next();
                filePath = File.separator + groupId + File.separator + delFileStr;
                FileUtils.copyFile(new File(Constants.UPLOAD_TEMP_PATH + File.separator + delFileStr), new File(Constants.ARTS_PATH + filePath));
              }
            }

            if (deleteMarkingSchemeFileList.size() > 0) {
              it = deleteMarkingSchemeFileList.iterator();

              while(it.hasNext()) {
                delFileStr = (String)it.next();
                if (delFileStr.indexOf("..") == -1) {
                  File delFile = new File(Constants.ARTS_PATH + delFileStr);
                  this.logger.info("delete:" + delFileStr + " " + delFile.delete());
                }
              }
            }

            this.transactionManager.commit(status);
            return 0;
          } catch (Exception var59) {
            var59.printStackTrace();
            this.transactionManager.rollback(status);
            this.logger.log(Level.SEVERE, " ", var59);
            DataAccessExceptionResolver.getInstance().logDAOException("ArtsAssignmentServiceImpl updateArtsAssignment ", var59);
            return -2;
          }
        }

        key = (String)it.next();
      } while(!key.startsWith("name_"));

      level = key.substring(5);
      tbl = new Arts_assignment_tbl();
      tbl.setUpdate_by(userId);
      tbl.setUpdate_date(now);
      tbl.setName(((String[])((String[])parameters.get(key)))[0]);
      tbl.setDescription(((String[])((String[])parameters.get("desc_" + level)))[0].trim());
      String stringObj = ((String[])((String[])parameters.get("no_of_marks_" + level)))[0];
      if (stringObj != null && stringObj.length() > 0) {
        tbl.setSelect_no_max(Integer.parseInt(stringObj));
      } else {
        tbl.setSelect_no_max(0);
      }

      obj = parameters.get("mapAssId_" + level);
      if (obj != null && ((String[])((String[])obj))[0].length() > 0) {
        tbl.setAssignment_id(((String[])((String[])obj))[0]);
      }

      tbl.setWeight(Integer.parseInt(((String[])((String[])parameters.get("weight_" + level)))[0].trim()));
      obj = parameters.get("max_mark_" + level);
      if (obj != null && ((String[])((String[])obj))[0].length() > 0) {
        tbl.setMax_mark(Integer.parseInt(((String[])((String[])obj))[0]));
      } else {
        tbl.setMax_mark(0);
      }

      obj = parameters.get("artsAssId_" + level);
      idStr = null;
      if (obj != null) {
        idStr = ((String[])((String[])obj))[0];
        if (idStr.length() > 0) {
          tbl.setArts_assignment_id(idStr);
          allIds = allIds + "','" + idStr;
          Integer order = (Integer)oldAssignmentOrderMap.get(idStr);
          if (order != null) {
            tbl.setAssignment_order(order);
          } else {
            tbl.setAssignment_order(-1);
          }
        }
      } else {
        tbl.setAssignment_order(-1);
      }

      iloId = null;
      obj = parameters.get("mapIloId_" + level);
      if (obj != null && ((String[])((String[])obj))[0].length() > 0) {
        iloId = ((String[])((String[])obj))[0];
        tbl.setArts_ilo_id(iloId);
      }

      markingMethod = null;
      obj = parameters.get("marked_by_mark_" + level);
      if (obj != null) {
        markingMethod = ((String[])((String[])obj))[0];
        tbl.setMarked_by_mark(markingMethod);
      }

      rubricsId = null;
      obj = parameters.get("mapRubricsId_" + level);
      if (obj != null && ((String[])((String[])obj))[0].length() > 0) {
        rubricsId = ((String[])((String[])obj))[0];
        tbl.setArts_rubrics_id(rubricsId);
      }

      if (idStr != null && markingMethod != null && !markingMethod.equals(oldMarkingMethodMap.get(idStr)) || rubricsId != null && !rubricsId.equals(oldRubricsMap.get(idStr))) {
        markingMethodChanged = markingMethodChanged + "','" + idStr;
      }

      obj = parameters.get("pass_" + level);
      if (obj != null) {
        tbl.setMust_pass(((String[])((String[])obj))[0]);
      } else {
        tbl.setMust_pass("0");
      }

      tbl.setArts_group_id(groupId);
      String markingSchemeFile = "";
      obj = parameters.get("scheme_saved_" + level);
      if (obj != null) {
        markingSchemeFile = ((String[])((String[])obj))[0];
      }

      obj = parameters.get("scheme_" + level);
      if (obj != null) {
        oldMarkingSchemeFile = ((String[])((String[])obj))[0];
        if (markingSchemeFile.length() == 0 && oldMarkingSchemeFile.length() > 0) {
          tbl.setMarking_scheme_filepath(oldMarkingSchemeFile);
        } else if (markingSchemeFile.length() > 0) {
          filePath = null;
          if (groupId != null) {
            filePath = File.separator + groupId + File.separator + markingSchemeFile;
          } else {
            filePath = File.separator + subjectGroupId + File.separator + markingSchemeFile;
          }

          tbl.setMarking_scheme_filepath(filePath);
          markingSchemeSavedFileList.add(markingSchemeFile);
          if (oldMarkingSchemeFile.length() > 0) {
            deleteMarkingSchemeFileList.add(oldMarkingSchemeFile);
          }
        }
      }

      artsAssignments.put(level, tbl);
    }
  }

  public Object getArtsAssignmentName(String artsAssId) {
    String sql = "select name from arts_assignment_tbl where arts_assignment_id = ?";
    return this.jdbcTemplate.queryForObject(sql, new Object[]{artsAssId}, String.class);
  }

  public Map getArtsAssignments(String subjectGroupId, String assignmentId) throws Arts_assignment_viewDTOException {
    List list = null;
    Arts_assignment_viewDTO dto = CPCE.DAOFACTORY.createArts_assignment_viewDTO();
    if (subjectGroupId != null && subjectGroupId.length() > 0) {
      Object[] param = new Object[]{subjectGroupId};
      list = dto.find(" subject_group_id = ? ", "", param, this.jdbcTemplate);
    } else if (assignmentId != null && assignmentId.length() > 0) {
      String whereClause = " exists (select 1 from arts_assignment_mapping_tbl A where  A.arts_assignment_id = arts_assignment_view.arts_assignment_id and A.assignment_id = ? )  ";
      Object[] sqlParam = new Object[]{assignmentId};
      list = dto.find(whereClause, " arts_assignment_id", sqlParam, this.jdbcTemplate);
    }

    Map map = new HashMap();
    if (list.size() > 0) {
      Iterator it = list.iterator();

      while(it.hasNext()) {
        Arts_assignment_viewTO tbl = (Arts_assignment_viewTO)it.next();
        map.put(tbl.getArts_assignment_id(), tbl);
      }
    }

    return map;
  }

  public Map getArtsAssignments(List subjectGroupIds) throws Arts_assignment_viewDTOException {
    if (subjectGroupIds == null) {
      return null;
    } else {
      Arts_assignment_viewDTO dto = CPCE.DAOFACTORY.createArts_assignment_viewDTO();
      String ids = "";

      Iterator it;
      for(it = subjectGroupIds.iterator(); it.hasNext(); ids = ids + "','" + (String)it.next()) {
        ;
      }

      if (ids.length() > 0) {
        ids = ids.substring(2) + "'";
      }

      String whereClause = " subject_group_id in (" + ids + ") ";
      List list = dto.find(whereClause, " subject_group_id ", (Object[])null, this.jdbcTemplate);
      Map result = new HashMap();
      Map map = null;
      String preSubjectGroupId = "";
      if (list.size() > 0) {
        map = new HashMap();

        Arts_assignment_viewTO tbl;
        for(it = list.iterator(); it.hasNext(); map.put(tbl.getArts_assignment_id(), tbl)) {
          tbl = (Arts_assignment_viewTO)it.next();
          String subjectGroupId = tbl.getSubject_group_id();
          if (!subjectGroupId.equals(preSubjectGroupId)) {
            if (map != null) {
              result.put(preSubjectGroupId, map);
            }

            map = new HashMap();
            preSubjectGroupId = subjectGroupId;
          }
        }

        if (map != null) {
          result.put(preSubjectGroupId, map);
        }
      }

      return result;
    }
  }

  public String getMarkingSchemeFilePath(String artsAssId) {
    String filePath = null;
    String getMarkingSchemeFilePath = "select marking_scheme_filepath from arts_assignment_tbl  where arts_assignment_id = ? ";
    List list = this.jdbcTemplate.queryForList(getMarkingSchemeFilePath, new Object[]{artsAssId});
    if (list != null && list.size() > 0) {
      Map map = (Map)list.get(0);
      filePath = (String)map.get("marking_scheme_filepath");
    }

    return filePath;
  }

  public int mergeArtsAssignment(String fromSubjectGroupId, String attachIds, String userId) throws Exception {
    String ids = attachIds.replaceAll(",", "','");
    ids = "'" + ids + "'";
    String delArtsRawMark = "delete arts_raw_mark_entries_tbl from  arts_raw_mark_entries_tbl A join arts_assignment_tbl B  on A.arts_assignment_id = B.arts_assignment_id  left join arts_assignment_mapping_tbl C on C.arts_assignment_id = B.arts_assignment_id  where B.arts_group_id in (" + ids + ") and C.assignment_id is null";
    String delArtsMark = "delete arts_mark_entries_tbl from  arts_mark_entries_tbl A join arts_assignment_tbl B  on A.arts_assignment_id = B.arts_assignment_id  left join arts_assignment_mapping_tbl C on C.arts_assignment_id = B.arts_assignment_id  where B.arts_group_id in (" + ids + ") and C.assignment_id is null ";
    String delArtsGradeConv = "delete arts_grade_conv_tbl from arts_grade_conv_tbl A  left join arts_assignment_mapping_tbl C on C.arts_assignment_id = A.arts_assignment_id  where A.arts_group_id in (" + ids + ") and C.assignment_id is null";
    String delArtsAssignment = "delete arts_assignment_tbl from arts_assignment_tbl A  left join arts_assignment_mapping_tbl C on C.arts_assignment_id = A.arts_assignment_id  where A.arts_group_id in (" + ids + ") and C.assignment_id is null";
    String updateArtsAssignment_mappingToAssignment_resetArtsGroup = "update arts_assignment_tbl set arts_group_id = null where exists (select 1 from arts_assignment_mapping_tbl A where A.arts_assignment_id = arts_assignment_tbl.arts_assignment_id) and arts_assignment_tbl.arts_group_id in (" + ids + ")";
    String updateArtsAssingmnet_mappingToAssignment_resetRoot = "update arts_assignment_tbl set parent = null  where exists (select 1 from arts_assignment_mapping_tbl A where A.arts_assignment_id = arts_assignment_tbl.arts_assignment_id )  and not exists (select 1 from arts_assignment_mapping_tbl B where B.arts_assignment_id = arts_assignment_tbl.parent) and arts_assignment_tbl.arts_group_id in (" + ids + ")";
    String upateArtsGradeConv = "update arts_grade_conv_tbl set arts_group_id = ? where exists (select 1 from arts_assignment_mapping_tbl A where A.arts_assignment_id = arts_grade_conv_tbl.arts_assignment_id) and arts_grade_conv_tbl.arts_group_id in (" + ids + ")";
    String selArtsGroup = "select A.arts_group_id, count(*) cnt from arts_group_tbl A where exists ( select 1 from  arts_group_tbl  B where B.arts_group_id = A.arts_group_id and   B.subject_group_id in ( " + ids + " ) ) " + "group by A.arts_group_id " + " having count(*) > 1 ";
    String selArtsGroupId = "select arts_group_id from arts_group_tbl where subject_group_id = ? ";
    String delArtsGroup = "delete arts_group_tbl where subject_group_id in (" + ids + ")";
    String insertArtsGroup = "insert into arts_group_tbl (arts_group_id, subject_group_id)  values (? , ? )";
    new Timestamp(System.currentTimeMillis());
    final String[] subjectGroupIds = attachIds.split(",");
    boolean needUpdate = false;
    List list = this.jdbcTemplate.queryForList(selArtsGroup);
    if (list != null && list.size() > 0) {
      return -1;
    } else {
      list = this.jdbcTemplate.queryForList(selArtsGroupId, new Object[]{fromSubjectGroupId});
      String artsGroupId = null;
      if (list != null && list.size() != 0) {
        Map map = (Map)list.get(0);
        artsGroupId = (String)map.get("arts_group_id");
      } else {
        artsGroupId = fromSubjectGroupId;
        needUpdate = true;
      }

      final String finalArtsGroupId = artsGroupId;
      BatchPreparedStatementSetter setterArtsGroup = new BatchPreparedStatementSetter() {
        public void setValues(PreparedStatement ps, int i) throws SQLException {
          ps.setString(1, finalArtsGroupId);
          ps.setString(2, subjectGroupIds[i]);
        }

        public int getBatchSize() {
          return subjectGroupIds.length;
        }
      };
      TransactionStatus status = this.transactionManager.getTransaction(this.defaultTransactionDefinition);

      try {
        this.jdbcTemplate.update(delArtsRawMark);
        this.jdbcTemplate.update(delArtsMark);
        this.jdbcTemplate.update(delArtsGradeConv);
        this.jdbcTemplate.update(updateArtsAssingmnet_mappingToAssignment_resetRoot);
        this.jdbcTemplate.update(updateArtsAssignment_mappingToAssignment_resetArtsGroup);
        this.jdbcTemplate.update(delArtsAssignment);
        this.jdbcTemplate.update(upateArtsGradeConv, new Object[]{finalArtsGroupId});
        this.jdbcTemplate.update(delArtsGroup);
        this.jdbcTemplate.batchUpdate(insertArtsGroup, setterArtsGroup);
        if (needUpdate) {
          this.jdbcTemplate.update(insertArtsGroup, new Object[]{artsGroupId, fromSubjectGroupId});
        }

        this.transactionManager.commit(status);
        return 0;
      } catch (Exception var25) {
        this.transactionManager.rollback(status);
        throw var25;
      }
    }
  }

  public int DetachArtsAssignment(final String fromSubjectGroupId, String detachIds, final String userId) throws Exception {
    if (detachIds != null && detachIds.length() > 0) {
      String ids = detachIds.replaceAll(",", "','");
      ids = "'" + ids + "'";
      String updateArtsRawMark = "update arts_raw_mark_entries_tbl  set arts_assignment_id = ?  where arts_assignment_id = ? and exists (  select 1 from student_belong_group_tbl B where arts_raw_mark_entries_tbl.user_id = B.user_id  and B.subject_group_id = ? )";
      String updateArtsMark = "update arts_mark_entries_tbl  set arts_assignment_id = ?  where arts_assignment_id = ? and exists (  select 1 from student_belong_group_tbl B where arts_mark_entries_tbl.user_id = B.user_id  and B.subject_group_id = ? )";
      String updateArtsAssignmentMapping = "update arts_assignment_mapping_tbl set arts_assignment_id = ? where arts_assignment_id = ? and exists ( select 1 from assignment_tbl B where  B.assignment_id = arts_assignment_mapping_tbl.assignment_id  and B.subject_group_id = ? )";
      String updateArtsRawMark_unDetached = "update arts_raw_mark_entries_tbl  set arts_assignment_id = ?  where arts_assignment_id = ? and not exists (  select 1 from student_belong_group_tbl B where arts_raw_mark_entries_tbl.user_id = B.user_id  and B.subject_group_id = ?  ) and exists (  select 1 from arts_assignment_tbl C where arts_raw_mark_entries_tbl.arts_assignment_id = C.arts_assignment_id  and C.arts_group_id = ?) ";
      String updateArtsMark_unDetached = "update arts_mark_entries_tbl  set arts_assignment_id = ?  where arts_assignment_id = ? and  not exists (  select 1 from student_belong_group_tbl B where arts_mark_entries_tbl.user_id = B.user_id  and B.subject_group_id = ?  ) and exists (  select 1 from arts_assignment_tbl C where arts_mark_entries_tbl.arts_assignment_id = C.arts_assignment_id  and C.arts_group_id = ?) ";
      String updateArtsAssignmentMapping_unDetached = "update arts_assignment_mapping_tbl set arts_assignment_id = ?  where arts_assignment_id = ?  and not exists (select 1 from assignment_tbl B where B.assignment_id = arts_assignment_mapping_tbl.assignment_id  and B.subject_group_id = ? ) and exists (select 1 from arts_assignment_tbl C where arts_assignment_mapping_tbl.arts_assignment_id = C.arts_assignment_id  and C.arts_group_id = ? )";
      String updateGradeConvForAssignmentArtsWithoutApply = "update arts_grade_conv_tbl set arts_group_id = ?  where exists ( select 1 from arts_assignment_tbl A join arts_assignment_mapping_tbl B  on A.arts_assignment_id = B.arts_assignment_id  join assignment_tbl C on C.assignment_id = B.assignment_id  where A.arts_group_id is null and arts_grade_conv_tbl.arts_assignment_id = A.arts_assignment_id  and C.subject_group_id = ? )";
      String updateGradeConvForAssignmentArtsWithoutApply_unDetached = " update arts_grade_conv_tbl set arts_group_id = ?  where exists ( select 1 from arts_assignment_tbl A join arts_assignment_mapping_tbl B  on A.arts_assignment_id = B.arts_assignment_id  join assignment_tbl C on C.assignment_id = B.assignment_id  where A.arts_group_id is null and arts_grade_conv_tbl.arts_assignment_id = A.arts_assignment_id  and C.subject_group_id = ? ) and arts_group_id = ? ";
      String selArtsGroup = "select arts_group_id , subject_group_id from arts_group_tbl A where exists (select 1 from arts_group_tbl B where A.arts_group_id = B.arts_group_id  and B.subject_group_id  = ? )";
      String updateArtsGroup = "update arts_group_tbl set arts_group_id = ? where subject_group_id = ? ";
      String updateArtsGroup_unDetached = "update arts_group_tbl set arts_group_id = ?  where subject_group_id != ? and arts_group_id = ? ";
      String selArtsGradeConv = "select A.* from arts_grade_conv_tbl A where A.arts_group_id = ?  and A.arts_assignment_id is null union all select A.*  from arts_grade_conv_tbl A join arts_assignment_tbl B on A.arts_assignment_id = B.arts_assignment_id and A.arts_group_id = B.arts_group_id  where A.arts_group_id = ?  order by A.arts_grade_conv_id ";
      String insertArtsAss = "insert into arts_assignment_tbl \t(arts_assignment_id, arts_group_id, name, description, marked_by_mark, \tparent, select_no_max, must_pass, weight, max_mark, update_by, update_date, marking_scheme_filepath,arts_ilo_id, arts_rubrics_id, no_of_children  )   values (?, ?, ?,? , ?,? ,? , ?,? ,? ,?, ?,?,?,?,0)";
      String insertGradeConv = "insert into arts_grade_conv_tbl  (arts_grade_conv_id, arts_assignment_id, arts_group_id, grade, mark_from, mark_to, update_by, update_date)  values (?, ?, ?, ?,? ,? ,?,?)";
      String[] subjectGroupIds = detachIds.split(",");
      int i = 0;

      for(int j = subjectGroupIds.length; i < j; ++i) {
        this.logger.info("subjectGroupIds:" + subjectGroupIds[i]);
      }

      List subjectGroupIdList = new ArrayList();
      subjectGroupIdList.addAll(Arrays.asList(subjectGroupIds));
      subjectGroupIdList.add(fromSubjectGroupId);
      List list = this.jdbcTemplate.queryForList(selArtsGroup, new Object[]{fromSubjectGroupId});
      String artsGroupId = null;
      List allOriginalAttachedSubjectGroups = new ArrayList();
      if (list != null && list.size() > 0) {
        if (list.size() < subjectGroupIds.length + 1) {
          return -1;
        }

        Iterator it = list.iterator();
        Map map = null;
        String subjectGroupId = null;

        while(it.hasNext()) {
          map = (Map)it.next();
          if (artsGroupId == null) {
            artsGroupId = (String)map.get("arts_group_id");
          }

          subjectGroupId = (String)map.get("subject_group_id");
          allOriginalAttachedSubjectGroups.add(subjectGroupId);
          if (subjectGroupIdList.contains(subjectGroupId)) {
            subjectGroupIdList.remove(subjectGroupId);
          }
        }

        if (subjectGroupIdList.size() > 0) {
          return -1;
        }
      }

      i = 0;

      for(int j = subjectGroupIds.length; i < j; ++i) {
        if (subjectGroupIds[i].equals(artsGroupId)) {
          subjectGroupIds[i] = subjectGroupIds[j - 1];
          subjectGroupIds[j - 1] = artsGroupId;
          break;
        }
      }

      final Map assignments = this.getArtsAssignmentsBySubjectGroup(fromSubjectGroupId);
      final List assignmentList = ArtsAssignmentUtil.preOrderAssignments(ArtsAssignmentUtil.getArtsAssignmentRoot(assignments), assignments);
      final List gradeConvList = this.jdbcTemplate.queryForList(selArtsGradeConv, new Object[]{artsGroupId, artsGroupId});
      final Timestamp now = new Timestamp(System.currentTimeMillis());
      BatchPreparedStatementSetter[] setterInsertArts = new BatchPreparedStatementSetter[subjectGroupIds.length];
      BatchPreparedStatementSetter[] setterInsertGradeConv = new BatchPreparedStatementSetter[subjectGroupIds.length];
      BatchPreparedStatementSetter[] setterUpdateArtsMark = new BatchPreparedStatementSetter[subjectGroupIds.length];
      final List schemeFilePathList = new ArrayList();
      int p = 0;

      for(i = subjectGroupIds.length; p < i; ++p) {
        final Map assignmentIdMatch = new HashMap();
        final String subjectGroupId = subjectGroupIds[p];
        if (!subjectGroupId.equals(artsGroupId)) {
          setterInsertArts[p] = new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) throws SQLException {
              Arts_assignment_viewTO arts_assignment_view = (Arts_assignment_viewTO)assignments.get(assignmentList.get(i));
              String artsAssignmentId = Sequencer.getInstance().getNextSeqNum(ArtsAssignmentServiceImpl.this.jdbcTemplate, "ARTS_ASSIGNMENT_ID");
              assignmentIdMatch.put(arts_assignment_view.getArts_assignment_id(), artsAssignmentId);
              ps.setString(1, artsAssignmentId);
              ps.setString(2, subjectGroupId);
              ps.setString(3, arts_assignment_view.getName());
              ps.setString(4, arts_assignment_view.getDescription());
              ps.setString(5, arts_assignment_view.getMarked_by_mark());
              if (arts_assignment_view.getParent() == null) {
                ps.setString(6, (String)null);
              } else {
                ps.setString(6, (String)assignmentIdMatch.get(arts_assignment_view.getParent()));
              }

              ps.setInt(7, arts_assignment_view.getSelect_no_max());
              ps.setString(8, arts_assignment_view.getMust_pass());
              ps.setInt(9, arts_assignment_view.getWeight());
              ps.setInt(10, arts_assignment_view.getMax_mark());
              ps.setString(11, userId);
              ps.setTimestamp(12, now);
              String fromFilePath = arts_assignment_view.getMarking_scheme_filepath();
              if (fromFilePath == null) {
                ps.setString(13, (String)null);
              } else {
                String toFilePath = fromFilePath.replaceFirst(fromSubjectGroupId, subjectGroupId);
                ps.setString(13, toFilePath);
                schemeFilePathList.add(new String[]{fromFilePath, toFilePath});
              }

              ps.setString(14, arts_assignment_view.getArts_ilo_id());
              ps.setString(15, arts_assignment_view.getArts_rubrics_id());
            }

            public int getBatchSize() {
              return assignmentList.size();
            }
          };
          setterInsertGradeConv[p] = new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) throws SQLException {
              Map map = (Map)gradeConvList.get(i);
              String id = Sequencer.getInstance().getNextSeqNum(ArtsAssignmentServiceImpl.this.jdbcTemplate, "ARTS_GRADE_CONV_ID");
              ps.setString(1, id);
              String artsAssId = (String)map.get("arts_assignment_id");
              if (artsAssId == null) {
                ps.setString(2, (String)null);
              } else {
                ps.setString(2, (String)assignmentIdMatch.get(artsAssId));
              }

              ps.setString(3, subjectGroupId);
              ps.setString(4, (String)map.get("grade"));
              ps.setBigDecimal(5, (BigDecimal)map.get("mark_from"));
              ps.setBigDecimal(6, (BigDecimal)map.get("mark_to"));
              ps.setString(7, userId);
              ps.setTimestamp(8, now);
            }

            public int getBatchSize() {
              return gradeConvList.size();
            }
          };
          setterUpdateArtsMark[p] = new BatchPreparedStatementSetter() {
            public int getBatchSize() {
              return assignmentList.size();
            }

            public void setValues(PreparedStatement ps, int i) throws SQLException {
              Arts_assignment_viewTO arts_assignment_view = (Arts_assignment_viewTO)assignments.get(assignmentList.get(i));
              ps.setString(1, (String)assignmentIdMatch.get(arts_assignment_view.getArts_assignment_id()));
              ps.setString(2, arts_assignment_view.getArts_assignment_id());
              ps.setString(3, subjectGroupId);
            }
          };
        } else {
          setterInsertArts[p] = new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) throws SQLException {
              Arts_assignment_viewTO arts_assignment_view = (Arts_assignment_viewTO)assignments.get(assignmentList.get(i));
              String artsAssignmentId = Sequencer.getInstance().getNextSeqNum(ArtsAssignmentServiceImpl.this.jdbcTemplate, "ARTS_ASSIGNMENT_ID");
              assignmentIdMatch.put(arts_assignment_view.getArts_assignment_id(), artsAssignmentId);
              ps.setString(1, artsAssignmentId);
              ps.setString(2, fromSubjectGroupId);
              ps.setString(3, arts_assignment_view.getName());
              ps.setString(4, arts_assignment_view.getDescription());
              ps.setString(5, arts_assignment_view.getMarked_by_mark());
              if (arts_assignment_view.getParent() == null) {
                ps.setString(6, (String)null);
              } else {
                ps.setString(6, (String)assignmentIdMatch.get(arts_assignment_view.getParent()));
              }

              ps.setInt(7, arts_assignment_view.getSelect_no_max());
              ps.setString(8, arts_assignment_view.getMust_pass());
              ps.setInt(9, arts_assignment_view.getWeight());
              ps.setInt(10, arts_assignment_view.getMax_mark());
              ps.setString(11, userId);
              ps.setTimestamp(12, now);
              String fromFilePath = arts_assignment_view.getMarking_scheme_filepath();
              if (fromFilePath == null) {
                ps.setString(13, (String)null);
              } else {
                String toFilePath = fromFilePath.replaceFirst(subjectGroupId, fromSubjectGroupId);
                ps.setString(13, toFilePath);
                schemeFilePathList.add(new String[]{fromFilePath, toFilePath});
              }

              ps.setString(14, arts_assignment_view.getArts_ilo_id());
              ps.setString(15, arts_assignment_view.getArts_rubrics_id());
            }

            public int getBatchSize() {
              return assignmentList.size();
            }
          };
          setterInsertGradeConv[p] = new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) throws SQLException {
              Map map = (Map)gradeConvList.get(i);
              String id = Sequencer.getInstance().getNextSeqNum(ArtsAssignmentServiceImpl.this.jdbcTemplate, "ARTS_GRADE_CONV_ID");
              ps.setString(1, id);
              String artsAssId = (String)map.get("arts_assignment_id");
              if (artsAssId == null) {
                ps.setString(2, (String)null);
              } else {
                ps.setString(2, (String)assignmentIdMatch.get(artsAssId));
              }

              ps.setString(3, fromSubjectGroupId);
              ps.setString(4, (String)map.get("grade"));
              ps.setBigDecimal(5, (BigDecimal)map.get("mark_from"));
              ps.setBigDecimal(6, (BigDecimal)map.get("mark_to"));
              ps.setString(7, userId);
              ps.setTimestamp(8, now);
            }

            public int getBatchSize() {
              return gradeConvList.size();
            }
          };
          setterUpdateArtsMark[p] = new BatchPreparedStatementSetter() {
            public int getBatchSize() {
              return assignmentList.size();
            }

            public void setValues(PreparedStatement ps, int i) throws SQLException {
              Arts_assignment_viewTO arts_assignment_view = (Arts_assignment_viewTO)assignments.get(assignmentList.get(i));
              ps.setString(1, (String)assignmentIdMatch.get(arts_assignment_view.getArts_assignment_id()));
              ps.setString(2, arts_assignment_view.getArts_assignment_id());
              ps.setString(3, subjectGroupId);
              ps.setString(4, subjectGroupId);
            }
          };
        }
      }

      TransactionStatus status = this.transactionManager.getTransaction(this.defaultTransactionDefinition);

      try {
        i = 0;

        int j;
        for(j = subjectGroupIds.length; i < j; ++i) {
          FileUtils.deleteDirectory(new File(Constants.ARTS_PATH + File.separator + subjectGroupIds[i]));
        }

        i = 0;

        for(j = subjectGroupIds.length; i < j; ++i) {
          if (!subjectGroupIds[i].equals(artsGroupId)) {
            Object[] param = new Object[]{subjectGroupIds[i], subjectGroupIds[i]};
            this.jdbcTemplate.update(updateArtsGroup, param);
            this.jdbcTemplate.update("update arts_grade_conv_tbl set arts_group_id = ?  where exists ( select 1 from arts_assignment_tbl A join arts_assignment_mapping_tbl B  on A.arts_assignment_id = B.arts_assignment_id  join assignment_tbl C on C.assignment_id = B.assignment_id  where A.arts_group_id is null and arts_grade_conv_tbl.arts_assignment_id = A.arts_assignment_id  and C.subject_group_id = ? )", param);
          }

          this.jdbcTemplate.batchUpdate("insert into arts_assignment_tbl \t(arts_assignment_id, arts_group_id, name, description, marked_by_mark, \tparent, select_no_max, must_pass, weight, max_mark, update_by, update_date, marking_scheme_filepath,arts_ilo_id, arts_rubrics_id, no_of_children  )   values (?, ?, ?,? , ?,? ,? , ?,? ,? ,?, ?,?,?,?,0)", setterInsertArts[i]);
          this.jdbcTemplate.batchUpdate("insert into arts_grade_conv_tbl  (arts_grade_conv_id, arts_assignment_id, arts_group_id, grade, mark_from, mark_to, update_by, update_date)  values (?, ?, ?, ?,? ,? ,?,?)", setterInsertGradeConv[i]);
          if (!subjectGroupIds[i].equals(artsGroupId)) {
            this.jdbcTemplate.batchUpdate(updateArtsRawMark, setterUpdateArtsMark[i]);
            this.jdbcTemplate.batchUpdate(updateArtsMark, setterUpdateArtsMark[i]);
            this.jdbcTemplate.batchUpdate(updateArtsAssignmentMapping, setterUpdateArtsMark[i]);
          } else {
            this.jdbcTemplate.batchUpdate(updateArtsRawMark_unDetached, setterUpdateArtsMark[i]);
            this.jdbcTemplate.batchUpdate(updateArtsMark_unDetached, setterUpdateArtsMark[i]);
            this.jdbcTemplate.batchUpdate(updateArtsAssignmentMapping_unDetached, setterUpdateArtsMark[i]);
          }

          this.logger.info("update for " + subjectGroupIds[i]);
        }

        if (subjectGroupIds[subjectGroupIds.length - 1].equals(artsGroupId)) {
          Object[] param = new Object[]{fromSubjectGroupId, artsGroupId, artsGroupId};
          this.jdbcTemplate.update(updateArtsGroup_unDetached, param);
          this.jdbcTemplate.update(" update arts_grade_conv_tbl set arts_group_id = ?  where exists ( select 1 from arts_assignment_tbl A join arts_assignment_mapping_tbl B  on A.arts_assignment_id = B.arts_assignment_id  join assignment_tbl C on C.assignment_id = B.assignment_id  where A.arts_group_id is null and arts_grade_conv_tbl.arts_assignment_id = A.arts_assignment_id  and C.subject_group_id = ? ) and arts_group_id = ? ", param);
          this.jdbcTemplate.update("update arts_grade_conv_tbl set arts_group_id = ?  where exists ( select 1 from arts_assignment_tbl A join arts_assignment_mapping_tbl B  on A.arts_assignment_id = B.arts_assignment_id  join assignment_tbl C on C.assignment_id = B.assignment_id  where A.arts_group_id is null and arts_grade_conv_tbl.arts_assignment_id = A.arts_assignment_id  and C.subject_group_id = ? )", new Object[]{fromSubjectGroupId, fromSubjectGroupId});
        }

        Iterator it = schemeFilePathList.iterator();

        while(it.hasNext()) {
          String[] tmp = (String[])((String[])it.next());
          FileUtils.copyFile(new File(Constants.ARTS_PATH + tmp[0]), new File(Constants.ARTS_PATH + tmp[1]));
        }

        this.transactionManager.commit(status);
      } catch (Exception var36) {
        this.transactionManager.rollback(status);
        throw var36;
      }
    }

    return 0;
  }

  public void deleteArts(String subjectGroupIds) throws Exception {
    String ids = subjectGroupIds.replaceAll(",", "','");
    ids = "'" + ids + "'";
    String delArtsRawMark = "delete arts_raw_mark_entries_tbl from  arts_raw_mark_entries_tbl A join arts_assignment_view B  on A.arts_assignment_id = B.arts_assignment_id  where B.subject_group_id in (" + ids + ")";
    String delArtsMark = "delete arts_mark_entries_tbl from  arts_mark_entries_tbl A join arts_assignment_view B  on A.arts_assignment_id = B.arts_assignment_id  where B.subject_group_id in (" + ids + ")";
    String delArtsGroupTbl = "delete from arts_group_tbl  where subject_group_tbl in (" + ids + ")";
    String delArtsGradeConv2 = "delete arts_grade_conv_tbl  from arts_grade_conv_tbl A where not exists  (select 1 from arts_group_tbl B where A.arts_group_id = B.arts_group_id ) ";
    String selArtsGroupId = "select arts_group_id from arts_assignment_tbl A  where A.marking_scheme_filepath is not null and not exists  (select 1 from arts_group_tbl B where A.arts_group_id = B.arts_group_id )";
    String delArtsAssignment2 = "delete arts_assignment_tbl  from arts_assignment_tbl A where not exists  (select 1 from arts_group_tbl B where A.arts_group_id = B.arts_group_id ) ";
    TransactionStatus status = this.transactionManager.getTransaction(this.defaultTransactionDefinition);

    try {
      this.jdbcTemplate.execute(delArtsRawMark);
      this.jdbcTemplate.execute(delArtsMark);
      this.jdbcTemplate.execute(delArtsGroupTbl);
      List list = this.jdbcTemplate.queryForList(selArtsGroupId);
      this.jdbcTemplate.execute(delArtsGradeConv2);
      this.jdbcTemplate.execute(delArtsAssignment2);
      String[] id = subjectGroupIds.split(",");
      int i = 0;

      for(int j = id.length; i < j; ++i) {
        FileUtils.deleteDirectory(new File(Constants.ARTS_PATH + File.separator + id[i]));
      }

      if (list != null && list.size() > 0) {
        Iterator it = list.iterator();
        Map map = null;
        String artsGroupId = null;

        while(it.hasNext()) {
          map = (Map)it.next();
          artsGroupId = (String)map.get("arts_group_id");
          FileUtils.deleteDirectory(new File(Constants.ARTS_PATH + File.separator + artsGroupId));
        }
      }

      this.transactionManager.commit(status);
    } catch (Exception var15) {
      this.transactionManager.rollback(status);
      throw var15;
    }
  }

  public String getGroupIdFromSubjectGroupId(String subjectGroupId) {
    String sql = "select arts_group_id from arts_group_tbl where subject_group_id = ? ";
    List list = this.jdbcTemplate.queryForList(sql, new Object[]{subjectGroupId});
    if (list != null && list.size() != 0) {
      Map map = (Map)list.get(0);
      return (String)map.get("arts_group_id");
    } else {
      return subjectGroupId;
    }
  }

  public Map getSubjectGroupsFromGroupId(String groupId, String userId, String role) {
    Map result = new HashMap();
    List list = null;
    String sql = "";
    if (!role.equals(Constants.SCHADMIN) && !role.equals(Constants.SYSADMIN)) {
      if (role.equals(Constants.TEACHER)) {
        sql = " select A.subject_group_id, B.subject_group_no  from arts_group_tbl A join subject_group_tbl B  on A.subject_group_id = B.subject_group_id   join teacher_manage_group_tbl C  on C.subject_group_id = B.subject_group_id  where A.arts_group_id = ? and C.user_id = ? ";
        list = this.jdbcTemplate.queryForList(sql, new Object[]{groupId, userId});
      }
    } else {
      sql = " select A.subject_group_id, B.subject_group_no  from arts_group_tbl A join subject_group_tbl B  on A.subject_group_id = B.subject_group_id  where A.arts_group_id = ? ";
      list = this.jdbcTemplate.queryForList(sql, new Object[]{groupId});
    }

    if (list != null && list.size() > 0) {
      Iterator it = list.iterator();

      while(it.hasNext()) {
        Map map = (Map)it.next();
        result.put((String)map.get("subject_group_id"), (String)map.get("subject_group_no"));
      }
    }

    return result;
  }

  public int addAssignmentToArts(String subjectGroupId, String assignmentId, String parentArtsAssId, String updateBy) throws Exception {
    Timestamp now = new Timestamp(System.currentTimeMillis());
    Arts_assignment_tblDAO dao = CPCE.DAOFACTORY.createArts_assignment_tblDAO();
    Arts_assignment_tbl parentArtsAssTbl = dao.findByPrimaryKey(new Arts_assignment_tblPK(parentArtsAssId), this.jdbcTemplate);
    String whereClause = " exists ( select 1 from arts_assignment_mapping_tbl A   where A.arts_assignment_id = arts_assignment_tbl.arts_assignment_id  and A.assignment_id = ? )  and  arts_group_id is null and parent is null ";
    List list = dao.findExecutingUserWhere(whereClause, new Object[]{assignmentId}, this.jdbcTemplate);
    if (list != null && list.size() != 0) {
      Arts_assignment_tbl childArtsAssTbl = (Arts_assignment_tbl)list.get(0);
      if (parentArtsAssTbl.getMarked_by_mark().equals("1") && childArtsAssTbl.getMarked_by_mark().equals("0")) {
        return -1;
      } else {
        parentArtsAssTbl.setUpdate_by(updateBy);
        parentArtsAssTbl.setUpdate_date(now);
        parentArtsAssTbl.setArts_rubrics_id((String)null);
        parentArtsAssTbl.setArts_ilo_id((String)null);
        String artsGroupId = parentArtsAssTbl.getArts_group_id();
        childArtsAssTbl.setParent(parentArtsAssId);
        TransactionStatus status = this.transactionManager.getTransaction(this.defaultTransactionDefinition);
        String updateArtsAssignmentSql = " update arts_assignment_tbl set arts_group_id = ?, update_by=?, update_date = ?  where exists ( select 1 from arts_assignment_mapping_tbl A  where A.assignment_id = ? and A.arts_assignment_id = arts_assignment_tbl.arts_assignment_id )";

        try {
          dao.update(new Arts_assignment_tblPK(childArtsAssTbl.getArts_assignment_id()), childArtsAssTbl, this.jdbcTemplate);
          dao.update(new Arts_assignment_tblPK(parentArtsAssId), parentArtsAssTbl, this.jdbcTemplate);
          this.jdbcTemplate.update(updateArtsAssignmentSql, new Object[]{artsGroupId, updateBy, now, assignmentId});
          this.transactionManager.commit(status);
          return 0;
        } catch (Exception var15) {
          this.transactionManager.rollback(status);
          throw var15;
        }
      }
    } else {
      return -3;
    }
  }

  public int deleteAssignmentArts(String assignmentId) throws Exception {
    if (assignmentId != null && assignmentId.length() != 0) {
      String delArtsMark = "delete arts_mark_entries_tbl from arts_mark_entries_tbl A  join arts_assignment_mapping_tbl B on A.arts_assignment_id = B.arts_assignment_id  join assignment_tbl C on B.assignment_id = C.assignment_id  join student_belong_group_tbl D on D.subject_group_id = C.subject_group_id and D.user_id = A.user_id   where C.assignment_id = ? ";
      String delArtsRawMark = "delete arts_raw_mark_entries_tbl from arts_raw_mark_entries_tbl A  join arts_assignment_mapping_tbl B on A.arts_assignment_id = B.arts_assignment_id  join assignment_tbl C on B.assignment_id = C.assignment_id  join student_belong_group_tbl D on D.subject_group_id = C.subject_group_id and D.user_id = A.user_id   where C.assignment_id = ? ";
      String delMapping = "delete from arts_assignment_mapping_tbl where assignment_id = ? ";
      String delArtsAssignment = null;
      String selArtsAssignment = "select A.arts_assignment_id   from arts_assignment_tbl A join arts_assignment_mapping_tbl B  on A.arts_assignment_id = B.arts_assignment_id and B.assignment_id = ?  left join arts_assignment_mapping_tbl C  on C.arts_assignment_id = A.arts_assignment_id and C.assignment_id != ?   where C.assignment_id is null ";
      String delIds = "";
      List list = this.jdbcTemplate.queryForList(selArtsAssignment, new Object[]{assignmentId, assignmentId});
      if (list != null && list.size() > 0) {
        Map map;
        for(Iterator it = list.iterator(); it.hasNext(); delIds = delIds + ",'" + (String)map.get("arts_assignment_id") + "'") {
          map = (Map)it.next();
        }

        delIds = delIds.substring(1);
        delArtsAssignment = "delete arts_assignment_tbl where arts_assignment_id in (" + delIds + ")";
      }

      TransactionStatus status = this.transactionManager.getTransaction(this.defaultTransactionDefinition);

      try {
        Object[] param = new Object[]{assignmentId};
        this.jdbcTemplate.update(delArtsMark, param);
        this.jdbcTemplate.update(delArtsRawMark, param);
        this.jdbcTemplate.update(delMapping, param);
        if (delArtsAssignment != null) {
          this.jdbcTemplate.update(delArtsAssignment);
        }

        this.transactionManager.commit(status);
        return 0;
      } catch (Exception var11) {
        this.transactionManager.rollback(status);
        throw var11;
      }
    } else {
      return -1;
    }
  }

  public int mapAssignmentArts(String assignmentId, String artsAssId) throws Exception {
    if (assignmentId != null && assignmentId.length() != 0) {
      String delArtsMark = "delete arts_mark_entries_tbl from arts_mark_entries_tbl A  join arts_assignment_mapping_tbl B on A.arts_assignment_id = B.arts_assignment_id  join assignment_tbl C on B.assignment_id = C.assignment_id  join student_belong_group_tbl D on D.subject_group_id = C.subject_group_id and D.user_id = A.user_id  join arts_assignment_tbl E on E.arts_assignment_id = A.arts_assignment_id  where C.assignment_id = ?  and E.arts_group_id is null ";
      String delArtsRawMark = "delete arts_raw_mark_entries_tbl from arts_raw_mark_entries_tbl A  join arts_assignment_mapping_tbl B on A.arts_assignment_id = B.arts_assignment_id  join assignment_tbl C on B.assignment_id = C.assignment_id  join student_belong_group_tbl D on D.subject_group_id = C.subject_group_id and D.user_id = A.user_id   join arts_assignment_tbl E on E.arts_assignment_id = A.arts_assignment_id  where C.assignment_id = ?  and E.arts_group_id is null ";
      String delMapping = "delete from arts_assignment_mapping_tbl where assignment_id = ? ";
      String selArtsAssignment = "select A.arts_assignment_id   from arts_assignment_tbl A join arts_assignment_mapping_tbl B  on A.arts_assignment_id = B.arts_assignment_id and B.assignment_id = ?  where A.arts_group_id is null ";
      String delArtsAssignment = null;
      String delIds = "";
      List list = this.jdbcTemplate.queryForList(selArtsAssignment, new Object[]{assignmentId});
      if (list != null && list.size() > 0) {
        Map map;
        for(Iterator it = list.iterator(); it.hasNext(); delIds = delIds + ",'" + (String)map.get("arts_assignment_id") + "'") {
          map = (Map)it.next();
        }

        delIds = delIds.substring(1);
        delArtsAssignment = "delete arts_assignment_tbl where arts_assignment_id in (" + delIds + ")";
      }

      BatchPreparedStatementSetter batchSetter = null;
      String insertArtsSq = "insert into arts_assignment_mapping_tbl (arts_assignment_id, assignment_id) values (?, '" + assignmentId + "')";
      final List descentArtsAssIds = new ArrayList();
      if (artsAssId != null && artsAssId.length() > 0) {
        String subjectGroupId = null;
        subjectGroupId = this.getSubjectGroupIdFromAssignmentId(assignmentId);
        Map assignments = this.getArtsAssignmentsBySubjectGroup(subjectGroupId);
        descentArtsAssIds.addAll(ArtsAssignmentUtil.getdescentAssignemnts(assignments, artsAssId));
        descentArtsAssIds.add(artsAssId);
        String childIds = "";

        for(Iterator it = descentArtsAssIds.iterator(); it.hasNext(); childIds = childIds + ",'" + (String)it.next() + "'") {
          ;
        }

        childIds = childIds.substring(1);
        String sql = " select count(1) cnt from arts_assignment_mapping_tbl A join assignment_tbl B on A.assignment_id = B.assignment_id and B.assignment_id = ?  join assignment_tbl C on A.assignment_id = C.assignment_id and C.assignment_id != ? and B.subject_group_id = C.subject_group_id  and A.arts_assignment_id in (" + childIds + ")";
        int cnt = this.jdbcTemplate.queryForInt(sql, new Object[]{assignmentId, assignmentId});
        if (cnt > 0) {
          return -1;
        }

        sql = "select count(1) cnt from arts_assignment_tbl A where A.arts_assignment_id in (" + childIds + ") " + " and exists ( select 1 from arts_assignment_mapping_tbl B where A.arts_assignment_id = b.arts_assignment_id )";
        cnt = this.jdbcTemplate.queryForInt(sql);
        if (cnt != 0 && cnt < descentArtsAssIds.size()) {
          return -2;
        }

        batchSetter = new BatchPreparedStatementSetter() {
          public int getBatchSize() {
            return descentArtsAssIds.size();
          }

          public void setValues(PreparedStatement ps, int i) throws SQLException {
            String id = (String)descentArtsAssIds.get(i);
            ps.setString(1, id);
          }
        };
      }

      TransactionStatus status = this.transactionManager.getTransaction(this.defaultTransactionDefinition);

      try {
        Object[] param = new Object[]{assignmentId};
        this.jdbcTemplate.update(delArtsMark, param);
        this.jdbcTemplate.update(delArtsRawMark, param);
        this.jdbcTemplate.update(delMapping, param);
        if (delArtsAssignment != null) {
          this.jdbcTemplate.update(delArtsAssignment);
        }

        if (batchSetter != null) {
          this.jdbcTemplate.batchUpdate(insertArtsSq, batchSetter);
        }

        this.transactionManager.commit(status);
        return 0;
      } catch (Exception var19) {
        this.transactionManager.rollback(status);
        throw var19;
      }
    } else {
      return -2;
    }
  }

  public int changeParentAssignment(String subjectGroupId, String childArtsAssId, String parentArtsAssId, String updateBy) throws Arts_assignment_tblException, Arts_assignment_viewDTOException {
    Map assignments = this.getArtsAssignmentsBySubjectGroup(subjectGroupId);
    List parentsList = ArtsAssignmentUtil.getAncestorAssignments(assignments, parentArtsAssId);
    Timestamp now = new Timestamp(System.currentTimeMillis());
    if (!parentsList.contains(childArtsAssId)) {
      Arts_assignment_viewTO child = (Arts_assignment_viewTO)assignments.get(childArtsAssId);
      Arts_assignment_viewTO parent = (Arts_assignment_viewTO)assignments.get(parentArtsAssId);
      if (parent.getMarked_by_mark().equals("1") && child.getMarked_by_mark().equals("0")) {
        return -1;
      } else {
        Arts_assignment_tblDAO dao = CPCE.DAOFACTORY.createArts_assignment_tblDAO();
        Arts_assignment_tbl parentArtsAss = dao.findByPrimaryKey(new Arts_assignment_tblPK(parentArtsAssId), this.jdbcTemplate);
        Arts_assignment_tbl childArtsAss = dao.findByPrimaryKey(new Arts_assignment_tblPK(childArtsAssId), this.jdbcTemplate);
        parentArtsAss.setUpdate_by(updateBy);
        parentArtsAss.setUpdate_date(now);
        parentArtsAss.setArts_rubrics_id((String)null);
        parentArtsAss.setArts_ilo_id((String)null);
        childArtsAss.setParent(parentArtsAssId);
        childArtsAss.setUpdate_date(now);
        childArtsAss.setUpdate_by(updateBy);
        TransactionStatus status = this.transactionManager.getTransaction(this.defaultTransactionDefinition);

        try {
          dao.update(new Arts_assignment_tblPK(childArtsAssId), childArtsAss, this.jdbcTemplate);
          dao.update(new Arts_assignment_tblPK(parentArtsAssId), parentArtsAss, this.jdbcTemplate);
          this.transactionManager.commit(status);
          return 0;
        } catch (Arts_assignment_tblException var15) {
          this.transactionManager.rollback(status);
          throw var15;
        }
      }
    } else {
      return -2;
    }
  }

  public Map getArtsAssignmentsByAssignment(String assignmentId) throws Arts_assignment_viewDTOException {
    String whereClause = " exists (select 1 from arts_assignment_mapping_tbl A where  A.arts_assignment_id = arts_assignment_view.arts_assignment_id and A.assignment_id = ? )  ";
    Object[] sqlParam = new Object[]{assignmentId};
    List list = CPCE.DAOFACTORY.createArts_assignment_viewDTO().find(whereClause, " arts_assignment_id", sqlParam, this.jdbcTemplate);
    Map map = new HashMap();
    if (list.size() > 0) {
      map = new HashMap();
      Iterator it = list.iterator();

      while(it.hasNext()) {
        Arts_assignment_viewTO tbl = (Arts_assignment_viewTO)it.next();
        map.put(tbl.getArts_assignment_id(), tbl);
      }
    }

    return map;
  }

  public Map getArtsAssignmentsBySubjectGroup(String subjectGroupId) throws Arts_assignment_viewDTOException {
    Object[] param = new Object[]{subjectGroupId};
    List list = CPCE.DAOFACTORY.createArts_assignment_viewDTO().find(" subject_group_id = ? ", "", param, this.jdbcTemplate);
    Map map = null;
    if (list.size() > 0) {
      map = new HashMap();
      Iterator it = list.iterator();

      while(it.hasNext()) {
        Arts_assignment_viewTO tbl = (Arts_assignment_viewTO)it.next();
        map.put(tbl.getArts_assignment_id(), tbl);
      }
    }

    return map;
  }

  public List getArtsAssignmentMappingList(String groupId, boolean isForSelectChild) {
    List resultList = new ArrayList();
    String sql = null;
    if (isForSelectChild) {
      sql = "select arts_assignment_id  from arts_assignment_tbl A  where exists (  select 1 from arts_assignment_mapping_tbl B  where B.arts_assignment_id = A.arts_assignment_id )  and exists ( select 1 from arts_assignment_mapping_tbl C  where C.arts_assignment_id = A.parent ) and A.arts_group_id = ?  ";
    } else {
      sql = "select arts_assignment_id  from arts_assignment_tbl A  where exists (  select 1 from arts_assignment_mapping_tbl B  where B.arts_assignment_id = A.arts_assignment_id ) and A.arts_group_id = ?  ";
    }

    List list = this.jdbcTemplate.queryForList(sql, new Object[]{groupId});
    if (list != null && list.size() > 0) {
      Iterator it = list.iterator();

      while(it.hasNext()) {
        Map map = (Map)it.next();
        resultList.add((String)map.get("arts_assignment_id"));
      }
    }

    return resultList;
  }

  public List getMappedAssignmentList(String groupId) {
    List resultList = new ArrayList();
    String sql = "select A.assignment_id, A.title from assignment_tbl A  where exists (select 1 from arts_assignment_tbl B  join arts_assignment_mapping_tbl C  on B.arts_assignment_id = C.arts_assignment_id  where A.assignment_id = C.assignment_id and B.arts_group_id is null )  and A.subject_group_id = ?  order by A.title ";
    List list = this.jdbcTemplate.queryForList(sql, new Object[]{groupId});
    if (list != null && list.size() > 0) {
      Iterator it = list.iterator();

      while(it.hasNext()) {
        Map map = (Map)it.next();
        resultList.add(new String[]{(String)map.get("assignment_id"), (String)map.get("title")});
      }
    }

    return resultList;
  }

  public Map getSubjectGroupIdTitleFromAssignmentId(String assignmentId) {
    Map resultMap = null;
    String sql = "select subject_group_id, title from assignment_tbl where assignment_id = ?";
    List list = this.jdbcTemplate.queryForList(sql, new Object[]{assignmentId});
    if (list != null && list.size() > 0) {
      resultMap = (Map)list.get(0);
    }

    return resultMap;
  }

  public String getSubjectGroupIdFromAssignmentId(String assignmentId) {
    String subjectGroupId = null;
    String sql = "select subject_group_id from assignment_tbl where assignment_id = ?";
    List list = this.jdbcTemplate.queryForList(sql, new Object[]{assignmentId});
    if (list != null && list.size() > 0) {
      Map map = (Map)list.get(0);
      subjectGroupId = (String)map.get("subject_group_id");
    }

    return subjectGroupId;
  }

  public List getNotSelectabeArtsAssignmentForChangeAssignmentArtsSetting(String subjectGroupId) {
    String sql = "select A.arts_assignment_id from arts_assignment_tbl A join arts_group_tbl B on A.arts_group_id = B.arts_group_id   join arts_assignment_mapping_tbl C on A.arts_assignment_id = C.arts_assignment_id  join assignment_tbl D on D.assignment_id = C.assignment_id and D.subject_group_id = B.subject_group_id  where B.subject_group_id = ?  union all  select A.arts_assignment_id from arts_assignment_tbl A  join arts_group_tbl B on A.arts_group_id = B.arts_group_id  join arts_assignment_mapping_tbl C on A.arts_assignment_id = C.arts_assignment_id  join assignment_tbl D on D.assignment_id = C.assignment_id and D.subject_group_id != B.subject_group_id  where B.subject_group_id = ? and exists (select 1 from arts_assignment_mapping_tbl E  where A.parent = E.arts_assignment_id )";
    List list = this.jdbcTemplate.queryForList(sql, new Object[]{subjectGroupId, subjectGroupId});
    List resultList = new ArrayList();
    if (list != null && list.size() > 0) {
      Iterator it = list.iterator();

      while(it.hasNext()) {
        Map map = (Map)it.next();
        resultList.add(map.get("arts_assignment_id"));
      }
    }

    return resultList;
  }

  public String getMappedArtsAssignmentRootByAssignment(String assignmentId) {
    String sql = " select A.arts_assignment_id from arts_assignment_tbl A  join  arts_assignment_mapping_tbl B on A.arts_assignment_id = B.arts_assignment_id and B.assignment_id = ?  left join arts_assignment_mapping_tbl C on A.parent = C.arts_assignment_id  where C.assignment_id is null ";
    List list = this.jdbcTemplate.queryForList(sql, new Object[]{assignmentId});
    String rootId = null;
    if (list != null && list.size() > 0) {
      Map map = (Map)list.get(0);
      rootId = (String)map.get("arts_assignment_id");
    }

    return rootId;
  }

  public void updateArtsAssignmentOrder(Map parameterMap, final String updateBy) {
    String sql = "update arts_assignment_tbl set assignment_order = ?, update_by = ? , update_date = ? where arts_assignment_id = ? ";
    Iterator it = parameterMap.keySet().iterator();
    final ArrayList orderList = new ArrayList();

    while(it.hasNext()) {
      String parameter = (String)it.next();
      if (parameter.startsWith("order_")) {
        String[] obj = (String[])((String[])parameterMap.get(parameter));
        orderList.add(new String[]{parameter.substring(6), obj[0]});
      }
    }

    final Timestamp now = new Timestamp(System.currentTimeMillis());
    BatchPreparedStatementSetter updateSetter = new BatchPreparedStatementSetter() {
      public int getBatchSize() {
        return orderList.size();
      }

      public void setValues(PreparedStatement ps, int i) throws SQLException {
        String[] tmp = (String[])((String[])orderList.get(i));
        if (tmp[1].length() == 0) {
          ps.setInt(1, -1);
        } else {
          ps.setInt(1, Integer.parseInt(tmp[1]));
        }

        ps.setString(2, updateBy);
        ps.setTimestamp(3, now);
        ps.setString(4, tmp[0]);
      }
    };
    this.jdbcTemplate.batchUpdate(sql, updateSetter);
  }

  public List getArtsAssignmentPreOrderListByAssignment(String assignmentId) throws Arts_assignment_viewDTOException {
    return this.getArtsAssignmentPreOrderList((String)null, assignmentId);
  }

  public List getArtsAssignmentPreOrderListBySubjectGroup(String subjectGroupId) throws Arts_assignment_viewDTOException {
    return this.getArtsAssignmentPreOrderList(subjectGroupId, (String)null);
  }

  private List getArtsAssignmentPreOrderList(String subjectGroupId, String assignmentId) throws Arts_assignment_viewDTOException {
    List resultList = new ArrayList();
    Map assignments = this.getArtsAssignments(subjectGroupId, assignmentId);
    if (assignments.size() == 0) {
      return resultList;
    } else {
      String sql = null;
      Object[] param = new Object[1];
      if (subjectGroupId != null) {
        sql = "select A.arts_ilo_id, A.name from arts_ilo_tbl A join subject_group_tbl B on A.subject_id = B.subject_id  where B.subject_group_id = ?  ";
        param[0] = subjectGroupId;
      } else {
        sql = "select A.arts_ilo_id, A.name from arts_ilo_tbl A join subject_group_tbl B  on A.subject_id = B.subject_id  join assignment_tbl C on C.subject_group_id = B.subject_group_id  where C.assignment_id = ? ";
        param[0] = assignmentId;
      }

      Map iloMap = new HashMap();
      List iloList = this.jdbcTemplate.queryForList(sql, param);
      Iterator it = iloList.iterator();

      while(it.hasNext()) {
        Map map = (Map)it.next();
        iloMap.put((String)map.get("arts_ilo_id"), (String)map.get("name"));
      }

      List assignmentIdList = ArtsAssignmentUtil.preOrderAssignments(ArtsAssignmentUtil.getArtsAssignmentRoot(assignments), assignments);
      it = assignmentIdList.iterator();
      String iloIds = "";
      String iloNames = null;

      while(it.hasNext()) {
        Arts_assignment_viewTO view = (Arts_assignment_viewTO)assignments.get(it.next());
        iloIds = view.getArts_ilo_id();
        iloNames = "";
        if (iloIds != null) {
          String[] iloId = iloIds.split(",");
          int i = 0;

          for(int j = iloId.length; i < j; ++i) {
            iloNames = iloNames + ", " + (i + 1) + ") " + iloMap.get(iloId[i]);
          }

          iloNames = iloNames.substring(2);
        }

        String parentId = view.getParent();
        view.setInput_no_max(assignmentIdList.indexOf(parentId) + 1);
        view.setIlo_title(iloNames);
        resultList.add(view);
      }

      return resultList;
    }
  }

  class MyComparator implements Comparator {
    MyComparator() {
    }

    public int compare(Object s1, Object s2) {
      String[] ss1 = ((String)s1).split("_");
      String[] ss2 = ((String)s2).split("_");
      int t1 = ss1.length;
      int t2 = ss2.length;
      if (t1 > t2) {
        return 1;
      } else if (t1 < t2) {
        return -1;
      } else if (s1.equals(s2)) {
        return 0;
      } else {
        int i = 0;

        for(int j = t1; i < j; ++i) {
          t1 = Integer.parseInt(ss1[i]);
          t2 = Integer.parseInt(ss2[i]);
          if (t1 < t2) {
            return -1;
          }

          if (t1 > t2) {
            return 1;
          }
        }

        return 1;
      }
    }
  }
}
