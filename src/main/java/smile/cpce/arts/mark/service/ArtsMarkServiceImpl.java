//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.arts.mark.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import smile.cpce.arts.assignment.common.ArtsAssignmentUtil;
import smile.cpce.arts.assignment.service.ArtsAssignmentService;
import smile.cpce.arts.lock.service.ArtsLockService;
import smile.cpce.arts.mark.common.CompareAssMarkDiffForm;
import smile.cpce.arts.mark.common.CompareAssMarkResultData;
import smile.cpce.common.CPCE;
import smile.cpce.common.Constants;
import smile.cpce.common.DataAccessExceptionResolver;
import smile.cpce.common.Sequencer;
import smile.cpce.common.ToStringHelper;
import smile.cpce.common.exception.CpceException;
import smile.cpce.db.exception.Arts_assignment_viewDTOException;
import smile.cpce.db.exception.Arts_lock_tblException;
import smile.cpce.db.exception.Email_tblException;
import smile.cpce.db.exception.Student_enroll_programme_tblException;
import smile.cpce.db.exception.Student_remarks_tblException;
import smile.cpce.db.to.Arts_assignment_viewTO;
import smile.cpce.db.vo.Arts_assignment_tbl;
import smile.cpce.db.vo.Arts_grade_conv_tbl;
import smile.cpce.db.vo.Arts_mark_entries_tbl;
import smile.cpce.db.vo.Arts_raw_mark_entries_tbl;
import smile.cpce.db.vo.Email_tbl;
import smile.cpce.email.domain.Email;
import smile.cpce.email.service.EmailService;
import smile.cpce.school.programme.domain.ProgrammeSubjectGroup;
import smile.cpce.school.studentRemarks.domain.Remarks;
import smile.cpce.school.studentRemarks.service.StudentRemarksService;

public class ArtsMarkServiceImpl implements ArtsMarkService {
  private PlatformTransactionManager transactionManager;
  private DefaultTransactionDefinition defaultTransactionDefinition;
  private JdbcTemplate jdbcTemplate;
  private final Logger logger = Logger.getLogger("cpce");
  private ArtsAssignmentService artsAssignmentService;
  private ArtsLockService artsLockService;
  private StudentRemarksService studentRemarksService;
  private EmailService emailService;

  public void setEmailService(EmailService emailService) {
    this.emailService = emailService;
  }

  public void setStudentRemarksService(StudentRemarksService studentRemarksService) {
    this.studentRemarksService = studentRemarksService;
  }

  public void setArtsLockService(ArtsLockService artsLockService) {
    this.artsLockService = artsLockService;
  }

  public void setArtsAssignmentService(ArtsAssignmentService artsAssignmentService) {
    this.artsAssignmentService = artsAssignmentService;
  }

  public ArtsMarkServiceImpl(JdbcTemplate jdbcTemplate, DefaultTransactionDefinition defaultTransactionDefinition, PlatformTransactionManager transactionManager) {
    this.jdbcTemplate = jdbcTemplate;
    this.defaultTransactionDefinition = defaultTransactionDefinition;
    this.transactionManager = transactionManager;
  }

  public ArtsMarkServiceImpl() {
  }

  public void setDefaultTransactionDefinition(DefaultTransactionDefinition defaultTransactionDefinition) {
    this.defaultTransactionDefinition = defaultTransactionDefinition;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  public Map getMarks(List students, List leafAssignments, Map assignments) {
    if (students != null && students.size() != 0) {
      if (leafAssignments != null && leafAssignments.size() != 0) {
        Map marks = new HashMap();
        String studentIds = "";
        String artsAssignments = "";
        if (students != null && leafAssignments != null) {
          Iterator it;
          for(it = students.iterator(); it.hasNext(); studentIds = studentIds + "','" + ((String[])((String[])it.next()))[0]) {
            ;
          }

          if (studentIds.length() > 0) {
            studentIds = studentIds.substring(2) + "'";
          }

          for(it = leafAssignments.iterator(); it.hasNext(); artsAssignments = artsAssignments + "','" + (String)it.next()) {
            ;
          }

          if (artsAssignments.length() > 0) {
            artsAssignments = artsAssignments.substring(2) + "'";
          }

          String sql = "select user_id, arts_assignment_id, mark, grade  from arts_raw_mark_entries_tbl where user_id in (" + studentIds + ") and " + " arts_assignment_id in (" + artsAssignments + ")" + " order by arts_assignment_id ";
          List list = this.jdbcTemplate.queryForList(sql);
          if (list != null && list.size() > 0) {
            it = list.iterator();
            String previousArts_assignment_id = "";
            boolean markedByMark = true;

            while(it.hasNext()) {
              Map map = (Map)it.next();
              String arts_assignment_id = (String)map.get("arts_assignment_id");
              String user_id = (String)map.get("user_id");
              if (!previousArts_assignment_id.equals(arts_assignment_id)) {
                Arts_assignment_viewTO arts_assignment_view = (Arts_assignment_viewTO)assignments.get(arts_assignment_id);
                previousArts_assignment_id = arts_assignment_id;
                if (arts_assignment_view.getMarked_by_mark().equals("1")) {
                  markedByMark = true;
                } else {
                  markedByMark = false;
                }
              }

              if (markedByMark) {
                marks.put(user_id + "_" + arts_assignment_id, ((BigDecimal)map.get("mark")).toString());
              } else {
                marks.put(user_id + "_" + arts_assignment_id, map.get("grade"));
              }
            }
          }
        }

        return marks;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  public Map updateMarks(String subjectGroupId, Map marks, String userId, String changeItems, boolean updateDB) throws CpceException {
    Map errorMap = new HashMap();
    if (marks != null && marks.size() != 0) {
      if (changeItems != null && changeItems.length() != 0) {
        Set students = new HashSet();
        Set assignments = new HashSet();
        final Timestamp now = new Timestamp(System.currentTimeMillis());
        String studentIds = "";
        String assignmentIds = "";
        Map currentMarks = new HashMap();
        final List deleteMarksList = new ArrayList();
        final List insertMarksList = new ArrayList();
        final List updateMarksList = new ArrayList();
        Set keys = marks.keySet();
        Iterator it = keys.iterator();

        String id;
        String studentId;
        String assignmentId;
        int idx;
        while(it.hasNext()) {
          id = (String)it.next();
          idx = id.indexOf("_");
          if (idx != -1 && changeItems.indexOf(id) != -1) {
            studentId = id.substring(0, idx);
            assignmentId = id.substring(idx + 1);
            students.add(studentId);
            assignments.add(assignmentId);
          }
        }

        if (students.size() <= 0) {
          return errorMap;
        } else {
          for(it = students.iterator(); it.hasNext(); studentIds = studentIds + "','" + (String)it.next()) {
            ;
          }

          studentIds = studentIds.substring(2) + "'";

          for(it = assignments.iterator(); it.hasNext(); assignmentIds = assignmentIds + "','" + (String)it.next()) {
            ;
          }

          assignmentIds = assignmentIds.substring(2) + "'";
          String updateMarks = "update arts_raw_mark_entries_tbl  set mark=?, grade=? , update_by='" + userId + "',update_date=?" + " where user_id =? and arts_assignment_id=?";
          String insertMarks = "insert into arts_raw_mark_entries_tbl (user_id, arts_assignment_id, mark, grade, update_date, update_by ) values  (?, ?, ?, ?, ?,  '" + userId + "')";
          String var26 = "delete arts_raw_mark_entries_tbl where user_id =? and arts_assignment_id=?";
          String insertDeleteMarkAuditTrail = "INSERT INTO AUDIT_TRAIL_TBL (ID, action_type, updated_data, update_user, update_date)values(?,'DELETE',?,'" + userId + "', ? )";
          String getCurrentMarks = "select A.user_id, B.arts_assignment_id, case when B.marked_by_mark = '1' then cast(A.mark AS varchar(8)) else A.grade end as final_mark  from arts_raw_mark_entries_tbl A join arts_assignment_tbl B on A.arts_assignment_id = B.arts_assignment_id where A.user_id in (" + studentIds + ") and B.arts_assignment_id in (" + assignmentIds + ")";
          String getAssignmentInfo = "select arts_assignment_id, marked_by_mark, max_mark from  arts_assignment_tbl where arts_assignment_id in (" + assignmentIds + ")";
          String getGradeInfo = "select grade from arts_gpa_conv_tbl A join subject_tbl B on A.institute_id = B.institute_id and A.academic_year = B.academic_year  and A.semester = B.semester join subject_group_tbl C  on C.subject_id = B.subject_id where  C.subject_group_id = ? order by gpa_point desc ";
          Object[] params = new Object[]{subjectGroupId};
          List list = this.jdbcTemplate.queryForList(getGradeInfo, params);
          it = list.iterator();
          LinkedList gradeList = new LinkedList();

          Map map;
          while(it.hasNext()) {
            map = (Map)it.next();
            gradeList.add((String)map.get("grade"));
          }

          list = this.jdbcTemplate.queryForList(getAssignmentInfo);
          Map assignmentInfoMap = new HashMap();
          it = list.iterator();

          while(it.hasNext()) {
            map = (Map)it.next();
            assignmentInfoMap.put(map.get("arts_assignment_id"), new Object[]{(String)map.get("marked_by_mark"), map.get("max_mark")});
          }

          list = this.jdbcTemplate.queryForList(getCurrentMarks);
          if (list != null && list.size() > 0) {
            it = list.iterator();

            while(it.hasNext()) {
              map = (Map)it.next();
              studentId = (String)map.get("user_id");
              assignmentId = (String)map.get("arts_assignment_id");
              currentMarks.put(studentId + "_" + assignmentId, (String)map.get("final_mark"));
            }
          }

          keys = marks.keySet();
          it = keys.iterator();
          boolean markedByMark = true;
          float markFloat = 0.0F;

          while(true) {
            while(true) {
              while(true) {
                Object obj;
                String mark;
                String oldMark;
                do {
                  while(true) {
                    do {
                      do {
                        if (!it.hasNext()) {
                          if (errorMap.size() <= 0 && updateDB) {
                            BatchPreparedStatementSetter setterDel = new BatchPreparedStatementSetter() {
                              public void setValues(PreparedStatement ps, int i) throws SQLException {
                                String id = (String)deleteMarksList.get(i);
                                int idx = id.indexOf("_");
                                ps.setString(1, id.substring(0, idx));
                                ps.setString(2, id.substring(idx + 1));
                              }

                              public int getBatchSize() {
                                return deleteMarksList.size();
                              }
                            };
                            BatchPreparedStatementSetter setterInsertDelTrail = new BatchPreparedStatementSetter() {
                              public void setValues(PreparedStatement ps, int i) throws SQLException {
                                String id = (String)deleteMarksList.get(i);
                                int idx = id.indexOf("_");
                                String str = "<arts_raw_mark_entries_tbl arts_assignment_id=\"" + id.substring(idx + 1) + "\" user_id= \"" + id.substring(0, idx) + "\"/>";
                                ps.setString(1, id + "_" + Math.round(Math.random() * 1000000.0D));
                                ps.setString(2, str);
                                ps.setTimestamp(3, now);
                              }

                              public int getBatchSize() {
                                return deleteMarksList.size();
                              }
                            };
                            BatchPreparedStatementSetter setterUpdate = new BatchPreparedStatementSetter() {
                              public void setValues(PreparedStatement ps, int i) throws SQLException {
                                Arts_raw_mark_entries_tbl arts_mark_entries_tbl = (Arts_raw_mark_entries_tbl)updateMarksList.get(i);
                                ps.setBigDecimal(1, arts_mark_entries_tbl.getMark());
                                ps.setString(2, arts_mark_entries_tbl.getGrade());
                                ps.setTimestamp(3, now);
                                ps.setString(4, arts_mark_entries_tbl.getUser_id());
                                ps.setString(5, arts_mark_entries_tbl.getArts_assignment_id());
                              }

                              public int getBatchSize() {
                                return updateMarksList.size();
                              }
                            };
                            BatchPreparedStatementSetter setterInsert = new BatchPreparedStatementSetter() {
                              public void setValues(PreparedStatement ps, int i) throws SQLException {
                                Arts_raw_mark_entries_tbl arts_mark_entries_tbl = (Arts_raw_mark_entries_tbl)insertMarksList.get(i);
                                ps.setString(1, arts_mark_entries_tbl.getUser_id());
                                ps.setString(2, arts_mark_entries_tbl.getArts_assignment_id());
                                ps.setBigDecimal(3, arts_mark_entries_tbl.getMark());
                                ps.setString(4, arts_mark_entries_tbl.getGrade());
                                ps.setTimestamp(5, now);
                              }

                              public int getBatchSize() {
                                return insertMarksList.size();
                              }
                            };
                            TransactionStatus status = this.transactionManager.getTransaction(this.defaultTransactionDefinition);

                            try {
                              this.jdbcTemplate.batchUpdate("delete arts_raw_mark_entries_tbl where user_id =? and arts_assignment_id=?", setterDel);
                              this.jdbcTemplate.batchUpdate(insertMarks, setterInsert);
                              this.jdbcTemplate.batchUpdate(updateMarks, setterUpdate);
                              this.jdbcTemplate.batchUpdate(insertDeleteMarkAuditTrail, setterInsertDelTrail);
                              this.transactionManager.commit(status);
                              return errorMap;
                            } catch (Exception var47) {
                              this.transactionManager.rollback(status);
                              this.logger.log(Level.SEVERE, " ", var47);
                              DataAccessExceptionResolver.getInstance().logDAOException("ArtsMarkServiceImpl updateMark ", var47);
                              throw new CpceException(1001, "cpce.exceptionCode.sessionNotFound");
                            }
                          }

                          return errorMap;
                        }

                        mark = null;
                        id = (String)it.next();
                      } while(changeItems.indexOf(id) == -1);

                      idx = id.indexOf("_");
                    } while(idx == -1);

                    obj = marks.get(id);
                    if (obj != null) {
                      mark = ((String[])((String[])obj))[0].trim();
                    }

                    oldMark = (String)currentMarks.get(id);
                    if (mark != null && mark.length() != 0) {
                      break;
                    }

                    if (oldMark != null) {
                      deleteMarksList.add(id);
                    }
                  }
                } while(mark.equals(oldMark));

                studentId = id.substring(0, idx);
                assignmentId = id.substring(idx + 1);
                obj = assignmentInfoMap.get(assignmentId);
                if (obj != null) {
                  String marked_by_mark = (String)((Object[])((Object[])obj))[0];
                  int max_mark = (Integer)((Object[])((Object[])obj))[1];
                  if (marked_by_mark.equals("1")) {
                    markedByMark = true;
                    if (mark.equals(Constants.EXEMPT_STR)) {
                      mark = Constants.EXEMPT_MARK;
                    }

                    try {
                      markFloat = Float.parseFloat(mark);
                    } catch (NumberFormatException var48) {
                      errorMap.put(id, "Wrong number");
                      continue;
                    }

                    if (!mark.equals(Constants.EXEMPT_MARK) && (markFloat > (float)max_mark || markFloat < 0.0F)) {
                      errorMap.put(id, "Incorrect mark range");
                      continue;
                    }
                  } else {
                    markedByMark = false;
                    if (!gradeList.contains(mark)) {
                      errorMap.put(id, "Incorrect grade");
                      continue;
                    }
                  }

                  Arts_raw_mark_entries_tbl arts_mark_entries_tbl = new Arts_raw_mark_entries_tbl();
                  arts_mark_entries_tbl.setArts_assignment_id(assignmentId);
                  arts_mark_entries_tbl.setUser_id(studentId);
                  if (markedByMark) {
                    arts_mark_entries_tbl.setMark(new BigDecimal(mark));
                  } else {
                    arts_mark_entries_tbl.setGrade(mark);
                  }

                  if (oldMark == null) {
                    insertMarksList.add(arts_mark_entries_tbl);
                  } else {
                    updateMarksList.add(arts_mark_entries_tbl);
                  }
                } else {
                  errorMap.put(id, "No such assignment");
                }
              }
            }
          }
        }
      } else {
        return errorMap;
      }
    } else {
      throw new CpceException(2003, "cpce.exceptionCode.noMarks");
    }
  }

  public List getStudentListByMarkOrdering(String artsAssId, String subjectGroupIds) {
    String ids = subjectGroupIds.replaceAll(",", "','");
    ids = "'" + ids + "'";
    String sql = "select A.user_id, B.student_no, A.name_eng, A.name_chi,  E.subject_group_no, D.arts_assignment_id,  D.gpa,  D.mark  from user_tbl A join student_tbl B on A.user_id = B.user_id join student_belong_group_tbl C on C.user_id = A.user_id join subject_group_tbl E on C.subject_group_id = E.subject_group_id  left  join arts_mark_entries_tbl D  on D.user_id = A.user_id and  D.arts_assignment_id = ? where  E.subject_group_id in (" + ids + ")" + " and A.account_status=1 and  C.belong=1 order by D.gpa desc, D.mark desc, A.name_eng";
    Object[] param = new Object[]{artsAssId};
    List list = this.jdbcTemplate.queryForList(sql, param);
    List result = new LinkedList();
    Iterator it = list.iterator();

    while(it.hasNext()) {
      Map map = (Map)it.next();
      String[] student = new String[]{(String)map.get("user_id"), (String)map.get("name_eng"), null, null};
      String chiName = (String)map.get("name_chi");
      if (chiName != null) {
        student[1] = student[1] + "(" + chiName + ")";
      }

      student[2] = (String)map.get("student_no");
      student[3] = (String)map.get("subject_group_no");
      result.add(student);
    }

    return result;
  }

  public Map getStudentAllMarks(String studentId) {
    String sql = "select A.user_id,  A.arts_assignment_id, B.parent, cast(A.gpa as char(8)) as gpa,  cast(A.mark as varchar(8)) as mark, A.grade from arts_mark_entries_tbl A join arts_assignment_tbl B on a.arts_assignment_id = B.arts_assignment_id where A.user_id = ? ";
    Map studentMarksMap = new HashMap();
    Object[] param = new Object[]{studentId};
    List list = this.jdbcTemplate.queryForList(sql, param);
    if (list != null && list.size() > 0) {
      Iterator it = list.iterator();

      while(it.hasNext()) {
        Map map = (Map)it.next();
        String assId = (String)map.get("arts_assignment_id");
        String grade = (String)map.get("grade");
        String mark = (String)map.get("mark");
        if (mark == null) {
          mark = "";
        } else if (mark.equals(Constants.EXEMPT_MARK)) {
          mark = Constants.EXEMPT_STR;
        }

        if (grade == null) {
          grade = "";
        }

        mark = mark + " " + grade;
        String gpa = (String)map.get("gpa");
        String parent = (String)map.get("parent");
        if (mark == null) {
          mark = "";
        }

        if (parent == null) {
          if (mark.length() > 0 && gpa != null) {
            mark = gpa + mark;
          }
        } else if ("-1.00".equals(gpa)) {
          mark = "*";
        }

        mark = mark.replaceAll(" ", "&nbsp;");
        studentMarksMap.put(assId, mark);
      }
    }

    return studentMarksMap;
  }

  private Map getStudentMarksForView(boolean getRaw, String subjectGroupIds, String assignmentId) {
    String sql = "select A.user_id,  A.arts_assignment_id, B.parent, gpaTbl.fail,  cast(A.gpa as char(8)) as gpa, case when A.mark is not null and A.grade is not null then left(cast(A.mark as varchar(8)) + '         ',9) +    A.grade when A.mark is not null and A.grade is null then cast(A.mark as varchar(8) ) when A.mark is null  and B.marked_by_mark='0' and  A.grade is not null then A.grade  else '' end as markStr ";
    List marksList = null;
    if (subjectGroupIds != null) {
      String ids = subjectGroupIds.replaceAll(",", "','");
      ids = "'" + ids + "'";
      sql = sql + "from " + (getRaw ? "arts_raw_mark_entries_tbl " : "arts_mark_entries_tbl ") + " A  join student_belong_group_tbl C " + "on A.user_id = C.user_id " + "join arts_assignment_tbl B " + "on B.arts_assignment_id = A.arts_assignment_id " + "join arts_group_tbl D on B.arts_group_id = D.arts_group_id and D.subject_group_id = C.subject_group_id  " + "join subject_group_tbl E on E.subject_group_id = C.subject_group_id " + "join subject_tbl F on E.subject_id = F.subject_id " + "left join arts_gpa_conv_tbl gpaTbl on F.institute_id = gpaTbl.institute_id and F.academic_year = gpaTbl.academic_year " + "and F.semester = gpaTbl.semester and gpaTbl.grade = A.grade " + "where C.subject_group_id in (" + ids + ")  and C.belong = 1 ";
      marksList = this.jdbcTemplate.queryForList(sql);
    } else {
      sql = sql + "from " + (getRaw ? "arts_raw_mark_entries_tbl " : "arts_mark_entries_tbl ") + " A join arts_assignment_tbl B " + "on B.arts_assignment_id = A.arts_assignment_id " + "join arts_assignment_mapping_tbl C " + "on C.arts_assignment_id = A.arts_assignment_id " + "join assignment_tbl D on D.assignment_id = C.assignment_id " + "join student_belong_group_tbl E on A.user_id = E.user_id  and E.subject_group_id = D.subject_group_id " + "join subject_group_tbl F on F.subject_group_id = E.subject_group_id " + "join subject_tbl G on G.subject_id = F.subject_id " + "left join arts_gpa_conv_tbl gpaTbl on G.institute_id = gpaTbl.institute_id " + " and G.academic_year = gpaTbl.academic_year and G.semester = gpaTbl.semester and gpaTbl.grade = A.grade " + "where D.assignment_id = ?  and E.belong = 1 ";
      marksList = this.jdbcTemplate.queryForList(sql, new Object[]{assignmentId});
    }

    Map studentMarksMap = new HashMap();
    if (marksList != null && marksList.size() > 0) {
      Iterator it = marksList.iterator();

      while(it.hasNext()) {
        Map map = (Map)it.next();
        String assId = (String)map.get("arts_assignment_id");
        String mark = (String)map.get("markStr");
        String gpa = (String)map.get("gpa");
        String parent = (String)map.get("parent");
        String fail = (String)map.get("fail");
        if (parent == null) {
          if (mark.length() > 0 && gpa != null) {
            mark = gpa + mark;
          }
        } else if ("-1.00".equals(gpa)) {
          mark = "*";
        }

        mark = mark.replaceAll(" ", "&nbsp;");
        studentMarksMap.put(map.get("user_id") + "_" + assId, new String[]{mark, fail});
      }
    }

    return studentMarksMap;
  }

  public Map getStudentMarksForViewByAssignment(String assignmentId) {
    return this.getStudentMarksForView(false, (String)null, assignmentId);
  }

  public Map getStudentMarksForViewBySubjectGroups(String subjectGroupIds) {
    return this.getStudentMarksForView(false, subjectGroupIds, (String)null);
  }

  public Map getStudentRawMarksByAssignment(String assignmentId) {
    return this.getStudentMarksForView(true, (String)null, assignmentId);
  }

  public Map getStudentRawMarksBySubjectGroups(String subjectGroupIds) {
    return this.getStudentMarksForView(true, subjectGroupIds, (String)null);
  }

  public void needCalculate(String groupId, String assignmentId, List students, Map assignments, String userId) throws Exception {
    String studentIds = "";
    if (students.size() > 0) {
      Iterator it;
      for(it = students.iterator(); it.hasNext(); studentIds = studentIds + ",'" + ((String[])((String[])it.next()))[0] + "'") {
        ;
      }

      studentIds = studentIds.substring(1);
      List var8 = ArtsAssignmentUtil.getLeafArtsAssignments(assignments);
      it = var8.iterator();

      String assignmentIds;
      for(assignmentIds = ""; it.hasNext(); assignmentIds = assignmentIds + ",'" + (String)it.next() + "'") {
        ;
      }

      boolean needCalculatedAll = false;
      if (assignmentIds.length() > 0) {
        assignmentIds = assignmentIds.substring(1);
        String sql;
        int cnt;
        if (!needCalculatedAll) {
          if (groupId != null) {
            sql = "select count(1) cnt from arts_assignment_tbl A join arts_mark_entries_tbl B  on A.arts_assignment_id = B.arts_assignment_id  where A.update_date >= B.update_date  and B.user_id in (" + studentIds + ") " + " and A.arts_group_id = ? ";
            cnt = this.jdbcTemplate.queryForInt(sql, new Object[]{groupId});
            if (cnt > 0) {
              needCalculatedAll = true;
            }
          } else if (assignmentId != null) {
            sql = "select count(1) cnt from arts_assignment_tbl A join arts_assignment_mapping_tbl B  on A.arts_assignment_id = B.arts_assignment_id  join arts_mark_entries_tbl C on C.arts_assignment_id = B.arts_assignment_id  where A.update_date >= C.update_date  and C.user_id in (" + studentIds + ") " + " and B.assignment_id = ? ";
            cnt = this.jdbcTemplate.queryForInt(sql, new Object[]{assignmentId});
            if (cnt > 0) {
              needCalculatedAll = true;
            }
          }

          if (needCalculatedAll) {
            this.logger.info("need Calculate all - assignment setting changed ");
          }
        }

        if (!needCalculatedAll) {
          sql = "select count(1) cnt from arts_grade_conv_tbl A  where (A.arts_assignment_id in (" + assignmentIds + ")";
          if (groupId != null && groupId.length() > 0) {
            sql = sql + " or A.arts_group_id = '" + groupId + "' ";
          }

          sql = sql + " ) and A.update_date >  (select min(B.update_date) from arts_mark_entries_tbl B where  B.user_id in (" + studentIds + ") " + " and B.arts_assignment_id in  (" + assignmentIds + "))";
          cnt = this.jdbcTemplate.queryForInt(sql);
          if (cnt > 0) {
            needCalculatedAll = true;
          } else if (assignmentId != null) {
            sql = "\tselect count(1) cnt from arts_grade_conv_tbl A join arts_group_tbl B  on A.arts_group_id = B.arts_group_id  join assignment_tbl C on C.subject_group_id = B.subject_group_id where C.assignment_id = ? and A.update_date >= (select min(B.update_date)   from arts_mark_entries_tbl B where B.user_id in (" + studentIds + ") and B.arts_assignment_id in (" + assignmentIds + "))";
            cnt = this.jdbcTemplate.queryForInt(sql, new Object[]{assignmentId});
            if (cnt > 0) {
              needCalculatedAll = true;
            }
          }

          if (needCalculatedAll) {
            this.logger.info("need Calculate all - grade changed ");
          }
        }

        if (!needCalculatedAll) {
          if (groupId != null && groupId.length() > 0) {
            sql = " select count(1) cnt from arts_gpa_conv_tbl A join subject_tbl B on A.institute_id = B.institute_id  and A.academic_year = B.academic_year and A.semester = B.semester  join subject_group_tbl C on C.subject_id = B.subject_id  join arts_group_tbl D on D.subject_group_id = C.subject_group_id  where D.arts_group_id = ? and  A.update_date >= (select min(E.update_date)   from arts_mark_entries_tbl E where E.user_id in (" + studentIds + ") " + "  and E.arts_assignment_id in (" + assignmentIds + "))";
            cnt = this.jdbcTemplate.queryForInt(sql, new Object[]{groupId});
            if (cnt > 0) {
              needCalculatedAll = true;
            }
          } else if (!needCalculatedAll && assignmentId != null && assignmentId.length() > 0) {
            sql = "\tselect count(1) cnt from arts_gpa_conv_tbl A join subject_tbl B on A.institute_id = B.institute_id  and A.academic_year = B.academic_year and A.semester = B.semester  join subject_group_tbl C on C.subject_id = B.subject_id join assignment_tbl D on D.subject_group_id = C.subject_group_id  where D.assignment_id = ? and   A.update_date >= (select min(E.update_date)   from arts_mark_entries_tbl E where E.user_id in (" + studentIds + ") " + " \tand E.arts_assignment_id in (" + assignmentIds + ") )";
            cnt = this.jdbcTemplate.queryForInt(sql, new Object[]{assignmentId});
            if (cnt > 0) {
              needCalculatedAll = true;
            }
          }

          if (needCalculatedAll) {
            this.logger.info("need Calculate all - gpa changed ");
          }
        }

        sql = "select distinct(left(id,25)) user_assignment_id from audit_trail_tbl A left join arts_mark_entries_tbl B  on  LEFT(A.id, 12) = B.user_id and   SUBSTRING(A.id, 14,12) = B.arts_assignment_id  where (A.update_date > B.update_date or B.update_date is null) and  LEFT(A.id, 12) in (" + studentIds + ") " + " and  SUBSTRING(A.id, 14,12) in  (" + assignmentIds + ")  ";
        List changedUserAssignmentList = this.jdbcTemplate.queryForList(sql);
        final Set changeStudentIds = new HashSet();
        final List deleteIds = new ArrayList();
        if (changedUserAssignmentList != null && changedUserAssignmentList.size() > 0) {
          final List updateList = new ArrayList();
          final List insertList = new ArrayList();
          it = changedUserAssignmentList.iterator();

          while(it.hasNext()) {
            Map map = (Map)it.next();
            String id = (String)map.get("user_assignment_id");
            deleteIds.add(id);
          }

          sql = "select distinct B.user_id, B.arts_assignment_id from audit_trail_tbl A join arts_mark_entries_tbl B  on  LEFT(A.id, 12) = B.user_id and   SUBSTRING(A.id, 14,12) = B.arts_assignment_id  where A.update_date > B.update_date  and  B.user_id in (" + studentIds + ") " + " and  B.arts_assignment_id in  (" + assignmentIds + ")  ";
          List list = this.jdbcTemplate.queryForList(sql);
          List marksList = new ArrayList();
          it = list.iterator();

          String updateMarks;
          while(it.hasNext()) {
            Map map = (Map)it.next();
            updateMarks = (String)map.get("user_id") + "_" + (String)map.get("arts_assignment_id");
            marksList.add(updateMarks);
          }

          sql = "select distinct C.*  from audit_trail_tbl A left join arts_mark_entries_tbl B  on   LEFT(A.id, 12) = B.user_id and   SUBSTRING(A.id, 14,12) = B.arts_assignment_id  join arts_raw_mark_entries_tbl C on C.arts_assignment_id = SUBSTRING(A.id, 14,12)and C.user_id =  LEFT(A.id, 12)  where (A.update_date > B.update_date or B.update_date is null) and  LEFT(A.id, 12)in (" + studentIds + ") " + " and   SUBSTRING(A.id, 14,12) in  (" + assignmentIds + ")  ";
          List rawMarksList = CPCE.DAOFACTORY.createArts_mark_entries_tblDAO().findExecutingUserSelect(sql, (Object[])null, this.jdbcTemplate);

          String id;
          for(it = rawMarksList.iterator(); it.hasNext(); deleteIds.remove(id)) {
            Arts_mark_entries_tbl tbl = (Arts_mark_entries_tbl)it.next();
            id = tbl.getUser_id() + "_" + tbl.getArts_assignment_id();
            if (marksList.contains(id)) {
              updateList.add(tbl);
            } else {
              insertList.add(tbl);
            }
          }

          deleteIds.retainAll(marksList);
          updateMarks = "update arts_mark_entries_tbl  set mark=?, grade=? , update_by='" + userId + "',update_date=?, gpa=? " + " where user_id =? and arts_assignment_id=?";
          id = "insert into arts_mark_entries_tbl (user_id, arts_assignment_id, mark, grade, gpa, update_date, update_by ) values  (?, ?, ?, ?,?, ?,  '" + userId + "')";
          String deleteMarks = "delete arts_mark_entries_tbl where user_id =? and arts_assignment_id=?";
          final Timestamp now = new Timestamp(System.currentTimeMillis());
          BatchPreparedStatementSetter setterDel = new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) throws SQLException {
              String id = (String)deleteIds.get(i);
              ArtsMarkServiceImpl.this.logger.info("delete:" + id);
              String[] tmp = id.split("_");
              ps.setString(1, tmp[0]);
              ps.setString(2, tmp[1]);
              changeStudentIds.add(tmp[0]);
            }

            public int getBatchSize() {
              return deleteIds.size();
            }
          };
          BatchPreparedStatementSetter setterUpdate = new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) throws SQLException {
              Arts_mark_entries_tbl arts_mark_entries_tbl = (Arts_mark_entries_tbl)updateList.get(i);
              ArtsMarkServiceImpl.this.logger.info("update:" + arts_mark_entries_tbl.getUser_id() + "_" + arts_mark_entries_tbl.getArts_assignment_id());
              BigDecimal mark = arts_mark_entries_tbl.getMark();
              if (mark != null) {
                mark = mark.setScale(2, 4);
              }

              ps.setBigDecimal(1, mark);
              ps.setString(2, arts_mark_entries_tbl.getGrade());
              ps.setTimestamp(3, now);
              ps.setBigDecimal(4, arts_mark_entries_tbl.getGpa());
              ps.setString(5, arts_mark_entries_tbl.getUser_id());
              ps.setString(6, arts_mark_entries_tbl.getArts_assignment_id());
              changeStudentIds.add(arts_mark_entries_tbl.getUser_id());
            }

            public int getBatchSize() {
              return updateList.size();
            }
          };
          BatchPreparedStatementSetter setterInsert = new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) throws SQLException {
              Arts_mark_entries_tbl arts_mark_entries_tbl = (Arts_mark_entries_tbl)insertList.get(i);
              ArtsMarkServiceImpl.this.logger.info("insert:" + arts_mark_entries_tbl.getUser_id() + "_" + arts_mark_entries_tbl.getArts_assignment_id());
              ps.setString(1, arts_mark_entries_tbl.getUser_id());
              ps.setString(2, arts_mark_entries_tbl.getArts_assignment_id());
              BigDecimal mark = arts_mark_entries_tbl.getMark();
              if (mark != null) {
                mark = mark.setScale(2, 4);
              }

              ps.setBigDecimal(3, mark);
              ps.setString(4, arts_mark_entries_tbl.getGrade());
              ps.setBigDecimal(5, arts_mark_entries_tbl.getGpa());
              ps.setTimestamp(6, now);
              changeStudentIds.add(arts_mark_entries_tbl.getUser_id());
            }

            public int getBatchSize() {
              return insertList.size();
            }
          };
          TransactionStatus status = this.transactionManager.getTransaction(this.defaultTransactionDefinition);

          try {
            this.jdbcTemplate.batchUpdate("delete arts_mark_entries_tbl where user_id =? and arts_assignment_id=?", setterDel);
            this.jdbcTemplate.batchUpdate(id, setterInsert);
            this.jdbcTemplate.batchUpdate(updateMarks, setterUpdate);
            this.transactionManager.commit(status);
          } catch (Exception var30) {
            this.transactionManager.rollback(status);
            this.logger.log(Level.SEVERE, " ", var30);
            DataAccessExceptionResolver.getInstance().logDAOException("ArtsMarkServiceImpl updateMark ", var30);
            throw var30;
          }
        }

        String tmp = "";
        if (!needCalculatedAll) {
          if (students.size() > 0) {
            it = students.iterator();

            while(it.hasNext()) {
              String studentId = ((String[])((String[])it.next()))[0];
              if (!changeStudentIds.contains(studentId)) {
                it.remove();
              } else {
                tmp = tmp + "," + studentId;
              }
            }
          }

          this.logger.info("calculateList:" + tmp);
        }

      }
    }
  }

  private void calculateMarks(String groupId, String assignmentId, List students, Map assignments, String userId) throws Exception {
    this.needCalculate(groupId, assignmentId, students, assignments, userId);
    if (assignments != null && assignments.size() != 0 && students != null && students.size() != 0) {
      boolean isFromAssignment = false;
      if (assignmentId != null && assignmentId.length() > 0) {
        isFromAssignment = true;
      }

      String studentIds = "";
      Iterator it;
      if (students.size() > 0) {
        for(it = students.iterator(); it.hasNext(); studentIds = studentIds + ",'" + ((String[])((String[])it.next()))[0] + "'") {
          ;
        }

        studentIds = studentIds.substring(1);
      }

      String getGPAInfo = "";
      String getMarksClause = "";
      String getGradeClause = "";
      Object[] param = null;
      if (!isFromAssignment) {
        getGPAInfo = "select distinct A.grade, A.gpa_point, A.conversion, A.fail, B.subject_id  from arts_gpa_conv_tbl A  join subject_tbl B on A.institute_id = B.institute_id and A.academic_year = B.academic_year  and A.semester = B.semester join subject_group_tbl C  on C.subject_id = B.subject_id  join arts_group_tbl D  on  D.subject_group_id = C.subject_group_id   where D.subject_Group_id = ? order by A.gpa_point desc ";
        getMarksClause = " exists (select 1 from arts_assignment_tbl B where  B.arts_assignment_id = arts_mark_entries_tbl.arts_assignment_id and B.arts_group_id = ?)  and arts_mark_entries_tbl.user_id in (" + studentIds + ")";
        getGradeClause = " arts_group_id=? order by arts_assignment_id ";
        param = new Object[]{groupId};
      } else {
        getGPAInfo = "select distinct A.grade, A.gpa_point, A.conversion, A.fail, B.subject_id  from arts_gpa_conv_tbl A  join subject_tbl B on A.institute_id = B.institute_id and A.academic_year = B.academic_year  and A.semester = B.semester join subject_group_tbl C  on C.subject_id = B.subject_id  join assignment_tbl D on D.subject_group_id = C.subject_group_id  where D.assignment_id = ?   order by A.gpa_point desc ";
        getMarksClause = " exists (select 1 from arts_assignment_tbl B join arts_assignment_mapping_tbl C on B.arts_assignment_id = C.arts_assignment_id  where C.assignment_id = ? and arts_mark_entries_tbl.arts_assignment_id = B.arts_assignment_id )  and arts_mark_entries_tbl.user_id in (" + studentIds + ") ";
        getGradeClause = " exists ( select 1 from arts_group_tbl A join subject_group_tbl B on A.subject_group_id = B.subject_group_id  join assignment_tbl C on C.subject_group_id = B.subject_group_id  where C.assignment_id = ? and arts_grade_conv_tbl.arts_group_id = A.arts_group_id ) order by arts_assignment_id ";
        param = new Object[]{assignmentId};
      }

      Map studentMarksMap = new HashMap();
      Map gradeMap = new HashMap();
      Map gpaMap = new HashMap();
      String artsAssId = null;
      String failGrades = "";
      Map gpaConvMap = new TreeMap();
      List list = CPCE.DAOFACTORY.createArts_mark_entries_tblDAO().findExecutingUserWhere(getMarksClause, param, this.jdbcTemplate);
      if (list != null && list.size() > 0) {
        it = list.iterator();

        while(it.hasNext()) {
          Arts_mark_entries_tbl arts_mark_entries_tbl = (Arts_mark_entries_tbl)it.next();
          studentMarksMap.put(arts_mark_entries_tbl.getUser_id() + "_" + arts_mark_entries_tbl.getArts_assignment_id(), arts_mark_entries_tbl);
        }
      }

      list = CPCE.DAOFACTORY.createArts_grade_conv_tblDAO().findExecutingUserWhere(getGradeClause, param, this.jdbcTemplate);
      if (list != null && list.size() > 0) {
        it = list.iterator();
        String previousArtsAssId = null;
        TreeMap subGradeMap = new TreeMap();

        while(it.hasNext()) {
          Arts_grade_conv_tbl arts_grade_conv_tbl = (Arts_grade_conv_tbl)it.next();
          artsAssId = arts_grade_conv_tbl.getArts_assignment_id();
          if (artsAssId == null) {
            artsAssId = "";
          }

          if (artsAssId.equals(previousArtsAssId)) {
            subGradeMap.put(arts_grade_conv_tbl.getMark_from(), arts_grade_conv_tbl.getGrade());
          } else {
            if (previousArtsAssId != null) {
              gradeMap.put(previousArtsAssId, subGradeMap);
              subGradeMap = new TreeMap();
            }

            subGradeMap.put(arts_grade_conv_tbl.getMark_from(), arts_grade_conv_tbl.getGrade());
            previousArtsAssId = artsAssId;
          }
        }

        gradeMap.put(artsAssId, subGradeMap);
      }

      list = this.jdbcTemplate.queryForList(getGPAInfo, param);
      String studentId;
      if (list != null && list.size() > 0) {
        it = list.iterator();

        while(it.hasNext()) {
          Map map = (Map)it.next();
          studentId = (String)map.get("grade");
          BigDecimal gpa = (BigDecimal)map.get("gpa_point");
          gpaMap.put(studentId, gpa);
          gpaConvMap.put(map.get("conversion"), studentId);
          if (map.get("fail").equals("1")) {
            failGrades = failGrades + "," + studentId;
          }
        }
      }

      Arts_assignment_viewTO root = ArtsAssignmentUtil.getArtsAssignmentRoot(assignments);
      if (students != null && students.size() > 0) {
        it = students.iterator();

        while(it.hasNext()) {
          studentId = ((String[])((String[])it.next()))[0];
          this.postOrderCalculate(root, assignments, studentMarksMap, gradeMap, gpaMap, gpaConvMap, failGrades, studentId);
          if (root.getMarked_by_mark().equals("1")) {
            Arts_mark_entries_tbl arts_mark_entries_tbl = (Arts_mark_entries_tbl)studentMarksMap.get(studentId + "_" + root.getArts_assignment_id());
            if (arts_mark_entries_tbl != null) {
              arts_mark_entries_tbl.setGrade(this.findGradeFromMark(gradeMap, root.getArts_assignment_id(), arts_mark_entries_tbl.getMark(), root.getMax_mark()));
            }
          }
        }

        this.updateMarks(studentMarksMap, userId);
      }

    }
  }

  private void postOrderCalculate(Arts_assignment_viewTO parent, Map assignments, Map studentMarksMap, Map gradeMap, Map gpaMap, Map gpaConvMap, String failGrades, String studentId) {
    List childList = ArtsAssignmentUtil.getArtsAssignmentChildren(parent, assignments);
    int select_no_max = parent.getSelect_no_max();
    String marked_by_mark = parent.getMarked_by_mark();
    Map sort = new TreeMap(new ArtsMarkServiceImpl.MyComparator());
    int exempt_number = 0;
    int s_number = 0;
    Iterator it = childList.iterator();
    String artsAssId;
    Arts_mark_entries_tbl arts_mark_entries_tbl;
    String grade;
    BigDecimal mark;
    if (childList.size() <= 0) {
      artsAssId = parent.getArts_assignment_id();
      arts_mark_entries_tbl = (Arts_mark_entries_tbl)studentMarksMap.get(studentId + "_" + artsAssId);
      if (arts_mark_entries_tbl != null) {
        if (marked_by_mark.equals("1")) {
          mark = arts_mark_entries_tbl.getMark();
          if ((new BigDecimal(Constants.EXEMPT_MARK)).equals(mark)) {
            arts_mark_entries_tbl.setGrade(Constants.EXEMPT_STR);
            arts_mark_entries_tbl.setGpa(new BigDecimal(-1));
          } else {
            grade = this.findGradeFromMark(gradeMap, artsAssId, mark, parent.getMax_mark());
            arts_mark_entries_tbl.setGrade(grade);
            if (grade != null) {
              arts_mark_entries_tbl.setGpa((BigDecimal)gpaMap.get(grade));
            } else {
              arts_mark_entries_tbl.setGpa((BigDecimal)null);
            }
          }
        } else {
          grade = arts_mark_entries_tbl.getGrade();
          if (grade != null && gpaMap.containsKey(grade)) {
            arts_mark_entries_tbl.setGpa((BigDecimal)gpaMap.get(grade));
          }
        }
      } else {
        arts_mark_entries_tbl = new Arts_mark_entries_tbl();
        arts_mark_entries_tbl.setArts_assignment_id(artsAssId);
        arts_mark_entries_tbl.setUser_id(studentId);
        studentMarksMap.put(studentId + "_" + artsAssId, arts_mark_entries_tbl);
      }
    } else {
      while(true) {
        Arts_assignment_viewTO child;
        if (!it.hasNext()) {
          Collection values = sort.values();
          if (select_no_max == 0) {
            select_no_max = sort.size();
          }

          it = values.iterator();
          int i = 0;
          float total = 0.0F;
          int totalWeight = 0;

          label170:
          while(true) {
            while(true) {
              if (!it.hasNext()) {
                break label170;
              }

              arts_mark_entries_tbl = (Arts_mark_entries_tbl)it.next();
              if (i >= sort.size() - select_no_max) {
                ++i;
                child = (Arts_assignment_viewTO)assignments.get(arts_mark_entries_tbl.getArts_assignment_id());
                if (marked_by_mark.equals("1")) {
                  if (arts_mark_entries_tbl.getMark() != null) {
                    if (arts_mark_entries_tbl.getMark().equals(new BigDecimal(Constants.EXEMPT_MARK))) {
                      ++exempt_number;
                      continue;
                    }

                    total += arts_mark_entries_tbl.getMark().floatValue() * (float)parent.getMax_mark() / (float)child.getMax_mark() * (float)child.getWeight();
                  }
                } else {
                  grade = arts_mark_entries_tbl.getGrade();
                  BigDecimal gpa = arts_mark_entries_tbl.getGpa();
                  if (grade != null && grade.equals(Constants.SPECIAL_GRADE)) {
                    ++s_number;
                  }

                  if (grade != null) {
                    if (grade.equals(Constants.EXEMPT_STR)) {
                      ++exempt_number;
                      continue;
                    }

                    if ("1".equals(child.getMust_pass()) && grade != null && failGrades.indexOf(grade) != -1) {
                      total = 0.0F;
                      totalWeight = child.getWeight();
                      break label170;
                    }

                    if (!gpaMap.containsKey(grade) || ((BigDecimal)gpaMap.get(grade)).floatValue() < 0.0F) {
                      total = -1.0F;
                      break label170;
                    }

                    total += ((BigDecimal)gpaMap.get(grade)).floatValue() * (float)child.getWeight();
                  }
                }

                totalWeight += child.getWeight();
              } else {
                arts_mark_entries_tbl.setMark((BigDecimal)null);
                arts_mark_entries_tbl.setGrade((String)null);
                arts_mark_entries_tbl.setGpa((BigDecimal)null);
                ++i;
              }
            }
          }

          Arts_mark_entries_tbl parentMarks = (Arts_mark_entries_tbl)studentMarksMap.get(studentId + "_" + parent.getArts_assignment_id());
          if (parentMarks == null) {
            if (parentMarks == null) {
              parentMarks = new Arts_mark_entries_tbl();
              parentMarks.setArts_assignment_id(parent.getArts_assignment_id());
              parentMarks.setUser_id(studentId);
            }

            studentMarksMap.put(studentId + "_" + parent.getArts_assignment_id(), parentMarks);
          }

          if (totalWeight == 0 && exempt_number == select_no_max) {
            if (marked_by_mark.equals("1")) {
              parentMarks.setMark(new BigDecimal(Constants.EXEMPT_MARK));
            } else {
              parentMarks.setMark((BigDecimal)null);
            }

            parentMarks.setGpa(new BigDecimal(-1));
            parentMarks.setGrade(Constants.EXEMPT_STR);
          } else if (s_number == select_no_max - exempt_number && parent.getParent() != null) {
            if (marked_by_mark.equals("0")) {
              Object tmp = gpaMap.get(Constants.SPECIAL_GRADE);
              if (tmp != null) {
                parentMarks.setGpa((BigDecimal)tmp);
                parentMarks.setGrade(Constants.SPECIAL_GRADE);
                parentMarks.setMark((BigDecimal)null);
              } else {
                parentMarks.setGrade((String)null);
                parentMarks.setMark((BigDecimal)null);
                parentMarks.setGpa((BigDecimal)null);
              }
            }
          } else if (total == -1.0F) {
            parentMarks.setMark((BigDecimal)null);
            parentMarks.setGpa(new BigDecimal(-1));
            parentMarks.setGrade((String)null);
          } else if (totalWeight > 0) {
            if (marked_by_mark.equals("1")) {
              total = (float)Math.round(total * 100.0F / (float)totalWeight) / 100.0F;
              grade = this.findGradeFromMark(gradeMap, parent.getArts_assignment_id(), new BigDecimal((double)total), parent.getMax_mark());
              parentMarks.setMark(new BigDecimal((double)total));
              parentMarks.setGrade(grade);
              this.logger.info("artsAssId:" + parent.getArts_assignment_id() + " mark:" + total + " grade:" + grade + " gpa:" + (BigDecimal)gpaMap.get(grade));
              if (grade != null && parent.getParent() != null) {
                parentMarks.setGpa((BigDecimal)gpaMap.get(grade));
              } else {
                parentMarks.setGpa((BigDecimal)null);
              }
            } else {
              total /= (float)totalWeight;
              parentMarks.setGpa(new BigDecimal((double)total));
              grade = null;
              Set keys = gpaConvMap.keySet();
              Iterator it2 = keys.iterator();
              boolean found = false;
              float from = 0.0F;

              Object fromObj;
              String lastGrade;
              for(lastGrade = null; it2.hasNext(); lastGrade = (String)gpaConvMap.get(fromObj)) {
                fromObj = it2.next();
                from = ((BigDecimal)fromObj).floatValue();
                if (total < from) {
                  if (found) {
                    grade = lastGrade;
                  } else {
                    grade = (String)gpaConvMap.get(fromObj);
                  }
                  break;
                }

                found = true;
              }

              if (found && grade == null) {
                grade = lastGrade;
              }

              parentMarks.setGrade(grade);
              parentMarks.setMark((BigDecimal)null);
            }
          } else {
            parentMarks.setGrade((String)null);
            parentMarks.setMark((BigDecimal)null);
            parentMarks.setGpa((BigDecimal)null);
          }
          break;
        }

        child = (Arts_assignment_viewTO)it.next();
        artsAssId = child.getArts_assignment_id();
        this.postOrderCalculate(child, assignments, studentMarksMap, gradeMap, gpaMap, gpaConvMap, failGrades, studentId);
        arts_mark_entries_tbl = (Arts_mark_entries_tbl)studentMarksMap.get(studentId + "_" + artsAssId);
        if (marked_by_mark.equals("1")) {
          mark = arts_mark_entries_tbl.getMark();
          if (mark != null) {
            sort.put(mark + "_" + artsAssId, arts_mark_entries_tbl);
          } else {
            sort.put("-1000_" + artsAssId, arts_mark_entries_tbl);
          }
        } else {
          grade = arts_mark_entries_tbl.getGrade();
          if (Constants.EXEMPT_STR.equals(grade)) {
            sort.put("-500_" + artsAssId, arts_mark_entries_tbl);
          } else if (grade != null && gpaMap.containsKey(grade)) {
            sort.put(arts_mark_entries_tbl.getGpa() + "_" + artsAssId, arts_mark_entries_tbl);
          } else {
            sort.put("-1000_" + artsAssId, arts_mark_entries_tbl);
          }
        }
      }
    }

  }

  private void updateMarks(Map studentMarks, String userId) throws Exception {
    if (studentMarks != null && studentMarks.size() != 0) {
      final List insertMarksList = new ArrayList();
      final List deleteMarksList = new ArrayList();
      final List updateMarksList = new ArrayList();
      Iterator it = studentMarks.values().iterator();

      while(true) {
        Arts_mark_entries_tbl arts_mark_entries_tbl;
        label47:
        do {
          while(it.hasNext()) {
            arts_mark_entries_tbl = (Arts_mark_entries_tbl)it.next();
            if (arts_mark_entries_tbl.getUpdate_by() == null) {
              continue label47;
            }

            if (arts_mark_entries_tbl.getMark() == null && arts_mark_entries_tbl.getGrade() == null && arts_mark_entries_tbl.getGpa() == null) {
              deleteMarksList.add(arts_mark_entries_tbl);
            } else {
              updateMarksList.add(arts_mark_entries_tbl);
            }
          }

          String updateMarks = "update arts_mark_entries_tbl  set mark=?, grade=? , update_by='" + userId + "',update_date=?, gpa=? " + " where user_id =? and arts_assignment_id=?";
          String insertMarks = "insert into arts_mark_entries_tbl (user_id, arts_assignment_id, mark, grade, gpa, update_date, update_by ) values  (?, ?, ?, ?,?, ?,  '" + userId + "')";
          String deleteMarks = "delete arts_mark_entries_tbl where user_id =? and arts_assignment_id=?";
          final Timestamp now = new Timestamp(System.currentTimeMillis());
          BatchPreparedStatementSetter setterDel = new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) throws SQLException {
              Arts_mark_entries_tbl arts_mark_entries_tbl = (Arts_mark_entries_tbl)deleteMarksList.get(i);
              ps.setString(1, arts_mark_entries_tbl.getUser_id());
              ps.setString(2, arts_mark_entries_tbl.getArts_assignment_id());
            }

            public int getBatchSize() {
              return deleteMarksList.size();
            }
          };
          BatchPreparedStatementSetter setterUpdate = new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) throws SQLException {
              Arts_mark_entries_tbl arts_mark_entries_tbl = (Arts_mark_entries_tbl)updateMarksList.get(i);
              BigDecimal mark = arts_mark_entries_tbl.getMark();
              if (mark != null) {
                mark = mark.setScale(2, 4);
              }

              ps.setBigDecimal(1, mark);
              ps.setString(2, arts_mark_entries_tbl.getGrade());
              ps.setTimestamp(3, now);
              ps.setBigDecimal(4, arts_mark_entries_tbl.getGpa());
              ps.setString(5, arts_mark_entries_tbl.getUser_id());
              ps.setString(6, arts_mark_entries_tbl.getArts_assignment_id());
            }

            public int getBatchSize() {
              return updateMarksList.size();
            }
          };
          BatchPreparedStatementSetter setterInsert = new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) throws SQLException {
              Arts_mark_entries_tbl arts_mark_entries_tbl = (Arts_mark_entries_tbl)insertMarksList.get(i);
              ps.setString(1, arts_mark_entries_tbl.getUser_id());
              ps.setString(2, arts_mark_entries_tbl.getArts_assignment_id());
              BigDecimal mark = arts_mark_entries_tbl.getMark();
              if (mark != null) {
                mark = mark.setScale(2, 4);
              }

              ps.setBigDecimal(3, mark);
              ps.setString(4, arts_mark_entries_tbl.getGrade());
              ps.setBigDecimal(5, arts_mark_entries_tbl.getGpa());
              ps.setTimestamp(6, now);
            }

            public int getBatchSize() {
              return insertMarksList.size();
            }
          };
          TransactionStatus status = this.transactionManager.getTransaction(this.defaultTransactionDefinition);

          try {
            this.jdbcTemplate.batchUpdate("delete arts_mark_entries_tbl where user_id =? and arts_assignment_id=?", setterDel);
            this.jdbcTemplate.batchUpdate(insertMarks, setterInsert);
            this.jdbcTemplate.batchUpdate(updateMarks, setterUpdate);
            this.transactionManager.commit(status);
            return;
          } catch (Exception var16) {
            this.transactionManager.rollback(status);
            this.logger.log(Level.SEVERE, " ", var16);
            DataAccessExceptionResolver.getInstance().logDAOException("ArtsMarkServiceImpl updateMark ", var16);
            throw var16;
          }
        } while(arts_mark_entries_tbl.getMark() == null && arts_mark_entries_tbl.getGrade() == null && arts_mark_entries_tbl.getGpa() == null);

        insertMarksList.add(arts_mark_entries_tbl);
      }
    }
  }

  public Map getStudentMarksStatisticByAssignment(String assignmentId) {
    return this.getStudentMarksStatistic((String)null, assignmentId);
  }

  public Map getStudentMarksStatisticBySubjectGroup(String subjectGroupIds) {
    return this.getStudentMarksStatistic(subjectGroupIds, (String)null);
  }

  private Map getStudentMarksStatistic(String subjectGroupIds, String assignmentId) {
    Map result = null;
    Map conversionMap = new HashMap();
    String getStudentCntSql;
    String sql;
    String getGradeSql;
    Object[] param;
    int studentCnt;
    List getGradeList;
    List markList;
    String gradeListStr;
    if (assignmentId != null && assignmentId.length() != 0) {
      param = new Object[]{assignmentId};
      getStudentCntSql = "select count(B.user_id) cnt from assignment_tbl A join student_belong_group_tbl B  on A.subject_group_id = B.subject_group_id  join user_tbl C on C.user_id = B.user_id   where A.assignment_id = ?  and B.belong = '1' and C.account_status = '1'";
      sql = "select A.arts_assignment_id, b.name, isnull(A.grade,'') grade, count(1) cnt from arts_mark_entries_tbl A join arts_assignment_tbl B on B.arts_assignment_id = A.arts_assignment_id  join arts_assignment_mapping_tbl E on E.arts_assignment_id = B.arts_assignment_id join assignment_tbl F on F.assignment_id = E.assignment_id join student_belong_group_tbl C on C.user_id = A.user_id and C.subject_group_id = F.subject_group_id join user_tbl D on D.user_id = A.user_id  where F.assignment_id = ? and D.account_status = '1' and C.belong = 1 group by A.arts_assignment_id, B.name, A.grade  order by A.arts_assignment_id ";
      getGradeSql = "select D.grade, D.conversion from  assignment_tbl A join subject_group_tbl E on A.subject_group_id = E.subject_group_id  join subject_tbl  F  on F.subject_id = E.subject_id  join arts_gpa_conv_tbl D on  F.institute_id = D.institute_id  and F.academic_year = D.academic_year  and F.semester = D.semester  where A.assignment_id = ?  order by D.arts_gpa_conv_id ";
      studentCnt = this.jdbcTemplate.queryForInt(getStudentCntSql, param);
      getGradeList = this.jdbcTemplate.queryForList(getGradeSql, param);
      markList = this.jdbcTemplate.queryForList(sql, param);
    } else {
      gradeListStr = subjectGroupIds.replaceAll(",", "','");
      gradeListStr = "'" + gradeListStr + "'";
      getStudentCntSql = "select count(1) cnt from student_belong_group_tbl A join  user_tbl B on A.user_id = b.user_id   where subject_group_id in (" + gradeListStr + ")  and belong = '1' and B.account_status = 1 ";
      sql = "select A.arts_assignment_id, b.name, isnull(A.grade,'') grade, count(1) cnt from arts_mark_entries_tbl A join arts_assignment_tbl B on B.arts_assignment_id = A.arts_assignment_id  join student_belong_group_tbl C on C.user_id = A.user_id join arts_group_tbl D on D.arts_group_id = B.arts_group_id  and D.subject_group_id = C.subject_group_id join user_tbl E on E.user_id = A.user_id and E.account_status = 1 where C.subject_group_id in (" + gradeListStr + ")" + "and C.belong = 1 " + "group by A.arts_assignment_id, B.name, A.grade  " + "order by A.arts_assignment_id ";
      getGradeSql = "select D.grade, D.conversion from  subject_group_tbl E  join subject_tbl  F  on F.subject_id = E.subject_id  join arts_gpa_conv_tbl D on  F.institute_id = D.institute_id  and F.academic_year = D.academic_year  and F.semester = D.semester  where E.subject_group_id = ?  order by D.arts_gpa_conv_id ";
      String[] tmp = subjectGroupIds.split(",");
      param = new Object[]{tmp[0]};
      studentCnt = this.jdbcTemplate.queryForInt(getStudentCntSql);
      getGradeList = this.jdbcTemplate.queryForList(getGradeSql, param);
      markList = this.jdbcTemplate.queryForList(sql);
    }

    ArrayList gradeList = null;
    if (getGradeList != null && getGradeList.size() > 0) {
      Iterator it = getGradeList.iterator();
      gradeList = new ArrayList();

      String grade;
      Map map;
      while(it.hasNext()) {
        map = (Map)it.next();
        grade = (String)map.get("grade");
        gradeList.add(grade);
        conversionMap.put(grade, map.get("conversion"));
      }

      Map artsAssGradeMap = new HashMap();
      String artsAssId;
      if (markList != null && markList.size() > 0) {
        it = markList.iterator();
        artsAssId = "";
        String currArtsAssId = "";
        HashMap gradeMap = null;

        while(it.hasNext()) {
          map = (Map)it.next();
          currArtsAssId = (String)map.get("arts_assignment_id");
          if (!artsAssId.equals(currArtsAssId)) {
            if (artsAssId.length() > 0) {
              artsAssGradeMap.put(artsAssId, gradeMap);
            }

            artsAssId = currArtsAssId;
            gradeMap = new HashMap();
          }

          grade = (String)map.get("grade");
          gradeMap.put(grade, map.get("cnt"));
        }

        artsAssGradeMap.put(currArtsAssId, gradeMap);
      }

      if (artsAssGradeMap.size() > 0) {
        result = new HashMap();
        it = artsAssGradeMap.keySet().iterator();

        while(it.hasNext()) {
          artsAssId = (String)it.next();
          map = (Map)artsAssGradeMap.get(artsAssId);
          String statisticPerStr = "";
          String statisticMarkStr = "";
          String statisticCumutiveStr = "";
          int cumulativePercentage = 0;
          int totCnt = 0;
          Iterator it2 = gradeList.iterator();

          int cnt;
          int pencentage;
          Object cntObj;
          float tmp;
          while(it2.hasNext()) {
            grade = (String)it2.next();
            cntObj = map.get(grade);
            if (cntObj != null) {
              cnt = (Integer)cntObj;
              totCnt += cnt;
              tmp = (float)cnt;
              pencentage = Math.round(tmp * 100.0F / (float)studentCnt);
              tmp = (float)totCnt;
              cumulativePercentage = Math.round(tmp * 100.0F / (float)studentCnt);
            } else {
              pencentage = 0;
              cnt = 0;
            }

            statisticPerStr = statisticPerStr + ";" + grade + ";" + pencentage;
            statisticMarkStr = statisticMarkStr + ";" + grade + ";" + cnt;
            BigDecimal conversion = (BigDecimal)conversionMap.get(grade);
            if (conversion.floatValue() < 0.0F) {
              statisticCumutiveStr = statisticCumutiveStr + ";" + grade + ";";
            } else {
              statisticCumutiveStr = statisticCumutiveStr + ";" + grade + ";" + cumulativePercentage;
            }
          }

          cntObj = map.get("");
          if (cntObj != null) {
            cnt = (Integer)cntObj;
            totCnt += cnt;
            tmp = (float)cnt;
            pencentage = Math.round(tmp * 100.0F / (float)studentCnt);
          } else {
            cnt = 0;
            pencentage = 0;
          }

          statisticPerStr = statisticPerStr + ";-;" + (studentCnt - totCnt) * 100 / studentCnt + ";*;" + pencentage;
          statisticPerStr = statisticPerStr.substring(1);
          statisticMarkStr = statisticMarkStr + ";-;" + (studentCnt - totCnt) + ";*;" + cnt;
          statisticMarkStr = statisticMarkStr.substring(1);
          statisticCumutiveStr = statisticCumutiveStr + ";-;;*;";
          statisticCumutiveStr = statisticCumutiveStr.substring(1);
          result.put(artsAssId + "_per", statisticPerStr);
          result.put(artsAssId + "_mark", statisticMarkStr);
          result.put(artsAssId + "_cumulative", statisticCumutiveStr);
        }
      }

      return result;
    } else {
      return null;
    }
  }

  public HSSFWorkbook downloadArtsMarks(Map parameterMap, String subjectGroupIds, String rsID) throws Student_remarks_tblException {
    String artsAssIds = "";
    TreeMap treeMap = new TreeMap();
    Object flag = ((String[])((String[])parameterMap.get("isPolyU")))[0];
    boolean isPolyU = flag.equals("1");
    HashMap markMap = new HashMap();
    Iterator it = parameterMap.keySet().iterator();

    String parameter;
    String id;
    String columnName;
    while(it.hasNext()) {
      parameter = (String)it.next();
      if (parameter.startsWith("artsAssId_")) {
        String idx = parameter.substring(10);
        id = ((String[])((String[])parameterMap.get(parameter)))[0];
        columnName = ((String[])((String[])parameterMap.get("columnName_" + idx)))[0];
        treeMap.put(new Integer(idx), id + "_" + columnName);
        artsAssIds = artsAssIds + "','" + id;
      }
    }

    if (treeMap.size() == 0) {
      return null;
    } else {
      if (artsAssIds.length() > 0) {
        artsAssIds = artsAssIds.substring(2) + "'";
      }

      String ids = subjectGroupIds.replaceAll(",", "','");
      ids = "'" + ids + "'";
      String sql = "select A.user_id, A.arts_assignment_id, B.marked_by_mark, A.mark, A.grade from arts_mark_entries_tbl A  join student_belong_group_tbl C on A.user_id = C.user_id join arts_assignment_tbl B on B.arts_assignment_id = A.arts_assignment_id join arts_group_tbl D on B.arts_group_id = D.arts_group_id and D.subject_group_id = C.subject_group_id where C.subject_group_id in (" + ids + ")  and C.belong = 1 ";
      List markList = this.jdbcTemplate.queryForList(sql);
      it = markList.iterator();

      Map map;
      while(it.hasNext()) {
        map = (Map)it.next();
        markMap.put(map.get("user_id") + "_" + map.get("arts_assignment_id") + "_mark", map.get("mark"));
        markMap.put(map.get("user_id") + "_" + map.get("arts_assignment_id") + "_grade", map.get("grade"));
      }

      if (!isPolyU) {
        sql = "select A.user_id, A.arts_assignment_id, B.marked_by_mark, A.mark, A.grade from arts_raw_mark_entries_tbl A  join student_belong_group_tbl C on A.user_id = C.user_id join arts_assignment_tbl B on B.arts_assignment_id = A.arts_assignment_id join arts_group_tbl D on B.arts_group_id = D.arts_group_id and D.subject_group_id = C.subject_group_id where C.subject_group_id in (" + ids + ")  and C.belong = 1 ";
        markList = this.jdbcTemplate.queryForList(sql);
        it = markList.iterator();

        while(it.hasNext()) {
          map = (Map)it.next();
          String key = map.get("user_id") + "_" + map.get("arts_assignment_id");
          Object tmp = map.get("mark");
          if (!markMap.containsKey(key + "_mark") && tmp != null) {
            markMap.put(key + "_mark", tmp);
          }

          tmp = map.get("grade");
          if (!markMap.containsKey(key + "_grade") && tmp != null) {
            markMap.put(key + "_grade", tmp);
          }
        }
      }

      sql = "select A.user_id, A.name_eng, A.name_chi, B.student_no  from user_tbl A join student_tbl B on A.user_id = B.user_id  join student_belong_group_tbl C  on A.user_id = C.user_id  where C.subject_group_id in (" + ids + ")" + " and C.belong = 1 " + " and A.account_status = 1 " + " order by B.student_no";
      List studentList = this.jdbcTemplate.queryForList(sql);
      String[] tmp = subjectGroupIds.split(",");
      Object[] param = new Object[]{tmp[0]};
      sql = "select A.subject_title_eng, A.subject_code, B.subject_group_no from subject_tbl A join subject_group_tbl B on A.subject_id = B.subject_id where B.subject_group_id = ? ";
      Map subjectInfoMap = this.jdbcTemplate.queryForMap(sql, param);
      Map studentRemarkMap = this.studentRemarksService.getStudentRemarksMap_subjectOnly(subjectGroupIds);
      HSSFWorkbook workbook = new HSSFWorkbook();
      HSSFSheet sheet = workbook.createSheet();
      short next_line_no = -1;
      next_line_no = (short)(next_line_no + 1);
      HSSFRow row = sheet.createRow(next_line_no);
      HSSFCell cell = row.createCell((short)0);
      cell.setCellType(1);
      cell.setCellValue("Student Subject Result");
      ++next_line_no;
      row = sheet.createRow(next_line_no);
      cell = row.createCell((short)0);
      cell.setCellType(1);
      cell.setEncoding((short)1);
      cell.setCellValue("Time: " + DateFormat.getDateTimeInstance().format(new Date()));
      ++next_line_no;
      row = sheet.createRow(next_line_no);
      cell = row.createCell((short)0);
      cell.setCellType(1);
      cell.setEncoding((short)1);
      cell.setCellValue("Subject: " + subjectInfoMap.get("subject_code") + " " + subjectInfoMap.get("subject_title_eng"));
      ++next_line_no;
      row = sheet.createRow(next_line_no);
      cell = row.createCell((short)0);
      cell.setCellType(1);
      cell.setCellValue("Group: " + subjectInfoMap.get("subject_group_no"));
      ++next_line_no;
      row = sheet.createRow(next_line_no);
      cell = row.createCell((short)0);
      cell.setCellType(1);
      cell.setCellValue("Status: Not Confirmed");
      ++next_line_no;
      sheet.createRow(next_line_no);
      ++next_line_no;
      row = sheet.createRow(next_line_no);
      short colIdx = 0;
      colIdx = (short)(colIdx + 1);
      cell = row.createCell(colIdx);
      cell.setCellType(1);
      cell.setCellValue("Seq No");
      if (isPolyU) {
        cell = row.createCell(colIdx++);
        cell.setCellType(1);
        cell.setCellValue("Result Sheet ID");
      }

      cell = row.createCell(colIdx++);
      cell.setCellType(1);
      cell.setCellValue("Student Name");
      if (!isPolyU) {
        cell = row.createCell(colIdx++);
        cell.setCellType(1);
        cell.setCellValue("Student Name Chi");
      }

      cell = row.createCell(colIdx++);
      cell.setCellType(1);
      cell.setCellValue("Student No.");
      if (isPolyU) {
        cell = row.createCell(colIdx++);
        cell.setCellType(1);
        cell.setCellValue("RS");
      }

      it = treeMap.values().iterator();
      if (isPolyU) {
        while(it.hasNext()) {
          parameter = (String)it.next();
          columnName = parameter.substring(parameter.indexOf("_") + 1);
          cell = row.createCell(colIdx++);
          cell.setCellType(1);
          cell.setEncoding((short)1);
          cell.setCellValue(columnName);
        }
      } else {
        while(it.hasNext()) {
          parameter = (String)it.next();
          columnName = parameter.substring(parameter.indexOf("_") + 1);
          cell = row.createCell(colIdx++);
          cell.setCellType(1);
          cell.setEncoding((short)1);
          cell.setCellValue(columnName);
          cell = row.createCell(colIdx++);
          cell.setCellType(1);
          cell.setEncoding((short)1);
          cell.setCellValue(columnName + "_grade");
        }
      }

      if (!isPolyU) {
        cell = row.createCell(colIdx++);
        cell.setCellType(1);
        cell.setEncoding((short)1);
        cell.setCellValue("Remarks");
      }

      int rowIdx = 0;
      it = studentList.iterator();
      String rs = rsID.toUpperCase();

      while(true) {
        while(it.hasNext()) {
          colIdx = 0;
          ++rowIdx;
          map = (Map)it.next();
          ++next_line_no;
          row = sheet.createRow(next_line_no);
          colIdx = (short)(colIdx + 1);
          cell = row.createCell(colIdx);
          cell.setCellType(0);
          cell.setCellValue((double)rowIdx);
          if (isPolyU) {
            cell = row.createCell(colIdx++);
            cell.setCellType(1);
            cell.setCellValue(rs);
          }

          cell = row.createCell(colIdx++);
          cell.setCellType(1);
          cell.setCellValue((String)map.get("name_eng"));
          if (!isPolyU) {
            cell = row.createCell(colIdx++);
            cell.setCellType(1);
            cell.setEncoding((short)1);
            cell.setCellValue((String)map.get("name_chi"));
          }

          cell = row.createCell(colIdx++);
          cell.setCellType(1);
          cell.setCellValue((String)map.get("student_no"));
          if (isPolyU) {
            cell = row.createCell(colIdx++);
            cell.setCellType(1);
            cell.setCellValue("1");
          }

          Iterator it2 = treeMap.values().iterator();
          if (isPolyU) {
            while(it2.hasNext()) {
              parameter = (String)it2.next();
              id = parameter.substring(0, parameter.indexOf("_"));
              cell = row.createCell(colIdx++);
              cell.setCellType(1);
              cell.setCellValue((String)markMap.get(map.get("user_id") + "_" + id + "_grade"));
            }
          } else {
            while(it2.hasNext()) {
              parameter = (String)it2.next();
              id = parameter.substring(0, parameter.indexOf("_"));
              cell = row.createCell(colIdx++);
              Object mark = markMap.get(map.get("user_id") + "_" + id + "_mark");
              if (mark != null) {
                if (mark.equals(new BigDecimal(Constants.EXEMPT_MARK))) {
                  cell.setCellType(1);
                  cell.setCellValue(Constants.EXEMPT_STR);
                } else {
                  cell.setCellType(0);
                  cell.setCellValue(((BigDecimal)mark).doubleValue());
                }
              } else {
                cell.setCellType(3);
              }

              cell = row.createCell(colIdx++);
              cell.setCellType(1);
              cell.setEncoding((short)1);
              String grade = (String)markMap.get(map.get("user_id") + "_" + id + "_grade");
              cell.setCellValue(grade);
            }

            if (!isPolyU) {
              cell = row.createCell(colIdx++);
              cell.setCellType(1);
              cell.setEncoding((short)1);
              List remarkList = (List)studentRemarkMap.get(map.get("user_id"));
              String remarks = "";
              if (remarkList != null && remarkList.size() > 0) {
                Iterator it1 = remarkList.iterator();

                StringBuilder var10000;
                Remarks remark;
                for(int i = 0; it1.hasNext(); remarks = var10000.append(i).append(")").append(remark.getRemarks()).append("  ").toString()) {
                  remark = (Remarks)it1.next();
                  var10000 = (new StringBuilder()).append(remarks);
                  ++i;
                }
              }

              cell.setCellValue(remarks);
            }
          }
        }

        return workbook;
      }
    }
  }

  public HSSFWorkbook marksReport(String programmeId, String userId, String academicYear, String semester) throws Arts_assignment_viewDTOException, Student_remarks_tblException, Student_enroll_programme_tblException {
    String findSubjectGroups = "select distinct ag.arts_group_id, sg.subject_group_id, s.subject_code from programme_tbl p join student_enroll_programme_tbl sep on p.programme_id = sep.programme_id join student_belong_group_tbl sbg on sbg.user_id = sep.user_id join subject_group_tbl sg  on sg.subject_group_id = sbg.subject_group_id  join subject_tbl s  on s.subject_id = sg.subject_id  join arts_group_tbl ag  on ag.subject_group_id = sg.subject_group_id  where p.programme_id = ?  and s.academic_year = ? and s.semester = ?  order by s.subject_code ";
    String findSubjectGroupStudents = "select A.user_id from student_enroll_programme_tbl A join student_belong_group_tbl D on D.user_id = A.user_id where A.programme_id = ? and D.subject_group_id = ? and D.belong = 1";
    String findStudents = "select A.user_id, F.student_no, E.name_chi, E.name_eng, G.late_for_class, G.early_leave, G.absence from  student_enroll_programme_tbl A join programme_tbl B  on A.programme_id  = B.programme_id join user_tbl E on E.user_id = A.user_id join student_tbl F on F.user_id = E.user_id left join student_attendence_tbl G on G.user_id = A.user_id and G.programme_id = A.programme_id   and G.semester= ? and G.academic_year = ? where A.programme_id = ? ";
    String findProgrammeInfo = "select programme_code from programme_tbl where programme_id = ? ";
    List subjectGroupIds = new ArrayList();
    new ArrayList();
    String studentIdStr = "";
    String subjectGroupIdStr = "";
    Map markMap = new HashMap();
    Object[] params = new Object[]{programmeId};
    String programmeCode = (String)this.jdbcTemplate.queryForObject(findProgrammeInfo, params, String.class);
    this.logger.info("studentRemarksService is null? " + (this.studentRemarksService == null));
    ProgrammeSubjectGroup programmeSubjectGroup = new ProgrammeSubjectGroup();
    programmeSubjectGroup.setProgrammeId(programmeId);
    programmeSubjectGroup.setSemester(semester);
    programmeSubjectGroup.setAcademicYear(academicYear);
    Map studentRemarkMap = this.studentRemarksService.getStudentRemarksMap_programmeOnly(programmeSubjectGroup);
    params = new Object[]{semester, academicYear, programmeId};
    List studentList = this.jdbcTemplate.queryForList(findStudents, params);
    if (studentList != null && studentList.size() > 0) {
      Map map;
      for(Iterator it = studentList.iterator(); it.hasNext(); studentIdStr = studentIdStr + ",'" + (String)map.get("user_id") + "'") {
        map = (Map)it.next();
      }

      studentIdStr = studentIdStr.substring(1);
    }

    params = new Object[]{programmeId, academicYear, semester};
    List list = this.jdbcTemplate.queryForList(findSubjectGroups, params);
    if (list != null && list.size() > 0) {
      Iterator it = list.iterator();
      HashMap artsGroupMap = new HashMap();

      Map map;
      String subjectGroupId;
      while(it.hasNext()) {
        map = (Map)it.next();
        subjectGroupId = (String)map.get("subject_group_id");
        subjectGroupIdStr = subjectGroupIdStr + ",'" + subjectGroupId + "'";
        subjectGroupIds.add(subjectGroupId);
        artsGroupMap.put(subjectGroupId, (String)map.get("arts_group_id"));
      }

      subjectGroupIdStr = subjectGroupIdStr.substring(1);
      Map allAssignments = this.artsAssignmentService.getArtsAssignments(subjectGroupIds);
      it = subjectGroupIds.iterator();

      Iterator it2;
      String groupId;
      try {
        while(it.hasNext()) {
          subjectGroupId = (String)it.next();
          if (this.artsLockService.needCalculateMarkBySubjectGroup((String)artsGroupMap.get(subjectGroupId))) {
            params = new Object[]{programmeId, subjectGroupId};
            it2 = this.jdbcTemplate.queryForList(findSubjectGroupStudents, params).iterator();
            ArrayList subjectGroupStudentList = new ArrayList();

            while(it2.hasNext()) {
              map = (Map)it2.next();
              subjectGroupStudentList.add(new String[]{(String)map.get("user_id")});
            }

            groupId = this.artsAssignmentService.getGroupIdFromSubjectGroupId(subjectGroupId);
            this.calculateMarks(groupId, (String)null, subjectGroupStudentList, (Map)allAssignments.get(subjectGroupId), userId);
          }
        }
      } catch (Arts_lock_tblException var45) {
        var45.printStackTrace();
      } catch (Exception var46) {
        var46.printStackTrace();
      }

      groupId = "select A.user_id, A.arts_assignment_id, A.mark, A.grade from arts_mark_entries_tbl A  join arts_assignment_tbl B  on A.arts_assignment_id = B.arts_assignment_id  join arts_group_tbl E on E.arts_group_id = B.arts_group_id  join student_belong_group_tbl c on C.user_id = A.user_id and E.subject_group_id = C.subject_group_id  where E.subject_group_id in (" + subjectGroupIdStr + ") " + " and A.user_id in (" + studentIdStr + ")";
      List markList = this.jdbcTemplate.queryForList(groupId);
      it2 = markList.iterator();

      while(it2.hasNext()) {
        map = (Map)it2.next();
        markMap.put((String)map.get("user_id") + "_" + (String)map.get("arts_assignment_id"), new Object[]{map.get("mark"), map.get("grade")});
      }

      HSSFWorkbook workbook = new HSSFWorkbook();
      HSSFSheet sheet = workbook.createSheet();
      short next_line_no = -1;
      short next_column_no = -1;
      next_line_no = (short)(next_line_no + 1);
      HSSFRow row = sheet.createRow(next_line_no);
      next_column_no = (short)(next_column_no + 1);
      HSSFCell cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setCellValue("semester");
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setCellValue("registerid");
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setCellValue("class");
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setCellValue("class_no");
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setCellValue("eng_name");
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setCellValue("chi_name");
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setCellValue("late_for_class");
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setCellValue("early_leave");
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setCellValue("absence");
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setCellValue("remarks");
      it = subjectGroupIds.iterator();

      List assignmentIdList;
      Arts_assignment_viewTO ass;
      while(it.hasNext()) {
        map = (Map)allAssignments.get((String)it.next());
        assignmentIdList = ArtsAssignmentUtil.getPreOrder2LvAssignments(map);
        it2 = assignmentIdList.iterator();

        while(it2.hasNext()) {
          String id = (String)it2.next();
          ass = (Arts_assignment_viewTO)map.get(id);
          ++next_column_no;
          cell = row.createCell(next_column_no);
          cell.setCellType(1);
          cell.setEncoding((short)1);
          cell.setCellValue(ass.getName());
          ++next_column_no;
          cell = row.createCell(next_column_no);
          cell.setCellType(1);
          cell.setEncoding((short)1);
          cell.setCellValue(ass.getName() + "_grade");
        }
      }

      it = studentList.iterator();

      while(it.hasNext()) {
        Map studentMap = (Map)it.next();
        ++next_line_no;
        row = sheet.createRow(next_line_no);
        next_column_no = -1;
        next_column_no = (short)(next_column_no + 1);
        cell = row.createCell(next_column_no);
        cell.setCellType(1);
        cell.setCellValue(academicYear + " " + semester);
        ++next_column_no;
        cell = row.createCell(next_column_no);
        cell.setCellType(1);
        cell.setCellValue((String)studentMap.get("student_no"));
        ++next_column_no;
        cell = row.createCell(next_column_no);
        cell.setCellType(1);
        cell.setCellValue(programmeCode);
        Integer seatNo = (Integer)studentMap.get("seat_no");
        ++next_column_no;
        cell = row.createCell(next_column_no);
        if (seatNo != null) {
          cell.setCellType(0);
          cell.setCellValue((double)seatNo);
        } else {
          cell.setCellType(3);
        }

        ++next_column_no;
        cell = row.createCell(next_column_no);
        cell.setCellType(1);
        cell.setCellValue((String)studentMap.get("name_eng"));
        ++next_column_no;
        cell = row.createCell(next_column_no);
        cell.setCellType(1);
        cell.setEncoding((short)1);
        cell.setCellValue((String)studentMap.get("name_chi"));
        BigDecimal bigDec = (BigDecimal)studentMap.get("late_for_class");
        ++next_column_no;
        cell = row.createCell(next_column_no);
        cell.setCellType(0);
        cell.setCellValue(bigDec == null ? 0.0D : bigDec.doubleValue());
        bigDec = (BigDecimal)studentMap.get("early_leave");
        ++next_column_no;
        cell = row.createCell(next_column_no);
        cell.setCellType(0);
        cell.setCellValue(bigDec == null ? 0.0D : bigDec.doubleValue());
        bigDec = (BigDecimal)studentMap.get("absence");
        ++next_column_no;
        cell = row.createCell(next_column_no);
        cell.setCellType(0);
        cell.setCellValue(bigDec == null ? 0.0D : bigDec.doubleValue());
        List remarkList = (List)studentRemarkMap.get(studentMap.get("user_id"));
        String remarks = "";
        if (remarkList != null && remarkList.size() > 0) {
          Iterator it1 = remarkList.iterator();

          StringBuilder var10000;
          Remarks remark;
          for(int i = 0; it1.hasNext(); remarks = var10000.append(i).append(")").append(remark.getRemarks()).append("  ").toString()) {
            remark = (Remarks)it1.next();
            var10000 = (new StringBuilder()).append(remarks);
            ++i;
          }
        }

        ++next_column_no;
        cell = row.createCell(next_column_no);
        cell.setCellType(1);
        cell.setEncoding((short)1);
        cell.setCellValue(remarks);
        it2 = subjectGroupIds.iterator();

        while(it2.hasNext()) {
          map = (Map)allAssignments.get((String)it2.next());
          assignmentIdList = ArtsAssignmentUtil.getPreOrder2LvAssignments(map);
          Iterator it3 = assignmentIdList.iterator();

          while(it3.hasNext()) {
            ass = (Arts_assignment_viewTO)map.get(it3.next());
            Object[] obj = (Object[])((Object[])markMap.get((String)studentMap.get("user_id") + "_" + ass.getArts_assignment_id()));
            if (obj == null) {
              ++next_column_no;
              cell = row.createCell(next_column_no);
              cell.setCellType(3);
              ++next_column_no;
              cell = row.createCell(next_column_no);
              cell.setCellType(3);
            } else {
              ++next_column_no;
              cell = row.createCell(next_column_no);
              if (obj[0] != null) {
                if (obj[0].equals(new BigDecimal(Constants.EXEMPT_MARK))) {
                  cell.setCellType(1);
                  cell.setCellValue(Constants.EXEMPT_STR);
                } else {
                  cell.setCellType(0);
                  cell.setCellValue((double)((BigDecimal)obj[0]).floatValue());
                }
              } else {
                cell.setCellType(3);
              }

              ++next_column_no;
              cell = row.createCell(next_column_no);
              if (obj[1] != null) {
                cell.setCellType(1);
                cell.setCellValue((String)obj[1]);
              } else {
                cell.setCellType(3);
              }
            }
          }
        }
      }

      return workbook;
    } else {
      return null;
    }
  }

  public Map uploadArtsMarks(String artsGroupId, String subjectGroupId, String assignmentId, File dataFile, String userId, String role) throws Arts_assignment_viewDTOException, CpceException {
    Map errorMap = new TreeMap();
    Map allStudentInfoMap = new HashMap();
    List validSubjectGroupList = new ArrayList();
    StringBuffer changeItems = new StringBuffer();
    Map lineNoMap = new HashMap();
    boolean isFromAssignment = assignmentId != null && assignmentId.length() > 0;
    Map assignments;
    logger.info("isFromAssignment: " + isFromAssignment);
    logger.info("subjectGroupId: " + subjectGroupId);
    logger.info("assignmentId: " + assignmentId);
    if (!isFromAssignment) {
      assignments = this.artsAssignmentService.getArtsAssignmentsBySubjectGroup(subjectGroupId);
    } else {
      assignments = this.artsAssignmentService.getArtsAssignmentsByAssignment(assignmentId);
    }

    String getStudents = "select C.student_no, C.user_id, A.subject_group_id  from student_belong_group_tbl A join  arts_group_tbl B on A.subject_group_id = B.subject_group_id  join student_tbl C on A.user_id = C.user_id  where B.arts_group_id = ? ";
    String getSubjectGroup = "select A.subject_group_id from teacher_manage_group_tbl A  join arts_group_tbl B on A.subject_group_id = B.subject_group_id  where A.user_id = ? and B.arts_group_id = ? ";
    if (assignments != null && assignments.size() != 0) {
      List leafAssignmentsIds = ArtsAssignmentUtil.getLeafArtsAssignments(assignments);
      Iterator it;
      List list;
      Map map;
      if (role.equals(Constants.TEACHER)) {
        list = this.jdbcTemplate.queryForList(getSubjectGroup, new Object[]{userId, artsGroupId});
        if (list != null && list.size() > 0) {
          it = list.iterator();

          while(it.hasNext()) {
            map = (Map)it.next();
            validSubjectGroupList.add((String)map.get("subject_group_id"));
          }
        }
      }

      list = this.jdbcTemplate.queryForList(getStudents, new Object[]{artsGroupId});
      String studentId;
      String studentNo;
      if (list != null && list.size() > 0) {
        it = list.iterator();

        while(it.hasNext()) {
          map = (Map)it.next();
          studentNo = (String)map.get("student_no");
          studentId = (String)map.get("user_id");
          String sgId = (String)map.get("subject_group_id");
          allStudentInfoMap.put(studentId, new String[]{studentNo, sgId});
        }
      }

      map = null;

      HSSFWorkbook workbook;
      try {
        workbook = new HSSFWorkbook(new FileInputStream(dataFile));
      } catch (IOException var40) {
        this.logger.log(Level.SEVERE, "not a proper Excel file:" + dataFile.getName());
        throw new CpceException(2003, "cpce.exceptionCode.excelFileOnly");
      }

      HSSFSheet sheet = workbook.getSheetAt(0);
      HSSFRow row = sheet.getRow(1);
      HSSFCell cell = row.getCell((short)1);
      String groupId = this.readStringCellValue(cell);
      if (!artsGroupId.equals(groupId)) {
        throw new CpceException(2003, "cpce.exceptionCode.wrongSubjectGroup");
      } else {
        List artsAssignmentIds = new ArrayList();
        row = sheet.getRow(3);
        short currentCellNo = 2;
        currentCellNo = (short)(currentCellNo + 1);

        String artsAssId;
        for(cell = row.getCell(currentCellNo); cell != null; cell = row.getCell(currentCellNo)) {
          artsAssId = this.readStringCellValue(cell);
          if (!leafAssignmentsIds.contains(artsAssId)) {
            logger.info("wrongAssignment error, leafAssignmentsIds: " + ToStringHelper.toString(leafAssignmentsIds) + ", " + "artsAssId: " + artsAssId);
            throw new CpceException(2003, "cpce.exceptionCode.wrongAssignment");
          }

          artsAssignmentIds.add(artsAssId);
          ++currentCellNo;
        }

        Map marks = new HashMap();
        short currentRowNo = 3;
        currentRowNo = (short)(currentRowNo + 1);

        for(row = sheet.getRow(currentRowNo); row != null; row = sheet.getRow(currentRowNo)) {
          cell = row.getCell((short)0);
          studentNo = this.readStringCellValue(cell);
          cell = row.getCell((short)2);
          studentId = this.readStringCellValue(cell);
          currentCellNo = 2;
          String[] tmp = (String[])((String[])allStudentInfoMap.get(studentId));
          if (tmp != null) {
            if (!tmp[0].equals(studentNo)) {
              errorMap.put(new Integer(currentRowNo + 1), "cpce.exceptonCode.wrongStudentNo");
            } else if (!validSubjectGroupList.contains(tmp[1])) {
              errorMap.put(new Integer(currentRowNo + 1), "cpce.exceptionCode.noAuthority");
            } else {
              it = artsAssignmentIds.iterator();
              currentCellNo = 2;

              while(it.hasNext()) {
                artsAssId = (String)it.next();
                ++currentCellNo;
                cell = row.getCell(currentCellNo);
                String mark = "";
                if (cell != null) {
                  switch(cell.getCellType()) {
                    case 0:
                      mark = String.valueOf(cell.getNumericCellValue());
                      if (mark.endsWith(".0")) {
                        mark = mark.substring(0, mark.lastIndexOf("."));
                      }
                      break;
                    case 1:
                      mark = cell.getStringCellValue().trim();
                      break;
                    case 2:
                    default:
                      mark = "";
                      break;
                    case 3:
                      mark = "";
                  }
                }

                marks.put(studentId + "_" + artsAssId, new String[]{mark});
                changeItems.append("," + studentId + "_" + artsAssId);
                lineNoMap.put(studentId, new Integer(currentRowNo + 1));
              }
            }
          } else {
            errorMap.put(new Integer(currentRowNo + 1), "cpce.exceptionCode.noSuchStudent");
          }

          ++currentRowNo;
        }

        boolean updateDB = true;
        if (errorMap.size() > 0) {
          updateDB = false;
        }

        Map error = this.updateMarks(subjectGroupId, marks, userId, changeItems.toString(), updateDB);
        if (error.size() > 0) {
          errorMap.put(new Integer(0), "-1");
          it = error.keySet().iterator();

          while(it.hasNext()) {
            String id = (String)it.next();
            int idx = id.indexOf("_");
            studentId = id.substring(0, idx);
            artsAssId = id.substring(idx + 1);
            Integer line = (Integer)lineNoMap.get(studentId);
            Object errorObj = errorMap.get(line);
            String errorStr;
            if (errorObj == null) {
              errorStr = "";
            } else {
              errorStr = (String)errorObj;
            }

            errorStr = errorStr + artsAssId + ":" + error.get(id) + ";";
            errorMap.put(line, errorStr);
          }
        } else if (updateDB) {
          errorMap.put(new Integer(0), "1");
        } else {
          errorMap.put(new Integer(0), "-1");
        }

        return errorMap;
      }
    } else {
      logger.info("wrongAssignment error, assignments: " + ToStringHelper.toString(assignments));
      throw new CpceException(2003, "cpce.exceptionCode.wrongAssignment");
    }
  }

  public HSSFWorkbook downloadArtsMarkTemplate(String subjectGroupId, String assignmentId, String subjectGroupIds, String groupId, List students, List artsAssIds) throws Arts_assignment_viewDTOException {
    boolean isFromAssignment = assignmentId != null && assignmentId.length() > 0;
    Map assignments;
    if (!isFromAssignment) {
      assignments = this.artsAssignmentService.getArtsAssignmentsBySubjectGroup(subjectGroupId);
    } else {
      assignments = this.artsAssignmentService.getArtsAssignmentsByAssignment(assignmentId);
    }

    List leafAssignments = ArtsAssignmentUtil.getLeafArtsAssignments(assignments);
    Iterator it = leafAssignments.iterator();

    while(it.hasNext()) {
      if (!artsAssIds.contains(it.next())) {
        it.remove();
      }
    }

    Map studentMarks = this.getMarks(students, leafAssignments, assignments);
    String sql = "select A.subject_title_eng, A.subject_code, B.subject_group_no from subject_tbl A join subject_group_tbl B on A.subject_id = B.subject_id where B.subject_group_id = ? ";
    Map map = this.jdbcTemplate.queryForMap(sql, new Object[]{subjectGroupId});
    String subjectTitle = (String)map.get("subject_title_eng");
    String subjectCode = (String)map.get("subject_code");
    HSSFWorkbook workbook = new HSSFWorkbook();
    HSSFSheet sheet = workbook.createSheet();
    short next_line_no = -1;
    short next_column_no = -1;
    next_line_no = (short)(next_line_no + 1);
    HSSFRow row = sheet.createRow(next_line_no);
    next_column_no = (short)(next_column_no + 1);
    HSSFCell cell = row.createCell(next_column_no);
    cell.setCellType(1);
    cell.setCellValue("Subject");
    ++next_column_no;
    cell = row.createCell(next_column_no);
    cell.setCellType(1);
    cell.setEncoding((short)1);
    cell.setCellValue(subjectCode + " " + subjectTitle);
    ++next_line_no;
    row = sheet.createRow(next_line_no);
    next_column_no = -1;
    next_column_no = (short)(next_column_no + 1);
    cell = row.createCell(next_column_no);
    cell.setCellType(1);
    cell.setCellValue("Group");
    ++next_column_no;
    cell = row.createCell(next_column_no);
    cell.setCellType(1);
    cell.setCellValue(groupId);
    next_column_no = 2;
    ++next_line_no;
    row = sheet.createRow(next_line_no);
    it = leafAssignments.iterator();

    String artsAssId;
    while(it.hasNext()) {
      artsAssId = (String)it.next();
      Arts_assignment_viewTO arts_assignment_view = (Arts_assignment_viewTO)assignments.get(artsAssId);
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setEncoding((short)1);
      cell.setCellValue(arts_assignment_view.getName());
    }

    next_column_no = 2;
    ++next_line_no;
    row = sheet.createRow(next_line_no);
    it = leafAssignments.iterator();

    while(it.hasNext()) {
      artsAssId = (String)it.next();
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setCellValue(artsAssId);
    }

    it = students.iterator();

    while(it.hasNext()) {
      ++next_line_no;
      row = sheet.createRow(next_line_no);
      next_column_no = -1;
      String[] tmp = (String[])((String[])it.next());
      next_column_no = (short)(next_column_no + 1);
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setCellValue(tmp[2]);
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setEncoding((short)1);
      cell.setCellValue(tmp[1]);
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setCellValue(tmp[0]);

      String mark;
      for(Iterator it2 = leafAssignments.iterator(); it2.hasNext(); cell.setCellValue(mark)) {
        artsAssId = (String)it2.next();
        ++next_column_no;
        cell = row.createCell(next_column_no);
        cell.setCellType(1);
        mark = (String)studentMarks.get(tmp[0] + "_" + artsAssId);
        if (Constants.EXEMPT_MARK.equals(mark)) {
          mark = Constants.EXEMPT_STR;
        }
      }
    }

    return workbook;
  }

  private String readStringCellValue(HSSFCell cell) throws CpceException {
    if (cell != null && cell.getCellType() == 1) {
      return cell.getStringCellValue().trim();
    } else if (cell != null && cell.getCellType() == 3) {
      return "";
    } else {
      throw new CpceException(2002, "cpce.exceptionCode.dataFormatError");
    }
  }

  private String findGradeFromMark(Map gradeMap, String artsAssId, Object mark, int maxMark) {
    Map subGradeMap = null;
    String grade = null;
    if (gradeMap.containsKey(artsAssId)) {
      subGradeMap = (Map)gradeMap.get(artsAssId);
    } else if (gradeMap.containsKey("")) {
      subGradeMap = (Map)gradeMap.get("");
    }

    if (subGradeMap != null) {
      Set keys = subGradeMap.keySet();
      Iterator it2 = keys.iterator();
      boolean found = false;
      if (mark != null) {
        float markFloat = ((BigDecimal)mark).floatValue();
        float from = 0.0F;

        Object fromObj;
        String lastGrade;
        for(lastGrade = null; it2.hasNext(); lastGrade = (String)subGradeMap.get(fromObj)) {
          fromObj = it2.next();
          from = ((BigDecimal)fromObj).floatValue();
          if (markFloat < from) {
            if (found) {
              grade = lastGrade;
            } else {
              grade = (String)subGradeMap.get(fromObj);
            }
            break;
          }

          found = true;
        }

        if (found && grade == null) {
          grade = lastGrade;
        }
      }
    }

    return grade;
  }

  public void calculateMarksByAssignment(String assignmentId, List studentList, Map assignments, String userId) throws Exception {
    this.calculateMarks((String)null, assignmentId, studentList, assignments, userId);
  }

  public void calculateMarksBySubjectGroup(String groupId, List studentList, Map assignments, String userId) throws Exception {
    this.calculateMarks(groupId, (String)null, studentList, assignments, userId);
  }

  public Map sendEmailArtsMark(String subjectGroupIds, String artsAssignmentIds, Map assignments, String userId, Locale locale) throws CpceException, Arts_assignment_viewDTOException, Email_tblException {
    if (subjectGroupIds != null && subjectGroupIds.length() != 0) {
      if (artsAssignmentIds != null && artsAssignmentIds.length() != 0) {
        String sgIds = subjectGroupIds.replaceAll(",", "','");
        sgIds = "'" + sgIds + "'";
        String artsIds = artsAssignmentIds.replaceAll(",", "','");
        artsIds = "'" + artsIds + "'";
        String sqlStudent = "select A.email, A.name_chi, A.name_eng, A.user_id, C.student_no, B.subject_group_id from user_tbl A join student_belong_group_tbl B on A.user_id = B.user_id join student_tbl C on C.user_id = A.user_id where B.subject_group_id in (" + sgIds + ") and A.account_status = 1  and B.belong='1'  order by C.student_no ";
        String sqlTeacher = "select A.user_id, A.email, A.name_chi, A.name_eng , B.subject_group_id  from user_tbl A join teacher_manage_group_tbl B  on A.user_id = B.user_id  where B.subject_group_id in (" + sgIds + ") and A.account_status = 1 ";
        String sqlMarks = "select B.user_id, A.arts_assignment_id, A.mark, A.grade, A.gpa from arts_mark_entries_tbl  A join student_belong_group_tbl B on A.user_id = B.user_id  where B.subject_group_id in (" + sgIds + ") and  A.arts_assignment_id in (" + artsIds + ")";
        String sqlRawMarks = "select B.user_id, A.arts_assignment_id, A.mark, A.grade, A.gpa from arts_raw_mark_entries_tbl  A join student_belong_group_tbl B on A.user_id = B.user_id  where B.subject_group_id in (" + sgIds + ") and  A.arts_assignment_id in (" + artsIds + ")";
        String sqlCountArtsGroup = "select count(arts_group_id) cnt from arts_group_tbl where subject_group_id in (" + sgIds + ")";
        String sqlAdminEmail = "select A.email, A.user_id, A.name_eng, A.name_chi from user_tbl A join school_admin_tbl B   on A.user_id = b.user_id  and (B.institute_id is null or exists (  select 1 from subject_tbl C join subject_group_tbl D   on C.subject_id = D.subject_id where  D.subject_group_id = ? and B.institute_id = C.institute_id )) and A.account_status = 1";
        String sqlSubjectInfo = "select A.subject_code, A.subject_title_chi, A.subject_title_eng, B.subject_group_no, B.subject_group_id  from subject_tbl A join subject_group_tbl B on A.subject_id = B.subject_id  where B.subject_group_id in (" + sgIds + ")";
        List studentList = this.jdbcTemplate.queryForList(sqlStudent);
        List teacherList = this.jdbcTemplate.queryForList(sqlTeacher);
        List list = this.jdbcTemplate.queryForList(sqlMarks);
        Map studentMarkMap = new HashMap();
        Map map;
        Object mark;
        String markStr;
        if (list != null && list.size() > 0) {
          for(Iterator it = list.iterator(); it.hasNext(); studentMarkMap.put(map.get("user_id") + "_" + map.get("arts_assignment_id"), markStr)) {
            map = (Map)it.next();
            mark = map.get("mark");
            mark = map.get("grade");
            markStr = "";
            if (mark != null) {
              markStr = mark.toString();
            }

            if (mark != null) {
              if (mark != null) {
                markStr = markStr + " ";
              }

              markStr = markStr + mark;
            }
          }
        }

        list = this.jdbcTemplate.queryForList(sqlRawMarks);
        Map studentRawMarkMap = new HashMap();
        String fromEmail;
        if (list != null && list.size() > 0) {
          for(Iterator it = list.iterator(); it.hasNext(); studentRawMarkMap.put(map.get("user_id") + "_" + map.get("arts_assignment_id"), fromEmail)) {
            map = (Map)it.next();
            mark = map.get("mark");
            Object grade = map.get("grade");
            fromEmail = "";
            if (mark != null) {
              fromEmail = mark.toString();
            }

            if (grade != null) {
              if (mark != null) {
                fromEmail = fromEmail + " ";
              }

              fromEmail = fromEmail + grade;
            }
          }
        }

        int cnt = this.jdbcTemplate.queryForInt(sqlCountArtsGroup);
        if (cnt != 0 && cnt <= 1) {
          mark = null;
          int idx = subjectGroupIds.indexOf(",");
          String subjectGroupId;
          if (idx == -1) {
            subjectGroupId = subjectGroupIds;
          } else {
            subjectGroupId = subjectGroupIds.substring(0, subjectGroupIds.indexOf(","));
          }

          List postOrderArtsList = ArtsAssignmentUtil.postOrderAssignments(ArtsAssignmentUtil.getArtsAssignmentRoot(assignments), assignments);
          fromEmail = null;
          String fromName = null;
          list = this.jdbcTemplate.queryForList(sqlAdminEmail, new Object[]{subjectGroupId});
          String[] schoolAdminEmails = null;
          Iterator it;
          int i;
          if (list != null && list.size() > 0) {
            schoolAdminEmails = new String[list.size()];
            it = list.iterator();

            for(i = 0; it.hasNext(); ++i) {
              map = (Map)it.next();
              schoolAdminEmails[i] = (String)map.get("email");
              if (fromEmail == null && userId.equals(map.get("user_id"))) {
                fromEmail = (String)map.get("email");
                fromName = (String)map.get("name_chi");
              }
            }
          }

          it = null;
          if (teacherList.size() > 0) {
            i = 0;
            String[] teacherEmails = new String[teacherList.size()];

            for(it = teacherList.iterator(); it.hasNext(); ++i) {
              map = (Map)it.next();
              teacherEmails[i] = (String)map.get("email");
              if (fromEmail == null && userId.equals(map.get("user_id"))) {
                fromEmail = (String)map.get("email");
                fromName = (String)map.get("name_chi");
              }
            }
          }

          list = this.jdbcTemplate.queryForList(sqlSubjectInfo);
          Map subjectGroupMap = new HashMap();
          String subjectCode = null;
          String subjectName = null;
          if (list.size() > 0) {
            for(it = list.iterator(); it.hasNext(); subjectGroupMap.put((String)map.get("subject_group_id"), (String)map.get("subject_group_no"))) {
              map = (Map)it.next();
              if (subjectCode == null) {
                subjectCode = (String)map.get("subject_code");
                if (locale.getLanguage().toUpperCase() == "ZH") {
                  subjectName = (String)map.get("subject_title_chi");
                } else {
                  subjectName = (String)map.get("subject_title_eng");
                }
              }
            }
          }

          ResourceBundle res = ResourceBundle.getBundle("messages", locale);
          String tableHeaderContent = "<tr>";
          tableHeaderContent = tableHeaderContent + "<td>" + res.getString("user.name") + "</td>";
          tableHeaderContent = tableHeaderContent + "<td>" + res.getString("user.studentNo") + "</td>";
          List artsAssIdList = new ArrayList();
          it = postOrderArtsList.iterator();

          String wholeContent;
          while(it.hasNext()) {
            wholeContent = (String)it.next();
            if (artsAssignmentIds.indexOf(wholeContent) != -1) {
              tableHeaderContent = tableHeaderContent + "<td>" + ((Arts_assignment_viewTO)assignments.get(wholeContent)).getName() + "</td>";
              artsAssIdList.add(wholeContent);
            }
          }

          tableHeaderContent = tableHeaderContent + "</tr>";
          it = studentList.iterator();
          wholeContent = "<table border='1'>";
          wholeContent = wholeContent + tableHeaderContent;

          TreeMap resultMap;
          String teacherNames;
          String studentDetailContent;
          String artsAssId;
          for(resultMap = new TreeMap(); it.hasNext(); wholeContent = wholeContent + studentDetailContent) {
            map = (Map)it.next();
            Email email = new Email();
            email.setToArray(new String[]{(String)map.get("email")});
            email.setBcArray(schoolAdminEmails);
            email.setFrom(fromEmail);
            email.setFromName(fromName);
            teacherNames = subjectCode + " " + subjectName + " " + subjectGroupMap.get(map.get("subject_group_id")) + " - " + res.getString("arts.mark");
            email.setSubject(teacherNames);
            String studentContent = "<table border='1'>";
            studentContent = studentContent + tableHeaderContent;
            studentDetailContent = "<tr>";
            if (locale.getLanguage().toUpperCase() == "ZH") {
              studentDetailContent = studentDetailContent + "<td>" + map.get("name_chi") + "</td>";
            } else {
              studentDetailContent = studentDetailContent + "<td>" + map.get("name_eng") + "</td>";
            }

            studentDetailContent = studentDetailContent + "<td>" + map.get("student_no") + "</td>";
            String studentId = (String)map.get("user_id");

            for(Iterator it2 = artsAssIdList.iterator(); it2.hasNext(); studentDetailContent = studentDetailContent + "</td>") {
              artsAssId = (String)it2.next();
              mark = studentMarkMap.get(studentId + "_" + artsAssId);
              studentDetailContent = studentDetailContent + "<td>";
              if (mark != null && ((String)mark).length() > 0) {
                String tmpMark = (String)mark;
                if (tmpMark.startsWith(Constants.EXEMPT_MARK)) {
                  tmpMark = tmpMark.replaceFirst(Constants.EXEMPT_MARK, Constants.EXEMPT_STR);
                }

                studentDetailContent = studentDetailContent + tmpMark;
              } else {
                studentDetailContent = studentDetailContent + "&nbsp;";
              }
            }

            studentDetailContent = studentDetailContent + "</tr>";
            studentContent = studentContent + studentDetailContent;
            studentContent = studentContent + "</table>";
            email.setContent(studentContent);
            Map result = this.emailService.send(email, locale.getLanguage(), locale.getCountry());
            String statusMessage = (String)result.get("statusMessage");
            if (locale.getLanguage().toUpperCase() == "ZH") {
              resultMap.put((String)map.get("student_no") + " " + (String)map.get("name_chi"), statusMessage);
            } else {
              resultMap.put((String)map.get("student_no") + " " + (String)map.get("name_eng"), statusMessage);
            }
          }

          wholeContent = wholeContent + "</table>";
          it = teacherList.iterator();
          String teacherEmailStr = "";
          String subject = null;
          teacherNames = "";
          i = 0;
          if (teacherList != null && teacherList.size() > 0) {
            String[] toArray;
            for(toArray = new String[teacherList.size()]; it.hasNext(); ++i) {
              map = (Map)it.next();
              toArray[i] = (String)map.get("email");
              teacherEmailStr = teacherEmailStr + "," + (String)map.get("email");
              if (locale.getLanguage().toUpperCase() == "ZH") {
                teacherNames = teacherNames + "," + (String)map.get("name_chi");
              } else {
                teacherNames = teacherNames + "," + (String)map.get("name_eng");
              }

              if (subject == null) {
                subject = subjectCode + " " + subjectName + " " + subjectGroupMap.get(map.get("subject_group_id")) + " - " + res.getString("arts.mark");
              }
            }

            Email email = new Email();
            email.setToArray(toArray);
            email.setBcArray(schoolAdminEmails);
            email.setFrom(fromEmail);
            email.setFromName(fromName);
            email.setSubject(subject);
            email.setContent(wholeContent);
            Map result = this.emailService.send(email, locale.getLanguage(), locale.getCountry());
            artsAssId = (String)result.get("statusMessage");
            resultMap.put(teacherNames.substring(1), artsAssId);
            Email_tbl emailTbl = new Email_tbl();
            teacherEmailStr = teacherEmailStr.substring(1);
            emailTbl.setReceiver(teacherEmailStr);
            subject = subjectCode + " " + subjectName + " - " + res.getString("arts.mark");
            emailTbl.setTitle(subject);
            emailTbl.setContent(wholeContent);
            emailTbl.setCreated_by(userId);
            emailTbl.setCreated_date(new Timestamp(System.currentTimeMillis()));
            emailTbl.setEmail_id(Sequencer.getInstance().getNextSeqNum(this.jdbcTemplate, "EMAIL_ID"));
            CPCE.DAOFACTORY.createEmail_tblDAO().insert(emailTbl, this.jdbcTemplate);
          }

          return resultMap;
        } else {
          throw new CpceException(2003, "cpce.exceptionCode.wrongSubjectGroup");
        }
      } else {
        return new HashMap();
      }
    } else {
      return new HashMap();
    }
  }

  public Map compareAssMarkDifference(CompareAssMarkDiffForm form, Locale locale) {
    String artsAssId1 = form.getArtsAssId1();
    String artsAssId2 = form.getArtsAssId2();
    String subjectGroupIds = form.getSubjectGroupIds();
    List resultList = new ArrayList();
    if (artsAssId1 != null && artsAssId1.length() != 0) {
      if (artsAssId2 != null && artsAssId2.length() != 0) {
        if (subjectGroupIds != null && subjectGroupIds.length() != 0) {
          String ids = form.getSubjectGroupIds();
          ids = ids.replaceAll(",", "','");
          ids = "'" + ids + "'";
          String getStudentSql = "select C.subject_group_id, D.subject_group_no, A.user_id, B.student_no, A.name_chi, A.name_eng from user_tbl A join student_tbl B on A.user_id = B.user_id join student_belong_group_tbl C on C.user_id = A.user_id join subject_group_tbl D on D.subject_group_id = C.subject_group_id  where C.subject_group_id in (" + ids + ") and A.account_status = 1 and C.belong = '1' " + " order by B.student_no";
          String getMarkSql = "select A.user_id, A.arts_assignment_id, A.mark, A.grade from arts_mark_entries_tbl A join  student_belong_group_tbl B on A.user_id = B.user_id  where B.subject_group_id in (" + ids + ") " + " and A.arts_assignment_id in ('" + form.getArtsAssId1() + "','" + form.getArtsAssId2() + "')";
          String getArtsAssSql = "select A.arts_assignment_id, A.marked_by_mark, A.name, A.max_mark from arts_assignment_tbl A  join arts_group_tbl B on A.arts_group_id = B.arts_group_id  where arts_assignment_id in ('" + form.getArtsAssId1() + "','" + form.getArtsAssId2() + "') " + " and B.subject_group_id in (" + ids + ") ";
          String getGradesSql = "select A.grade from arts_gpa_conv_tbl A  join subject_tbl B  on A.institute_id = B.institute_id and A.academic_year = B.academic_year and A.semester = B.semester  join subject_group_tbl C on C.subject_id = B.subject_id  where C.subject_group_id = ?  order by arts_gpa_conv_id  ";
          new ArrayList();
          Map studentMarkMap = new HashMap();
          Map artsAssMap = new HashMap();
          List gradeList = new ArrayList();
          String artsAssName1 = null;
          String artsAssName2 = null;
          List list = this.jdbcTemplate.queryForList(getArtsAssSql);
          if (list != null && list.size() > 0) {
            Iterator it = list.iterator();

            while(it.hasNext()) {
              Map markedByMark = (Map)it.next();
              Arts_assignment_tbl artsAssignmentTbl = new Arts_assignment_tbl();
              artsAssignmentTbl.setArts_assignment_id((String)markedByMark.get("arts_assignemnt_id"));
              artsAssignmentTbl.setMarked_by_mark((String)markedByMark.get("marked_by_mark"));
              artsAssignmentTbl.setName((String)markedByMark.get("name"));
              artsAssignmentTbl.setMax_mark((Integer)markedByMark.get("max_mark"));
              artsAssMap.put(markedByMark.get("arts_assignment_id"), artsAssignmentTbl);
            }
          }

          if (artsAssMap.size() <= 0) {
            return null;
          } else {
            Arts_assignment_tbl artsAssignmentTbl = (Arts_assignment_tbl)artsAssMap.get(artsAssId1);
            String markedByMark1 = null;
            String markedByMark2 = null;
            if (artsAssignmentTbl != null) {
              markedByMark1 = artsAssignmentTbl.getMarked_by_mark();
            }

            artsAssignmentTbl = (Arts_assignment_tbl)artsAssMap.get(artsAssId2);
            if (artsAssignmentTbl != null) {
              markedByMark2 = artsAssignmentTbl.getMarked_by_mark();
            }

            if (markedByMark1 != null && markedByMark2 != null) {
              String markedByMark;
              if (markedByMark1.equals(markedByMark2)) {
                markedByMark = markedByMark1;
              } else {
                markedByMark = "0";
              }

              list = this.jdbcTemplate.queryForList(getMarkSql);
              if (list != null && list.size() > 0) {
                Iterator it = list.iterator();

                while(it.hasNext()) {
                  Map map = (Map)it.next();
                  String artsAssId = (String)map.get("arts_assignment_id");
                  String studentId = (String)map.get("user_id");
                  Arts_mark_entries_tbl markEntriesTbl = new Arts_mark_entries_tbl();
                  markEntriesTbl.setMark((BigDecimal)map.get("mark"));
                  markEntriesTbl.setGrade((String)map.get("grade"));
                  studentMarkMap.put(studentId + "_" + artsAssId, markEntriesTbl);
                }
              }

              int idx = subjectGroupIds.indexOf(",");
              String subjectGroupId;
              if (idx != -1) {
                subjectGroupId = subjectGroupIds.substring(0, idx);
              } else {
                subjectGroupId = subjectGroupIds;
              }

              list = this.jdbcTemplate.queryForList(getGradesSql, new Object[]{subjectGroupId});
              Iterator it;
              Map map;
              if (list != null && list.size() > 0) {
                it = list.iterator();

                while(it.hasNext()) {
                  map = (Map)it.next();
                  gradeList.add((String)map.get("grade"));
                }
              }

              list = this.jdbcTemplate.queryForList(getStudentSql);
              if (list != null && list.size() > 0) {
                it = list.iterator();

                label135:
                while(true) {
                  String grade1;
                  String grade2;
                  int idx1;
                  do {
                    while(true) {
                      if (!it.hasNext()) {
                        break label135;
                      }

                      map = (Map)it.next();
                      String studentId = (String)map.get("user_id");
                      grade1 = null;
                      grade2 = null;
                      BigDecimal mark1 = null;
                      BigDecimal mark2 = null;
                      Arts_mark_entries_tbl markEntriesTbl;
                      if (markedByMark.equals("0")) {
                        markEntriesTbl = (Arts_mark_entries_tbl)studentMarkMap.get(studentId + "_" + artsAssId1);
                        if (markEntriesTbl != null) {
                          grade1 = markEntriesTbl.getGrade();
                        }

                        markEntriesTbl = (Arts_mark_entries_tbl)studentMarkMap.get(studentId + "_" + artsAssId2);
                        if (markEntriesTbl != null) {
                          grade2 = markEntriesTbl.getGrade();
                        }

                        idx1 = gradeList.indexOf(grade1);
                        idx1 = gradeList.indexOf(grade2);
                        break;
                      }

                      markEntriesTbl = (Arts_mark_entries_tbl)studentMarkMap.get(studentId + "_" + artsAssId1);
                      if (markEntriesTbl != null) {
                        grade1 = markEntriesTbl.getGrade();
                        mark1 = markEntriesTbl.getMark();
                      }

                      markEntriesTbl = (Arts_mark_entries_tbl)studentMarkMap.get(studentId + "_" + artsAssId2);
                      if (markEntriesTbl != null) {
                        grade2 = markEntriesTbl.getGrade();
                        mark2 = markEntriesTbl.getMark();
                      }

                      if (mark1 == null || mark2 == null || Math.abs(mark1.floatValue() - mark2.floatValue()) >= form.getMarkDiff()) {
                        CompareAssMarkResultData data = new CompareAssMarkResultData();
                        data.setArtsAssId1(artsAssId1);
                        data.setArtsAssId2(artsAssId2);
                        data.setGrade1(grade1);
                        data.setGrade2(grade2);
                        data.setMark1(mark1);
                        data.setMark2(mark2);
                        data.setStudentNo((String)map.get("student_no"));
                        data.setSubjectGroupId((String)map.get("subject_group_id"));
                        data.setSubjectGroupNo((String)map.get("subject_group_no"));
                        if (mark1 != null && mark2 != null) {
                          data.setMarkDiff(new BigDecimal((double)Math.abs(mark1.floatValue() - mark2.floatValue())));
                        }

                        idx1 = gradeList.indexOf(grade1);
                        int idx2 = gradeList.indexOf(grade2);
                        if (idx1 != -1 && idx2 != -1) {
                          data.setGradeLevelDiff(Math.abs(idx1 - idx2));
                        } else {
                          data.setGradeLevelDiff(-1);
                        }

                        if (locale.getLanguage().equals("zh")) {
                          data.setName((String)map.get("name_chi"));
                        } else {
                          data.setName((String)map.get("name_eng"));
                        }

                        resultList.add(data);
                      }
                    }
                  } while(idx1 != -1 && idx1 != -1 && Math.abs(idx1 - idx1) < form.getGradeLevelDiff());

                  CompareAssMarkResultData data = new CompareAssMarkResultData();
                  data.setArtsAssId1(artsAssId1);
                  data.setArtsAssId2(artsAssId2);
                  data.setGrade1(grade1);
                  data.setGrade2(grade2);
                  data.setStudentNo((String)map.get("student_no"));
                  data.setSubjectGroupId((String)map.get("subject_group_id"));
                  data.setSubjectGroupNo((String)map.get("subject_group_no"));
                  if (idx1 != -1 && idx1 != -1) {
                    data.setGradeLevelDiff(Math.abs(idx1 - idx1));
                  } else {
                    data.setGradeLevelDiff(-1);
                  }

                  if (locale.getLanguage().equals("zh")) {
                    data.setName((String)map.get("name_chi"));
                  } else {
                    data.setName((String)map.get("name_eng"));
                  }

                  resultList.add(data);
                }
              }

              artsAssignmentTbl = (Arts_assignment_tbl)artsAssMap.get(artsAssId1);
              artsAssName1 = artsAssignmentTbl.getName();
              artsAssignmentTbl = (Arts_assignment_tbl)artsAssMap.get(artsAssId2);
              artsAssName2 = artsAssignmentTbl.getName();
              Map resultMap = new HashMap();
              resultMap.put("dataList", resultList);
              resultMap.put("artsAssName1", artsAssName1);
              resultMap.put("artsAssName2", artsAssName2);
              return resultMap;
            } else {
              return null;
            }
          }
        } else {
          return null;
        }
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  class MyComparator implements Comparator {
    MyComparator() {
    }

    public int compare(Object s1, Object s2) {
      if (s1.equals(s2)) {
        return 0;
      } else {
        float t1 = 0.0F;
        float t2 = 0.0F;
        String ss1 = (String)s1;
        ss1 = ss1.substring(0, ss1.indexOf("_"));
        String ss2 = (String)s2;
        ss2 = ss2.substring(0, ss2.indexOf("_"));
        t1 = Float.parseFloat(ss1);
        t2 = Float.parseFloat(ss2);
        return t1 >= t2 ? 1 : -1;
      }
    }
  }
}
