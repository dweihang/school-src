//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.arts.mark.web;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jdom.Document;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import smile.cpce.arts.assignment.common.ArtsAssignmentUtil;
import smile.cpce.arts.assignment.service.ArtsAssignmentService;
import smile.cpce.arts.lock.service.ArtsLockService;
import smile.cpce.arts.mark.common.ArtsMarksXML;
import smile.cpce.arts.mark.service.ArtsMarkService;
import smile.cpce.common.Constants;
import smile.cpce.common.ToStringHelper;
import smile.cpce.common.Tools;
import smile.cpce.common.domain.SessionAttribute;
import smile.cpce.common.exception.CpceException;
import smile.cpce.db.exception.Arts_assignment_viewDTOException;
import smile.cpce.db.exception.Arts_lock_tblException;
import smile.cpce.db.to.Arts_assignment_viewTO;
import smile.cpce.school.institute.domain.Institute;

public class ArtsMarkInputController extends AbstractController {
  protected final Logger logger = Logger.getLogger("cpce");
  private ArtsAssignmentService artsAssignmentService;
  private ArtsMarkService artsMarkService;
  private ArtsLockService artsLockService;

  public ArtsMarkInputController() {
  }

  public void setArtsAssignmentService(ArtsAssignmentService artsAssignmentService) {
    this.artsAssignmentService = artsAssignmentService;
  }

  public void setArtsMarkService(ArtsMarkService artsMarkService) {
    this.artsMarkService = artsMarkService;
  }

  public void setArtsLockService(ArtsLockService artsLockService) {
    this.artsLockService = artsLockService;
  }

  protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
    this.logger.info("ArtsMarkInputController called");
    String start = request.getParameter("start");
    String subjectGroupId = request.getParameter("subjectGroupId");
    String assignmentId = request.getParameter("assignmentId");
    String subjectId = request.getParameter("subjectId");
    String orderBy = request.getParameter("orderBy");
    String noPerPage = request.getParameter("noPerPage");
    String changeItems = request.getParameter("changeItems");
    logger.info("subjectGroupId: " + subjectGroupId);
    logger.info("assignmentId: " + assignmentId);
    logger.info("subjectId: " + subjectId);
    boolean isFromAssignment = assignmentId != null && assignmentId.length() > 0;
    if (isFromAssignment) {
      subjectGroupId = this.artsAssignmentService.getSubjectGroupIdFromAssignmentId(assignmentId);
      if (subjectGroupId == null) {
        throw new CpceException(2003, "cpce.exceptionCode.wrongAssignment");
      }
    } else {
      Map assignments = this.artsAssignmentService.getArtsAssignmentsBySubjectGroup(subjectGroupId);
      logger.info("assignments: " + ToStringHelper.toString(assignments));
      logger.info("leaf:" + ArtsAssignmentUtil.getLeafArtsAssignments(assignments));
    }

    Object[] ids = (Object[])((Object[])request.getParameterMap().get("artsAssignmentIds"));
    String artsAssIds = "";
    int noOfRecords;
    if (ids != null) {
      int i = 0;

      for(noOfRecords = ids.length; i < noOfRecords; ++i) {
        artsAssIds = artsAssIds + "," + ids[i];
      }

      artsAssIds = artsAssIds.substring(1);
    }

    ids = (Object[])((Object[])request.getParameterMap().get("subjectGroupIds"));
    String subjectGroupIds = subjectGroupId;
    int startFrom;
    if (ids != null && ids.length > 0) {
      noOfRecords = 0;

      for(startFrom = ids.length; noOfRecords < startFrom; ++noOfRecords) {
        subjectGroupIds = subjectGroupIds + "," + ids[noOfRecords];
      }
    }

    noOfRecords = 100;
    if (noPerPage != null && noPerPage.trim().length() > 0) {
      noOfRecords = Integer.parseInt(noPerPage.trim());
    }

    if (start == null) {
      startFrom = 0;
    } else {
      startFrom = Integer.parseInt(start);
    }

    if (!"name_eng".equals(orderBy) && !"student_no".equals(orderBy)) {
      orderBy = "student_no";
    }

    SessionAttribute sessionAttribute = (SessionAttribute)request.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
    String userId = sessionAttribute.getUserId();
    Institute institute = sessionAttribute.getInstitute();
    if (!Tools.checkAccessRightSubjectGroup(userId, sessionAttribute.getRole(), institute == null ? null : institute.getInstituteId(), subjectGroupId, Constants.jdbcTemplate)) {
      this.logger.info("Access Denied: user:" + sessionAttribute.getUserId() + " || subjectGroupId: " + subjectGroupId + "||action: input arts mark");
      throw new CpceException(2003, "cpce.exceptionCode.accessDenied");
    } else {
      Document doc = null;
      ArtsMarksXML artsMarksXML = null;
      int status = -1000;
      String groupId = this.artsAssignmentService.getGroupIdFromSubjectGroupId(subjectGroupId);
      Object artsGroupMap = this.artsAssignmentService.getSubjectGroupsFromGroupId(groupId, userId, sessionAttribute.getRole());

      try {
        if (start != null) {
          boolean updateDB = true;
          Map errorMap = this.artsMarkService.updateMarks(subjectGroupId, request.getParameterMap(), userId, changeItems, updateDB);
          if (errorMap.size() > 0) {
            status = -1;
          } else {
            status = 0;
          }
        }

        Map assignments = null;
        if (isFromAssignment) {
          assignments = this.artsAssignmentService.getArtsAssignmentsByAssignment(assignmentId);
          logger.info("assignments: " + ToStringHelper.toString(assignments));
          if (assignments.size() > 0) {
            Iterator it = assignments.values().iterator();
            Arts_assignment_viewTO view = (Arts_assignment_viewTO)it.next();
            if (view.getArts_group_id() == null) {
              artsGroupMap = new HashMap();
              ((Map)artsGroupMap).put(subjectGroupId, "");
            }
          } else {
            artsGroupMap = new HashMap();
            ((Map)artsGroupMap).put(subjectGroupId, "");
          }
        } else {
          assignments = this.artsAssignmentService.getArtsAssignmentsBySubjectGroup(subjectGroupId);
        }

        artsMarksXML = new ArtsMarksXML(subjectGroupIds, groupId, subjectGroupId, subjectId, assignmentId, startFrom, noOfRecords, orderBy, status, artsAssIds, sessionAttribute.getLocale(), assignments, (Map)artsGroupMap);
        doc = artsMarksXML.generate();
      } catch (Arts_assignment_viewDTOException var28) {
        logger.log(Level.SEVERE, var28.getMessage(), var28);
        throw new CpceException(2004, "cpce.exceptionCode.DBError");
      } catch (CpceException var29) {
        logger.log(Level.SEVERE, var29.getMessage(), var29);
        throw var29;
      }

      if (doc == null) {
        throw new CpceException(2003, "cpce.exceptionCode.noData");
      } else {
        String view = "";

        try {
          if (this.artsLockService.canInputArtsMarks(groupId)) {
            view = "artsInputMark";
          } else {
            view = "artsLockInputMark";
          }
        } catch (Arts_lock_tblException var27) {
          logger.log(Level.SEVERE, var27.getMessage(), var27);
          throw new CpceException(2004, "cpce.exceptionCode.DBError");
        }

        ModelAndView mav = new ModelAndView(view);
        mav.addObject("doc", doc);
        return mav;
      }
    }
  }
}
