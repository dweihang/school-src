//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.arts.mark.web;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.RequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import smile.cpce.arts.mark.service.ArtsMarkService;
import smile.cpce.common.Constants;
import smile.cpce.common.ToStringHelper;
import smile.cpce.common.domain.SessionAttribute;
import smile.cpce.common.service.FileUploadService;

public class UploadArtsMarkController implements Controller {

  protected final Logger logger = Logger.getLogger("cpce");
  private String successView;
  private String formView;
  private int maxUploadSize;
  private FileUploadService fileUploadService;
  private ArtsMarkService artsMarkService;

  public UploadArtsMarkController() {
  }

  public void setArtsMarkService(ArtsMarkService artsMarkService) {
    this.artsMarkService = artsMarkService;
  }

  public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    throws Exception {
    String groupId = RequestUtils.getStringParameter(request, "groupId", "");
    String subjectGroupId = RequestUtils.getStringParameter(request, "subjectGroupId", "");
    String assignmentId = RequestUtils.getStringParameter(request, "assignmentId", "");
    if (request.getContentType() == null) {
      logger.info("content-type is null");
      Map map = new HashMap();
      map.put("groupId", groupId);
      map.put("subjectGroupId", subjectGroupId);
      map.put("assignmentId", assignmentId);
      return new ModelAndView(this.formView, "map", map);
    } else {
      ModelAndView mav = null;
      try {
        SessionAttribute sessionAttribute = (SessionAttribute) request.getSession()
          .getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
        String userId = sessionAttribute.getUserId();
        String role = sessionAttribute.getRole();
        Map map = this.fileUploadService
          .fileUpload(request, this.maxUploadSize, "artsMark_", Constants.UPLOAD_TEMP_PATH);
        groupId = (String) map.get("groupId");
        subjectGroupId = (String) map.get("subjectGroupId");
        assignmentId = (String) map.get("assignmentId");
        File dataFile = (File) map.get("dataFile");
        Map rst = null;
        if (dataFile != null) {
          logger.info("assignmentId: " + assignmentId);
          rst = this.artsMarkService
            .uploadArtsMarks(groupId, subjectGroupId, assignmentId, dataFile, userId, role);
          mav = new ModelAndView(this.successView);
          logger.info(ToStringHelper.toString(rst));
          if (rst.get(new Integer(0)).equals("1")) {
            mav.addObject("status", "common.upload.success");
          } else {
            mav.addObject("status", "common.upload.fail");
          }

          rst.remove(new Integer(0));
          mav.addObject("errors", rst);
          return mav;
        } else {
          logger.info("dataFile is null");
          mav = new ModelAndView(this.formView);
          map.remove("dataFile");
          mav.addAllObjects(map);
          Map parameterMap = new HashMap();
          parameterMap.put("groupId", groupId);
          parameterMap.put("subjectGroupId", subjectGroupId);
          parameterMap.put("assignmentId", assignmentId);
          mav.addObject("map", parameterMap);
          mav.addObject("status", "common.upload.fail");
          return mav;
        }
      } catch (Exception e) {
        logger.log(Level.SEVERE, e.getMessage(), e);
        throw e;
      }
    }
  }

  public void setFormView(String formView) {
    this.formView = formView;
  }

  public void setMaxUploadSize(int maxUploadSize) {
    this.maxUploadSize = maxUploadSize;
  }

  public void setFileUploadService(FileUploadService service) {
    this.fileUploadService = service;
  }

  public void setSuccessView(String successView) {
    this.successView = successView;
  }
}

