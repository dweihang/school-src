//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.batchEnroll.service;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import smile.cpce.batchEnroll.domain.BaseEnrollData;
import smile.cpce.batchEnroll.domain.Constants;
import smile.cpce.batchEnroll.domain.DataDefinition;
import smile.cpce.batchEnroll.domain.DataDependent;
import smile.cpce.batchEnroll.domain.Institute;
import smile.cpce.common.ToStringHelper;

public class DataServiceImpl implements DataService {
  private Map dataDefinitionMap = new HashMap();
  private Map instituteMap = new HashMap();
  protected final Logger logger = Logger.getLogger("cpce");

  public DataServiceImpl() {
    this.parseDataXML();
  }

  public void parseDataXML() {
    Document doc = null;
    DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
    docBuilderFactory.setIgnoringElementContentWhitespace(true);

    try {
      DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
      doc = docBuilder.parse(new InputSource(new FileReader(Constants.DEFINITION_PATH)));
    } catch (Exception var32) {
      this.logger.info("cannot find definition file");
      var32.printStackTrace();
    }

    Node dataNode = null;
    NamedNodeMap dataAttrMap = null;
    String instituteId = null;
    String instituteCode = null;
    String academicYear = null;
    String semester = null;
    String deptCode = null;
    String[] dependName = null;
    String[][] dependKeys = (String[][])null;
    String emailSuffix = null;
    NodeList dataNodes = doc.getElementsByTagName("institutes").item(0).getChildNodes();
    int i = 0;

    int j;
    for(j = dataNodes.getLength(); i < j; ++i) {
      dataNode = dataNodes.item(i);
      dataAttrMap = dataNode.getAttributes();
      instituteId = dataAttrMap.getNamedItem("id").getNodeValue();
      instituteCode = dataAttrMap.getNamedItem("code").getNodeValue();
      academicYear = dataAttrMap.getNamedItem("academicYear").getNodeValue();
      semester = dataAttrMap.getNamedItem("semester").getNodeValue();
      deptCode = dataAttrMap.getNamedItem("departmentCode").getNodeValue();
      emailSuffix = dataAttrMap.getNamedItem("emailSuffix").getNodeValue();
      Institute institute = new Institute(instituteId, instituteCode, academicYear, semester, deptCode, emailSuffix);
      this.instituteMap.put(instituteCode, institute);
    }

    dataNodes = doc.getElementsByTagName("definition").item(0).getChildNodes();
    i = 0;

    for(j = dataNodes.getLength(); i < j; ++i) {
      dataNode = dataNodes.item(i);
      dataAttrMap = dataNode.getAttributes();
      String dataName = dataAttrMap.getNamedItem("name").getNodeValue();
      List fieldList = new ArrayList();
      List dependentList = new ArrayList();
      NodeList nodeList = dataNode.getChildNodes();
      NodeList fieldNodes = nodeList.item(0).getChildNodes();
      String key = "";
      int k;
      for(k = 0; k < fieldNodes.getLength(); ++k) {
        NamedNodeMap fieldAttrMap = fieldNodes.item(k).getAttributes();
        String name = fieldAttrMap.getNamedItem("name").getNodeValue();
        String maxLength = fieldAttrMap.getNamedItem("maxLength").getNodeValue();
        boolean required = "y".equals(fieldAttrMap.getNamedItem("required").getNodeValue());
        String option = fieldAttrMap.getNamedItem("option").getNodeValue();
        DataDefinition definition;
        if (fieldAttrMap.getNamedItem("key").getNodeValue().equals("y")) {
          key = key + Constants.KEY_SEPARATOR + name;
          definition = new DataDefinition(Integer.parseInt(maxLength), required, option, "y", name);
        } else {
          definition = new DataDefinition(Integer.parseInt(maxLength), required, option, "n", name);
        }

        Node node = fieldAttrMap.getNamedItem("default");
        if (node != null) {
          definition.setDefaultVal(node.getNodeValue());
        }

        fieldList.add(definition);
      }

      try {
        if (nodeList.item(1) != null) {
          NodeList dependNodes = nodeList.item(1).getChildNodes();
          k = 0;

          for(int l = dependNodes.getLength(); k < l; ++k) {
            dataAttrMap = dependNodes.item(k).getAttributes();
            dependName = new String[]{dataAttrMap.getNamedItem("name").getNodeValue(), dataAttrMap.getNamedItem("table_name").getNodeValue()};
            NodeList keyNodes = dependNodes.item(k).getChildNodes();
            dependKeys = new String[keyNodes.getLength()][3];

            for(int m = keyNodes.getLength() - 1; m >= 0; --m) {
              dataAttrMap = keyNodes.item(m).getAttributes();
              dependKeys[m][0] = dataAttrMap.getNamedItem("name") != null ? dataAttrMap.getNamedItem("name").getNodeValue() : null;
              dependKeys[m][1] = dataAttrMap.getNamedItem("table_field_name").getNodeValue();
              dependKeys[m][2] = dataAttrMap.getNamedItem("value") != null ? dataAttrMap.getNamedItem("value").getNodeValue() : null;
            }

            dependentList.add(new DataDependent(dependName, dependKeys));
          }
        }
      } catch (Exception var33) {
        var33.printStackTrace();
      }

      if (key.length() > 0) {
        key = key.substring(Constants.KEY_SEPARATOR.length());
      }

      this.dataDefinitionMap.put(dataName, fieldList);
      this.dataDefinitionMap.put(dataName + "_key", key);
      this.dataDefinitionMap.put(dataName + "_depend", dependentList);
    }

  }

  public List readData(String name) {
    List tempList = new ArrayList();
    if (!this.dataDefinitionMap.containsKey(name)) {
      return tempList;
    } else {
      List dataDefinitionList = (List)this.dataDefinitionMap.get(name);

      try {
        LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(new File(Constants.DATA_PATH + name + ".txt")));
        int dataDefinitionListSize = dataDefinitionList.size();

        for(String contentLine = lineNumberReader.readLine(); contentLine != null; contentLine = lineNumberReader.readLine()) {
          String[] contentArr = contentLine.split(Constants.SEPARATOR);
          BaseEnrollData baseEnrollData = new BaseEnrollData();
          if (contentArr.length != dataDefinitionListSize) {
            baseEnrollData.getErrorList().add("ERROR_DATA_STRUCTURE");
          } else {
            int i = 0;

            for(int j = contentArr.length; i < j; ++i) {
              DataDefinition dataDefinition = (DataDefinition)dataDefinitionList.get(i);
              baseEnrollData.setData(dataDefinition.name, contentArr[i]);
            }
          }

          tempList.add(baseEnrollData);
        }
      } catch (IOException var12) {
        var12.printStackTrace();
      }

      return tempList;
    }
  }

  public Map getAllDataDefinitions() {
    return Collections.unmodifiableMap(this.dataDefinitionMap);
  }

  public List getDataDefinition(String name) {
    return this.dataDefinitionMap.containsKey(name) ? (List)this.dataDefinitionMap.get(name) : null;
  }

  public String[] getDataKeyDefinition(String name) {
    String[] keys = null;
    Iterator it = null;
    String keyStr = (String)this.dataDefinitionMap.get(name + "_key");
    if (keyStr != null) {
      keys = keyStr.split(Constants.KEY_SEPARATOR);
    } else {
      List list = (List)this.dataDefinitionMap.get(name);
      it = list.iterator();
      keys = new String[list.size()];

      DataDefinition definition;
      for(int var6 = 0; it.hasNext(); keys[var6++] = definition.name) {
        definition = (DataDefinition)it.next();
      }
    }

    return keys;
  }

  public String getInstituteIdByCode(String code) {
    Institute institute = (Institute)this.instituteMap.get(code);
    return institute != null ? institute.getId() : null;
  }

  public Map getInstitute() {
    return this.instituteMap;
  }

  public String getInstituteByDepartmentCode(String departmentCode) {
    Iterator it = this.instituteMap.keySet().iterator();

    Institute institute;
    do {
      if (!it.hasNext()) {
        return null;
      }

      institute = (Institute)this.instituteMap.get(it.next());
    } while(!institute.getDeptCode().equals(departmentCode));

    return institute.getId();
  }

  public String getInstituteEmailSuffixByDepartmentCode(String departmentCode) {
    Iterator it = this.instituteMap.keySet().iterator();

    Institute institute;
    do {
      if (!it.hasNext()) {
        return null;
      }

      institute = (Institute)this.instituteMap.get(it.next());
    } while(!institute.getDeptCode().equals(departmentCode));

    return institute.getEmailSuffix();
  }

  public boolean updateInstitute(String instituteId, String academicYear, String semester) {
    Iterator it = this.instituteMap.keySet().iterator();
    boolean done = false;
    logger.info("Definition file path: " + Constants.DEFINITION_PATH);
    logger.info(ToStringHelper.toString(instituteMap));

    while(it.hasNext()) {
      Institute institute = (Institute)this.instituteMap.get(it.next());
      if (institute.getId().equals(instituteId)) {
        institute.setAcademicYear(academicYear);
        institute.setSemester(semester);
        done = true;
        break;
      }
    }

    return done;
  }
}
