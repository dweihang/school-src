//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.common;

import java.util.List;
import java.util.logging.Logger;
import org.acegisecurity.GrantedAuthority;
import org.acegisecurity.userdetails.UserDetails;
import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.acegisecurity.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.dao.DataAccessException;


/**
 * Not needed
 *
 */
public class UserInfoService extends JdbcDaoImpl {
  private boolean isFromNetLogon = true;
  protected final Logger logger = Logger.getLogger("cpce");

  public UserInfoService() {
  }

  public boolean getIsFromNetLogon() {
    return this.isFromNetLogon;
  }

  public void setIsFromNetLogon(boolean isFromNetLogon) {
    this.isFromNetLogon = isFromNetLogon;
  }

  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
    List users = this.usersByUsernameMapping.execute(username);
    if (users.size() == 0) {
      logger.info("User not found");
      throw new UsernameNotFoundException("User not found");
    } else {
      UserDetails user = (UserDetails)users.get(0);
      List dbAuths = this.authoritiesByUsernameMapping.execute(user.getUsername());
      this.addCustomAuthorities(user.getUsername(), dbAuths);
      if (dbAuths.size() == 0) {
        logger.info("User has no GrantedAuthority");
        throw new UsernameNotFoundException("User has no GrantedAuthority");
      } else {
        GrantedAuthority[] arrayAuths = (GrantedAuthority[])((GrantedAuthority[])dbAuths.toArray(new GrantedAuthority[dbAuths.size()]));
        String returnUsername = user.getUsername();
        if (!this.isUsernameBasedPrimaryKey()) {
          returnUsername = username;
        }

        this.logger.info("return userInfo: with fromNetLogon: " + this.isFromNetLogon);
        return new UserInfo(returnUsername, user.getPassword(), user.isEnabled(), true, true, true, arrayAuths, this.isFromNetLogon);
      }
    }
  }
}
