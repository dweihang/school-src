//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.common.service;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.servlet.ModelAndView;
import smile.cpce.common.Constants;
import smile.cpce.common.Tools;
import smile.cpce.common.domain.SessionAttribute;

public class FileUploadServiceImpl implements FileUploadService {
  protected final Logger logger = Logger.getLogger("cpce");

  public FileUploadServiceImpl() {
  }

  public Map fileUploadWithFixedName(HttpServletRequest request, int maxUploadSize, String newFileName, String uploadTmpPath) throws Exception {
    Map map = new HashMap();
    File dataFile = null;
    DiskFileUpload fu = new DiskFileUpload();
    fu.setSizeMax((long)maxUploadSize);
    fu.setSizeThreshold(4096);
    fu.setRepositoryPath(System.getProperty("java.io.tmpdir"));

    try {
      List items = fu.parseRequest(request);
      Iterator iter = items.iterator();

      while(true) {
        if (iter.hasNext()) {
          FileItem item = (FileItem)iter.next();
          String fileName;
          String storedPath;
          if (item.isFormField()) {
            fileName = item.getFieldName();
            storedPath = item.getString();
            map.put(fileName, storedPath);
            continue;
          }

          fileName = item.getName();
          if (fileName == null || fileName.equals("")) {
            map.put("uploadError", "commonError.noFileUploaded");
            return map;
          }

          storedPath = uploadTmpPath + System.getProperty("file.separator") + newFileName;
          dataFile = new File(storedPath);
          item.write(dataFile);
        }

        return map;
      }
    } catch (SizeLimitExceededException var13) {
      map.put("uploadError", "commonError.attachmentSizeExceed");
      return map;
    } catch (Exception var14) {
      map.put("uploadError", "commonError.errorFound");
      return map;
    }
  }

  public Map fileUpload(HttpServletRequest request, int maxUploadSize, String tmpFileUploadPrefix, String uploadTmpPath) throws Exception {
    Map map = new HashMap();
    String error = null;
    File dataFile = null;
    DiskFileUpload fu = new DiskFileUpload();
    fu.setSizeMax((long)maxUploadSize);
    fu.setSizeThreshold(4096);
    fu.setRepositoryPath(System.getProperty("java.io.tmpdir"));

    try {
      fu.setHeaderEncoding("UTF-8");
      List items = fu.parseRequest(request);
      Iterator iter = items.iterator();

      while(iter.hasNext()) {
        FileItem item = (FileItem)iter.next();
        String fileName;
        if (!item.isFormField()) {
          fileName = item.getName();
          if (fileName != null && !fileName.equals("")) {
            SessionAttribute sessionAttribute = (SessionAttribute)((SessionAttribute)request.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_NAME));
            fileName = FilenameUtils.getName(fileName);
            String extension = FilenameUtils.getExtension(fileName);
            String savedFilename = tmpFileUploadPrefix + sessionAttribute.getUserId() + "_" + this.getCurrentTimeParse() + "." + extension;
            String storedPath = uploadTmpPath + System.getProperty("file.separator") + savedFilename;
            dataFile = new File(storedPath);
            item.write(dataFile);
            break;
          }

          map.put("uploadError", "commonError.noFileUploaded");
          return map;
        } else {
          String fieldName = item.getFieldName();
          String fieldValue = item.getString();
          map.put(fieldName, fieldValue);
        }

        fileName = item.getFieldName();
        String fieldValue = item.getString();
        map.put(fileName, fieldValue);
      }
    } catch (SizeLimitExceededException var18) {
      logger.log(Level.SEVERE, var18.getMessage(), var18);
      map.put("uploadError", "commonError.attachmentSizeExceed");
      return map;
    } catch (Exception var19) {
      logger.log(Level.SEVERE, var19.getMessage(), var19);
      map.put("uploadError", "commonError.errorFound");
      return map;
    }

    map.put("dataFile", dataFile);
    return map;
  }

  public ModelAndView fileUpload(HttpServletRequest request, int maxUploadSize, String formPage, String successPage, String uploadPath) throws Exception {
    Map map = new HashMap();
    Map error = new HashMap();
    File dataFile = null;
    DiskFileUpload fu = new DiskFileUpload();
    fu.setSizeMax((long)maxUploadSize);
    fu.setSizeThreshold(4096);
    fu.setRepositoryPath(System.getProperty("java.io.tmpdir"));
    String successFileNameList = "";
    String failFileNameList = "";

    ModelAndView mav;
    try {
      fu.setHeaderEncoding("UTF-8");
      List items = fu.parseRequest(request);
      Iterator iter = items.iterator();
      boolean var14 = false;

      while(true) {
        while(iter.hasNext()) {
          FileItem item = (FileItem)iter.next();
          String fileName;
          if (item.isFormField()) {
            fileName = item.getFieldName();
            String fieldValue = item.getString();
            map.put(fileName, fieldValue);
          } else {
            fileName = item.getName();
            if (fileName != null && !fileName.equals("")) {
              SessionAttribute sessionAttribute = (SessionAttribute)((SessionAttribute)request.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_NAME));
              fileName = FilenameUtils.getName(fileName);
              File folder = new File(uploadPath);
              if (!folder.exists()) {
                folder.mkdir();
              }

              dataFile = new File(folder, fileName);
              item.write(dataFile);
              String content = this.getFileContent(dataFile);
              if (dataFile.length() > 0L && content != null && !content.trim().equals("")) {
                successFileNameList = successFileNameList + "," + fileName;
              } else {
                dataFile.delete();
                failFileNameList = failFileNameList + "," + fileName;
              }
            }
          }
        }

        mav = new ModelAndView(successPage);
        if (successFileNameList.length() > 0) {
          successFileNameList = successFileNameList.substring(1);
          mav.addObject("successFiles", successFileNameList);
        }

        if (failFileNameList.length() > 0) {
          failFileNameList = failFileNameList.substring(1);
          mav.addObject("failFiles", failFileNameList);
        }

        return mav;
      }
    } catch (SizeLimitExceededException var20) {
      this.logger.log(Level.SEVERE, " ", var20);
      mav = new ModelAndView(formPage);
      mav.addObject("uploadError", "commonError.attachmentSizeExceed");
      error.put("uploadError", "commonError.attachmentSizeExceed");
      error.putAll(map);
      mav.addObject("errorMap", map);
      return mav;
    } catch (Exception var21) {
      this.logger.log(Level.SEVERE, " ", var21);
      mav = new ModelAndView(formPage);
      mav.addObject("uploadError", "commonError.attachmentSizeExceed");
      error.put("uploadError", "commonError.errorFound");
      error.putAll(map);
      mav.addObject("errorMap", map);
      return mav;
    }
  }

  public ModelAndView fileUpload(HttpServletRequest request, int maxUploadSize, String formPage, String successPage, String tmpFileUploadPrefix, String uploadTmpPath) throws Exception {
    Map map = new HashMap();
    Map error = new HashMap();
    File dataFile = null;
    DiskFileUpload fu = new DiskFileUpload();
    fu.setSizeMax((long)maxUploadSize);
    fu.setSizeThreshold(4096);
    fu.setRepositoryPath(System.getProperty("java.io.tmpdir"));

    try {
      fu.setHeaderEncoding("UTF-8");
      List items = fu.parseRequest(request);
      Iterator iter = items.iterator();
      boolean var13 = false;

      while(iter.hasNext()) {
        FileItem item = (FileItem)iter.next();
        String fileName;
        if (item.isFormField()) {
          fileName = item.getFieldName();
          String fieldValue = item.getString();
          map.put(fileName, fieldValue);
          error.put(fileName, fieldValue);
        } else {
          fileName = item.getName();
          if (fileName == null || fileName.equals("")) {
            error.put("uploadError", "commonError.noFileUploaded");
            return new ModelAndView(formPage, "errorMap", error);
          }

          SessionAttribute sessionAttribute = (SessionAttribute)((SessionAttribute)request.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_NAME));
          fileName = FilenameUtils.getName(fileName);
          String extension = FilenameUtils.getExtension(fileName);
          String savedFilename = tmpFileUploadPrefix + sessionAttribute.getUserId() + "_" + this.getCurrentTimeParse() + "." + extension;
          String storedPath = uploadTmpPath + System.getProperty("file.separator") + savedFilename;
          dataFile = new File(storedPath);
          item.write(dataFile);
          map.put("fileName", Tools.toJavascriptScriptString(fileName));
          map.put("savedFilename", savedFilename);
        }
      }

      if (dataFile == null) {
        error.put("uploadError", "commonError.noFileUploaded");
        return new ModelAndView(formPage, "errorMap", error);
      } else {
        String content = this.getFileContent(dataFile);
        if (dataFile.length() <= 0L || content == null || content.trim().equals("")) {
          dataFile.delete();
          error.put("uploadError", "commonError.blankAttachment");
          return new ModelAndView(formPage, "errorMap", error);
        } else {
          return new ModelAndView(successPage, "map", map);
        }
      }
    } catch (SizeLimitExceededException var20) {
      error.put("uploadError", "commonError.attachmentSizeExceed");
      return new ModelAndView(formPage, "errorMap", error);
    } catch (Exception var21) {
      error.put("uploadError", "commonError.errorFound");
      this.logger.log(Level.SEVERE, " ", var21);
      return new ModelAndView(formPage, "errorMap", error);
    }
  }

  private String getCurrentTimeParse() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    Timestamp t = new Timestamp(System.currentTimeMillis());
    return sdf.format(t);
  }

  private String getFileContent(File f) {
    byte[] filebyte = null;

    try (FileInputStream fin = new FileInputStream(f)) {
      filebyte = new byte[fin.available()];
      fin.read(filebyte);
      fin.close();
    } catch (Exception var4) {
      var4.printStackTrace();
    }

    return filebyte != null ? new String(filebyte) : null;
  }
}
