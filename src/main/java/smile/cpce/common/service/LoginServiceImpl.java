package smile.cpce.common.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import smile.cpce.common.Constants;
import smile.cpce.common.ToStringHelper;
import smile.cpce.common.domain.LoginForm;
import smile.cpce.common.domain.SessionAttribute;
import smile.cpce.school.institute.domain.Institute;
import smile.cpce.school.institute.service.InstituteService;
import smile.cpce.school.school.domain.School;
import smile.cpce.school.user.domain.Schadmin;
import smile.cpce.school.user.domain.Student;
import smile.cpce.school.user.domain.User;
import smile.cpce.school.user.service.UserService;

public class LoginServiceImpl
    implements LoginService
{
  protected final Logger logger = Logger.getLogger("cpce");
  private JdbcTemplate jdbcTemplate;
  private UserService userService;
  private InstituteService instituteService;

  public void setInstituteService(InstituteService instituteService)
  {
    this.instituteService = instituteService;
  }

  public String checkLogin(LoginForm loginForm)
  {
    String userId = null;
    String sql = "select user_id from ldap_tbl where login_name=? and password=? order by user_id desc";
    Object[] params = { loginForm.getLoginName(), loginForm.getPassword() };
    List rows = this.jdbcTemplate.queryForList("select user_id from ldap_tbl where login_name=? and password=? order by user_id desc", params);
    Iterator iterator = rows.iterator();
    if (iterator.hasNext())
    {
      Map userIdMap = (Map)iterator.next();
      userId = (String)userIdMap.get("user_id");
    }
    return userId;
  }

  public String[] checkLogin(String loginName, String password)
  {
    String[] result = null;
    String sql = "select user_id, password from ldap_tbl where login_name=? order by user_id desc ";
    Object[] params = { loginName };
    List rows = this.jdbcTemplate.queryForList("select user_id, password from ldap_tbl where login_name=? order by user_id desc ", params);
    Iterator iterator = rows.iterator();
    if (iterator.hasNext())
    {
      result = new String[2];
      Map userIdMap = (Map)iterator.next();
      result[0] = ((String)userIdMap.get("user_id"));
      if (!password.equals(userIdMap.get("password"))) {
        result[1] = "1";
      } else {
        result[1] = "0";
      }
    }
    return result;
  }

  public SessionAttribute getSessionAttribute(String userId)
  {
    SessionAttribute sessionAttribute = null;
    logger.info("userId: " + userId);
    User user = this.userService.getUserById(userId);
    if (user != null)
    {
      Institute institute = null;
      if (Constants.SCHADMIN.equals(user.getSystemRole()))
      {
        Schadmin schadmin = (Schadmin)user;
        institute = schadmin.getInstitute();
      }
      else if (Constants.STUDENT.equals(user.getSystemRole()))
      {
        Student student = (Student)user;
        institute = student.getInstitute();
      }
      sessionAttribute = new SessionAttribute(userId, user.getNameEng(), user.getNameChi(), user.getSystemRole(), School.SCHOOL_CPCE.getNameEng(), School.SCHOOL_CPCE.getNameChi(), School.SCHOOL_CPCE.getSchoolId(), user.getUserLocale(), institute);

      List instituteList = new ArrayList();
      if (institute != null)
      {
        instituteList.add(new String[] { institute.getInstituteId(), institute.getInstitutePrefix() });
      }
      else
      {
        List list = this.instituteService.listAllInstitutes();
        Iterator it = list.iterator();
        while (it.hasNext())
        {
          Institute inst = (Institute)it.next();
          instituteList.add(new String[] { inst.getInstituteId(), inst.getInstitutePrefix() });
        }
      }
      sessionAttribute.setInstituteList(instituteList);
    }

    if (sessionAttribute == null) {
      logger.info("sessionAttribute is null: " + String.valueOf(sessionAttribute == null));
      return null;
    }
    logger.info("sessionAttribute userId: " + sessionAttribute.getUserId());
    logger.info(ToStringHelper.toString(sessionAttribute));
    return sessionAttribute;
  }

  public void setJdbcTemplate(JdbcTemplate template)
  {
    this.jdbcTemplate = template;
  }

  public void setUserService(UserService userService)
  {
    this.userService = userService;
  }
}


