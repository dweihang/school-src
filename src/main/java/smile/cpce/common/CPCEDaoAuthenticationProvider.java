//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.common;

import java.util.logging.Logger;
import org.acegisecurity.AuthenticationException;
import org.acegisecurity.AuthenticationServiceException;
import org.acegisecurity.providers.UsernamePasswordAuthenticationToken;
import org.acegisecurity.providers.dao.AbstractUserDetailsAuthenticationProvider;
import org.acegisecurity.userdetails.UserDetails;
import org.acegisecurity.userdetails.UserDetailsService;
import org.springframework.dao.DataAccessException;

public class CPCEDaoAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
  protected final Logger logger = Logger.getLogger("cpce");
  private UserDetailsService userDetailsService;

  public CPCEDaoAuthenticationProvider() {
  }

  protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
//    CPCEWebAuthenticationDetails details = (CPCEWebAuthenticationDetails)authentication.getDetails();
//    Map parameterMap = details.getRequestParameterMap();
//    logger.info("additionalAuthenticationChecks");
//    Object parameter = String.valueOf(System.currentTimeMillis());
//    if (parameter == null) {
//      logger.info("parameter is null");
//      throw new BadCredentialsException(this.messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Wrong parameter"), userDetails);
//    } else {
//      String time = (String)parameter;
//
//      long loginTime;
//      try {
//        loginTime = Long.parseLong(time);
//      } catch (NumberFormatException var12) {
//        logger.throwing(getClass().getName(), "additionalAuthenticationChecks", var12);
//        throw new BadCredentialsException(this.messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Wrong parameter"), userDetails);
//      }
//
//      String username = userDetails.getUsername();
//      if (Math.abs(System.currentTimeMillis() - loginTime) < 300000L) {
//        String password = Encoder.getMD5_Base64(username + Constants.APSS_TRANSFER_LOGIN_SECURITY_KEY + time);
//        String str = "provided password:" + authentication.getCredentials() + " || calculated password:" + password + " || username:" + username + " ||time:" + time + " || currentTime:" + System.currentTimeMillis();
//        if (!password.equals(authentication.getCredentials())) {
//          logger.info("bad credentials");
//          throw new BadCredentialsException(this.messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials: " + str), userDetails);
//        }
//      } else {
//        logger.info("bad credentials 2");
//        throw new BadCredentialsException(this.messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Time-out"), userDetails);
//      }
//    }
  }

  protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    UserDetails loadedUser;
    logger.info("retrieveUser");
    try {
      loadedUser = this.getUserDetailsService().loadUserByUsername(username);
    } catch (DataAccessException var5) {
      logger.throwing(getClass().getName(), "retrieveUser", var5);
      throw new AuthenticationServiceException(var5.getMessage(), var5);
    }

    if (loadedUser == null) {
      logger.info("loadedUser is null");
      throw new AuthenticationServiceException("AuthenticationDao returned null, which is an interface contract violation");
    } else {
      logger.info("return loaddedUser");
      return loadedUser;
    }
  }

  public UserDetailsService getUserDetailsService() {
    return this.userDetailsService;
  }

  public void setUserDetailsService(UserDetailsService authenticationDao) {
    this.userDetailsService = authenticationDao;
  }
}
