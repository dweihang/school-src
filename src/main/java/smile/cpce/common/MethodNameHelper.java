package smile.cpce.common;

/**
 * @author raymonddu 18/7/2018
 */
public class MethodNameHelper {

  private MethodNameHelper() {
  }

  public static String getMethodName() {
    return new Exception().getStackTrace()[0].toString();
  }

}
