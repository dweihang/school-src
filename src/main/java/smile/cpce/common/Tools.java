//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.springframework.jdbc.core.JdbcTemplate;

public class Tools {
  public Tools() {
  }

  public static String toJavascriptScriptString(String source) {
    StringBuffer sb = new StringBuffer();

    for(int i = 0; i < source.length(); ++i) {
      char ch = source.charAt(i);
      if (ch >= ' ' && ch <= '~' && ch != '\\' && ch != '"' && ch != '\'') {
        sb.append(ch);
      } else {
        sb.append("\\u");
        String code = Integer.toHexString(ch);

        for(int j = 0; j < 4 - code.length(); ++j) {
          sb.append('0');
        }

        sb.append(code);
      }
    }

    return sb.toString();
  }

  public static Timestamp getCurrentTimestamp() {
    return new Timestamp(System.currentTimeMillis());
  }

  public static Timestamp getTimestampFromString(String src) {
    try {
      return Timestamp.valueOf(src);
    } catch (Exception var2) {
      return null;
    }
  }

  public static void moveFile(File srcFile, File destDir) throws IOException {
    FileUtils.copyFileToDirectory(srcFile, destDir);
    FileUtils.forceDelete(srcFile);
  }

  public static void removeFile(File file) throws IOException {
    FileUtils.forceDelete(file);
  }

  public static boolean fileExists(String path) {
    try {
      File file = new File(path);
      return file.exists();
    } catch (Exception var2) {
      return false;
    }
  }

  public static String getSQLStatmentFromException(String s) {
    int startPos = s.indexOf("[");
    int endPos = s.indexOf("]");
    return startPos < endPos ? s.substring(startPos + 1, endPos) : "";
  }

  public static String formatDateTime(Timestamp t, String toFormat) {
    try {
      SimpleDateFormat output = new SimpleDateFormat(toFormat);
      Date date = new Date(t.getTime());
      return output.format(date);
    } catch (Exception var4) {
      return "";
    }
  }

  public static String formatDateTime(Date d, String toFormat) {
    try {
      SimpleDateFormat output = new SimpleDateFormat(toFormat);
      return output.format(d);
    } catch (Exception var3) {
      return "";
    }
  }

  public static String formatJSPString(String str) {
    if (str != null && str.trim().length() != 0) {
      str = str.replaceAll("[\\\\]", "\\\\\\\\");
      str = str.replaceAll("[']", "\\\\'");
      str = str.replaceAll("[\"]", "\\\\\"");
      return str;
    } else {
      return str;
    }
  }

  public static boolean checkAccessRightSubjectGroup(String userId, String role, String instituteId, String subjectGroupIds, JdbcTemplate jdbcTemplate) {
    if (subjectGroupIds == null) {
      return false;
    } else {
      String ids = subjectGroupIds.replaceAll("[,]", "','");
      ids = "'" + ids + "'";
      String[] id = subjectGroupIds.split(",");
      if (role.equals(Constants.SYSADMIN)) {
        return true;
      } else {
        String sql;
        if (role.equals(Constants.SCHADMIN)) {
          if (instituteId == null) {
            return true;
          } else {
            sql = "select count(*) from subject_group_tbl A join subject_tbl B  on A.subject_id = B.subject_id  where A.subject_group_id in (" + ids + ") and institute_id=? ";
            return jdbcTemplate.queryForInt(sql, new Object[]{instituteId}) >= id.length;
          }
        } else if (role.equals(Constants.TEACHER)) {
          sql = "select count(*) from teacher_manage_group_tbl where user_id = ? and subject_group_id in (" + ids + ")";
          return jdbcTemplate.queryForInt(sql, new Object[]{userId}) >= id.length;
        } else if (role.equals(Constants.STUDENT)) {
          sql = "select count(*) from student_belong_group_tbl where user_id = ? and subject_group_id in (" + ids + ")";
          return jdbcTemplate.queryForInt(sql, new Object[]{userId}) >= id.length;
        } else {
          return false;
        }
      }
    }
  }

  public static boolean checkAccessRightAnySubjectGroup(String userId, String role, String instituteId, String artsGroupId, JdbcTemplate jdbcTemplate) {
    if (artsGroupId == null) {
      return false;
    } else if (role.equals(Constants.SYSADMIN)) {
      return true;
    } else {
      String sql;
      if (role.equals(Constants.SCHADMIN)) {
        if (instituteId == null) {
          return true;
        } else {
          sql = "select count(*) from arts_group_tbl B  join subject_group_tbl C  on C.subject_group_id = B.subject_group_id  join subject_tbl D  on D.subject_id = C.subject_id  where B.arts_group_id = ?  and D.institute_id=? ";
          return jdbcTemplate.queryForInt(sql, new Object[]{artsGroupId, instituteId}) != 0;
        }
      } else if (role.equals(Constants.TEACHER)) {
        sql = "select count(*) from arts_group_tbl B  join teacher_manage_group_tbl C  on C.subject_group_id = B.subject_group_id  where C.user_id = ? and B.arts_group_id = ? ";
        return jdbcTemplate.queryForInt(sql, new Object[]{userId, artsGroupId}) != 0;
      } else {
        return false;
      }
    }
  }

  public static boolean checkAccessRightProgramme(String userId, String role, String instituteId, String programmeId, String academicYear, JdbcTemplate jdbcTemplate) {
    if (programmeId == null) {
      return false;
    } else if (role.equals(Constants.SYSADMIN)) {
      return true;
    }  else {
      String sql;
      if (role.equals(Constants.SCHADMIN)) {
        return true;
//        if (instituteId == null) {
//          return true;
//        } else {
//          sql = " select count(*) from programme_tbl A  where A.programme_id = ? and institute_id = ? ";
//          return jdbcTemplate.queryForInt(sql, new Object[]{programmeId, instituteId}) > 0;
//        }
      } else if (role.equals(Constants.TEACHER)) {
        sql = "select count(1) from teacher_class_master  where user_id = ? and academic_year = ? and programme_id = ? ";
        return jdbcTemplate.queryForInt(sql, new Object[]{userId, academicYear, programmeId}) > 0;
      } else {
        return false;
      }
    }
  }

  public static boolean checkAccessRightSubject(String userId, String role, String instituteId, String subjectId, JdbcTemplate jdbcTemplate) {
    if (subjectId == null) {
      return false;
    } else if (role.equals(Constants.SYSADMIN)) {
      return true;
    } else {
      String sql;
      if (role.equals(Constants.SCHADMIN)) {
        if (instituteId == null) {
          return true;
        } else {
          sql = "select count(*) from subject_tbl A  where A.subject_id = ?  and institute_id=? ";
          return jdbcTemplate.queryForInt(sql, new Object[]{subjectId, instituteId}) > 0;
        }
      } else if (role.equals(Constants.TEACHER)) {
        sql = "select count(*) from teacher_manage_group_tbl A join subject_group_tbl B on A.subject_group_id = B.subject_group_id  join subject_tbl C on C.subject_id = B.subject_id  where A.user_id = ? and C.subject_id = ?";
        return jdbcTemplate.queryForInt(sql, new Object[]{userId, subjectId}) > 0;
      } else if (role.equals(Constants.STUDENT)) {
        sql = "select count(1) cnt  from student_belong_group_tbl A join subject_group_tbl B on A.subject_group_id = B.subject_group_id where A.user_id = ?  and B.subject_id = ?  ";
        return jdbcTemplate.queryForInt(sql, new Object[]{userId, subjectId}) > 0;
      } else {
        return false;
      }
    }
  }

  public static String getFileEncoding(File f) {
    String charset = "BIG5";
    byte[] bytes = new byte[3];

    try {
      FileInputStream fis = new FileInputStream(f);
      fis.read(bytes);
      if (bytes[0] == -17 && bytes[1] == -69 && bytes[2] == -65) {
        charset = "UTF-8";
      }
    } catch (Exception var4) {
      var4.printStackTrace();
    }

    return charset;
  }

  public static boolean checkAccessRightCameraGroup(String userId, String role, String instituteId, String camerMarkingGroupId, JdbcTemplate jdbcTemplate) {
    if (camerMarkingGroupId != null && camerMarkingGroupId.length() != 0) {
      if (Constants.SYSADMIN.equals(role)) {
        return true;
      } else {
        String sql;
        if (role.equals(Constants.SCHADMIN)) {
          if (instituteId == null) {
            return true;
          }

          sql = "select count(1) from camera_marking_sg_tbl A join assignment_tbl B  on A.assignment_id = B.assignment_id  join subject_group_tbl C    on C.subject_group_id = B.subject_group_id  join subject_tbl D  on C.subject_id = D.subject_id  where A.marking_group_id = ? and D.institute_id=? ";
          if (jdbcTemplate.queryForInt(sql, new Object[]{camerMarkingGroupId, instituteId}) > 0) {
            return true;
          }
        } else if (role.equals(Constants.TEACHER)) {
          sql = "select count(1) from camera_marking_sg_tbl A where marker_user_id  = ? and marking_group_id = ? ";
          if (jdbcTemplate.queryForInt(sql, new Object[]{userId, camerMarkingGroupId}) > 0) {
            return true;
          }
        }

        return false;
      }
    } else {
      return false;
    }
  }

  public static boolean checkAccessRightByAssignmentId(String userId, String role, String instituteId, String assignmentId, JdbcTemplate jdbcTemplate) {
    if (assignmentId != null && assignmentId.length() != 0) {
      if (Constants.SYSADMIN.equals(role)) {
        return true;
      } else {
        String sql;
        if (role.equals(Constants.SCHADMIN)) {
          if (instituteId == null) {
            return true;
          }

          sql = "select count(1) from assignment_tbl A  join subject_group_tbl B on A.subject_group_id = B.subject_group_id  join subject_tbl C on B.subject_id = C.subject_id  where A.assignment_id = ? and C.institute_id = ? ";
          if (jdbcTemplate.queryForInt(sql, new Object[]{assignmentId, instituteId}) > 0) {
            return true;
          }
        } else if (role.equals(Constants.TEACHER)) {
          sql = "select count(1) from assignment_tbl A  join teacher_manage_group_tbl B on B.subject_group_id = A.subject_group_id  where A.assignment_id = ?  and B.user_id = ? ";
          if (jdbcTemplate.queryForInt(sql, new Object[]{assignmentId, userId}) > 0) {
            return true;
          }
        }

        return false;
      }
    } else {
      return false;
    }
  }

  public static boolean isTeacher(String role) {
    return Constants.TEACHER.equals(role);
  }

  public static boolean isSchoolAdmin(String role) {
    return Constants.SCHADMIN.equals(role);
  }

  public static boolean isSysAdmin(String role) {
    return Constants.SYSADMIN.equals(role);
  }

}
