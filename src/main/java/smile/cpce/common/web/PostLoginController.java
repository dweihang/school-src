//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.common.web;

import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.userdetails.UserDetails;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.mvc.AbstractController;
import smile.cpce.common.Constants;
import smile.cpce.common.ToStringHelper;
import smile.cpce.common.domain.SessionAttribute;
import smile.cpce.common.service.LoginService;

public class PostLoginController extends AbstractController {
  protected final Logger logger = Logger.getLogger("cpce");
  private LoginService loginService;

  public PostLoginController() {
  }

  protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
    this.logger.info("PostLoginController handleRequestInternal");
    Authentication auth = null;
    ModelAndView mav = null;
    auth = SecurityContextHolder.getContext().getAuthentication();
    UserDetails userDetails = (UserDetails)auth.getPrincipal();
    logger.info(ToStringHelper.toString(userDetails));
    if (!userDetails.isEnabled()) {
      mav = new ModelAndView("redirect:/");
      mav.addObject("status", "login.validate.noSuchUser");
    }

    String loginName = userDetails.getUsername();
    String password = userDetails.getPassword();
    String[] result = this.loginService.checkLogin(loginName, password);
    SessionAttribute sessionAttribute = null;
    if (result != null) {
      sessionAttribute = this.loginService.getSessionAttribute(result[0]);
    }

    if (sessionAttribute != null) {
      sessionAttribute.setAuthenticaledFromAD(result[1].equals("1"));
      request.getSession().setAttribute(Constants.SESSION_ATTRIBUTE_NAME, sessionAttribute);
      request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, sessionAttribute.getLocale());
      mav = new ModelAndView("/main/main");
    } else {
      HttpSession session = request.getSession(false);
      if (session != null) {
        this.logger.info("session is not null");
        session.invalidate();
      }

      mav = new ModelAndView("redirect:/");
      mav.addObject("status", "login.validate.noSuchUser");
    }

    return mav;
  }

  public void setLoginService(LoginService loginService) {
    this.loginService = loginService;
  }
}
