package smile.cpce.common;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.util.logging.Logger;

/**
 * @author raymonddu 18/7/2018
 */
public class ToStringHelper {

  private static Logger log = Logger.getLogger("cpce");

  private ToStringHelper() {
  }

  public static String toString(Object obj) {
    try {
      ObjectMapper om = new ObjectMapper();
      om.enable(SerializationFeature.INDENT_OUTPUT); //pretty print
      return om.writeValueAsString(obj);
    } catch (Exception e) {
      log.throwing(ToStringHelper.class.getName(), MethodNameHelper.getMethodName(), e);
      return "";
    }
  }

}
