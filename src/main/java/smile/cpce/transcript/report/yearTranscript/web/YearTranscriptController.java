//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.transcript.report.yearTranscript.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import smile.cpce.common.Constants;
import smile.cpce.common.Tools;
import smile.cpce.common.domain.SessionAttribute;
import smile.cpce.school.institute.domain.Institute;
import smile.cpce.transcript.classMaster.service.ClassMasterService;
import smile.cpce.transcript.programme.domain.Programme;
import smile.cpce.transcript.programme.service.ProgrammeService;
import smile.cpce.transcript.report.ReportUtil;
import smile.cpce.transcript.report.yearTranscript.service.YearTranscriptService;

public class YearTranscriptController extends SimpleFormController {
  private ProgrammeService programmeService;
  private YearTranscriptService yearTranscriptService;
  private ClassMasterService classMasterService;
  private Logger logger = Logger.getLogger("cpce");

  public YearTranscriptController() {
    this.setCommandName("yearTranscriptForm");
    this.setCommandClass(YearTranscriptForm.class);
    this.setFormView("transcript/report/yearTranscript/yearTranscript");
    this.setSuccessView("redirect:/transcript/report/yearTranscript/yearTranscript.htm");
  }

  protected Object formBackingObject(HttpServletRequest request) throws Exception {
    YearTranscriptForm form = new YearTranscriptForm();
    HttpSession session = request.getSession();
    SessionAttribute sessionAttribute = (SessionAttribute)session.getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
    String academicYear = sessionAttribute.getCurrentAcademicYear();
    String instituteId = sessionAttribute.getCurrentInstituteId();
    String userId = sessionAttribute.getUserId();
    String role = sessionAttribute.getRole();
    form.setAcademicYear(academicYear);
    String actionStatus = request.getParameter("actionStatus");
    List programmeList = null;
    if (Constants.SYSADMIN.equals(role)) {
      programmeList = this.programmeService.getAllByInstitute(instituteId);
    } else if (Constants.SCHADMIN.equals(role)) {
      Institute institute = sessionAttribute.getInstitute();
      if (institute == null || institute.getInstituteId().equals(instituteId)) {
        programmeList = this.programmeService.getAllByInstitute(instituteId);
      }
    } else {
      programmeList = this.classMasterService.getByUserIdInstituteAcademicYear(userId, instituteId, academicYear);
      if (programmeList != null && programmeList.size() == 1) {
        Programme programme = (Programme)programmeList.get(0);
        form.setProgrammeId(programme.getProgrammeId());
      }
    }

    request.setAttribute("programmeList", programmeList);
    return form;
  }

  protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
    ModelAndView mav = null;
    this.logger.info("onSubmit");
    YearTranscriptForm form = (YearTranscriptForm)command;
    HttpSession session = request.getSession();
    SessionAttribute sessionAttribute = (SessionAttribute)session.getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
    Institute institute = sessionAttribute.getInstitute();
    String updateBy = sessionAttribute.getUserId();
    String role = sessionAttribute.getRole();
    if (!Tools.checkAccessRightProgramme(updateBy, role, institute == null ? null : institute.getInstituteId(), form.getProgrammeId(), form.getAcademicYear(), Constants.jdbcTemplate)) {
      this.logger.info("Deny Access: user:" + updateBy + " || programmeId: " + form.getProgrammeId() + "||academicYear:" + form.getAcademicYear() + "||action:updateAttendence");
      mav = new ModelAndView(this.getFormView());
      mav.addObject("status", "cpce.exceptionCode.accessDenied");
      mav.addAllObjects(errors.getModel());
      return mav;
    } else {
      Map model = this.yearTranscriptService.genReport(form);
      boolean success = ((String)model.get("success")).equals("Y");
      if (success) {
        if (ReportUtil.fileExists((String)model.get("filepath"))) {
          Map h = new HashMap();
          h.put("filepath", model.get("filepath"));
          h.put("filename", model.get("filename"));
          h.put("extension", model.get("extension"));
          return new ModelAndView("redirect:/transcript/report/reportFileDownload.htm", h);
        }

        mav = new ModelAndView(this.getFormView());
        request.setAttribute("status", "N");
        mav.addAllObjects(errors.getModel());
      } else {
        mav = new ModelAndView(this.getFormView());
        request.setAttribute("status", model.get("status"));
        mav.addAllObjects(errors.getModel());
      }

      return mav;
    }
  }

  public void setProgrammeService(ProgrammeService programmeService) {
    this.programmeService = programmeService;
  }

  public void setYearTranscriptService(YearTranscriptService yearTranscriptService) {
    this.yearTranscriptService = yearTranscriptService;
  }

  public void setClassMasterService(ClassMasterService classMasterService) {
    this.classMasterService = classMasterService;
  }
}
