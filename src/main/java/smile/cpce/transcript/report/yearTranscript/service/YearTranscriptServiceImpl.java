//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.transcript.report.yearTranscript.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.springframework.jdbc.core.JdbcTemplate;
import smile.cpce.common.Constants;
import smile.cpce.common.MethodNameHelper;
import smile.cpce.transcript.report.ReportUtil;
import smile.cpce.transcript.report.yearTranscript.web.YearTranscriptForm;

public class YearTranscriptServiceImpl implements YearTranscriptService {
  private static final String RPT_NAME = "YearTranscript.pdf";
  private JdbcTemplate jdbcTemplate;
  private Logger log = Logger.getLogger("cpce");

  public YearTranscriptServiceImpl() {
  }

  public Map genReport(YearTranscriptForm yearTranscriptForm) {
    HashMap model = new HashMap();

    try {
      String subReportFilePath = ReportUtil.REPORT_TEMPLATE_PATH + "yearTranscript/yearTranscript_subreport1.jrxml";
      String subReport1FilePath = ReportUtil.REPORT_TEMPLATE_PATH + "yearTranscript/yearTranscript_subreport1_subreport1.jrxml";
      String subReport2FilePath = ReportUtil.REPORT_TEMPLATE_PATH + "yearTranscript/yearTranscript_subreport1_subreport2.jrxml";
      File reportOutputFile = new File(subReportFilePath.replace(".jrxml", ".jasper"));
      File report1OutputFile = new File(subReport1FilePath.replace(".jrxml", ".jasper"));
      File report2OutputFile = new File(subReport2FilePath.replace(".jrxml", ".jasper"));

      if (reportOutputFile.exists()) {
        log.info("Delete file " + reportOutputFile.getAbsolutePath());
        reportOutputFile.delete();
      }
      if (report1OutputFile.exists()) {
        log.info("Delete file " + report1OutputFile.getAbsolutePath());
        report1OutputFile.delete();
      }
      if (report2OutputFile.exists()) {
        log.info("Delete file " + report2OutputFile.getAbsolutePath());
        report2OutputFile.delete();
      }

      log.info("yearTranscript sub report path " + reportOutputFile.getAbsolutePath());
      log.info("yearTranscript sub report 1 path" + report1OutputFile.getAbsolutePath());
      log.info("yearTranscript sub report 2 path" + report2OutputFile.getAbsolutePath());

      JasperCompileManager.compileReportToFile(JRXmlLoader.load(new FileInputStream(subReport1FilePath)), report1OutputFile.getAbsolutePath());
      JasperCompileManager.compileReportToFile(JRXmlLoader.load(new FileInputStream(subReport2FilePath)), report2OutputFile.getAbsolutePath());
      JasperCompileManager.compileReportToFile(JRXmlLoader.load(new FileInputStream(subReportFilePath)), reportOutputFile.getAbsolutePath());

      InputStream is = new FileInputStream(ReportUtil.REPORT_TEMPLATE_PATH + ReportUtil.YEAR_TRANSCRIPT_RPT_PATH);
      Connection connection = this.jdbcTemplate.getDataSource().getConnection();
      Map reportParameter = new HashMap();
      reportParameter.put("para_academic_year", yearTranscriptForm.getAcademicYear());
      reportParameter.put("para_programme_id", yearTranscriptForm.getProgrammeId());
      reportParameter.put("para_date_of_issue", yearTranscriptForm.getDateOfIssue());
      reportParameter.put("para_exempt_mark", Constants.EXEMPT_MARK);
      reportParameter.put("para_exempt_grade", Constants.EXEMPT_STR);
      reportParameter.put("SUBREPORT_DIR", ReportUtil.REPORT_TEMPLATE_PATH + ReportUtil.YEAR_TRANSCRIPT_RPT_SAVE_PATH);
      JasperDesign jasperDesign = JRXmlLoader.load(is);
      JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, reportParameter, connection);
      if (connection != null) {
        connection.close();
      }

      Map h = ReportUtil.genReport(jasperPrint, ReportUtil.REPORT_SAVE_PATH + ReportUtil.YEAR_TRANSCRIPT_RPT_SAVE_PATH, "pdf");
      model.put("filepath", (String)h.get("filePath"));
      model.put("extension", (String)h.get("fileExtension"));
      model.put("filename", "YearTranscript.pdf");
      model.put("success", "Y");
    } catch (Exception var10) {
      log.throwing(this.getClass().getName(), MethodNameHelper.getMethodName(), var10);
      model.put("success", "N");
    }

    return model;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }
}
