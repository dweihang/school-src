package smile.cpce.transcript.studentMerit.web;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.servlet.mvc.SimpleFormController;
import smile.cpce.common.Constants;
import smile.cpce.common.domain.SessionAttribute;
import smile.cpce.school.institute.domain.Institute;
import smile.cpce.transcript.classMaster.service.ClassMasterService;
import smile.cpce.transcript.programme.domain.Programme;
import smile.cpce.transcript.programme.service.ProgrammeService;
import smile.cpce.transcript.studentMerit.web.DisplayStudentMeritForm.DisplayData;

/**
 * @author raymonddu 10/8/2018
 */
public class DisplayStudentMeritController extends SimpleFormController {

  private ClassMasterService classMasterService;

  private ProgrammeService programmeService;

  private JdbcTemplate jdbcTemplate;

  private Logger log = Logger.getLogger("cpce");

  private String sqlTemplate = "SELECT"
    + "  A.code as code,"
    + "  B.chi_desc as name,"
    + "  C.name_eng as teacherNameEng,"
    + "  D.student_no as studentNo,"
    + "  E.name_eng as nameEng,"
    + "  E.name_chi as nameChi"
    + "  FROM"
    + "  student_com_merit_tbl A"
    + "  LEFT JOIN student_tbl D on A.user_id = D.user_id"
    + "  LEFT JOIN student_com_merit_metadata_tbl B on A.code = B.code"
    + "  LEFT JOIN user_tbl C on A.teacher_user_id = C.user_id"
    + "  LEFT JOIN user_tbl E on A.user_id = E.user_id"
    + "  WHERE"
    + "  A.user_id in ("
    + "    SELECT"
    + "      user_id"
    + "    FROM"
    + "      student_enroll_programme_tbl"
    + "    WHERE"
    + "      student_enroll_programme_tbl.programme_id = ?"
    + "  ) AND"
    + " A.academic_year = ?";

  public DisplayStudentMeritController() {
    this.setCommandName("displayStudentMeritForm");
    this.setCommandClass(DisplayStudentMeritForm.class);
    this.setFormView("transcript/studentMerit/displayStudentMerit");
    this.setSuccessView("redirect:/transcript/studentMerit/displayStudentMerit.htm");
  }

  @Override
  protected Object formBackingObject(HttpServletRequest request) throws Exception {
    DisplayStudentMeritForm form = new DisplayStudentMeritForm();
    String paramAcademicYear = request.getParameter("paramAcademicYear");
    String paramProgrammeId = request.getParameter("paramProgrammeId");

    HttpSession session = request.getSession();
    SessionAttribute sessionAttribute = (SessionAttribute) session
      .getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
    String instituteId = sessionAttribute.getCurrentInstituteId();
    String academicYear = sessionAttribute.getCurrentAcademicYear();
    String userId = sessionAttribute.getUserId();
    String role = sessionAttribute.getRole();
    if (paramAcademicYear != null && !paramAcademicYear.equals("")) {
      form.setAcademicYear(paramAcademicYear);
    } else {
      form.setAcademicYear(academicYear);
    }

    List programmeList = null;
    if (Constants.SYSADMIN.equals(role)) {
      programmeList = this.programmeService.getAllByInstitute(instituteId);
    } else {
      Institute institute = sessionAttribute.getInstitute();
      if (institute == null || institute.getInstituteId().equals(instituteId)) {
        programmeList = this.programmeService.getAllByInstitute(instituteId);
      }
    }


    if (paramProgrammeId != null) {
      form.setProgrammeId(paramProgrammeId);
      String pragrammeName = null;
      for (Object programme : programmeList) {
        if (paramProgrammeId.equals(((Programme)programme).getProgrammeId())) {
          pragrammeName = ((Programme) programme).getProgrammeTitleEng();
          break;
        }
      }
      log.info(pragrammeName);
      List<Map> rows = jdbcTemplate.queryForList(sqlTemplate, new Object[]{paramProgrammeId, paramAcademicYear});
      List<DisplayData> displayDataList = new ArrayList<DisplayData>();
      for (Map row : rows) {
        DisplayData data = new DisplayData();
        data.setProgrammeName(pragrammeName);
        data.setCode((String)row.get("CODE"));
        data.setName((String)row.get("NAME"));
        data.setNameChi((String)row.get("NAMECHI"));
        data.setNameEng((String)row.get("NAMEENG"));
        data.setStudentNo((String)row.get("STUDENTNO"));
        data.setTeacherNameEng((String)row.get("TEACHERNAMEENG"));
        displayDataList.add(data);
      }
      displayDataList.sort((o1, o2) -> {
        if (o1.getStudentNo().compareTo(o2.getStudentNo()) == 0) {
          return o1.getCode().compareTo(o2.getCode());
        }
        return o1.getStudentNo().compareTo(o2.getStudentNo());
      });
      form.setData(displayDataList);
    }
    request.setAttribute("programmeList", programmeList);
    return form;
  }

  public ClassMasterService getClassMasterService() {
    return classMasterService;
  }

  public void setClassMasterService(
    ClassMasterService classMasterService) {
    this.classMasterService = classMasterService;
  }

  public ProgrammeService getProgrammeService() {
    return programmeService;
  }

  public void setProgrammeService(
    ProgrammeService programmeService) {
    this.programmeService = programmeService;
  }

  public JdbcTemplate getJdbcTemplate() {
    return jdbcTemplate;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

}
