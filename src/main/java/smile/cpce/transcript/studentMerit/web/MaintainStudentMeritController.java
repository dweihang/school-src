package smile.cpce.transcript.studentMerit.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import smile.cpce.common.Constants;
import smile.cpce.common.MethodNameHelper;
import smile.cpce.common.ToStringHelper;
import smile.cpce.common.Tools;
import smile.cpce.common.domain.SessionAttribute;
import smile.cpce.school.institute.domain.Institute;
import smile.cpce.transcript.classMaster.service.ClassMasterService;
import smile.cpce.transcript.permission.service.EditPermissionService;
import smile.cpce.transcript.permission.service.EditPermissionServiceImpl;
import smile.cpce.transcript.programme.domain.Programme;
import smile.cpce.transcript.programme.service.ProgrammeService;
import smile.cpce.transcript.student.domain.Student;
import smile.cpce.transcript.student.service.StudentService;
import smile.cpce.transcript.studentComment.domain.StudentMerit;
import smile.cpce.transcript.studentComment.domain.metaData.MeritAndDemerit;
import smile.cpce.transcript.studentComment.service.StudentCommentService;

/**
 * @author raymonddu 3/8/2018
 */
public class MaintainStudentMeritController extends SimpleFormController {

  private Logger log = Logger.getLogger("cpce");

  private StudentService studentService;

  private StudentCommentService studentCommentService;

  private ClassMasterService classMasterService;

  private ProgrammeService programmeService;

  private JdbcTemplate jdbcTemplate;

  public MaintainStudentMeritController() {
    this.setCommandName("studentMeritForm");
    this.setCommandClass(StudentMeritForm.class);
    this.setFormView("transcript/studentMerit/studentMerit");
    this.setSuccessView("redirect:/transcript/studentMerit/maintainStudentMerit.htm");
  }

  protected Map referenceData(HttpServletRequest request) {
    Map data = new HashMap();
    MeritAndDemerit meritAndDemerit = new MeritAndDemerit(this.jdbcTemplate);
    data.put("meritAndDemeritMap", meritAndDemerit.getMap());
    return data;
  }

  protected Object formBackingObject(HttpServletRequest request) throws Exception {
    StudentMeritForm form = new StudentMeritForm();
    String paramAcademicYear = request.getParameter("paramAcademicYear");
    String paramProgrammeId = request.getParameter("paramProgrammeId");
    String paramStudentNo = request.getParameter("paramStudentNo");
    HttpSession session = request.getSession();
    SessionAttribute sessionAttribute = (SessionAttribute) session
        .getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
    String instituteId = sessionAttribute.getCurrentInstituteId();
    String academicYear = sessionAttribute.getCurrentAcademicYear();
    String userId = sessionAttribute.getUserId();
    String role = sessionAttribute.getRole();
    if (paramAcademicYear != null && !paramAcademicYear.equals("")) {
      form.setAcademicYear(paramAcademicYear);
    } else {
      form.setAcademicYear(academicYear);
    }

    List programmeList = null;
    if (Constants.SYSADMIN.equals(role)) {
      programmeList = this.programmeService.getAllByInstitute(instituteId);
    } else {
      Institute institute = sessionAttribute.getInstitute();
      if (institute == null || institute.getInstituteId().equals(instituteId)) {
        programmeList = this.programmeService.getAllByInstitute(instituteId);
      }
    }

    List studentList = null;
    if (paramProgrammeId != null && !paramProgrammeId.equals("")) {
      form.setProgrammeId(paramProgrammeId);
      studentList = this.studentService.getByProgrammeId(paramProgrammeId);

      if (paramStudentNo != null && !paramStudentNo.equals("")) {
        Iterator<Student> studentIterator = studentList.iterator();
        while (studentIterator.hasNext()) {
          Student student = studentIterator.next();
          if (paramStudentNo.equals(student.getStudentNo())) {
            form.setSelectedStudentNo(student.getStudentNo());
            form.setSelectedStudentUserId(student.getUser().getUserId());
            form.setSelectedStudentNameChi(student.getUser().getNameChi());
            form.setSelectedStudentNameEng(student.getUser().getNameEng());
            List<StudentMerit> meritList = this.studentCommentService.getStudentMeritByIdAndTeacherUserId(academicYear, student.getUser().getUserId(), userId);
            MeritAndDemerit meritAndDemerit = new MeritAndDemerit(this.jdbcTemplate);
            Map<String, Integer> map = new HashMap<String, Integer>();

            Set<String> codes = meritAndDemerit.getMap().keySet();
            for (String code: codes) {
              map.put(code, 0);
            }

            for (StudentMerit studentMerit: meritList) {
              int count = map.get(studentMerit.getCode());
              count += 1;
              map.put(studentMerit.getCode(), count);
            }
            Map<String, String> output = new HashMap<String, String>();
            for (Entry<String, Integer> entry : map.entrySet()) {
              output.put(entry.getKey(), String.valueOf(entry.getValue()));
            }
            form.setMeritCodeCountMap(output);
            log.info(ToStringHelper.toString(form));
            break;
          }
        }
      }
    }

    request.setAttribute("programmeList", programmeList);
    request.setAttribute("studentList", studentList);
    return form;
  }

  protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response,
      Object command, BindException errors) throws Exception {
    ModelAndView mav = null;
    StudentMeritForm form = (StudentMeritForm) command;
    HttpSession session = request.getSession();
    SessionAttribute sessionAttribute = (SessionAttribute) session
        .getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
    Institute institute = sessionAttribute.getInstitute();
    String updateBy = sessionAttribute.getUserId();
    String role = sessionAttribute.getRole();
    EditPermissionService editPermissionService = new EditPermissionServiceImpl(jdbcTemplate);
    if (!(Tools.isTeacher(role) || Tools.isSchoolAdmin(role) || Tools.isSysAdmin(role)) || !editPermissionService.isMeritCanEdit()) {
      this.log.info("Deny Access: user:" + updateBy + " || programmeId: " + form.getProgrammeId()
          + "||academicYear:" + form.getAcademicYear() + "||action:updateAttendence");
      mav = new ModelAndView(this.getSuccessView());
      mav.addObject("status", "cpce.exceptionCode.accessDenied");
      mav.addAllObjects(errors.getModel());
      return mav;
    } else {
      mav = new ModelAndView(this.getSuccessView());
      try {
        int isSuccess = 0;

        String meritClass = getMeritClass();

        List<StudentMerit> meritList = new ArrayList<StudentMerit>();
        for (Entry<String, String> meritCodeEntry : form.getMeritCodeCountMap().entrySet()) {
          int count = Integer.parseInt(meritCodeEntry.getValue());
          if (count > 0) {
            for (int i = 0; i < count; i++) {
              StudentMerit merit = new StudentMerit();
              merit.setTeacherUserId(updateBy);
              merit.setCode(meritCodeEntry.getKey());
              merit.setAcademicYear(form.getAcademicYear());
              merit.setMeritClass(meritClass);
              merit.setUserId(form.getSelectedStudentUserId());
              meritList.add(merit);
            }
          }
        }
        isSuccess = this.studentCommentService.updateStudentMeritByIdAndTeacherUserId(form.getAcademicYear(),
          form.getSelectedStudentUserId(), updateBy, meritList);
        log.info("is success:" + String.valueOf(isSuccess));
        if (isSuccess == 0) {
          mav.addObject("actionStatus", "common.editSuccess");
        } else {
          mav.addObject("actionStatus", "common.editFail");
        }

        mav.addObject("paramAcademicYear", form.getAcademicYear());
        mav.addObject("paramProgrammeId", form.getProgrammeId());
      } catch (Exception e) {
        mav.addObject("actionStatus", "common.editFail");
        log.throwing(getClass().getName(), MethodNameHelper.getMethodName(), e);
      }
      return mav;
    }
  }

  public StudentService getStudentService() {
    return studentService;
  }

  public void setStudentService(StudentService studentService) {
    this.studentService = studentService;
  }

  public ClassMasterService getClassMasterService() {
    return classMasterService;
  }

  public void setClassMasterService(
      ClassMasterService classMasterService) {
    this.classMasterService = classMasterService;
  }

  public ProgrammeService getProgrammeService() {
    return programmeService;
  }

  public void setProgrammeService(
      ProgrammeService programmeService) {
    this.programmeService = programmeService;
  }

  public JdbcTemplate getJdbcTemplate() {
    return jdbcTemplate;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public StudentCommentService getStudentCommentService() {
    return studentCommentService;
  }

  public void setStudentCommentService(
      StudentCommentService studentCommentService) {
    this.studentCommentService = studentCommentService;
  }

  private String getMeritClass() {
    List rows = jdbcTemplate.queryForList("select * from student_com_merit_class_metadata_tbl order by code ");
    Iterator iterator = rows.iterator();

    int level = 1000;
    String meritClass = null;
    while(iterator.hasNext()) {
      Map map = (Map)iterator.next();
      log.info(ToStringHelper.toString(map));
      Integer meritClassLevel = (Integer) map.get("level");
      if (meritClassLevel < level) {
        String meritClassCode = (String)map.get("code");
        meritClass = meritClassCode;
      }
    }
    return meritClass;
  }
}
