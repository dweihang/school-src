package smile.cpce.transcript.permission.web;

import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import smile.cpce.transcript.permission.service.EditPermissionService;
import smile.cpce.transcript.permission.service.EditPermissionServiceImpl;

/**
 * @author raymonddu 22/9/2018
 */

public class EditPermissionController extends SimpleFormController {
  private Logger log = Logger.getLogger("cpce");
  private JdbcTemplate jdbcTemplate;

  public EditPermissionController() {
    this.setCommandName("editPermissionForm");
    this.setCommandClass(EditPermissionForm.class);
    this.setFormView("transcript/permission/editPermission");
    this.setSuccessView("redirect:/transcript/permission/editPermission.htm");
  }

  protected Object formBackingObject(HttpServletRequest request) throws Exception {
    EditPermissionForm form = new EditPermissionForm();
    EditPermissionService editPermissionService = new EditPermissionServiceImpl(this.jdbcTemplate);
    form.setIsExtraCurricularActivitiesCanEdit(editPermissionService.isExtraCurricularActivitiesCanEdit());
    form.setIsMeritCanEdit(editPermissionService.isMeritCanEdit());
    form.setIsPositionOfServiceCanEdit(editPermissionService.isPositionOfServiceCanEdit());
    form.setIsAttendenceCanEdit(editPermissionService.isAttendenceCanEdit());
    form.setIsStudentCommentCanEdit(editPermissionService.isStudentCommentCanEdit());
    return form;
  }

  protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
    EditPermissionForm form = (EditPermissionForm)command;
    EditPermissionService editPermissionService = new EditPermissionServiceImpl(this.jdbcTemplate);
    editPermissionService.setExtraCurricularActivitiesCanEdit(form.getIsExtraCurricularActivitiesCanEdit());
    editPermissionService.setMeritCanEdit(form.getIsMeritCanEdit());
    editPermissionService.setPositionOfServiceCanEdit(form.getIsPositionOfServiceCanEdit());
    editPermissionService.setAttendenceCanEdit(form.getIsAttendenceCanEdit());
    editPermissionService.setStudentCommentCanEdit(form.getIsStudentCommentCanEdit());
    ModelAndView modelAndView = new ModelAndView(this.getSuccessView());
    modelAndView.addObject("actionStatus", "common.editSuccess");
    return modelAndView;
  }

  public JdbcTemplate getJdbcTemplate() {
    return this.jdbcTemplate;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }
}
