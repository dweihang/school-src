package smile.cpce.transcript.permission.service;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author raymonddu 22/9/2018
 */
public class EditPermissionServiceImpl implements EditPermissionService {

  private static final String TABLE_NAME_MERIT = "student_com_merit_tbl";
  private static final String TABLE_NAME_POSITION = "student_com_position_tbl";
  private static final String TABLE_NAME_ACT = "student_com_act_tbl";
  private static final String TABLE_ATTENDENCE = "student_attendence_tbl";
  private static final String TABLE_STUDENT_COMMENT = "student_comment"; // NOT A TABLE NAME

  private JdbcTemplate jdbcTemplate;

  public EditPermissionServiceImpl(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public void setMeritCanEdit(boolean canEdit) {
    this.jdbcTemplate.execute(this.getSetterSql(TABLE_NAME_MERIT, canEdit));
  }

  public void setPositionOfServiceCanEdit(boolean canEdit) {
    this.jdbcTemplate.execute(this.getSetterSql(TABLE_NAME_POSITION, canEdit));
  }

  public void setExtraCurricularActivitiesCanEdit(boolean canEdit) {
    this.jdbcTemplate.execute(this.getSetterSql(TABLE_NAME_ACT, canEdit));
  }

  public void setAttendenceCanEdit(boolean canEdit) {
    this.jdbcTemplate.execute(this.getSetterSql(TABLE_ATTENDENCE, canEdit));
  }

  public void setStudentCommentCanEdit(boolean canEdit) {
    this.jdbcTemplate.execute(this.getSetterSql(TABLE_STUDENT_COMMENT, canEdit));
  }

  public boolean isMeritCanEdit() {
    List rows = this.jdbcTemplate.queryForList(this.getGetterSql(TABLE_NAME_MERIT));
    Iterator iterator = rows.iterator();
    if (iterator.hasNext()) {
      Map resultMap = (Map)iterator.next();
      Short canEdit = (Short)resultMap.get("can_edit");
      return canEdit == 1;
    } else {
      return false;
    }
  }

  public boolean isPositionOfServiceCanEdit() {
    List rows = this.jdbcTemplate.queryForList(this.getGetterSql(TABLE_NAME_POSITION));
    Iterator iterator = rows.iterator();
    if (iterator.hasNext()) {
      Map resultMap = (Map)iterator.next();
      Short canEdit = (Short)resultMap.get("can_edit");
      return canEdit == 1;
    } else {
      return false;
    }
  }

  public boolean isExtraCurricularActivitiesCanEdit() {
    List rows = this.jdbcTemplate.queryForList(this.getGetterSql(TABLE_NAME_ACT));
    Iterator iterator = rows.iterator();
    if (iterator.hasNext()) {
      Map resultMap = (Map)iterator.next();
      Short canEdit = (Short)resultMap.get("can_edit");
      return canEdit == 1;
    } else {
      return false;
    }
  }

  public boolean isAttendenceCanEdit() {
    List rows = this.jdbcTemplate.queryForList(this.getGetterSql(TABLE_ATTENDENCE));
    Iterator iterator = rows.iterator();
    if (iterator.hasNext()) {
      Map resultMap = (Map)iterator.next();
      Short canEdit = (Short)resultMap.get("can_edit");
      return canEdit == 1;
    } else {
      return false;
    }
  }

  public boolean isStudentCommentCanEdit() {
    List rows = this.jdbcTemplate.queryForList(this.getGetterSql(TABLE_STUDENT_COMMENT));
    Iterator iterator = rows.iterator();
    if (iterator.hasNext()) {
      Map resultMap = (Map) iterator.next();
      Short canEdit = (Short) resultMap.get("can_edit");
      return canEdit == 1;
    } else {
      return false;
    }
  }

  private String getSetterSql(String tableName, boolean canEdit) {
    String targetVal = canEdit ? "1" : "0";
    return "update transcript_edit_permission set can_edit=" + targetVal + " where table_name='" + tableName + "'IF @@ROWCOUNT=0  insert into transcript_edit_permission(can_edit) values(" + targetVal + ");";
  }

  private String getGetterSql(String tableName) {
    return "select can_edit from transcript_edit_permission where table_name='" + tableName + "';";
  }
}
