package smile.cpce.transcript.permission.service;

/**
 * @author raymonddu 22/9/2018
 */
public interface EditPermissionService {
  void setMeritCanEdit(boolean var1);

  void setPositionOfServiceCanEdit(boolean var1);

  void setExtraCurricularActivitiesCanEdit(boolean var1);

  void setAttendenceCanEdit(boolean var1);

  void setStudentCommentCanEdit(boolean var1);

  boolean isMeritCanEdit();

  boolean isPositionOfServiceCanEdit();

  boolean isExtraCurricularActivitiesCanEdit();

  boolean isAttendenceCanEdit();

  boolean isStudentCommentCanEdit();
}
