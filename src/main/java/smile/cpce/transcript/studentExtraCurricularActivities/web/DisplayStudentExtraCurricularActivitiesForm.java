package smile.cpce.transcript.studentExtraCurricularActivities.web;

import java.util.List;

/**
 * @author raymonddu 10/8/2018
 */
public class DisplayStudentExtraCurricularActivitiesForm {

  private String academicYear;

  private String programmeId;

  private List<DisplayData> data;

  public String getAcademicYear() {
    return academicYear;
  }

  public void setAcademicYear(String academicYear) {
    this.academicYear = academicYear;
  }

  public String getProgrammeId() {
    return programmeId;
  }

  public void setProgrammeId(String programmeId) {
    this.programmeId = programmeId;
  }

  public List<DisplayData> getData() {
    return data;
  }

  public void setData(
    List<DisplayData> data) {
    this.data = data;
  }

  public static class DisplayData {

    private String userId;

    private String programmeName;

    private String studentNo;

    private String nameEng;

    private String nameChi;

    private String teacherId;

    private String teacherNameEng;

    private String name;

    private String code;

    public String getUserId() {
      return userId;
    }

    public void setUserId(String userId) {
      this.userId = userId;
    }

    public String getProgrammeName() {
      return programmeName;
    }

    public void setProgrammeName(String programmeName) {
      this.programmeName = programmeName;
    }

    public String getStudentNo() {
      return studentNo;
    }

    public void setStudentNo(String studentNo) {
      this.studentNo = studentNo;
    }

    public String getNameEng() {
      return nameEng;
    }

    public void setNameEng(String nameEng) {
      this.nameEng = nameEng;
    }

    public String getNameChi() {
      return nameChi;
    }

    public void setNameChi(String nameChi) {
      this.nameChi = nameChi;
    }

    public String getTeacherId() {
      return teacherId;
    }

    public void setTeacherId(String teacherId) {
      this.teacherId = teacherId;
    }

    public String getTeacherNameEng() {
      return teacherNameEng;
    }

    public void setTeacherNameEng(String teacherNameEng) {
      this.teacherNameEng = teacherNameEng;
    }

    public String getName() {
      return name;
    }

    public void setName(String meritName) {
      this.name = meritName;
    }

    public String getCode() {
      return code;
    }

    public void setCode(String code) {
      this.code = code;
    }
  }
}
