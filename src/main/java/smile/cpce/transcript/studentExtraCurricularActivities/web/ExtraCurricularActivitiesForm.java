package smile.cpce.transcript.studentExtraCurricularActivities.web;

import java.util.Map;

/**
 * @author raymonddu 8/8/2018
 */
public class ExtraCurricularActivitiesForm {
  private String academicYear;

  private String programmeId;

  private String selectedStudentNo;

  private String selectedStudentUserId;

  private String selectedStudentNameEng;

  private String selectedStudentNameChi;

  private Map<String, String> extraCurricularActivitiesCodeCountMap;

  public ExtraCurricularActivitiesForm() {
  }

  public String getProgrammeId() {
    return this.programmeId;
  }

  public void setProgrammeId(String programmeId) {
    this.programmeId = programmeId;
  }

  public String getAcademicYear() {
    return this.academicYear;
  }

  public void setAcademicYear(String academicYear) {
    this.academicYear = academicYear;
  }

  public Map<String, String> getExtraCurricularActivitiesCodeCountMap() {
    return extraCurricularActivitiesCodeCountMap;
  }

  public void setExtraCurricularActivitiesCodeCountMap(Map<String, String> extraCurricularActivitiesCodeCountMap) {
    this.extraCurricularActivitiesCodeCountMap = extraCurricularActivitiesCodeCountMap;
  }

  public String getSelectedStudentNo() {
    return selectedStudentNo;
  }

  public void setSelectedStudentNo(String selectedStudentNo) {
    this.selectedStudentNo = selectedStudentNo;
  }

  public String getSelectedStudentNameEng() {
    return selectedStudentNameEng;
  }

  public void setSelectedStudentNameEng(String selectedStudentNameEng) {
    this.selectedStudentNameEng = selectedStudentNameEng;
  }

  public String getSelectedStudentNameChi() {
    return selectedStudentNameChi;
  }

  public void setSelectedStudentNameChi(String selectedStudentNameChi) {
    this.selectedStudentNameChi = selectedStudentNameChi;
  }

  public String getSelectedStudentUserId() {
    return selectedStudentUserId;
  }

  public void setSelectedStudentUserId(String selectedStudentUserId) {
    this.selectedStudentUserId = selectedStudentUserId;
  }
}
