package smile.cpce.transcript.commentMetadata.domain;

/**
 * @author raymonddu 6/8/2018
 */
public class MeritClassMetadata extends Metadata {
  private int level;
  private int upgradeTo;

  public MeritClassMetadata() {

  }

  /**
   * @return the level
   */
  public int getLevel() {
    return level;
  }

  /**
   * @param level the level to set
   */
  public void setLevel(int level) {
    this.level = level;
  }

  /**
   * @param upgradeTo the upgradeTo to set
   */
  public void setUpgradeTo(int upgradeTo) {
    this.upgradeTo = upgradeTo;
  }

  /**
   * @return the upgradeTo
   */
  public int getUpgradeTo() {
    return upgradeTo;
  }
}
