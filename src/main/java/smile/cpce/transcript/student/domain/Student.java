//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.transcript.student.domain;

import smile.cpce.transcript.programme.domain.Programme;
import smile.cpce.transcript.teacher.domain.User;

public class Student {
  private String userId;
  private String studentNo;
  private String hkidOrPassport;
  private User user;
  private Programme programme;

  public Student() {
  }

  public Programme getProgramme() {
    return this.programme;
  }

  public void setProgramme(Programme programme) {
    this.programme = programme;
  }

  public User getUser() {
    return this.user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getUserId() {
    return this.userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getStudentNo() {
    return this.studentNo;
  }

  public void setStudentNo(String studentNo) {
    this.studentNo = studentNo;
  }

  public String getHkidOrPassport() {
    return this.hkidOrPassport;
  }

  public void setHkidOrPassport(String hkidOrPassport) {
    this.hkidOrPassport = hkidOrPassport;
  }
}
