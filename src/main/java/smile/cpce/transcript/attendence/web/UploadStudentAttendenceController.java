package smile.cpce.transcript.attendence.web;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import smile.cpce.common.Constants;
import smile.cpce.common.MethodNameHelper;
import smile.cpce.common.ToStringHelper;
import smile.cpce.common.Tools;
import smile.cpce.common.domain.SessionAttribute;
import smile.cpce.school.institute.domain.Institute;
import smile.cpce.school.studentAttendence.domain.AttendenceForm;
import smile.cpce.school.studentAttendence.domain.AttendenceObject;
import smile.cpce.school.studentAttendence.service.StudentAttendenceService;
import smile.cpce.transcript.programme.service.ProgrammeService;
import smile.cpce.transcript.programme.domain.Programme;

/**
 * @author raymonddu 25/7/2018
 */
public class UploadStudentAttendenceController implements Controller {

  protected final Logger logger = Logger.getLogger("cpce");

  private StudentAttendenceService studentAttendenceService;

  private ProgrammeService programmeService;

  private static final String KW_CLASS = "班別";

  private static final String KW_STUDENT_NO = "班號";

  private static final String KW_STUDENT_ENG_NAME = "英文名稱";

  private static final String KW_LATE_FOR_CLASS = "遲到";

  private static final String KW_EARLY_LEAVE = "早退";

  private static final String KW_ABSENCE = "缺席";


  public UploadStudentAttendenceController() {
  }

  public void setStudentAttendenceService(StudentAttendenceService studentAttendenceService) {
    this.studentAttendenceService = studentAttendenceService;
  }

  public void setProgrammeService(ProgrammeService programmeService) {
    this.programmeService = programmeService;
  }

  @Override
  public ModelAndView handleRequest(HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse) throws Exception {
    try {
      List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory())
          .parseRequest(httpServletRequest);
      String academicYear = httpServletRequest.getParameter("paramAcademicYear");
      String semester = httpServletRequest.getParameter("paramSemester");

      if (academicYear == null || academicYear.trim().isEmpty()) {
        ModelAndView mav = new ModelAndView(
          "redirect:/transcript/studentAttendence/maintainStudentAttendence.htm");
        mav.addObject("status", "cpce.error.studentAttendence.upload.noAcademicYear");
        return mav;
      }

      if (semester == null || semester.trim().isEmpty()) {
        ModelAndView mav = new ModelAndView(
          "redirect:/transcript/studentAttendence/maintainStudentAttendence.htm");
        mav.addObject("status", "cpce.error.studentAttendence.upload.noSemester");
        return mav;
      }

      studentAttendenceService.checkSemesterExist(academicYear, semester);

      HttpSession session = httpServletRequest.getSession();
      SessionAttribute sessionAttribute = (SessionAttribute) session
        .getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
      String updateBy = sessionAttribute.getUserId();

      String role = sessionAttribute.getRole();
      Institute institute = sessionAttribute.getInstitute();

      AttendenceForm attendenceForm = null;
      for (FileItem item : items) {
        if (!item.isFormField()) {
          // Process form file field (input type="file").
          String fieldName = item.getFieldName();
          String fileName = FilenameUtils.getName(item.getName());

          if (!fileName.endsWith(".xls")) {
            ModelAndView mav = new ModelAndView(
                "redirect:/transcript/studentAttendence/maintainStudentAttendence.htm");
            mav.addObject("status", "cpce.error.studentAttendence.upload.invalidFileFormat");
            return mav;
          }

          logger.info(String.format("field:%s, file=%s", fieldName, fileName));

          InputStream fileInputStream = item.getInputStream();
          HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);

          HSSFSheet worksheet = workbook.getSheetAt(0);
          Iterator<HSSFRow> rows = worksheet.rowIterator();

          List attendanceList = null;

          int studentNoColumnIndex = -1;
          int engNameColumnIndex = -1;
          int lateForClassColumnIndex = -1;
          int earlyLeaveColumnIndex = -1;
          int absenceColumnIndex = -1;
          boolean isProgrammeFound = false;
          String studentNoPrefix = "";
          String programmeId = "";

          boolean isColumnIndexFound = false;
          String studentNoBridgeSymbol = "-";
          while (rows.hasNext()) {
            Iterator<HSSFCell> cells = rows.next().cellIterator();
            int column = 0;
            AttendenceObject attendence = null;
            if (isColumnIndexFound) {
              attendence = new AttendenceObject();
            }
            while (cells.hasNext()) {
              HSSFCell cell = cells.next();
              String cellVal = null;

              if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                cellVal = String.valueOf(cell.getNumericCellValue()).trim().toLowerCase();
              } else if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING){
                cellVal = cell.getStringCellValue().trim().toLowerCase();
              } else {
                continue;
              }

              if (!isColumnIndexFound) {
                String cellValWithNoSpace = cellVal.replace(" ", "");
                if (cellValWithNoSpace.equals(KW_STUDENT_NO)) {
                  studentNoColumnIndex = column;
                } else if (cellValWithNoSpace.equals(KW_STUDENT_ENG_NAME)) {
                  engNameColumnIndex = column;
                }  else if (cellValWithNoSpace.equals(KW_LATE_FOR_CLASS)) {
                  lateForClassColumnIndex = column;
                } else if (cellValWithNoSpace.equals(KW_EARLY_LEAVE)) {
                  earlyLeaveColumnIndex = column;
                } else if (cellValWithNoSpace.equals(KW_ABSENCE)) {
                  absenceColumnIndex = column;
                } else if (cellValWithNoSpace.startsWith(KW_CLASS)) {
                  isProgrammeFound = true;
                } else if (isProgrammeFound) {
                  if (cellValWithNoSpace.length() == 3) {
                    String programmeCode = cellValWithNoSpace.substring(0, 2).toUpperCase();
                    String programmeStreamCode = cellValWithNoSpace.substring(1).toUpperCase();
                    List<Programme> programmes = programmeService.getAll();
                    for (Programme programme: programmes) {
                      if (programmeCode.equals(programme.getProgrammeCode()) && programmeStreamCode.equals(programme.getStreamCode())) {
                        studentNoPrefix = programmeCode + studentNoBridgeSymbol + programmeStreamCode + studentNoBridgeSymbol;
                        programmeId = programme.getProgrammeId();
                      }
                    }
                    if (!Tools.checkAccessRightProgramme(updateBy, role,
                      institute == null ? null : institute.getInstituteId(),
                      programmeId, academicYear,
                      Constants.jdbcTemplate)) {
                      this.logger.info(
                        "Deny Access: user:" + updateBy + " || programmeId: " +
                          programmeId + "||academicYear:" + academicYear
                          + "||action:updateAttendence");
                      ModelAndView mav = new ModelAndView(
                        "redirect:/transcript/studentAttendence/maintainStudentAttendence.htm");
                      mav.addObject("status", "cpce.exceptionCode.accessDenied");
                      return mav;
                    }
                  }
                  isProgrammeFound = false;
                }

                if (studentNoColumnIndex >= 0 && engNameColumnIndex >= 0 && lateForClassColumnIndex >= 0
                  && earlyLeaveColumnIndex >=0 && absenceColumnIndex >= 0 && !programmeId.isEmpty()) {
                  isColumnIndexFound = true;

                  attendenceForm = new AttendenceForm();
                  attendenceForm.setAcademicYear(academicYear);
                  attendenceForm.setProgrammeId(programmeId);
                  attendenceForm.setSemester(semester);
                  attendanceList = new ArrayList();
                }
              } else {
                if (column == studentNoColumnIndex) {
                  String noStr = studentNoPrefix;
                  try {
                    int no = (int)Double.parseDouble(cellVal);
                    if (no <= 9) {
                      noStr = noStr + "0" + String.valueOf(no);
                    } else {
                      noStr = noStr + String.valueOf(no);
                    }
                  } catch (Exception e) {
                    logger.severe(e.getMessage());
                  }
                  attendence
                      .setStudentId(studentAttendenceService.getStudentIdByStudentNo(noStr, programmeId));
                  attendence.setStudentNo(noStr);

                  if (attendence.getStudentNo() == null || attendence.getStudentId() == null) {
                    ModelAndView mav = new ModelAndView(
                      "redirect:/transcript/studentAttendence/maintainStudentAttendence.htm");
                    mav.addObject("status", "cpce.error.studentAttendence.upload.invalidFileContent");
                    return mav;
                  }
                }

                if (column == engNameColumnIndex) {
                  attendence.setStudentEngName(cellVal);
                }

                if (column == lateForClassColumnIndex) {
                  attendence.setLateForClass(Float.valueOf(cellVal));
                }

                if (column == earlyLeaveColumnIndex) {
                  attendence.setEarlyLeave(Float.valueOf(cellVal));
                }

                if (column == absenceColumnIndex) {
                  attendence.setAbsence(Float.valueOf(cellVal));
                }

                attendence.setSkipRanking("0");
              }
              column++;
            }
            if (attendence != null) {
              attendanceList.add(attendence);
            }
          }

          if (!isColumnIndexFound) {
            ModelAndView mav = new ModelAndView(
              "redirect:/transcript/studentAttendence/maintainStudentAttendence.htm");
            mav.addObject("status", "cpce.error.studentAttendence.upload.invalidFileContent");
            return mav;
          }

          attendenceForm.setAttendenceList(attendanceList);

          studentAttendenceService
              .deleteAttendenceListByProgrammeAcademicYearSemester(programmeId, academicYear,
                  semester);
          studentAttendenceService.updateAttendence(attendenceForm, updateBy);

          ModelAndView mav = new ModelAndView(
              "redirect:/transcript/studentAttendence/maintainStudentAttendence.htm");
          mav.addObject("status", "common.editSuccess");
          return mav;
        }
      }

      logger.info(ToStringHelper.toString(attendenceForm));

      ModelAndView mav = new ModelAndView(
          "redirect:/transcript/studentAttendence/maintainStudentAttendence.htm");
      mav.addObject("status", "cpce.error.studentAttendence.upload.invalidRequest");
      return mav;

    } catch(Exception e){
        logger.throwing(this.getClass().getName(), MethodNameHelper.getMethodName(), e);
        ModelAndView mav = new ModelAndView(
            "redirect:/transcript/studentAttendence/maintainStudentAttendence.htm");
        mav.addObject("status", "cpce.error.studentAttendence.upload.invalidRequest");
        return mav;
      }
    }
  }
