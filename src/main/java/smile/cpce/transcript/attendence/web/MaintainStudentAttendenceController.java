//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.transcript.attendence.web;

import java.util.List;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.validation.BindException;
import org.springframework.web.bind.RequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import smile.cpce.common.Constants;
import smile.cpce.common.Tools;
import smile.cpce.common.domain.SessionAttribute;
import smile.cpce.common.exception.CpceException;
import smile.cpce.db.exception.Student_attendence_tblException;
import smile.cpce.school.institute.domain.Institute;
import smile.cpce.school.studentAttendence.domain.AttendenceForm;
import smile.cpce.school.studentAttendence.service.StudentAttendenceService;
import smile.cpce.transcript.classMaster.service.ClassMasterService;
import smile.cpce.transcript.programme.domain.Programme;
import smile.cpce.transcript.programme.service.ProgrammeService;
import smile.cpce.transcript.semester.service.SemesterService;

public class MaintainStudentAttendenceController extends SimpleFormController {
  protected final Logger logger = Logger.getLogger("cpce");
  private StudentAttendenceService studentAttendenceService;
  private SemesterService semesterService;
  private ClassMasterService classMasterService;
  private ProgrammeService programmeService;

  public void setSemesterService(SemesterService semesterService) {
    this.semesterService = semesterService;
  }

  public void setClassMasterService(ClassMasterService classMasterService) {
    this.classMasterService = classMasterService;
  }

  public void setProgrammeService(ProgrammeService programmeService) {
    this.programmeService = programmeService;
  }

  public void setStudentAttendenceService(StudentAttendenceService studentAttendenceService) {
    this.studentAttendenceService = studentAttendenceService;
  }

  public MaintainStudentAttendenceController() {
    this.setCommandName("attendenceForm");
    this.setCommandClass(AttendenceForm.class);
    this.setFormView("/transcript/studentAttendence/manageStudentAttendence");
  }

  protected Object formBackingObject(HttpServletRequest request) {
    AttendenceForm attendenceForm = null;
    String instituteId;
    if (!this.isFormSubmission(request)) {
      HttpSession session = request.getSession();
      SessionAttribute sessionAttribute = (SessionAttribute)session.getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
      instituteId = sessionAttribute.getCurrentInstituteId();
      String academicYear = sessionAttribute.getCurrentAcademicYear();
      String semester = sessionAttribute.getCurrentSemester();
      String userId = sessionAttribute.getUserId();
      String role = sessionAttribute.getRole();
      String paramProgrammeId = request.getParameter("paramProgrammeId");
      String paramAcademicYear = RequestUtils.getStringParameter(request, "paramAcademicYear", academicYear);
      String paramSemester = RequestUtils.getStringParameter(request, "paramSemester", semester);
      List programmeList = null;
      if (Constants.SYSADMIN.equals(role)) {
        programmeList = this.programmeService.getAllByInstitute(instituteId);
      } else  {
        Institute institute = sessionAttribute.getInstitute();
        if (institute == null || institute.getInstituteId().equals(instituteId)) {
          programmeList = this.programmeService.getAllByInstitute(instituteId);
        }
      }

      if (paramProgrammeId != null && paramProgrammeId.length() > 0) {
        attendenceForm = this.studentAttendenceService.getAttendenceListByProgrammeAcademicYearSemester(paramProgrammeId, paramAcademicYear, paramSemester);
      } else {
        attendenceForm = new AttendenceForm();
        attendenceForm.setAcademicYear(academicYear);
        attendenceForm.setSemester(semester);
      }

      List semesterList = this.semesterService.getSemesterByAcademicYearInstituteId(academicYear, instituteId);
      request.setAttribute("semesterList", semesterList);
      request.setAttribute("programmeList", programmeList);
    } else {
      String programmeId = request.getParameter("programmeId");
      String academicYear = request.getParameter("academicYear");
      instituteId = request.getParameter("semester");
      attendenceForm = this.studentAttendenceService.getAttendenceListByProgrammeAcademicYearSemester(programmeId, academicYear, instituteId);
    }

    return attendenceForm;
  }

  protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) {
    AttendenceForm attendenceForm = (AttendenceForm)command;
    this.logger.info("in onsubmit");
    HttpSession session = request.getSession();
    SessionAttribute sessionAttribute = (SessionAttribute)session.getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
    String updateBy = sessionAttribute.getUserId();
    String role = sessionAttribute.getRole();
    Institute institute = sessionAttribute.getInstitute();
    ModelAndView mav;
    if (!Tools.checkAccessRightProgramme(updateBy, role, institute == null ? null : institute.getInstituteId(), attendenceForm.getProgrammeId(), attendenceForm.getAcademicYear(), Constants.jdbcTemplate)) {
      this.logger.info("Deny Access: user:" + updateBy + " || programmeId: " + attendenceForm.getProgrammeId() + "||academicYear:" + attendenceForm.getAcademicYear() + "||action:updateAttendence");
      mav = new ModelAndView(this.getFormView());
      mav.addObject("status", "cpce.exceptionCode.accessDenied");
      mav.addAllObjects(errors.getModel());
      return mav;
    } else {
      try {
        this.studentAttendenceService.updateAttendence(attendenceForm, updateBy);
      } catch (Student_attendence_tblException var13) {
        var13.printStackTrace();
        mav = new ModelAndView(this.getFormView());
        mav.addObject("status", "common.editFail");
        mav.addAllObjects(errors.getModel());
        return mav;
      } catch (CpceException var14) {
        var14.printStackTrace();
        mav = new ModelAndView(this.getFormView());
        mav.addObject("status", "common.editFail");
        mav.addAllObjects(errors.getModel());
        return mav;
      }

      mav = new ModelAndView("redirect:/transcript/studentAttendence/maintainStudentAttendence.htm");
      mav.addObject("status", "common.editSuccess");
      return mav;
    }
  }
}
