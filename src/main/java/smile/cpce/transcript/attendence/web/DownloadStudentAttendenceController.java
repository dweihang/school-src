//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.transcript.attendence.web;

import static org.apache.poi.hssf.usermodel.HSSFCell.CELL_TYPE_BLANK;
import static org.apache.poi.hssf.usermodel.HSSFCell.ENCODING_UTF_16;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import smile.cpce.common.MethodNameHelper;
import smile.cpce.common.web.DownloadExcelWorkbookView;
import smile.cpce.school.studentAttendence.domain.AttendenceForm;
import smile.cpce.school.studentAttendence.domain.AttendenceObject;
import smile.cpce.school.studentAttendence.service.StudentAttendenceService;
import smile.cpce.transcript.programme.domain.Programme;
import smile.cpce.transcript.programme.service.ProgrammeService;

public class DownloadStudentAttendenceController implements Controller {

  protected final Logger logger = Logger.getLogger("cpce");
  private StudentAttendenceService studentAttendenceService;
  private ProgrammeService programmeService;

  private JdbcTemplate jdbcTemplate;

  public DownloadStudentAttendenceController() {
  }

  public void setStudentAttendenceService(StudentAttendenceService studentAttendenceService) {
    this.studentAttendenceService = studentAttendenceService;
  }

  public void setProgrammeService(ProgrammeService programmeService) {
    this.programmeService = programmeService;
  }

  public JdbcTemplate getJdbcTemplate() {
    return jdbcTemplate;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public ModelAndView handleRequest(HttpServletRequest request,
    HttpServletResponse httpServletResponse) throws Exception {
    String programmeId = request.getParameter("paramProgrammeId");
    String academicYear = request.getParameter("paramAcademicYear");
    String instituteId = request.getParameter("paramSemester");
    AttendenceForm attendenceForm = this.studentAttendenceService
      .getAttendenceListByProgrammeAcademicYearSemester(programmeId, academicYear, instituteId);
    Programme programme = this.programmeService.getById(programmeId);
    attendenceForm.setProgrammeCode(programme.getProgrammeTitleEng());

    HSSFWorkbook workbook = this.exportStudentAttendence(attendenceForm, instituteId);
    Map model = new HashMap();
    model.put("workbook", workbook);
    model.put("filename",
      "studentAttendance-" + academicYear + "-" + instituteId + "-" + programme.getProgrammeTitleEng() + ".xls");
    return new ModelAndView(new DownloadExcelWorkbookView(), model);
  }

  public HSSFWorkbook exportStudentAttendence(AttendenceForm attendenceForm, String instituteId) {
    try {
      HSSFWorkbook workbook = new HSSFWorkbook();
      HSSFSheet sheet = workbook.createSheet();
      ResourceBundle res = ResourceBundle.getBundle("messages");

      short next_line_no = -1;
      short next_column_no = -1;
      next_line_no = (short) (next_line_no + 1);
      HSSFRow row = sheet.createRow(next_line_no);
      next_column_no = (short) (next_column_no + 1);
      HSSFCell cell = row.createCell(next_column_no);
      cell.setCellType(3);
      cell.setEncoding(ENCODING_UTF_16);
      cell.setCellValue("民生書院小學");

      ++next_line_no;
      ++next_line_no;
      row = sheet.createRow(next_line_no);
      next_column_no = -1;
      next_column_no = (short) (next_column_no + 1);

      cell = row.createCell(next_column_no);
      cell.setCellType(3);
      cell.setEncoding(ENCODING_UTF_16);
      cell.setCellValue("班別:");

      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(3);
      cell.setEncoding(ENCODING_UTF_16);
      cell.setCellValue(
        attendenceForm.getProgrammeCode().substring(0, 2) + attendenceForm.getProgrammeCode()
          .substring(attendenceForm.getProgrammeCode().length() - 1));

      ++next_line_no;
      row = sheet.createRow(next_line_no);
      next_column_no = -1;
      next_column_no = (short) (next_column_no + 1);

      cell = row.createCell(next_column_no);
      cell.setCellType(3);
      cell.setEncoding(ENCODING_UTF_16);
      cell.setCellValue("日期:");

      String dateFrom = "NULL";
      String dateTo = "NULL";
      List rows = this.jdbcTemplate.queryForList(
        "select date_from, date_to from semester_tbl where institute_id=? and academic_year=? and semester=?",
        new Object[]{instituteId, attendenceForm.getAcademicYear(), attendenceForm.getSemester()});

      Iterator iterator = rows.iterator();
      if (iterator.hasNext()) {
        Map userIdMap = (Map) iterator.next();
        Date fromDate = ((Date) userIdMap.get("date_from"));
        if (fromDate != null) {
          Calendar calendar = Calendar.getInstance();
          calendar.setTime(fromDate);
          dateFrom = String.valueOf(calendar.get(Calendar.YEAR)) + "-" + String
            .valueOf(calendar.get(Calendar.MONTH) + 1);
        }
        Date toDate = ((Date) userIdMap.get("date_to"));
        if (toDate != null) {
          Calendar calendar = Calendar.getInstance();
          calendar.setTime(toDate);
          dateTo = String.valueOf(calendar.get(Calendar.YEAR)) + "-" + String
            .valueOf(calendar.get(Calendar.MONTH) + 1);
        }
      }

      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(3);
      cell.setEncoding(ENCODING_UTF_16);
      cell.setCellValue("由 " + dateFrom + " 到 " + dateTo);

      ++next_line_no;
      row = sheet.createRow(next_line_no);
      next_column_no = -1;
      next_column_no = (short) (next_column_no + 1);

      cell = row.createCell(next_column_no);
      cell.setCellType(3);
      cell.setEncoding(ENCODING_UTF_16);
      cell.setCellValue("原因:");

      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(3);
      cell.setEncoding(ENCODING_UTF_16);
      cell.setCellValue("全部");

      ++next_line_no;
      ++next_line_no;
      sheet.createRow(next_line_no);

      ++next_line_no;
      row = sheet.createRow(next_line_no);
      next_column_no = -1;
      next_column_no = (short) (next_column_no + 1);
      cell = row.createCell(next_column_no);
      cell.setCellType(3);
      cell.setEncoding(ENCODING_UTF_16);
      cell.setCellValue("班號");
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(3);
      cell.setEncoding(ENCODING_UTF_16);
      cell.setCellValue("英文名稱");
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(3);
      cell.setEncoding(ENCODING_UTF_16);
      cell.setCellValue("遲到");
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(3);
      cell.setEncoding(ENCODING_UTF_16);
      cell.setCellValue("早退");
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(3);
      cell.setEncoding(ENCODING_UTF_16);
      cell.setCellValue("缺席");

      String studentNoPrefix = programmeService.getById(attendenceForm.getProgrammeId())
        .getProgrammeTitleEng();
      String studentNoBridgeSymbol = "-";

      for (Object o : attendenceForm.getAttendenceList()) {
        AttendenceObject attendenceObject = (AttendenceObject) o;

        ++next_line_no;
        row = sheet.createRow(next_line_no);
        next_column_no = -1;
        next_column_no = (short) (next_column_no + 1);

        cell = row.createCell(next_column_no);
        cell.setCellType(3);
        cell.setEncoding(ENCODING_UTF_16);
        String value = attendenceObject.getStudentNo()
          .replace(studentNoPrefix + studentNoBridgeSymbol, "");
        if (value.startsWith("0")) {
          value = value.substring(1);
        }
        cell.setCellValue(value);

        ++next_column_no;
        cell = row.createCell(next_column_no);
        cell.setCellType(3);
        cell.setEncoding(ENCODING_UTF_16);
        cell.setCellValue(attendenceObject.getStudentChiName());

        ++next_column_no;
        cell = row.createCell(next_column_no);
        cell.setCellType(3);
        cell.setEncoding(ENCODING_UTF_16);
        cell.setCellValue(String.valueOf(attendenceObject.getLateForClass()));

        ++next_column_no;
        cell = row.createCell(next_column_no);
        cell.setCellType(3);
        cell.setEncoding(ENCODING_UTF_16);
        cell.setCellValue(String.valueOf(attendenceObject.getEarlyLeave()));

        ++next_column_no;
        cell = row.createCell(next_column_no);
        cell.setCellType(3);
        cell.setEncoding(ENCODING_UTF_16);
        cell.setCellValue(String.valueOf(attendenceObject.getAbsence()));
      }
      return workbook;
    } catch (Exception e) {
      logger.throwing(getClass().getSimpleName(), MethodNameHelper.getMethodName(), e);
      throw new RuntimeException(e);
    }
  }
}
