package smile.cpce.transcript.attendence.web;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.servlet.mvc.SimpleFormController;
import smile.cpce.common.Constants;
import smile.cpce.common.MethodNameHelper;
import smile.cpce.common.domain.SessionAttribute;
import smile.cpce.school.institute.domain.Institute;
import smile.cpce.transcript.programme.domain.Programme;
import smile.cpce.transcript.programme.service.ProgrammeService;
import smile.cpce.transcript.semester.service.SemesterService;

public class DisplayStudentAttendenceController extends SimpleFormController {
  protected final Logger logger = Logger.getLogger("cpce");

  private SemesterService semesterService;

  private ProgrammeService programmeService;

  private JdbcTemplate jdbcTemplate;

  private String sqlTemplate = "SELECT  A.late_for_class as lateForClass,  A.early_leave as earlyLeave,  A.skip_ranking as skipRanking,  A.absence as absence,  C.name_eng as teacherNameEng,  D.student_no as studentNo,  E.name_eng as nameEng,  E.name_chi as nameChi  FROM  student_attendence_tbl A  LEFT JOIN student_tbl D on A.user_id = D.user_id  LEFT JOIN user_tbl C on A.update_by = C.user_id  LEFT JOIN user_tbl E on A.user_id = E.user_id  WHERE  A.user_id in (    SELECT      user_id    FROM      student_enroll_programme_tbl    WHERE      student_enroll_programme_tbl.programme_id = ?  ) AND A.academic_year = ? AND A.semester = ?";

  public DisplayStudentAttendenceController() {
    setCommandName("displayStudentAttendenceForm");
    setCommandClass(DisplayStudentAttendenceForm.class);
    setFormView("transcript/studentAttendence/displayStudentAttendence");
    setSuccessView("redirect:/transcript/studentAttendence/displaysStudentAttendence.htm");
  }

  protected Object formBackingObject(HttpServletRequest request) throws Exception {
    try {
      DisplayStudentAttendenceForm form = new DisplayStudentAttendenceForm();
      String paramAcademicYear = request.getParameter("paramAcademicYear");
      String paramProgrammeId = request.getParameter("paramProgrammeId");
      String paramSemester = request.getParameter("paramSemester");
      HttpSession session = request.getSession();
      SessionAttribute sessionAttribute = (SessionAttribute)session.getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
      String instituteId = sessionAttribute.getCurrentInstituteId();
      String academicYear = sessionAttribute.getCurrentAcademicYear();
      String userId = sessionAttribute.getUserId();
      String role = sessionAttribute.getRole();
      if (paramAcademicYear != null && !paramAcademicYear.equals("")) {
        form.setAcademicYear(paramAcademicYear);
      } else {
        form.setAcademicYear(academicYear);
      }
      List programmeList = null;
      if (Constants.SYSADMIN.equals(role)) {
        programmeList = this.programmeService.getAllByInstitute(instituteId);
      } else {
        Institute institute = sessionAttribute.getInstitute();
        if (institute == null || institute.getInstituteId().equals(instituteId))
          programmeList = this.programmeService.getAllByInstitute(instituteId);
      }
      form.setSemester(paramSemester);
      if (paramProgrammeId != null) {
        form.setProgrammeId(paramProgrammeId);
        String pragrammeName = null;
        for (Object programme : programmeList) {
          if (paramProgrammeId.equals(((Programme)programme).getProgrammeId())) {
            pragrammeName = ((Programme)programme).getProgrammeTitleEng();
            break;
          }
        }
        List<Map> rows = this.jdbcTemplate.queryForList(this.sqlTemplate, new Object[] { paramProgrammeId, paramAcademicYear, paramSemester });
        List<DisplayStudentAttendenceForm.DisplayData> displayDataList = new ArrayList<>();
        for (Map row : rows) {
          DisplayStudentAttendenceForm.DisplayData data = new DisplayStudentAttendenceForm.DisplayData();
          data.setProgrammeName(pragrammeName);
          data.setEarlyLeave(((BigDecimal)row.get("EARLYLEAVE")).floatValue());
          data.setAbsence(((BigDecimal)row.get("ABSENCE")).floatValue());
          data.setLateForClass(((BigDecimal)row.get("LATEFORCLASS")).floatValue());
          data.setNameChi((String)row.get("NAMECHI"));
          data.setNameEng((String)row.get("NAMEENG"));
          data.setStudentNo((String)row.get("STUDENTNO"));
          data.setTeacherNameEng((String)row.get("TEACHERNAMEENG"));
          data.setSkipRanking(row.get("SKIPRANKING").equals("1"));
          displayDataList.add(data);
        }
        displayDataList.sort((o1, o2) -> {
          if (o1.getStudentNo().compareTo(o2.getStudentNo()) == 0) {
            return o1.getNameEng().compareTo(o2.getNameEng());
          }
          return o1.getStudentNo().compareTo(o2.getStudentNo());
        });
        form.setData(displayDataList);
      }
      request.setAttribute("programmeList", programmeList);
      List semesterList = this.semesterService.getSemesterByAcademicYearInstituteId(academicYear, instituteId);
      request.setAttribute("semesterList", semesterList);
      return form;
    } catch (Exception e) {
      this.logger.throwing(getClass().getSimpleName(), MethodNameHelper.getMethodName(), e);
      throw e;
    }
  }

  public ProgrammeService getProgrammeService() {
    return this.programmeService;
  }

  public void setProgrammeService(ProgrammeService programmeService) {
    this.programmeService = programmeService;
  }

  public JdbcTemplate getJdbcTemplate() {
    return this.jdbcTemplate;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public SemesterService getSemesterService() {
    return this.semesterService;
  }

  public void setSemesterService(SemesterService semesterService) {
    this.semesterService = semesterService;
  }
}
