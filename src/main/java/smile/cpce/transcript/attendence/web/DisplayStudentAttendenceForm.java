package smile.cpce.transcript.attendence.web;

import java.util.List;

public class DisplayStudentAttendenceForm {
  private String academicYear;

  private String programmeId;

  private String semester;

  private List<DisplayData> data;

  public String getAcademicYear() {
    return this.academicYear;
  }

  public void setAcademicYear(String academicYear) {
    this.academicYear = academicYear;
  }

  public String getProgrammeId() {
    return this.programmeId;
  }

  public void setProgrammeId(String programmeId) {
    this.programmeId = programmeId;
  }

  public List<DisplayData> getData() {
    return this.data;
  }

  public void setData(List<DisplayData> data) {
    this.data = data;
  }

  public String getSemester() {
    return this.semester;
  }

  public void setSemester(String semester) {
    this.semester = semester;
  }

  public static class DisplayData {
    private String userId;

    private String programmeName;

    private String studentNo;

    private String nameEng;

    private String nameChi;

    private String teacherNameEng;

    private float lateForClass;

    private float earlyLeave;

    private float absence;

    private boolean skipRanking;

    public String getUserId() {
      return this.userId;
    }

    public void setUserId(String userId) {
      this.userId = userId;
    }

    public String getProgrammeName() {
      return this.programmeName;
    }

    public void setProgrammeName(String programmeName) {
      this.programmeName = programmeName;
    }

    public String getStudentNo() {
      return this.studentNo;
    }

    public void setStudentNo(String studentNo) {
      this.studentNo = studentNo;
    }

    public String getNameEng() {
      return this.nameEng;
    }

    public void setNameEng(String nameEng) {
      this.nameEng = nameEng;
    }

    public String getNameChi() {
      return this.nameChi;
    }

    public void setNameChi(String nameChi) {
      this.nameChi = nameChi;
    }

    public String getTeacherNameEng() {
      return this.teacherNameEng;
    }

    public void setTeacherNameEng(String teacherNameEng) {
      this.teacherNameEng = teacherNameEng;
    }

    public float getLateForClass() {
      return this.lateForClass;
    }

    public void setLateForClass(float lateForClass) {
      this.lateForClass = lateForClass;
    }

    public float getEarlyLeave() {
      return this.earlyLeave;
    }

    public void setEarlyLeave(float earlyLeave) {
      this.earlyLeave = earlyLeave;
    }

    public float getAbsence() {
      return this.absence;
    }

    public void setAbsence(float absence) {
      this.absence = absence;
    }

    public boolean isSkipRanking() {
      return this.skipRanking;
    }

    public void setSkipRanking(boolean skipRanking) {
      this.skipRanking = skipRanking;
    }
  }
}
