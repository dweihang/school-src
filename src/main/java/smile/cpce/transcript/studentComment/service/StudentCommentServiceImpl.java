package smile.cpce.transcript.studentComment.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import smile.cpce.common.ToStringHelper;
import smile.cpce.transcript.commentMetadata.domain.Metadata;
import smile.cpce.transcript.commentMetadata.domain.MetadataType;
import smile.cpce.transcript.commentMetadata.service.MetadataService;
import smile.cpce.transcript.student.domain.Student;
import smile.cpce.transcript.studentComment.dao.StudentCommentDao;
import smile.cpce.transcript.studentComment.domain.StudentAward;
import smile.cpce.transcript.studentComment.domain.StudentComment;
import smile.cpce.transcript.studentComment.domain.StudentExtracurricularActivities;
import smile.cpce.transcript.studentComment.domain.StudentMerit;
import smile.cpce.transcript.studentComment.domain.StudentPositionOfService;
import smile.cpce.transcript.studentComment.domain.StudentRemark;
import smile.cpce.transcript.studentComment.domain.TeacherComment;
import smile.cpce.transcript.studentComment.domain.TeacherCommentCode;

public class StudentCommentServiceImpl implements StudentCommentService {
  private StudentCommentDao studentCommentDao;
  private MetadataService metadataService;
  protected final Logger logger = Logger.getLogger("cpce");

  public StudentCommentServiceImpl() {
  }

  public void setMetadataService(MetadataService metadataService) {
    this.metadataService = metadataService;
  }

  public List getAll() {
    return this.studentCommentDao.getAll();
  }

  public StudentComment getById(String academicYear, String userId) {
    return this.studentCommentDao.getById(academicYear, userId, false);
  }

  @Override
  public List getStudentMeritByIdAndTeacherUserId(String academicYear, String userId,
    String teacherUserId) {
    return this.studentCommentDao.getStudentMeritByIdAndTeacherUserId(academicYear, userId, teacherUserId);
  }

  @Override
  public int updateStudentMeritByIdAndTeacherUserId(String academicYear, String userId,
    String teacherUserId, List<StudentMerit> merits) {
    return this.studentCommentDao.updateStudentMeritByIdAndTeacherUserId(academicYear, userId, teacherUserId, merits);
  }

  @Override
  public List getStudentPositionOfServicesById(String academicYear, String userId) {
    return this.studentCommentDao.getStudentPositionOfServicesById(academicYear, userId);
  }

  @Override
  public int updateStudentPositionOfServicesByIdAndTeacherUserId(String academicYear, String userId,
    String teacherUserId, List<StudentPositionOfService> positionOfServicesList) {
    return this.studentCommentDao.updateStudentPositionOfServicesByIdAndTeacherUserId(academicYear, userId, teacherUserId, positionOfServicesList);
  }

  @Override
  public List getStudentExtraCurricularActivitiesById(String academicYear, String userId) {
    return this.studentCommentDao.getStudentExtraCurricularActivitiesById(academicYear, userId);
  }

  @Override
  public int updateStudentExtraCurricularActivitiesByIdAndTeacherUserId(String academicYear,
    String userId, String teacherUserId, List<StudentExtracurricularActivities> list) {
    return this.studentCommentDao.updateStudentExtraCurricularActivitiesByIdAndTeacherUserId(academicYear, userId, teacherUserId, list);
  }

  public void setStudentCommentDao(StudentCommentDao studentCommentDao) {
    this.studentCommentDao = studentCommentDao;
  }

  public int updateStudentComment(StudentComment studentComment) {
    int isSuccess = this.studentCommentDao.delete(studentComment);
    if (isSuccess == 0) {
      isSuccess = this.studentCommentDao.save(studentComment);
    }

    return isSuccess;
  }

  
  public synchronized int updateStudentComment(StudentComment studentComment, boolean withoutMerit, boolean withoutExtracurricularActivities, boolean withoutPositionOfService) {
    StudentComment oldRecord = this.studentCommentDao.getById(studentComment.getAcademicYear(), studentComment.getUserId(), false);
    if (oldRecord == null) {
      return FAIL;
    }
    List meritList = oldRecord.getMeritList();
    List extracurricularActivities = oldRecord.getExtracurricularActivitiesList();
    List positionOfService = oldRecord.getPositionOfServiceList();
    if (withoutMerit) {
      studentComment.setMeritList(meritList);
    }
    if (withoutExtracurricularActivities) {
      studentComment.setExtracurricularActivitiesList(extracurricularActivities);
    }
    if (withoutPositionOfService) {
      studentComment.setPositionOfServiceList(positionOfService);
    }
    return updateStudentComment(studentComment);
  }

  public String updateStudentCommentV2(StudentComment studentComment) {

    // Delete student comment
    logger.info(">>>>> ===================== Input Object ===================================");
    logger.info(ToStringHelper.toString(studentComment));
    logger.info(">>>>> ========================================================");

    String errorMessage = this.studentCommentDao.deleteAwardRemarkComment(studentComment);

    if (errorMessage.equalsIgnoreCase("OK")) {
      errorMessage = this.studentCommentDao.saveAwardRemarkComment(studentComment);
    }

    return errorMessage;
  }

  public HSSFWorkbook exportStudentComment(String academicYear, List studentList, Locale locale, String programmeTitle) {
    List positionList = this.metadataService.getAll(MetadataType.POSITION_METADATA);
    List meritList = this.metadataService.getAll(MetadataType.MERIT_METADATA);
    List meritClassList = this.metadataService.getAll(MetadataType.MERIT_CLASS_METADATA);
    List actList = this.metadataService.getAll(MetadataType.ACT_METADATA);
    List remarkList = this.metadataService.getAll(MetadataType.REMARK_METADATA);
    List teacherCommentCodeList = this.metadataService.getAll(MetadataType.TEACHER_COMMENT_METADATA);
    List awardList = this.metadataService.getAll(MetadataType.AWARD_METADATA);
    HashMap positionCodeMap = new HashMap();
    Iterator it = positionList.iterator();

    while(it.hasNext()) {
      Metadata data = (Metadata)it.next();
      positionCodeMap.put(data.getCode(), data.getChiDesc());
    }

    HashMap meritCodeMap = new HashMap();
    it = meritList.iterator();

    while(it.hasNext()) {
      Metadata data = (Metadata)it.next();
      meritCodeMap.put(data.getCode(), data.getChiDesc());
    }

    HashMap meritClassMap = new HashMap();
    it = meritClassList.iterator();

    while(it.hasNext()) {
      Metadata data = (Metadata)it.next();
      meritClassMap.put(data.getCode(), data.getChiDesc());
    }

    HashMap activitiesCodeMap = new HashMap();
    it = actList.iterator();

    while(it.hasNext()) {
      Metadata data = (Metadata)it.next();
      activitiesCodeMap.put(data.getCode(), data.getChiDesc());
    }

    HashMap remarkCodeMap = new HashMap();
    it = remarkList.iterator();

    while(it.hasNext()) {
      Metadata data = (Metadata)it.next();
      remarkCodeMap.put(data.getCode(), data.getChiDesc());
    }

    HashMap teacherCommentCodeMap = new HashMap();
    it = teacherCommentCodeList.iterator();

    while(it.hasNext()) {
      Metadata data = (Metadata)it.next();
      teacherCommentCodeMap.put(data.getCode(), data.getChiDesc());
    }

    HashMap awardCodeMap = new HashMap();
    it = awardList.iterator();

    while(it.hasNext()) {
      Metadata data = (Metadata)it.next();
      awardCodeMap.put(data.getCode(), data.getChiDesc());
    }

    int count = 0;
    ResourceBundle res = ResourceBundle.getBundle("messages", locale);
    HSSFWorkbook workbook = new HSSFWorkbook();
    HSSFSheet sheet = workbook.createSheet();
    short next_line_no = -1;
    short next_column_no = -1;
    next_line_no = (short)(next_line_no + 1);
    HSSFRow row = sheet.createRow(next_line_no);
    next_column_no = (short)(next_column_no + 1);
    HSSFCell cell = row.createCell(next_column_no);
    cell.setCellType(1);
    cell.setCellValue(academicYear);
    ++next_column_no;
    cell = row.createCell(next_column_no);
    cell.setCellType(1);
    cell.setEncoding((short)1);
    cell.setCellValue(programmeTitle);
    ++next_line_no;
    row = sheet.createRow(next_line_no);
    next_column_no = -1;
    next_column_no = (short)(next_column_no + 1);
    cell = row.createCell(next_column_no);
    cell.setCellType(1);
    cell.setEncoding((short)1);
    cell.setCellValue(res.getString("user.studentNo"));
    ++next_column_no;
    cell = row.createCell(next_column_no);
    cell.setCellType(1);
    cell.setEncoding((short)1);
    cell.setCellValue(res.getString("user.name"));
    ++next_column_no;
    cell = row.createCell(next_column_no);
    cell.setCellType(1);
    cell.setEncoding((short)1);
    cell.setCellValue(res.getString("arts.transcript.positionOfService"));
    ++next_column_no;
    cell = row.createCell(next_column_no);
    cell.setCellType(1);
    cell.setEncoding((short)1);
    cell.setCellValue(res.getString("arts.transcript.extraCurricularActivities"));
    ++next_column_no;
    cell = row.createCell(next_column_no);
    cell.setCellType(1);
    cell.setEncoding((short)1);
    cell.setCellValue(res.getString("arts.transcript.meritNDemerit"));
    ++next_column_no;
    cell = row.createCell(next_column_no);
    cell.setCellType(1);
    cell.setEncoding((short)1);
    cell.setCellValue(res.getString("arts.transcript.award"));
    ++next_column_no;
    cell = row.createCell(next_column_no);
    cell.setCellType(1);
    cell.setEncoding((short)1);
    cell.setCellValue(res.getString("user.student.remarks"));
    ++next_column_no;
    cell = row.createCell(next_column_no);
    cell.setCellType(1);
    cell.setEncoding((short)1);
    cell.setCellValue(res.getString("user.student.comments"));

    for(Iterator i = studentList.iterator(); i.hasNext(); ++count) {
      Student student = (Student)i.next();
      StudentComment studentComment = this.studentCommentDao.getById(academicYear, student.getUserId(), true);
      studentComment.setStudentNo(student.getHkidOrPassport());
      studentComment.setStudentName(student.getUser().getNameEng());
      studentComment.setClassNo(student.getStudentNo());
      if (studentComment.getRemark() == null) {
        studentComment.setRemark(new StudentRemark());
      }

      ++next_line_no;
      row = sheet.createRow(next_line_no);
      next_column_no = -1;
      next_column_no = (short)(next_column_no + 1);
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setCellValue(student.getStudentNo());
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setEncoding((short)1);
      if (locale.getCountry().equalsIgnoreCase("zh")) {
        cell.setCellValue(student.getUser().getNameChi());
      } else {
        cell.setCellValue(student.getUser().getNameEng());
      }

      String tmp = "";
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setEncoding((short)1);
      List list = studentComment.getPositionOfServiceList();
      it = list.iterator();

      StudentPositionOfService studentPositionOfService;
      for(tmp = ""; it.hasNext(); tmp = tmp + "," + positionCodeMap.get(studentPositionOfService.getCode())) {
        studentPositionOfService = (StudentPositionOfService)it.next();
      }

      if (tmp.length() > 0) {
        cell.setCellValue(tmp.substring(1));
      } else {
        cell.setCellType(3);
      }

      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setEncoding((short)1);
      list = studentComment.getExtracurricularActivitiesList();
      it = list.iterator();

      StudentExtracurricularActivities studentExtracurricularActivities;
      for(tmp = ""; it.hasNext(); tmp = tmp + "," + activitiesCodeMap.get(studentExtracurricularActivities.getCode())) {
        studentExtracurricularActivities = (StudentExtracurricularActivities)it.next();
      }

      if (tmp.length() > 0) {
        cell.setCellValue(tmp.substring(1));
      } else {
        cell.setCellType(3);
      }

      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setEncoding((short)1);
      String meritStr = studentComment.getMeritStr();
      if (meritStr != null && meritStr.length() > 0) {
        cell.setCellValue(meritStr);
      } else {
        cell.setCellType(3);
      }

      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setEncoding((short)1);
      list = studentComment.getAwardList();
      it = list.iterator();

      StudentAward studentAward;
      for(tmp = ""; it.hasNext(); tmp = tmp + "," + awardCodeMap.get(studentAward.getCode())) {
        studentAward = (StudentAward)it.next();
      }

      if (tmp.length() > 0) {
        cell.setCellValue(tmp.substring(1));
      } else {
        cell.setCellType(3);
      }

      tmp = "";
      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setEncoding((short)1);
      StudentRemark remark = studentComment.getRemark();
      Object remarkCode = remarkCodeMap.get(remark.getCode());
      if (remarkCode != null) {
        tmp = tmp + "," + (String)remarkCode;
      }

      String rmk = remark.getRemarks();
      if (rmk != null && rmk.length() > 0) {
        tmp = tmp + "," + rmk;
      }

      if (tmp.length() > 0) {
        cell.setCellValue(tmp.substring(1));
      } else {
        cell.setCellType(3);
      }

      ++next_column_no;
      cell = row.createCell(next_column_no);
      cell.setCellType(1);
      cell.setEncoding((short)1);
      list = studentComment.getTeacherCommentCodesList();
      it = list.iterator();

      TeacherCommentCode teacherCommentcode;
      for(tmp = ""; it.hasNext(); tmp = tmp + "," + teacherCommentCodeMap.get(teacherCommentcode.getCode())) {
        teacherCommentcode = (TeacherCommentCode)it.next();
      }

      TeacherComment teacherComment = studentComment.getTeacherComment();
      if (teacherComment != null) {
        String comments = teacherComment.getComments();
        if (comments != null && comments.length() > 0) {
          tmp = tmp + "," + comments;
        }
      }

      if (tmp.length() > 0) {
        cell.setCellValue(tmp.substring(1));
      } else {
        cell.setCellType(3);
      }
    }

    return workbook;
  }
}
