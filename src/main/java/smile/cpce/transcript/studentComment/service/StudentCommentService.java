//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.transcript.studentComment.service;

import java.util.List;
import java.util.Locale;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import smile.cpce.transcript.studentComment.domain.StudentComment;
import smile.cpce.transcript.studentComment.domain.StudentExtracurricularActivities;
import smile.cpce.transcript.studentComment.domain.StudentMerit;
import smile.cpce.transcript.studentComment.domain.StudentPositionOfService;

public interface StudentCommentService {
  int SUCCESS = 0;
  int FAIL = 1;

  List getAll();

  StudentComment getById(String var1, String var2);

  List getStudentMeritByIdAndTeacherUserId(String academicYear, String userId, String teacherUserId);

  int updateStudentMeritByIdAndTeacherUserId(String academicYear, String userId, String teacherUserId, List<StudentMerit> meritList);

  List getStudentPositionOfServicesById(String academicYear, String userId);

  int updateStudentPositionOfServicesByIdAndTeacherUserId(String academicYear, String userId, String teacherUserId, List<StudentPositionOfService> positionOfServicesList);

  List getStudentExtraCurricularActivitiesById(String academicYear, String userId);

  int updateStudentExtraCurricularActivitiesByIdAndTeacherUserId(String academicYear, String userId, String teacherUserId, List<StudentExtracurricularActivities> list);

  int updateStudentComment(StudentComment var1);

  int updateStudentComment(StudentComment studentComment, boolean withoutMerit, boolean withoutExtracurricularActivities, boolean withoutPositionOfService);

  String updateStudentCommentV2(StudentComment studentComment);

  HSSFWorkbook exportStudentComment(String var1, List var2, Locale var3, String var4);
}
