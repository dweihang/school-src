//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.transcript.studentComment.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import smile.cpce.common.Constants;
import smile.cpce.common.MethodNameHelper;
import smile.cpce.common.Sequencer;
import smile.cpce.transcript.student.domain.Student;
import smile.cpce.transcript.studentComment.domain.StudentAward;
import smile.cpce.transcript.studentComment.domain.StudentComment;
import smile.cpce.transcript.studentComment.domain.StudentExtracurricularActivities;
import smile.cpce.transcript.studentComment.domain.StudentMerit;
import smile.cpce.transcript.studentComment.domain.StudentMeritCount;
import smile.cpce.transcript.studentComment.domain.StudentPositionOfService;
import smile.cpce.transcript.studentComment.domain.StudentRemark;
import smile.cpce.transcript.studentComment.domain.TeacherComment;
import smile.cpce.transcript.studentComment.domain.TeacherCommentCode;

public class StudentCommentDaoImpl extends HibernateDaoSupport implements StudentCommentDao {

  private final Logger log = Logger.getLogger("cpce");

  public StudentCommentDaoImpl() {
  }

  @Override
  public void save(Student student) {
    this.getHibernateTemplate().save(student);
  }

  @Override
  public List getAll() {
    return this.getHibernateTemplate().find("from StudentComment as s");
  }

  @Override
  public List getStudentMeritByIdAndTeacherUserId(String academicYear, String userId, String teacherUserId) {
    Object[] params = new Object[]{academicYear, userId, teacherUserId};
    return this.getHibernateTemplate()
        .find("from StudentMerit as s where academicYear = ? and userId = ? and teacherUserId = ?", params);
  }

  @Override
  public int updateStudentMeritByIdAndTeacherUserId(String academicYear, String userId,
    String teacherUserId, List<StudentMerit> meritList) {
    try {
      String hql = "delete from StudentMerit where academicYear = :year and userId = :userId and teacherUserId = :teacherUserId";
      Query query = getSession().createQuery(hql);
      query.setParameter("year", academicYear);
      query.setParameter("userId", userId);
      query.setParameter("teacherUserId", teacherUserId);
      query.executeUpdate();

      Iterator i = meritList.iterator();

      while (i.hasNext()) {
        StudentMerit sm = (StudentMerit) i.next();
        sm.setTeacherUserId(teacherUserId);
        if (sm.getCode() != null && sm.getCode().length() > 0) {
          sm.setStudentComMeritId(Sequencer.getInstance()
            .getNextSeqNum(Constants.jdbcTemplate, "student_com_merit_id"));
          this.getHibernateTemplate().save(sm);
        }
      }
      return SUCCESS;
    } catch (Exception e) {
      log.throwing(this.getClass().getName(), MethodNameHelper.getMethodName(), e);
      return FAIL;
    }
  }

  @Override
  public List getStudentPositionOfServicesById(String academicYear, String userId) {
    Object[] params = new Object[]{academicYear, userId};
    return this.getHibernateTemplate()
      .find("from StudentPositionOfService as s where academicYear = ? and userId = ?", params);
  }

  @Override
  public int updateStudentPositionOfServicesByIdAndTeacherUserId(String academicYear, String userId,
    String teacherUserId, List<StudentPositionOfService> positionOfServicesList) {
    try {
      String hql = "delete from StudentPositionOfService where academicYear = :year and userId = :userId and teacherUserId = :teacherUserId";
      Query query = getSession().createQuery(hql);
      query.setParameter("year", academicYear);
      query.setParameter("userId", userId);
      query.setParameter("teacherUserId", teacherUserId);
      query.executeUpdate();

      Iterator i = positionOfServicesList.iterator();

      while (i.hasNext()) {
        StudentPositionOfService sm = (StudentPositionOfService) i.next();
        sm.setTeacherUserId(teacherUserId);
        if (sm.getCode() != null && sm.getCode().length() > 0) {
          this.getHibernateTemplate().save(sm);
        }
      }

      return SUCCESS;
    } catch (Exception e) {
      log.throwing(this.getClass().getName(), MethodNameHelper.getMethodName(), e);
      return FAIL;
    }
  }

  @Override
  public List getStudentExtraCurricularActivitiesById(String academicYear, String userId) {
    Object[] params = new Object[]{academicYear, userId};
    return this.getHibernateTemplate()
      .find("from StudentExtracurricularActivities as s where academicYear = ? and userId = ?", params);
  }

  @Override
  public int updateStudentExtraCurricularActivitiesByIdAndTeacherUserId(String academicYear,
    String userId, String teacherUserId,
    List<StudentExtracurricularActivities> studentExtracurricularActivitiesList) {
    try {
      String hql = "delete from StudentExtracurricularActivities where academicYear = :year and userId = :userId and teacherUserId = :teacherUserId";
      Query query = getSession().createQuery(hql);
      query.setParameter("year", academicYear);
      query.setParameter("userId", userId);
      query.setParameter("teacherUserId", teacherUserId);
      query.executeUpdate();

      Iterator i = studentExtracurricularActivitiesList.iterator();

      while (i.hasNext()) {
        StudentExtracurricularActivities sm = (StudentExtracurricularActivities) i.next();
        sm.setTeacherUserId(teacherUserId);
        if (sm.getCode() != null && sm.getCode().length() > 0) {
          this.getHibernateTemplate().save(sm);
        }
      }

      return SUCCESS;
    } catch (Exception e) {
      log.throwing(this.getClass().getName(), MethodNameHelper.getMethodName(), e);
      return FAIL;
    }
  }


  @Override
  public StudentComment getById(String academicYear, String userId, boolean withMeritStr) {
    StudentComment sc = new StudentComment();
    Object[] params = new Object[]{academicYear, userId};
    List positionOfServiceList = this.getHibernateTemplate()
        .find("from StudentPositionOfService as s where academicYear = ? and userId = ? ", params);
    List extracurricularActivitiesList = this.getHibernateTemplate()
        .find("from StudentExtracurricularActivities as s where academicYear = ? and userId = ? ",
            params);
    List meritList = this.getHibernateTemplate()
        .find("from StudentMerit as s where academicYear = ? and userId = ? ", params);
    List remarkList = this.getHibernateTemplate()
        .find("from StudentRemark as s where academicYear = ? and userId = ? ", params);
    List teacherCommentList = this.getHibernateTemplate()
        .find("from TeacherComment as s where academicYear = ? and user_id = ? ", params);
    List teacherCommentCodeList = this.getHibernateTemplate()
        .find("from TeacherCommentCode as s where academicYear = ? and user_id = ? ", params);
    List awardList = this.getHibernateTemplate()
        .find("from StudentAward as s where academicYear = ? and userId = ? ", params);
    String sql = String.format("select s.code, COUNT(s.code) from student_com_merit_tbl as s where s.academic_year = '%s' and s.user_id = '%s' group by s.code", academicYear, userId);
    List<Object[]> meritCodeCount = this.getSession().createSQLQuery(sql).list();

    List<StudentMeritCount> meritCountList = new ArrayList<StudentMeritCount>();
    for (Object[] aMeritCodeCount : meritCodeCount) {
      StudentMeritCount data = new StudentMeritCount();
      data.setAcademicYear(academicYear);
      data.setCode((String) (aMeritCodeCount[0]));
      data.setCount((Integer) (aMeritCodeCount[1]));
      data.setUserId(userId);
      meritCountList.add(data);
    }

    sc.setStudentMeritCountList(meritCountList);

    String meritStr = "";
    if (withMeritStr) {
      Query query = this.getSession().getNamedQuery("getStuMerit");
      query.setString("userId", userId);
      query.setString("academicYear", academicYear);
      List list = query.list();
      if (list != null && list.size() > 0) {
        meritStr = (String) list.get(0);
      }
    }

    sc.setUserId(userId);
    sc.setPositionOfServiceList(positionOfServiceList);
    sc.setExtracurricularActivitiesList(extracurricularActivitiesList);
    sc.setMeritList(meritList);
    sc.setTeacherCommentCodesList(teacherCommentCodeList);
    sc.setAwardList(awardList);
    sc.setMeritStr(meritStr);
    if (remarkList != null && remarkList.size() > 0) {
      StudentRemark studentRemark = (StudentRemark) remarkList.get(0);
      String remark = studentRemark.getCode();
      if (remark != null && remark.length() > 0
          || remark != null && studentRemark.getRemarks().length() > 0) {
        sc.setRemark(studentRemark);
      }
    }

    if (teacherCommentList != null && teacherCommentList.size() > 0) {
      TeacherComment teacherComment = (TeacherComment) teacherCommentList.get(0);
      sc.setTeacherComment(teacherComment);
    }

    return sc;
  }

  @Override
  public int delete(StudentComment studentComment) {
    Session session = null;
    String userId = studentComment.getUserId();
    String academicYear = studentComment.getAcademicYear();
    if (userId != null && academicYear != null) {
      try {
        session = this.getSession();
        String hql = "delete from StudentPositionOfService where academicYear = :year and userId = :userId";
        Query query = session.createQuery(hql);
        query.setParameter("year", academicYear);
        query.setParameter("userId", userId);
        int row = query.executeUpdate();
        String hql2 = "delete from StudentExtracurricularActivities where academicYear = :year and userId = :userId";
        Query query2 = session.createQuery(hql2);
        query2.setParameter("year", academicYear);
        query2.setParameter("userId", userId);
        int row2 = query2.executeUpdate();
        String hql3 = "delete from StudentMerit where academicYear = :year and userId = :userId";
        Query query3 = session.createQuery(hql3);
        query3.setParameter("year", academicYear);
        query3.setParameter("userId", userId);
        int row3 = query3.executeUpdate();
        String hql4 = "delete from StudentRemark where academicYear = :year and userId = :userId";
        Query query4 = session.createQuery(hql4);
        query4.setParameter("year", academicYear);
        query4.setParameter("userId", userId);
        int row4 = query4.executeUpdate();
        String hq15 = "delete from TeacherComment where academicYear = :year and userId = :userId";
        Query query5 = session.createQuery(hq15);
        query5.setParameter("year", academicYear);
        query5.setParameter("userId", userId);
        int row5 = query5.executeUpdate();
        String hq16 = "delete from TeacherCommentCode where academicYear = :year and userId = :userId";
        Query query6 = session.createQuery(hq16);
        query6.setParameter("year", academicYear);
        query6.setParameter("userId", userId);
        int row6 = query6.executeUpdate();
        String hql7 = "delete from StudentAward where academicYear = :year and userId = :userId";
        Query query7 = session.createQuery(hql7);
        query7.setParameter("year", academicYear);
        query7.setParameter("userId", userId);
        int row7 = query7.executeUpdate();
        session.flush();
        session.close();
        return 0;
      } catch (Exception var26) {
        System.out.println("Exception: " + var26.getMessage());
        session.close();
        return 1;
      }
    } else {
      return 1;
    }
  }

  @Override
  public int save(StudentComment studentComment) {
    try {
      List positionOfServiceList = studentComment.getPositionOfServiceList();
      Iterator i = positionOfServiceList.iterator();

      while (i.hasNext()) {
        StudentPositionOfService spos = (StudentPositionOfService) i.next();
        if (spos.getCode() != null && spos.getCode().length() > 0) {
          this.getHibernateTemplate().save(spos);
        }
      }

      List extracurricularActivitiesList = studentComment.getExtracurricularActivitiesList();
      i = extracurricularActivitiesList.iterator();

      while (i.hasNext()) {
        StudentExtracurricularActivities sea = (StudentExtracurricularActivities) i.next();
        if (sea.getCode() != null && sea.getCode().length() > 0) {
          this.getHibernateTemplate().save(sea);
        }
      }

      List meritList = studentComment.getMeritList();
      i = meritList.iterator();

      while (i.hasNext()) {
        StudentMerit sm = (StudentMerit) i.next();
        if (sm.getCode() != null && sm.getCode().length() > 0) {
          sm.setStudentComMeritId(Sequencer.getInstance()
              .getNextSeqNum(Constants.jdbcTemplate, "student_com_merit_id"));
          this.getHibernateTemplate().save(sm);
        }
      }

      StudentRemark studentRemark = studentComment.getRemark();
      if (studentRemark != null && studentRemark.getCode() != null
          && studentRemark.getCode().length() > 0
          || studentRemark != null && studentRemark.getRemarks().length() > 0) {
        this.getHibernateTemplate().save(studentRemark);
      }

      List teacherCommentCodeList = studentComment.getTeacherCommentCodesList();
      i = teacherCommentCodeList.iterator();

      while (i.hasNext()) {
        TeacherCommentCode sm = (TeacherCommentCode) i.next();
        if (sm.getCode() != null && sm.getCode().length() > 0) {
          this.getHibernateTemplate().save(sm);
        }
      }

      TeacherComment teacherComment = studentComment.getTeacherComment();
      String comment = teacherComment.getComments();
      if (comment != null && comment.length() > 0) {
        this.getHibernateTemplate().save(teacherComment);
      }

      List awardList = studentComment.getAwardList();
      i = awardList.iterator();

      while (i.hasNext()) {
        StudentAward studentAwd = (StudentAward) i.next();
        if (studentAwd.getCode() != null && studentAwd.getCode().length() > 0) {
          this.getHibernateTemplate().save(studentAwd);
        }
      }

      return 0;
    } catch (Exception var12) {
      log.throwing(this.getClass().getName(), MethodNameHelper.getMethodName(), var12);
      return 1;
    }
  }

  @Override
  public String deleteAwardRemarkComment(StudentComment studentComment) {
    Session session = null;
    String userId = studentComment.getUserId();
    String academicYear = studentComment.getAcademicYear();
    String errorMessage = "OK";

    if (userId != null && academicYear != null) {
      try {
        session = this.getSession();

        errorMessage = "Handling StudentAward";
        String hql7 = "delete from StudentAward where academicYear = :year and userId = :userId";
        Query query7 = session.createQuery(hql7);
        query7.setParameter("year", academicYear);
        query7.setParameter("userId", userId);
        int row7 = query7.executeUpdate();
        log.info(">>>>> Finish delete StudentAward ");

        errorMessage = "Handling StudentRemark";
        String hql4 = "delete from StudentRemark where academicYear = :year and userId = :userId";
        Query query4 = session.createQuery(hql4);
        query4.setParameter("year", academicYear);
        query4.setParameter("userId", userId);
        int row4 = query4.executeUpdate();
        log.info(">>>>> Finish delete StudentRemark ");

        errorMessage = "Handling TeacherComment";
        String hq15 = "delete from TeacherComment where academicYear = :year and userId = :userId";
        Query query5 = session.createQuery(hq15);
        query5.setParameter("year", academicYear);
        query5.setParameter("userId", userId);
        int row5 = query5.executeUpdate();
        log.info(">>>>> Finish delete TeacherComment ");

        errorMessage = "Handling TeacherCommentCode";
        String hq16 = "delete from TeacherCommentCode where academicYear = :year and userId = :userId";
        Query query6 = session.createQuery(hq16);
        query6.setParameter("year", academicYear);
        query6.setParameter("userId", userId);
        int row6 = query6.executeUpdate();
        log.info(">>>>> Finish delete TeacherCommentCode ");

        session.flush();
        session.close();
        return "OK";
      } catch (Exception var26) {
        session.close();
        log.throwing(this.getClass().getName(), MethodNameHelper.getMethodName(), var26);

        errorMessage = "Exception Found, current step : " + errorMessage;
        return errorMessage;
      }
    } else {
      errorMessage = "UserId and Academic Year are null";
      return errorMessage;
    }
  }

  @Override
  public String saveAwardRemarkComment(StudentComment studentComment) {

    String errorMessage = "OK";

    try {
      // Award
      errorMessage = "Handling save award";
      List awardList = studentComment.getAwardList();
      Iterator awardIterator = awardList.iterator();

      while (awardIterator.hasNext()) {
        StudentAward studentAwd = (StudentAward) awardIterator.next();
        if (studentAwd.getCode() != null && studentAwd.getCode().length() > 0) {
          this.getHibernateTemplate().save(studentAwd);
        }
      }
      log.info(">>>>> Finish save Award ");

      // Remark
      errorMessage = "Handling save remark";
      StudentRemark studentRemark = studentComment.getRemark();
      if (studentRemark != null && studentRemark.getCode() != null
        && studentRemark.getCode().length() > 0
        || studentRemark != null && studentRemark.getRemarks().length() > 0) {
        this.getHibernateTemplate().save(studentRemark);
      }
      log.info(">>>>> Finish save Remark ");

      // Teacher Comment Code
      errorMessage = "Handling save comment code";
      List teacherCommentCodeList = studentComment.getTeacherCommentCodesList();
      Iterator commentIterator = teacherCommentCodeList.iterator();

      while (commentIterator.hasNext()) {
        TeacherCommentCode sm = (TeacherCommentCode) commentIterator.next();
        if (sm.getCode() != null && sm.getCode().length() > 0) {
          this.getHibernateTemplate().save(sm);
        }
      }
      log.info(">>>>> Finish save Teacher Comment Code ");

      // Teacher Comment
      errorMessage = "Handling save comment";
      TeacherComment teacherComment = studentComment.getTeacherComment();
      String comment = teacherComment.getComments();
      if (comment != null && comment.length() > 0) {
        this.getHibernateTemplate().save(teacherComment);
      }
      log.info(">>>>> Finish save Teacher Comment");

      return "OK";
    } catch (Exception var12) {
      log.throwing(this.getClass().getName(), MethodNameHelper.getMethodName(), var12);

      errorMessage = "Exception Found, current step : " + errorMessage;
      return errorMessage;
    }
  }
}
