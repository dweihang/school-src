package smile.cpce.transcript.studentComment.domain;

import java.util.List;
import java.util.Map;
import org.springframework.dao.DataIntegrityViolationException;
import smile.cpce.transcript.studentComment.domain.metaData.MeritAndDemerit;
import smile.cpce.transcript.studentComment.domain.metaData.MeritClass;

/**
 * @author raymonddu 23/7/2018
 */
public class StudentSummaryMeritCount extends StudentMeritCount {

  private String meritClassCode;

  private String meritClassDesc;

  private String meritMetaDesc;

  private StudentSummaryMeritCount() {

  }

  public static StudentSummaryMeritCount convertFromStudentMeritCount(
      StudentMeritCount studentMeritCount, MeritClass meritClass, MeritAndDemerit meritAndDemerit)
      throws DataIntegrityViolationException {
    StudentSummaryMeritCount summaryMeritCount = new StudentSummaryMeritCount();
    summaryMeritCount.setUserId(studentMeritCount.getCode());
    summaryMeritCount.setCode(studentMeritCount.getCode());
    summaryMeritCount.setAcademicYear(studentMeritCount.getAcademicYear());
    summaryMeritCount.meritMetaDesc = meritAndDemerit.getDescByCode(studentMeritCount.getCode());

    List<Map> levelSortedList = meritClass.getLevelSortedList();
    if (levelSortedList.isEmpty()) {
      throw new DataIntegrityViolationException(
          "MeritClass record count 0, please check database config");
    }

    summaryMeritCount.setCount(studentMeritCount.getCount());
    summaryMeritCount.meritClassCode = (String) levelSortedList.get(0).get("code");
    summaryMeritCount.meritClassDesc = (String) levelSortedList.get(0).get("chi_desc");

    if (levelSortedList.size() == 1) {
      return summaryMeritCount;
    }

    int accumulatedDivider = 1;
    for (int i = 0; i < levelSortedList.size() - 1; i++) {
      Map levelSortedMerritClass = levelSortedList.get(i);
      Integer upgradeTo = (Integer) levelSortedMerritClass.get("upgrade_to");

      if (upgradeTo <= 0) {
        throw new DataIntegrityViolationException(
            "upgrade_to cannot <= 0, please check database config");
      }
      accumulatedDivider *= upgradeTo;
      if (studentMeritCount.getCount() / accumulatedDivider <= 0) {
        break;
      }
      summaryMeritCount.setCount(studentMeritCount.getCount() / accumulatedDivider);
      summaryMeritCount.meritClassCode = (String) levelSortedList.get(i + 1).get("code");
      summaryMeritCount.meritClassDesc = (String) levelSortedList.get(i + 1).get("chi_desc");
    }

    return summaryMeritCount;
  }

  public String getMeritClassCode() {
    return meritClassCode;
  }

  public String getMeritClassDesc() {
    return meritClassDesc;
  }

  public String getMeritMetaDesc() {
    return meritMetaDesc;
  }
}
