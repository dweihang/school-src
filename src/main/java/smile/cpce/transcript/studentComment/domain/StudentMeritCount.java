package smile.cpce.transcript.studentComment.domain;

/**
 * @author raymonddu 23/7/2018
 */
public class StudentMeritCount {

  /**
   * Student academic year
   */
  private String academicYear;

  /**
   * Student user id
   *
   */
  private String userId;

  /**
   * Merit Metadata Code
   */
  private String code;

  /**
   * Count of the merit of the student
   *
   */
  private int count;

  public String getAcademicYear() {
    return academicYear;
  }

  public void setAcademicYear(String academicYear) {
    this.academicYear = academicYear;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }
}
