//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.transcript.studentComment.domain;

import java.io.Serializable;

public class StudentExtracurricularActivities implements Serializable {
  private String academicYear;
  private String userId;
  private String code;
  private String teacherUserId;

  public StudentExtracurricularActivities() {
  }

  public String getAcademicYear() {
    return this.academicYear;
  }

  public void setAcademicYear(String academicYear) {
    this.academicYear = academicYear;
  }

  public String getUserId() {
    return this.userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getTeacherUserId() {
    return teacherUserId;
  }

  public void setTeacherUserId(String teacherUserId) {
    this.teacherUserId = teacherUserId;
  }
}
