//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.transcript.studentComment.domain.metaData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;

public class MeritClass {
  private Map hashMap;
  // level ascending order
  private List<Map> levelSortedMeritClass;

  private static final String TBL_NAME = "student_com_merit_class_metadata_tbl";

  public MeritClass(JdbcTemplate jdbcTemplate) {
    List rows = jdbcTemplate.queryForList("select * from student_com_merit_class_metadata_tbl order by code ");
    Iterator iterator = rows.iterator();
    this.hashMap = new LinkedHashMap();
    this.levelSortedMeritClass = new ArrayList();

    while(iterator.hasNext()) {
      Map map = (Map)iterator.next();
      this.hashMap.put((String)map.get("code"), map);
      this.levelSortedMeritClass.add(map);
    }
    Collections.sort(this.levelSortedMeritClass, new Comparator<Map>() {
      @Override
      public int compare(Map o1, Map o2) {
        Integer o1Level = (Integer) o1.get("level");
        Integer o2Level = (Integer) o2.get("level");
        if (o1Level.equals(o2Level)) {
          return 0;
        }
        if (o1Level > o2Level) {
          return 1;
        }
        return -1;
      }
    });
  }

  public Map getMap() {
    return this.hashMap;
  }

  public List<Map> getLevelSortedList() {
    return this.levelSortedMeritClass;
  }

  public String getDescByCode(String code) {
    return (String)(((Map)(this.hashMap.get(code))).get("chi_desc"));
  }
}
