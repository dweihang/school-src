//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.transcript.studentComment.domain;

import java.util.List;

public class StudentComment {
  private String academicYear;
  private String userId;
  private String classNo;
  private String studentName;
  private String studentNo;
  private List positionOfServiceList;
  private int positionOfServiceListSize;
  private List extracurricularActivitiesList;
  private int extracurricularActivitiesListSize;
  private List meritList;
  private int meritListSize;
  private List teacherCommentCodesList;
  private int teacherCommentCodesListSize;
  private List awardList;
  private int awardListSize;
  private String meritStr;
  private StudentRemark remark;
  private TeacherComment teacherComment;

  // newly added
  private List<StudentMeritCount> studentMeritCountList;
  private List<StudentSummaryMeritCount> studentSummaryMeritCountList;

  public StudentComment() {
  }

  public String getAcademicYear() {
    return this.academicYear;
  }

  public void setAcademicYear(String academicYear) {
    this.academicYear = academicYear;
  }

  public String getClassNo() {
    return this.classNo;
  }

  public void setClassNo(String classNo) {
    this.classNo = classNo;
  }

  public String getStudentName() {
    return this.studentName;
  }

  public void setStudentName(String studentName) {
    this.studentName = studentName;
  }

  public String getUserId() {
    return this.userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getStudentNo() {
    return this.studentNo;
  }

  public void setStudentNo(String studentNo) {
    this.studentNo = studentNo;
  }

  public List getPositionOfServiceList() {
    return this.positionOfServiceList;
  }

  public void setPositionOfServiceList(List positionOfServiceList) {
    this.positionOfServiceList = positionOfServiceList;
    if (positionOfServiceList != null) {
      this.positionOfServiceListSize = positionOfServiceList.size();
    } else {
      this.positionOfServiceListSize = 0;
    }

  }

  public List getExtracurricularActivitiesList() {
    return this.extracurricularActivitiesList;
  }

  public void setExtracurricularActivitiesList(List extracurricularActivitiesList) {
    this.extracurricularActivitiesList = extracurricularActivitiesList;
    if (extracurricularActivitiesList != null) {
      this.extracurricularActivitiesListSize = extracurricularActivitiesList.size();
    } else {
      this.extracurricularActivitiesListSize = 0;
    }

  }

  public List getMeritList() {
    return this.meritList;
  }

  public void setMeritList(List meritList) {
    this.meritList = meritList;
    if (meritList != null) {
      this.meritListSize = meritList.size();
    } else {
      this.meritListSize = 0;
    }

  }

  public StudentRemark getRemark() {
    return this.remark;
  }

  public void setRemark(StudentRemark remark) {
    this.remark = remark;
  }

  public int getPositionOfServiceListSize() {
    return this.positionOfServiceListSize;
  }

  public void setPositionOfServiceListSize(int positionOfServiceListSize) {
    this.positionOfServiceListSize = positionOfServiceListSize;
  }

  public int getExtracurricularActivitiesListSize() {
    return this.extracurricularActivitiesListSize;
  }

  public void setExtracurricularActivitiesListSize(int extracurricularActivitiesListSize) {
    this.extracurricularActivitiesListSize = extracurricularActivitiesListSize;
  }

  public int getMeritListSize() {
    return this.meritListSize;
  }

  public void setMeritListSize(int meritListSize) {
    this.meritListSize = meritListSize;
  }

  public List getTeacherCommentCodesList() {
    return this.teacherCommentCodesList;
  }

  public void setTeacherCommentCodesList(List teacherCommentCodesList) {
    this.teacherCommentCodesList = teacherCommentCodesList;
    if (teacherCommentCodesList != null) {
      this.teacherCommentCodesListSize = teacherCommentCodesList.size();
    } else {
      this.teacherCommentCodesListSize = 0;
    }

  }

  public int getTeacherCommentCodesListSize() {
    return this.teacherCommentCodesListSize;
  }

  public void setTeacherCommentCodesListSize(int teacherCommentCodesListSize) {
    this.teacherCommentCodesListSize = teacherCommentCodesListSize;
  }

  public TeacherComment getTeacherComment() {
    return this.teacherComment;
  }

  public void setTeacherComment(TeacherComment teacherComment) {
    this.teacherComment = teacherComment;
  }

  public List getAwardList() {
    return this.awardList;
  }

  public void setAwardList(List awardList) {
    this.awardList = awardList;
    if (awardList != null) {
      this.awardListSize = awardList.size();
    } else {
      this.awardListSize = 0;
    }

  }

  public int getAwardListSize() {
    return this.awardListSize;
  }

  public void setAwardListSize(int awardListSize) {
    this.awardListSize = awardListSize;
  }

  public String getMeritStr() {
    return this.meritStr;
  }

  public void setMeritStr(String meritStr) {
    this.meritStr = meritStr;
  }

  public List<StudentMeritCount> getStudentMeritCountList() {
    return studentMeritCountList;
  }

  public void setStudentMeritCountList(
      List<StudentMeritCount> studentMeritCountList) {
    this.studentMeritCountList = studentMeritCountList;
  }

  public List<StudentSummaryMeritCount> getStudentSummaryMeritCountList() {
    return studentSummaryMeritCountList;
  }

  public void setStudentSummaryMeritCountList(
      List<StudentSummaryMeritCount> studentSummaryMeritCountList) {
    this.studentSummaryMeritCountList = studentSummaryMeritCountList;
  }
}
