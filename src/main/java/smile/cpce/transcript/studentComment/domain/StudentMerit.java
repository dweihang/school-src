//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.transcript.studentComment.domain;

import java.io.Serializable;

public class StudentMerit implements Serializable {

  private String studentComMeritId;
  private String academicYear;
  private String userId;
  private String code;
  private String meritClass;
  private String teacherUserId;

  public StudentMerit() {
  }

  public String getAcademicYear() {
    return this.academicYear;
  }

  public void setAcademicYear(String academicYear) {
    this.academicYear = academicYear;
  }

  public String getUserId() {
    return this.userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMeritClass() {
    return this.meritClass;
  }

  public void setMeritClass(String meritClass) {
    this.meritClass = meritClass;
  }

  public String getStudentComMeritId() {
    return this.studentComMeritId;
  }

  public void setStudentComMeritId(String studentComMeritId) {
    this.studentComMeritId = studentComMeritId;
  }

  public String getTeacherUserId() {
    return teacherUserId;
  }

  public void setTeacherUserId(String teacherUserId) {
    this.teacherUserId = teacherUserId;
  }
}
