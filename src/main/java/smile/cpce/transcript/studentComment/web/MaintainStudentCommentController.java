//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package smile.cpce.transcript.studentComment.web;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import smile.cpce.common.Constants;
import smile.cpce.common.MethodNameHelper;
import smile.cpce.common.ToStringHelper;
import smile.cpce.common.Tools;
import smile.cpce.common.domain.SessionAttribute;
import smile.cpce.school.institute.domain.Institute;
import smile.cpce.transcript.classMaster.service.ClassMasterService;
import smile.cpce.transcript.programme.domain.Programme;
import smile.cpce.transcript.programme.service.ProgrammeService;
import smile.cpce.transcript.student.domain.Student;
import smile.cpce.transcript.student.service.StudentService;
import smile.cpce.transcript.studentComment.domain.*;
import smile.cpce.transcript.studentComment.domain.metaData.*;
import smile.cpce.transcript.studentComment.service.StudentCommentService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.logging.Logger;

public class MaintainStudentCommentController extends SimpleFormController {
  private StudentService studentService;
  private StudentCommentService studentCommentService;
  private ClassMasterService classMasterService;
  private ProgrammeService programmeService;
  private JdbcTemplate jdbcTemplate;
  private Logger log = Logger.getLogger("cpce");

  public MaintainStudentCommentController() {
    this.setCommandName("studentCommentForm");
    this.setCommandClass(StudentCommentForm.class);
    this.setFormView("transcript/studentComment/studentComment");
    this.setSuccessView("redirect:/transcript/studentComment/maintainStudentComment.htm");
  }

  protected Map referenceData(HttpServletRequest request) {
    Map data = new HashMap();
    try {
      ExtracurricularActivities extracurricularActivities = new ExtracurricularActivities(
        this.jdbcTemplate);
      MeritAndDemerit meritAndDemerit = new MeritAndDemerit(this.jdbcTemplate);
      MeritClass meritClass = new MeritClass(this.jdbcTemplate);
      PositionOfService positionOfService = new PositionOfService(this.jdbcTemplate);
      Remark remark = new Remark(this.jdbcTemplate);
      TeacherCommentMetaData teacherCommentMetadata = new TeacherCommentMetaData(this.jdbcTemplate);
      Award award = new Award(this.jdbcTemplate);
      data.put("extracurricularActivitiesMap", extracurricularActivities.getMap());
      data.put("meritAndDemeritMap", meritAndDemerit.getMap());
      data.put("meritClassMap", meritClass.getMap());
      data.put("positionOfServiceMap", positionOfService.getMap());
      data.put("remarkMap", remark.getMap());
      data.put("teacherCommentMetaDataMap", teacherCommentMetadata.getMap());
      data.put("awardMap", award.getMap());
    } catch (Exception e) {
      log.throwing(getClass().getSimpleName(), MethodNameHelper.getMethodName(), e);
    }
    return data;
  }

  protected Object formBackingObject(HttpServletRequest request) throws Exception {
    StudentCommentForm form = new StudentCommentForm();
    try {
      String paramAcademicYear = request.getParameter("paramAcademicYear");
      String paramProgrammeId = request.getParameter("paramProgrammeId");
      HttpSession session = request.getSession();
      SessionAttribute sessionAttribute = (SessionAttribute) session
        .getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
      String instituteId = sessionAttribute.getCurrentInstituteId();
      String academicYear = sessionAttribute.getCurrentAcademicYear();
      String userId = sessionAttribute.getUserId();
      String role = sessionAttribute.getRole();

      if (paramAcademicYear != null && !paramAcademicYear.equals("")) {
        form.setAcademicYear(paramAcademicYear);
      } else {
        form.setAcademicYear(academicYear);
      }

      List programmeList = null;
      if (Constants.SYSADMIN.equals(role)) {
        programmeList = this.programmeService.getAllByInstitute(instituteId);
      } else if (Constants.SCHADMIN.equals(role)) {
        Institute institute = sessionAttribute.getInstitute();
        if (institute == null || institute.getInstituteId().equals(instituteId)) {
          programmeList = this.programmeService.getAllByInstitute(instituteId);
        }
      } else {
        programmeList = this.classMasterService.getByUserIdInstituteAcademicYear(userId, instituteId, academicYear);
        if (programmeList != null && programmeList.size() == 1) {
          Programme programme = (Programme)programmeList.get(0);
          paramProgrammeId = programme.getProgrammeId();
        }
      }

      if (paramProgrammeId != null && !paramProgrammeId.equals("")) {
        form.setProgrammeId(paramProgrammeId);
        ArrayList studentCommentList;
        int p;
        List studentList;
        if (!this.isFormSubmission(request)) {
          MeritClass meritClass = new MeritClass(this.jdbcTemplate);
          MeritAndDemerit meritAndDemerit = new MeritAndDemerit(this.jdbcTemplate);
          studentList = this.studentService.getByProgrammeId(paramProgrammeId);
          studentCommentList = new ArrayList();
          p = 0;

          for (Iterator i = studentList.iterator(); i.hasNext(); ++p) {
            Student student = (Student) i.next();
            StudentComment studentComment = this.studentCommentService
              .getById(form.getAcademicYear(), student.getUserId());
            studentComment.setStudentNo(student.getHkidOrPassport());
            studentComment.setStudentName(student.getUser().getNameEng());
            studentComment.setClassNo(student.getStudentNo());
            List<StudentSummaryMeritCount> studentSummaryMeritCountList = new ArrayList<StudentSummaryMeritCount>();
            try {
              if (studentComment.getStudentMeritCountList() != null) {
                for (StudentMeritCount meritCount : studentComment.getStudentMeritCountList()) {
                  studentSummaryMeritCountList.add(
                    StudentSummaryMeritCount
                      .convertFromStudentMeritCount(meritCount, meritClass, meritAndDemerit));
                }
              }
            } catch (DataIntegrityViolationException e) {
              log.throwing(this.getClass().getName(), MethodNameHelper.getMethodName(), e);
            }
            studentComment.setStudentSummaryMeritCountList(studentSummaryMeritCountList);
            studentCommentList.add(studentComment);
            if (studentComment.getRemark() == null) {
              studentComment.setRemark(new StudentRemark());
            }

            if (studentComment.getTeacherComment() == null) {
              studentComment.setTeacherComment(new TeacherComment());
            }
            //log.info("Student comment\n" + ToStringHelper.toString(studentComment));
          }

          form.setStudentCommentList(studentCommentList);
        } else {
          form = (StudentCommentForm) super.formBackingObject(request);
          studentList = this.studentService.getByProgrammeId(paramProgrammeId);
          studentCommentList = new ArrayList();
          form.setStudentCommentList(studentCommentList);
          p = 0;

          for (int q = studentList.size(); p < q; ++p) {
            StudentComment studentComment = new StudentComment();
            studentCommentList.add(studentComment);
            String sizeStr = request
              .getParameter("studentCommentList[" + p + "].positionOfServiceListSize");
            int size = Integer.parseInt(sizeStr);
            List positionOfServiceList = new ArrayList();
            if (size > 0) {
              for (int i = 0; i < size; ++i) {
                positionOfServiceList.add(new StudentPositionOfService());
              }
            }

            studentComment.setPositionOfServiceList(positionOfServiceList);
            sizeStr = request
              .getParameter("studentCommentList[" + p + "].extracurricularActivitiesListSize");
            size = Integer.parseInt(sizeStr);
            List extracurricularActivitiesList = new ArrayList();
            if (size > 0) {
              for (int i = 0; i < size; ++i) {
                extracurricularActivitiesList.add(new StudentExtracurricularActivities());
              }
            }

            studentComment.setExtracurricularActivitiesList(extracurricularActivitiesList);
            sizeStr = request.getParameter("studentCommentList[" + p + "].meritListSize");
            size = Integer.parseInt(sizeStr);
            List studentMeritList = new ArrayList();
            if (size > 0) {
              for (int i = 0; i < size; ++i) {
                studentMeritList.add(new StudentMerit());
              }
            }

            studentComment.setMeritList(studentMeritList);
            sizeStr = request.getParameter("studentCommentList[" + p + "].awardListSize");
            size = Integer.parseInt(sizeStr);
            List awardList = new ArrayList();
            if (size > 0) {
              for (int i = 0; i < size; ++i) {
                awardList.add(new StudentAward());
              }
            }

            studentComment.setAwardList(awardList);
            sizeStr = request
              .getParameter("studentCommentList[" + p + "].teacherCommentCodesListSize");
            size = Integer.parseInt(sizeStr);
            List TeacherCommentCodeList = new ArrayList();
            if (size > 0) {
              for (int i = 0; i < size; ++i) {
                TeacherCommentCodeList.add(new TeacherCommentCode());
              }
            }

            studentComment.setTeacherCommentCodesList(TeacherCommentCodeList);
            studentComment.setRemark(new StudentRemark());
            studentComment.setTeacherComment(new TeacherComment());
          }
        }
      }
      request.setAttribute("programmeList", programmeList);

    } catch (Exception e) {
      log.throwing(this.getClass().getName(), MethodNameHelper.getMethodName(), e);
    }
    return form;
  }

  protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
    ModelAndView mav = null;
    StudentCommentForm form = (StudentCommentForm)command;
    HttpSession session = request.getSession();
    SessionAttribute sessionAttribute = (SessionAttribute)session.getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
    Institute institute = sessionAttribute.getInstitute();
    String updateBy = sessionAttribute.getUserId();
    String role = sessionAttribute.getRole();
    if (!Tools.checkAccessRightProgramme(updateBy, role, institute == null ? null : institute.getInstituteId(), form.getProgrammeId(), form.getAcademicYear(), Constants.jdbcTemplate)) {
      this.logger.info("Deny Access: user:" + updateBy + " || programmeId: " + form.getProgrammeId() + "||academicYear:" + form.getAcademicYear() + "||action:updateAttendence");
      mav = new ModelAndView(this.getFormView());
      mav.addObject("status", "cpce.exceptionCode.accessDenied");
      mav.addAllObjects(errors.getModel());
      return mav;
    } else {
      List studentCommentList = form.getStudentCommentList();
      Iterator i = studentCommentList.iterator();

      while(i.hasNext()) {
        StudentComment studentComment = (StudentComment)i.next();
        studentComment.setAcademicYear(form.getAcademicYear());
        List positionOfServiceList = studentComment.getPositionOfServiceList();
        Iterator j = positionOfServiceList.iterator();

        while(j.hasNext()) {
          StudentPositionOfService spos = (StudentPositionOfService)j.next();
          spos.setAcademicYear(form.getAcademicYear());
          spos.setUserId(studentComment.getUserId());
        }

        List extracurricularActivitiesList = studentComment.getExtracurricularActivitiesList();
        j = extracurricularActivitiesList.iterator();

        while(j.hasNext()) {
          StudentExtracurricularActivities sea = (StudentExtracurricularActivities)j.next();
          sea.setAcademicYear(form.getAcademicYear());
          sea.setUserId(studentComment.getUserId());
        }

        List meritList = studentComment.getMeritList();
        j = meritList.iterator();

        while(j.hasNext()) {
          StudentMerit sm = (StudentMerit)j.next();
          //log.info(ToStringHelper.toString(sm));
          sm.setAcademicYear(form.getAcademicYear());
          sm.setUserId(studentComment.getUserId());
        }
        //log.info(ToStringHelper.toString(studentComment.getMeritList()));

        List awardList = studentComment.getAwardList();
        j = awardList.iterator();

        while(j.hasNext()) {
          StudentAward sawd = (StudentAward)j.next();
          sawd.setAcademicYear(form.getAcademicYear());
          sawd.setUserId(studentComment.getUserId());
          System.out.println("sawd = " + sawd.getCode());
        }

        List teacherCommentcodeList = studentComment.getTeacherCommentCodesList();
        j = teacherCommentcodeList.iterator();

        while(j.hasNext()) {
          TeacherCommentCode tc = (TeacherCommentCode)j.next();
          tc.setAcademicYear(form.getAcademicYear());
          tc.setUserId(studentComment.getUserId());
        }

        StudentRemark sr = studentComment.getRemark();
        sr.setAcademicYear(form.getAcademicYear());
        sr.setUserId(studentComment.getUserId());
        TeacherComment tc = studentComment.getTeacherComment();
        tc.setAcademicYear(form.getAcademicYear());
        tc.setUserId(studentComment.getUserId());
      }

      String errorMessage = "";
      i = studentCommentList.iterator();

      while (i.hasNext()) {
        StudentComment studentComment = (StudentComment) i.next();
        errorMessage = this.studentCommentService.updateStudentCommentV2(studentComment);
        if (!errorMessage.equalsIgnoreCase("OK")) {
          errorMessage = errorMessage + " " + studentComment.getStudentName() + studentComment.getStudentNo() + " Time : " + new Date();
          break;
        }
      }

      mav = new ModelAndView(this.getSuccessView());
      if (errorMessage.equalsIgnoreCase("OK")) {
        mav.addObject("actionStatus", "common.editSuccess");
      } else {
        log.info(">>>>> edit failed");
        log.info(">>>>> errorMessage : " + errorMessage);
        mav.addObject("actionStatus", "common.editFail");
      }

      mav.addObject("paramAcademicYear", form.getAcademicYear());
      mav.addObject("paramProgrammeId", form.getProgrammeId());
      return mav;
    }
  }

  public void setStudentService(StudentService studentService) {
    this.studentService = studentService;
  }

  public void setStudentCommentService(StudentCommentService studentCommentService) {
    this.studentCommentService = studentCommentService;
  }

  public void setClassMasterService(ClassMasterService classMasterService) {
    this.classMasterService = classMasterService;
  }

  public void setProgrammeService(ProgrammeService programmeService) {
    this.programmeService = programmeService;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }
}
