package smile.cpce.transcript.index.web;

/**
 * @author raymonddu 22/9/2018
 */
public class TranscriptIndexForm {
  private boolean isMeritCanEdit;
  private boolean isPositionOfServiceCanEdit;
  private boolean isExtraCurricularActivitiesCanEdit;
  private boolean isAttendenceCanEdit;
  private boolean isStudentCommentCanEdit;

  public TranscriptIndexForm() {
  }

  public boolean getIsMeritCanEdit() {
    return this.isMeritCanEdit;
  }

  public void setIsMeritCanEdit(boolean meritCanEdit) {
    this.isMeritCanEdit = meritCanEdit;
  }

  public boolean getIsPositionOfServiceCanEdit() {
    return this.isPositionOfServiceCanEdit;
  }

  public void setIsPositionOfServiceCanEdit(boolean positionOfServiceCanEdit) {
    this.isPositionOfServiceCanEdit = positionOfServiceCanEdit;
  }

  public boolean getIsExtraCurricularActivitiesCanEdit() {
    return this.isExtraCurricularActivitiesCanEdit;
  }

  public void setIsExtraCurricularActivitiesCanEdit(boolean extraCurricularActivitiesCanEdit) {
    this.isExtraCurricularActivitiesCanEdit = extraCurricularActivitiesCanEdit;
  }

  public boolean getIsAttendenceCanEdit() {
    return this.isAttendenceCanEdit;
  }

  public void setAttendenceCanEdit(boolean attendenceCanEdit) {
    this.isAttendenceCanEdit = attendenceCanEdit;
  }

  public boolean getIsStudentCommentCanEdit() {
    return this.isStudentCommentCanEdit;
  }

  public void setStudentCommentCanEdit(boolean isStudentCommentCanEdit) {
    this.isStudentCommentCanEdit = isStudentCommentCanEdit;
  }
}
