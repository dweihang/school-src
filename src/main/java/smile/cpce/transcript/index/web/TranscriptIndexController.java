package smile.cpce.transcript.index.web;

import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.servlet.mvc.SimpleFormController;
import smile.cpce.transcript.permission.service.EditPermissionService;
import smile.cpce.transcript.permission.service.EditPermissionServiceImpl;

/**
 * @author raymonddu 22/9/2018
 */
public class TranscriptIndexController extends SimpleFormController {
  private Logger log = Logger.getLogger("cpce");
  private JdbcTemplate jdbcTemplate;

  public TranscriptIndexController() {
    this.setCommandName("transcriptIndexForm");
    this.setCommandClass(TranscriptIndexForm.class);
    this.setFormView("transcript/index");
    this.setSuccessView("redirect:/transcript/index.htm");
  }

  protected Object formBackingObject(HttpServletRequest request) throws Exception {
    TranscriptIndexForm form = new TranscriptIndexForm();
    EditPermissionService editPermissionService = new EditPermissionServiceImpl(this.jdbcTemplate);
    form.setIsExtraCurricularActivitiesCanEdit(editPermissionService.isExtraCurricularActivitiesCanEdit());
    form.setIsMeritCanEdit(editPermissionService.isMeritCanEdit());
    form.setIsPositionOfServiceCanEdit(editPermissionService.isPositionOfServiceCanEdit());
    form.setAttendenceCanEdit(editPermissionService.isAttendenceCanEdit());
    form.setStudentCommentCanEdit(editPermissionService.isStudentCommentCanEdit());
    return form;
  }

  public JdbcTemplate getJdbcTemplate() {
    return this.jdbcTemplate;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }
}
