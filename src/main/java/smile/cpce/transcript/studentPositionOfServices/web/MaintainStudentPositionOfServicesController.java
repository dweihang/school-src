package smile.cpce.transcript.studentPositionOfServices.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import smile.cpce.common.Constants;
import smile.cpce.common.MethodNameHelper;
import smile.cpce.common.ToStringHelper;
import smile.cpce.common.Tools;
import smile.cpce.common.domain.SessionAttribute;
import smile.cpce.school.institute.domain.Institute;
import smile.cpce.transcript.classMaster.service.ClassMasterService;
import smile.cpce.transcript.permission.service.EditPermissionService;
import smile.cpce.transcript.permission.service.EditPermissionServiceImpl;
import smile.cpce.transcript.programme.domain.Programme;
import smile.cpce.transcript.programme.service.ProgrammeService;
import smile.cpce.transcript.student.domain.Student;
import smile.cpce.transcript.student.service.StudentService;
import smile.cpce.transcript.studentComment.domain.StudentPositionOfService;
import smile.cpce.transcript.studentComment.domain.metaData.PositionOfService;
import smile.cpce.transcript.studentComment.service.StudentCommentService;

/**
 * @author raymonddu 3/8/2018
 */
public class MaintainStudentPositionOfServicesController extends SimpleFormController {

  private Logger log = Logger.getLogger("cpce");

  private StudentService studentService;

  private StudentCommentService studentCommentService;

  private ClassMasterService classMasterService;

  private ProgrammeService programmeService;

  private JdbcTemplate jdbcTemplate;

  public MaintainStudentPositionOfServicesController() {
    this.setCommandName("studentPositionOfServicesForm");
    this.setCommandClass(PositionOfServicesForm.class);
    this.setFormView("transcript/studentPositionOfServices/studentPositionOfServices");
    this.setSuccessView(
      "redirect:/transcript/studentPositionOfServices/maintainStudentPositionOfServices.htm");
  }

  protected Map referenceData(HttpServletRequest request) {
    Map data = new HashMap();
    PositionOfService positionOfService = new PositionOfService(this.jdbcTemplate);
    data.put("positionOfServiceMap", positionOfService.getMap());
    return data;
  }

  protected Object formBackingObject(HttpServletRequest request) throws Exception {
    log.info("form backing object");
    PositionOfServicesForm form = new PositionOfServicesForm();
    String paramAcademicYear = request.getParameter("paramAcademicYear");
    String paramProgrammeId = request.getParameter("paramProgrammeId");
    String paramStudentNo = request.getParameter("paramStudentNo");
    HttpSession session = request.getSession();
    SessionAttribute sessionAttribute = (SessionAttribute) session
      .getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
    String instituteId = sessionAttribute.getCurrentInstituteId();
    String academicYear = sessionAttribute.getCurrentAcademicYear();
    String userId = sessionAttribute.getUserId();
    String role = sessionAttribute.getRole();
    if (paramAcademicYear != null && !paramAcademicYear.equals("")) {
      form.setAcademicYear(paramAcademicYear);
    } else {
      form.setAcademicYear(academicYear);
    }

    List programmeList = null;
    if (Constants.SYSADMIN.equals(role)) {
      programmeList = this.programmeService.getAllByInstitute(instituteId);
    } else {
      Institute institute = sessionAttribute.getInstitute();
      if (institute == null || institute.getInstituteId().equals(instituteId)) {
        programmeList = this.programmeService.getAllByInstitute(instituteId);
      }
    }

    List studentList = null;
    if (paramProgrammeId != null && !paramProgrammeId.equals("")) {
      form.setProgrammeId(paramProgrammeId);
      studentList = this.studentService.getByProgrammeId(paramProgrammeId);

      if (paramStudentNo != null && !paramStudentNo.equals("")) {
        Iterator<Student> studentIterator = studentList.iterator();
        while (studentIterator.hasNext()) {
          Student student = studentIterator.next();
          if (paramStudentNo.equals(student.getStudentNo())) {
            form.setSelectedStudentNo(student.getStudentNo());
            form.setSelectedStudentUserId(student.getUser().getUserId());
            form.setSelectedStudentNameChi(student.getUser().getNameChi());
            form.setSelectedStudentNameEng(student.getUser().getNameEng());

            List<StudentPositionOfService> positionOfServicesList = this.studentCommentService
              .getStudentPositionOfServicesById(academicYear, student.getUser().getUserId());
            PositionOfService types = new PositionOfService(this.jdbcTemplate);
            Map<String, Integer> map = new HashMap<String, Integer>();

            Set<String> codes = types.getMap().keySet();
            for (String code : codes) {
              map.put(code, 0);
            }

            for (StudentPositionOfService positionOfService : positionOfServicesList) {
              if (!userId.equals(positionOfService.getTeacherUserId())) {
                map.put(positionOfService.getCode(), -1);
              }
              int count = map.get(positionOfService.getCode());
              if (count != -1) {
                count += 1;
                map.put(positionOfService.getCode(), count);
              }
            }
            Map<String, String> output = new HashMap<String, String>();
            for (Entry<String, Integer> entry : map.entrySet()) {
              output.put(entry.getKey(), String.valueOf(entry.getValue()));
            }

            form.setPositionOfServicesCodeCountMap(output);
            break;
          }
        }
      }
    }

    request.setAttribute("programmeList", programmeList);
    request.setAttribute("studentList", studentList);
    return form;
  }

  protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response,
    Object command, BindException errors) throws Exception {
    log.info("onSubmit");
    ModelAndView mav = null;
    PositionOfServicesForm form = (PositionOfServicesForm) command;
    HttpSession session = request.getSession();
    SessionAttribute sessionAttribute = (SessionAttribute) session
      .getAttribute(Constants.SESSION_ATTRIBUTE_NAME);
    Institute institute = sessionAttribute.getInstitute();
    String updateBy = sessionAttribute.getUserId();
    String role = sessionAttribute.getRole();
    EditPermissionService editPermissionService = new EditPermissionServiceImpl(jdbcTemplate);
    if (!(Tools.isTeacher(role) || Tools.isSchoolAdmin(role) || Tools.isSysAdmin(role)) || !editPermissionService.isPositionOfServiceCanEdit()) {
      this.logger.info("Deny Access: user:" + updateBy + " || programmeId: " + form.getProgrammeId()
        + "||academicYear:" + form.getAcademicYear() + "||action:updateAttendence");
      mav = new ModelAndView(this.getSuccessView());
      mav.addObject("status", "cpce.exceptionCode.accessDenied");
      mav.addAllObjects(errors.getModel());
      return mav;
    } else {
      mav = new ModelAndView(this.getSuccessView());
      try {
        int isSuccess = 0;
        log.info(ToStringHelper.toString(command));

        List<StudentPositionOfService> positionOfServicesList = new ArrayList<StudentPositionOfService>();
        for (Entry<String, String> positionOfServiceEntry : form.getPositionOfServicesCodeCountMap()
          .entrySet()) {
          int count = Integer.parseInt(positionOfServiceEntry.getValue());
          if (count > 0) {
            StudentPositionOfService positionOfService = new StudentPositionOfService();
            positionOfService.setTeacherUserId(updateBy);
            positionOfService.setCode(positionOfServiceEntry.getKey());
            positionOfService.setAcademicYear(form.getAcademicYear());
            positionOfService.setUserId(form.getSelectedStudentUserId());
            positionOfServicesList.add(positionOfService);
          }
        }
        isSuccess = this.studentCommentService
          .updateStudentPositionOfServicesByIdAndTeacherUserId(form.getAcademicYear(),
            form.getSelectedStudentUserId(), updateBy, positionOfServicesList);
        if (isSuccess == 0) {
          mav.addObject("actionStatus", "common.editSuccess");
        } else {
          mav.addObject("actionStatus", "common.editFail");
        }

        mav.addObject("paramAcademicYear", form.getAcademicYear());
        mav.addObject("paramProgrammeId", form.getProgrammeId());
      } catch (Exception e) {
        mav.addObject("actionStatus", "common.editFail");
        log.throwing(getClass().getName(), MethodNameHelper.getMethodName(), e);
      }
      return mav;
    }
  }

  public StudentService getStudentService() {
    return studentService;
  }

  public void setStudentService(StudentService studentService) {
    this.studentService = studentService;
  }

  public ClassMasterService getClassMasterService() {
    return classMasterService;
  }

  public void setClassMasterService(
    ClassMasterService classMasterService) {
    this.classMasterService = classMasterService;
  }

  public ProgrammeService getProgrammeService() {
    return programmeService;
  }

  public void setProgrammeService(
    ProgrammeService programmeService) {
    this.programmeService = programmeService;
  }

  public JdbcTemplate getJdbcTemplate() {
    return jdbcTemplate;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public StudentCommentService getStudentCommentService() {
    return studentCommentService;
  }

  public void setStudentCommentService(
    StudentCommentService studentCommentService) {
    this.studentCommentService = studentCommentService;
  }
}
