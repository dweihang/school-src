package smile.cpce.school.studentAttendence.service;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import smile.cpce.common.CPCE;
import smile.cpce.common.DataAccessExceptionResolver;
import smile.cpce.common.exception.CpceException;
import smile.cpce.db.exception.Programme_subjectgroup_viewDTOException;
import smile.cpce.db.exception.Student_attendence_tblException;
import smile.cpce.db.exception.Student_enroll_programme_tblException;
import smile.cpce.db.vo.Student_attendence_tbl;
import smile.cpce.db.vo.Student_enroll_programme_tbl;
import smile.cpce.school.programme.domain.ProgrammeSubjectGroup;
import smile.cpce.school.programme.service.ProgrammeService;
import smile.cpce.school.studentAttendence.domain.AttendenceForm;
import smile.cpce.school.studentAttendence.domain.AttendenceObject;

public class StudentAttendenceServiceImpl implements StudentAttendenceService {

  private ProgrammeService programmeService;
  private JdbcTemplate jdbcTemplate;
  protected final Logger logger = Logger.getLogger("cpce");
  private PlatformTransactionManager transactionManager;
  private DefaultTransactionDefinition defaultTransactionDefinition;

  public StudentAttendenceServiceImpl() {
  }

  public void setDefaultTransactionDefinition(
      DefaultTransactionDefinition defaultTransactionDefinition) {
    this.defaultTransactionDefinition = defaultTransactionDefinition;
  }

  public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public void setProgrammeService(ProgrammeService programmeService) {
    this.programmeService = programmeService;
  }

  public AttendenceForm getAttendenceListBySubjectGroup(String subjectGroupId)
      throws Programme_subjectgroup_viewDTOException {
    AttendenceForm form = null;
    ProgrammeSubjectGroup programmeSubjectGroup = this.programmeService
        .getProgrammeBySubjectGroup(subjectGroupId);
    if (programmeSubjectGroup != null) {
      form = this
          .getAttendenceListByProgrammeAcademicYearSemester(programmeSubjectGroup.getProgrammeId(),
              programmeSubjectGroup.getAcademicYear(), programmeSubjectGroup.getSemester());
    }

    return form;
  }

  public AttendenceForm getAttendenceListByProgrammeAcademicYearSemester(String programmeId,
      String academicYear, String semester) {
    AttendenceForm form = new AttendenceForm();
    form.setProgrammeId(programmeId);
    form.setAcademicYear(academicYear);
    form.setSemester(semester);
    String sql = "select A.user_id, F.student_no, E.name_chi, E.name_eng, G.late_for_class, G.early_leave, G.absence, G.skip_ranking from  student_enroll_programme_tbl A join programme_tbl B  on A.programme_id  = B.programme_id join user_tbl E on E.user_id = A.user_id join student_tbl F on F.user_id = E.user_id left join student_attendence_tbl G on G.user_id = A.user_id and G.programme_id = A.programme_id   and G.semester= ? and G.academic_year = ? where A.programme_id = ? order by F.student_no ";
    List list = this.jdbcTemplate
        .queryForList(sql, new Object[]{semester, academicYear, programmeId});
    if (list != null && list.size() > 0) {
      List<AttendenceObject> attendenceList = new ArrayList();

      AttendenceObject attendence;
      for (Iterator it = list.iterator(); it.hasNext(); attendenceList.add(attendence)) {
        Map map = (Map) it.next();
        attendence = new AttendenceObject();
        attendence.setStudentId((String) map.get("user_id"));
        attendence.setStudentNo((String) map.get("student_no"));
        attendence.setStudentEngName((String) map.get("name_eng"));
        attendence.setStudentChiName((String) map.get("name_chi"));
        float floatValue = 0.0F;
        BigDecimal bigDecimalValue = null;
        bigDecimalValue = (BigDecimal) map.get("late_for_class");
        if (bigDecimalValue != null) {
          floatValue = bigDecimalValue.floatValue();
        }

        attendence.setLateForClass(floatValue);
        floatValue = 0.0F;
        bigDecimalValue = (BigDecimal) map.get("early_leave");
        if (bigDecimalValue != null) {
          floatValue = bigDecimalValue.floatValue();
        }

        attendence.setEarlyLeave(floatValue);
        floatValue = 0.0F;
        bigDecimalValue = (BigDecimal) map.get("absence");
        if (bigDecimalValue != null) {
          floatValue = bigDecimalValue.floatValue();
        }

        attendence.setAbsence(floatValue);
        Object status = map.get("skip_ranking");
        if (status != null) {
          attendence.setSkipRanking((String) map.get("skip_ranking"));
        } else {
          attendence.setSkipRanking((String) map.get("0"));
        }
      }

      attendenceList.sort((o1, o2) -> {
        if (o1.getStudentNo().compareTo(o2.getStudentNo()) == 0) {
          return o1.getStudentEngName().compareTo(o2.getStudentEngName());
        }
        return o1.getStudentNo().compareTo(o2.getStudentNo());
      });

      form.setAttendenceList(attendenceList);
    } else {
      form.setAttendenceList(new ArrayList());
    }

    return form;
  }

  public void updateAttendence(final AttendenceForm form, final String userId)
      throws Student_attendence_tblException, CpceException {
    String updateSql = "update student_attendence_tbl set late_for_class = ? , early_leave = ? , absence = ?, update_by = ? , update_date = ?, skip_ranking = ?  where user_id = ? and programme_id = ? and academic_year = ? and semester = ?";
    String insertSql = "insert into student_attendence_tbl  (programme_id, user_id, academic_year, semester, late_for_class, early_leave, absence, update_by, update_date, skip_ranking) values (?, ?, ?, ?,? ,? ,? , ?,?, ? )";
    final String academicYear = form.getAcademicYear();
    final String semester = form.getSemester();
    List attendenceList = CPCE.DAOFACTORY.createStudent_attendence_tblDAO()
        .findExecutingUserWhere(" programme_id = ? and semester = ? and academic_year = ? ",
            new Object[]{form.getProgrammeId(), semester, academicYear}, this.jdbcTemplate);
    Map attendenceMap = new HashMap();
    final List updateList = new ArrayList();
    final List addList = new ArrayList();
    Iterator it;
    if (attendenceList != null && attendenceList.size() > 0) {
      it = attendenceList.iterator();

      while (it.hasNext()) {
        Student_attendence_tbl view = (Student_attendence_tbl) it.next();
        attendenceMap.put(view.getUser_id(),
            new StudentAttendenceServiceImpl.AttendenceData(view.getLate_for_class(),
                view.getEarly_leave(), view.getAbsence(), view.getSkip_ranking()));
      }
    }

    attendenceList = form.getAttendenceList();
    if (attendenceList != null && attendenceList.size() > 0) {
      it = attendenceList.iterator();

      label73:
      while (true) {
        while (true) {
          if (!it.hasNext()) {
            break label73;
          }

          AttendenceObject attend = (AttendenceObject) it.next();
          StudentAttendenceServiceImpl.AttendenceData data = (StudentAttendenceServiceImpl.AttendenceData) attendenceMap
              .get(attend.getStudentId());
          if (data != null) {
            if (attend.getSkipRanking() != null) {
              if (!attend.getSkipRanking().equals("1") && !attend.getSkipRanking().equals("0")) {
                attend.setSkipRanking("1");
              } else {
                attend.setSkipRanking("0");
              }
            }

            if (!data.isSame(attend.getLateForClass(), attend.getEarlyLeave(), attend.getAbsence(),
                attend.getSkipRanking())) {
              updateList.add(attend);
            }
          } else {
            if (attend.getSkipRanking() != null) {
              if (!attend.getSkipRanking().equals("1") && !attend.getSkipRanking().equals("0")) {
                attend.setSkipRanking("1");
              }
            } else {
              attend.setSkipRanking("0");
            }

            addList.add(attend);
          }
        }
      }
    }

    BatchPreparedStatementSetter setterUpdate = null;
    BatchPreparedStatementSetter setterInsert = null;
    final Timestamp now = new Timestamp(System.currentTimeMillis());
    if (updateList.size() > 0) {
      setterUpdate = new BatchPreparedStatementSetter() {
        public int getBatchSize() {
          return updateList.size();
        }

        public void setValues(PreparedStatement ps, int i) throws SQLException {
          AttendenceObject attend = (AttendenceObject) updateList.get(i);
          ps.setBigDecimal(1, new BigDecimal((double) attend.getLateForClass()));
          ps.setBigDecimal(2, new BigDecimal((double) attend.getEarlyLeave()));
          ps.setBigDecimal(3, new BigDecimal((double) attend.getAbsence()));
          ps.setString(4, userId);
          ps.setTimestamp(5, now);
          ps.setString(6, attend.getSkipRanking());
          ps.setString(7, attend.getStudentId());
          ps.setString(8, form.getProgrammeId());
          ps.setString(9, academicYear);
          ps.setString(10, semester);
        }
      };
    }

    if (addList.size() > 0) {
      setterInsert = new BatchPreparedStatementSetter() {
        public int getBatchSize() {
          return addList.size();
        }

        public void setValues(PreparedStatement ps, int i) throws SQLException {
          AttendenceObject attend = (AttendenceObject) addList.get(i);
          ps.setString(1, form.getProgrammeId());
          ps.setString(2, attend.getStudentId());
          ps.setString(3, academicYear);
          ps.setString(4, semester);
          ps.setBigDecimal(5, new BigDecimal((double) attend.getLateForClass()));
          ps.setBigDecimal(6, new BigDecimal((double) attend.getEarlyLeave()));
          ps.setBigDecimal(7, new BigDecimal((double) attend.getAbsence()));
          ps.setString(8, userId);
          ps.setTimestamp(9, now);
          ps.setString(10, attend.getSkipRanking());
        }
      };
    }

    TransactionStatus status = this.transactionManager
        .getTransaction(this.defaultTransactionDefinition);

    try {
      if (setterUpdate != null) {
        this.jdbcTemplate.batchUpdate(updateSql, setterUpdate);
      }

      if (setterInsert != null) {
        this.jdbcTemplate.batchUpdate(insertSql, setterInsert);
      }

      this.transactionManager.commit(status);
    } catch (Exception var16) {
      this.transactionManager.rollback(status);
      this.logger.log(Level.SEVERE, " ", var16);
      DataAccessExceptionResolver.getInstance()
          .logDAOException("StudentAttendenceServiceImpl-updateAttendence", var16);
      throw new CpceException(2004, "cpce.exceptionCode.DBError");
    }
  }

  @Override
  public void deleteAttendenceListByProgrammeAcademicYearSemester(String programmeId,
      String academicYear, String semester) {
    String sql = "delete from student_attendence_tbl where programme_id = ? and academic_year = ? and semester = ? ";
    this.jdbcTemplate.update(sql, new Object[] {programmeId, academicYear, semester});
  }

  public Map getAttendanceMap(String programmeId) throws Student_enroll_programme_tblException {
    List list = CPCE.DAOFACTORY.createStudent_enroll_programme_tblDAO()
        .findExecutingUserWhere(" where programmeId = ? ", new Object[]{programmeId},
            this.jdbcTemplate);
    Map result = new HashMap();
    if (list != null && list.size() > 0) {
      Iterator it = list.iterator();

      while (it.hasNext()) {
        Student_enroll_programme_tbl tbl = (Student_enroll_programme_tbl) it.next();
        result.put(tbl.getUser_id(), tbl);
      }
    }

    return result;
  }

  @Override
  public void checkSemesterExist(String academicYear, String semester) throws Student_attendence_tblException {
    String sql = "select * from semester_tbl where semester = ? and academic_year = ? ";
    List result = this.jdbcTemplate
        .queryForList(sql, new Object[]{semester, academicYear});
    if (result == null || result.isEmpty()) {
      throw new Student_attendence_tblException(String.format("Cannot find record in semester_tbl where academic_year = %s, semester = %s", academicYear, semester));
    }
  }

  @Override
  public String getProgrammeIdByName(String programme) throws Student_attendence_tblException{
    String sql = "select * from programme_tbl where programme_title_eng = ? or programme_title_chi = ?";
    List result = this.jdbcTemplate.queryForList(sql, new Object[] {programme,  programme});
    if (result == null || result.isEmpty()) {
      throw new Student_attendence_tblException(String.format("Cannot find record in programme_tbl where programme_title_eng = %s or programme_title_chi = %s", programme, programme));
    }
    if (result.size() > 1) {
      throw new Student_attendence_tblException(String.format("Find more than one record in programme_tbl where programme_title_eng = %s or programme_title_chi = %s", programme, programme));
    }
    return (String) ((Map)(result.iterator().next())).get("programme_id");
  }

  @Override
  public String getStudentIdByStudentNo(String studentNo) throws Student_attendence_tblException {
    String sql = "select user_id from student_tbl where student_no = ? ";
    List result = this.jdbcTemplate.queryForList(sql, new Object[] {studentNo});
    if (result == null || result.isEmpty()) {
      throw new Student_attendence_tblException(String.format("Cannot find record in student_tbl where student_no = %s", studentNo));
    }
    if (result.size() > 1) {
      throw new Student_attendence_tblException(String.format("Find more than one record in student_tbl where student_no = %s", studentNo));
    }
    return (String) ((Map)(result.iterator().next())).get("user_id");
  }

  @Override
  public String getStudentIdByStudentNo(String studentNo, String programmeId) throws Student_attendence_tblException {
    String sql = "select user_id from student_tbl where student_no = ? ";
    List result = this.jdbcTemplate.queryForList(sql, new Object[] {studentNo});
    if (programmeId == null || programmeId.trim().isEmpty()) {
      throw new Student_attendence_tblException("programmeId cannot be null or empty");
    }
    if (result == null || result.isEmpty()) {
      throw new Student_attendence_tblException(String.format("Cannot find record in student_tbl where student_no = %s", studentNo));
    }
    if (result.size() > 1) {
      for (Object data: result) {
        String userId = (String) ((Map)data).get("user_id");
        sql = "select * from student_enroll_programme_tbl where user_id = ? and programme_id = ?";
        List studentEnrollResult = this.jdbcTemplate.queryForList(sql, new Object[] {userId, programmeId});
        if (studentEnrollResult != null && !studentEnrollResult.isEmpty()) {
          return userId;
        }
      }
      throw new Student_attendence_tblException(String.format("Cannot find record in student_enroll_programme_tbl where student_no = %s and programme_id = %s", studentNo, programmeId));
    }
    return (String) ((Map)(result.iterator().next())).get("user_id");
  }

  class AttendenceData {

    private float lateForClass = 0.0F;
    private float earlyLeave = 0.0F;
    private float absence = 0.0F;
    private String skipRanking = null;

    public AttendenceData(BigDecimal lateForClass, BigDecimal earlyLeave, BigDecimal absence,
        String skipRanking) {
      if (lateForClass != null) {
        this.lateForClass = lateForClass.floatValue();
      }

      if (earlyLeave != null) {
        this.earlyLeave = earlyLeave.floatValue();
      }

      if (absence != null) {
        this.absence = absence.floatValue();
      }

      if (skipRanking != null) {
        this.skipRanking = skipRanking;
      }

    }

    public boolean isSame(float lateForClass, float earlyLeave, float absence, String skipRanking) {
      boolean isSame = this.lateForClass == lateForClass;
      if (!isSame) {
        return false;
      } else {
        isSame &= this.earlyLeave == earlyLeave;
        if (!isSame) {
          return false;
        } else {
          isSame &= this.absence == absence;
          if (!isSame) {
            return false;
          } else {
            isSame &= this.skipRanking.equals(skipRanking);
            return isSame;
          }
        }
      }
    }
  }
}
