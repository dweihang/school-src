package smile.cpce.school.studentAttendence.service;

import java.util.Map;
import smile.cpce.common.exception.CpceException;
import smile.cpce.db.exception.Programme_subjectgroup_viewDTOException;
import smile.cpce.db.exception.Student_attendence_tblException;
import smile.cpce.db.exception.Student_enroll_programme_tblException;
import smile.cpce.school.studentAttendence.domain.AttendenceForm;

public interface StudentAttendenceService {
  void updateAttendence(AttendenceForm var1, String var2) throws Student_attendence_tblException, CpceException;

  void deleteAttendenceListByProgrammeAcademicYearSemester(String programmeId, String academicYear, String semester);

  AttendenceForm getAttendenceListBySubjectGroup(String var1) throws Programme_subjectgroup_viewDTOException;

  AttendenceForm getAttendenceListByProgrammeAcademicYearSemester(String var1, String var2, String var3);

  Map getAttendanceMap(String var1) throws Student_enroll_programme_tblException;

  void checkSemesterExist(String academicYear, String semester) throws Student_attendence_tblException;

  String getProgrammeIdByName(String programme) throws Student_attendence_tblException;

  String getStudentIdByStudentNo(String studentNo) throws Student_attendence_tblException;

  String getStudentIdByStudentNo(String studentNo, String programmeId) throws Student_attendence_tblException;
}
