const path = require('path');
const fs = require('fs');

var sourceBasePath = path.join(__dirname, "/../../../build/classes/java/main");
var sourceResourcePath = path.join(__dirname, "/../../../build/resources/main");
var targetBasePath = '/usr/local/Cellar/tomcat/9.0.8/libexec/mscwebapp_dev/msc/WEB-INF/classes'

function extractJavaClasses(sourceBasePath, offsetPath) {
  var _offsetPath = offsetPath || '';
  var ret = [];
  var filesOrDir = fs.readdirSync(path.join(sourceBasePath, _offsetPath));
  for (var i=0, len=filesOrDir.length; i < len; i++) {
    var fileStats = fs.statSync(path.join(sourceBasePath, _offsetPath, filesOrDir[i]));
    if (fileStats.isDirectory()) {
      ret = ret.concat(extractJavaClasses(sourceBasePath, path.join(_offsetPath, filesOrDir[i])));
    } else if (fileStats.isFile() && (filesOrDir[i].indexOf('.class') > 0 || filesOrDir[i].indexOf('.xml') > 0)) {
      ret.push(path.join(_offsetPath, filesOrDir[i]));
    }
  }
  return ret;
}

function copyFile(sourceFilePath, destFilePath) {
  var fileStats = fs.statSync(sourceFilePath);
  if (!fileStats && !fileStats.isFile()) {
    console.log(`${sourceFilePath} is not a file`);
    return;
  }
  fs.createReadStream(sourceFilePath).pipe(fs.createWriteStream(destFilePath));
}

var classFiles = extractJavaClasses(sourceBasePath);
for (var i=0, len=classFiles.length; i < len; i++) {
  console.log(`copy file from ${path.join(sourceBasePath, classFiles[i])}\n to ${path.join(targetBasePath, classFiles[i])}`);
  copyFile(path.join(sourceBasePath, classFiles[i]), path.join(targetBasePath, classFiles[i]));
}

copyFile(path.join(sourceResourcePath, "messages.properties"), path.join(targetBasePath, "messages.properties"));
copyFile(path.join(sourceResourcePath, "messages_en_US.properties"), path.join(targetBasePath, "messages_en_US.properties"));