CREATE  PROCEDURE [dbo].[get_Stu_Merit_New_proc] @userId varchar(12), @academicYear varchar(4)
AS
BEGIN
SET NOCOUNT ON
SELECT dbo.get_Stu_Merit_New(@userId, @academicYear) as meritStr
SET NOCOUNT OFF
END
