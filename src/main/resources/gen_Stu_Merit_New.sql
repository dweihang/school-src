CREATE FUNCTION [dbo].[get_Stu_Merit_New] (@userId varchar(12), @academicYear varchar(4))
RETURNS nvarchar(200) WITH EXEC AS CALLER
AS
BEGIN
  DECLARE @MERIT_CLASS_RETURN nvarchar(200)
  SET @MERIT_CLASS_RETURN = ''
  DECLARE @meritConvertTable TABLE(ChiDesc nvarchar(20), Divider int)
  DECLARE meritClassMetaCursor CURSOR FOR SELECT chi_desc,level, upgrade_to FROM student_com_merit_class_metadata_tbl ORDER BY level ASC
  DECLARE @MERIT_CLASS_ACC int
  SET @MERIT_CLASS_ACC = 0
  DECLARE @MERIT_CLASS_CHI nvarchar(10)
  DECLARE @MERIT_CLASS_LEVEL int
  DECLARE @MERIT_CLASS_UPGRADE_TO int
  OPEN meritClassMetaCursor
  FETCH NEXT FROM meritClassMetaCursor INTO @MERIT_CLASS_CHI, @MERIT_CLASS_LEVEL, @MERIT_CLASS_UPGRADE_TO
  WHILE @@FETCH_STATUS = 0
  BEGIN
    IF @MERIT_CLASS_ACC = 0
	  BEGIN
      INSERT INTO @meritConvertTable (ChiDesc, Divider) VALUES (@MERIT_CLASS_CHI, 1)
      SET @MERIT_CLASS_ACC = @MERIT_CLASS_UPGRADE_TO
	  END
    ELSE
	  BEGIN
      INSERT INTO @meritConvertTable (ChiDesc, Divider) VALUES (@MERIT_CLASS_CHI, @MERIT_CLASS_ACC)
      SET @MERIT_CLASS_ACC = @MERIT_CLASS_UPGRADE_TO * @MERIT_CLASS_ACC
	  END
    FETCH NEXT FROM meritClassMetaCursor INTO @MERIT_CLASS_CHI, @MERIT_CLASS_LEVEL, @MERIT_CLASS_UPGRADE_TO
  END
  CLOSE meritClassMetaCursor
  DEALLOCATE meritClassMetaCursor
  DECLARE stuMeritCursor CURSOR FOR SELECT M.CHI_DESC, COUNT(*) FROM student_com_merit_tbl S INNER JOIN student_com_merit_metadata_tbl M ON (M.CODE = S.CODE) WHERE S.USER_ID = @userId AND S.ACADEMIC_YEAR = @academicYear GROUP BY M.CHI_DESC ORDER BY M.CHI_DESC
  DECLARE @STU_MERIT_COUNT int
  DECLARE @STU_MERIT_DESC nvarchar(20)
  OPEN stuMeritCursor
  FETCH NEXT FROM stuMeritCursor INTO @STU_MERIT_DESC, @STU_MERIT_COUNT
  WHILE @@FETCH_STATUS = 0
  BEGIN
    SET @MERIT_CLASS_RETURN = @MERIT_CLASS_RETURN + @STU_MERIT_DESC + CHAR(13)
    DECLARE @FIELD_CHI_DESC nvarchar(20)
    DECLARE @FIELD_DIVIDER int
    DECLARE @FINAL_STU_MERIT_COUNT int
    DECLARE convertCursor CURSOR FOR SELECT ChiDesc,Divider FROM @meritConvertTable ORDER BY Divider DESC
    OPEN convertCursor
    FETCH NEXT FROM convertCursor INTO @FIELD_CHI_DESC, @FIELD_DIVIDER
    WHILE @@FETCH_STATUS = 0
    BEGIN
      SET @FINAL_STU_MERIT_COUNT = @STU_MERIT_COUNT / @FIELD_DIVIDER
      IF @FINAL_STU_MERIT_COUNT >= 1
        BEGIN
        SET @MERIT_CLASS_RETURN = @MERIT_CLASS_RETURN + @FIELD_CHI_DESC + CAST(@FINAL_STU_MERIT_COUNT AS nvarchar(5)) + N'次' + CHAR(13)
        BREAK
        END
      FETCH NEXT FROM convertCursor INTO @FIELD_CHI_DESC, @FIELD_DIVIDER
    END
    CLOSE convertCursor
    DEALLOCATE convertCursor
	FETCH NEXT FROM stuMeritCursor INTO @STU_MERIT_DESC, @STU_MERIT_COUNT
  END
  CLOSE stuMeritCursor
  DEALLOCATE stuMeritCursor
  IF @MERIT_CLASS_RETURN = ''
    SET @MERIT_CLASS_RETURN = 'NIL'
  RETURN @MERIT_CLASS_RETURN
END
